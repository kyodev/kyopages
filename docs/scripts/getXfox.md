# getXfox

![version: 4.20.0](https://img.shields.io/badge/version-4.20.0-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)

> c'est un script bash qui télécharge les dernières versions officielles de Tor Browser (version stable) 
  ou Waterfox   
> les mises à jour de Waterfox et Tor Browser sont par défaut autorisées et gérées par ceux-ci.  
> le script se mettra éventuellement à jour, sans influence sur les programmes installés.  
> le script peut charger des fichiers de personnalisation et les mettra à jour périodiquement  
> le script peut désinstaller les programmes 
> script testé sur debian / ubuntu, mais devrait être compatible avec d'autres distributions


* **ce script n'est pas l'installeur officiel de TorBrowser**
   * son but n'est pas la sécurisation avant tout, d'autres moyens sont plus adaptés [Tails live](https://tails.boum.org/index.fr.html) par exemple
   * la signature et l'empreinte de l'archive ne sont pas vérifiés
   * cela permet juste d'atteindre l'oignon sans difficultés et sans pleurs ;)


* les installations/désinstallations doivent être faites avec les privilèges **root**
* les opérations sur l'utilisateur, ou sur la mise à jour du script, peuvent être faites en utilisateur.

> le programme Firefox est pris en charge par [getFirefox](https://framaclic.org/h/doc-getfirefox)


## installation rapide du script

* privilèges **root** requis

```shell
wget -nv -O getXfox https://framaclic.org/h/getxfox
chmod +x getXfox && ./getXfox
```
```text
              _  __  __ __ 
    __ _  ___| |_\ \/ // _| _____  __ 
   / _' |/ _ \ __|\  /| |_ / _ \ \/ / 
  | (_| |  __/ |_ /  \|  _| (_) >  <  
   \__, |\___|\__/_/\_\_|  \___/_/\_\Tor Browser   WaterFox
   |___/ version 4.17.0 - 12/06/2018

  getXfox 4.17.0 installé dans le système.
  maintenant, appel du script par: getXfox (sans ./)

```

* le script est maintenant dans le système et tout utilisateur peut s'en servir.
* **Aucun programme n'est encore installé**


## installation Waterfox

```shell
getXfox i-wfx
```


## installation Tor

```shell
getXfox i-tor
```


## installation de Tor Browser


* privilèges **root** requis
* la dernière version officielle stable de Tor Browser est installée, en étant directement chargée sur le 
  site [torproject](https://www.torproject.org/projects/torbrowser.html.en)
* un lanceur est placé dans les menus (Applications/Internet)
* chaque installation est refaite en **totalité**, **le profil est écrasée** étant donné les caractéristiques
  d'application portable de Tor Browser.
* Tor Browser peut être lancé en console: `tor-browser`
* Tor Browser est inscrit dans les update-alternatives (si disponible), mais pas configuré comme prioritaire
* une option permet de configurer manuellement, Tor Browser comme prioritaire
* Tor Browser se mettra à jour de lui-même, le script permet cet automatisme
* cette mise à jour se fait en tâche de fond et est disponible au prochain redémarrage
* la mise à jour éventuelle peut être déclenchée manuellement avec le menu `Aide/A propos de Tor Browser`


## installation de Waterfox


```shell
getXfox i-wfx
```
* privilèges **root** requis
* la dernière version officielle stable de Waterfox est installée, en étant directement chargée sur le 
  site [Waterfox](https://www.waterfoxproject.org/)
* un lanceur est placé dans les menus (Applications/Internet)
* chaque installation est refaite en **totalité**, mais **le profil n'est pas écrasé**
* Waterfox peut être lancé en console: `waterfox`
* Waterfox est inscrit dans les update-alternatives (si disponible) et configuré comme prioritaire
* Waterfox se mettra à jour de lui-même, le script permet cet automatisme
* cette mise à jour se fait en tâche de fond et est disponible au prochain redémarrage
* la mise à jour éventuelle peut être déclenchée manuellement avec le menu `Aide/A propos de Waterfox`


### présentation rapide


* code Firefox, compilé avec des options différentes de Mozilla
* 64bits uniquement
* extensions Media encryptés désactivées (EME)
* web runtime désactivé
* Pocket enlevé
* télémétrie et autres profilages enlevés
* plugins NPAPI  64Bits autorisés
* extensions non signées permises
* pas de liens sponsorisés sur la _Nouvelle Page_
* option: dupliquer l'onglet
* sélecteur de locale dans les préférences générales



## help

```shell
getXfox -h
```
```text
              _  __  __ __ 
    __ _  ___| |_\ \/ // _| _____  __ 
   / _' |/ _ \ __|\  /| |_ / _ \ \/ / 
  | (_| |  __/ |_ /  \|  _| (_) >  <  
   \__, |\___|\__/_/\_\_|  \___/_/\_\Tor Browser   WaterFox
   |___/ version 4.17.0 - 12/06/2018
  
      softs possibles: tor, wfx (Tor Browser, Waterfox)
  
  exemple, installation Tor Browser: getXfox i-tor
           installation Waterfox   : getXfox i-wfx
  ----------------------------------------------------------------------
  getXfox i-soft        : installation de <soft> (root)
  getXfox m-soft <archi>: installation de <soft> à partir d'une <archi>ve téléchargée manuellement (root)
  getXfox r-soft        : désinstallation (remove) de <soft> (root)
  getXfox ri            : réparation icône(s) dans le menu (Tor Browser ou Waterfox)
  getXfox t-soft        : téléchargement du <soft> dans le répertoire courant (sans installation)
  getXfox u-soft        : profil pour l'utilisateur en cours et comme défaut système (root) (Tor Browser n'est pas concerné)
  
  getXfox p-soft        : personnalisation sur <soft> de user.js & userChrome.css
  getXfox pr-soft       : suppression des personnalisations (remove) sur <soft>
  getXfox pu            : mise à jour des personnalisations (update) installées
  
  getXfox version : versions installées et en ligne
  
    --dev   : une version de dev du script (si existante) est recherchée
    --sauve : le téléchargement est sauvegardé dans le répertoire courant en plus de l'installation
  ----------------------------------------------------------------------
  ./getXfox (ou ./getXfox -i) : installation du script dans le système (root)
  getXfox -h, --help    : affichage aide
  getXfox -r, --remove  : désinstallation du script (root)
  getXfox -u, --upgrade : mise à jour du script
  getXfox -v, --version : version du script 
  
    plus d'infos: https://framaclic.org/h/doc-getxfox
  
```


## version

```shell
getXfox version
```
```text
              _  __  __ __ 
    __ _  ___| |_\ \/ // _| _____  __ 
   / _' |/ _ \ __|\  /| |_ / _ \ \/ / 
  | (_| |  __/ |_ /  \|  _| (_) >  <  
   \__, |\___|\__/_/\_\_|  \___/_/\_\Tor Browser   WaterFox
   |___/ version 4.17.0 - 12/06/2018

  script en place: 4.17.0
  script en ligne: 4.16.1

  Tor Browser en place: Tor Browser 7.5.5 - June 10 2018
  Tor Browser en ligne: 7.5.5

  perso. Tor Browser en place: Non installé
  perso. Tor Browser en ligne: 0.2

  Waterfox en place: 56.2.0
  Waterfox en ligne: 56.2.0

  perso. Waterfox en place: 0.6
  perso. Waterfox en ligne: 0.6

```


## nouvel utilisateur ou reconfiguration profil

```shell
getXfox u-wfx
```

* privilèges **root** requis
* seul Waterfox permet cette option
* ajoute un profil pour l'utilisateur en cours
* configure Waterfox comme navigateur par **défaut** (alternatives)
* évite de télécharger inutilement une nouvelle fois pour un nouvel utilisateur
* pour ajouter un autre utilisateur, titi par exemple: `USER_INSTALL=titi getXfox u-wfx`, ça devrait 
  marcher (pas testé)


## personnalisation (installation)

```shell
getXfox p-soft
```

* _user.js_ est ajouté dans le profil
* _userChrome.css_ est ajouté dans le profil, sous-répertoire _chrome/_
* la version indiquée dans user.js fait référence, la version de userChrome.css est indicative
* les personnalisations sont périodiquement mise à jour, comme le script (7jours)
* l'url de base peut être changée aisément, les fichiers modifiés et hébergés ailleurs pour mise à jour 
  automatique (sur une plateforme git ou un gist par exemple)
* pour chaque programme concerné, la configuration se trouve trouve dans le fichier:   
  `/home/<user>/.waterfox/waterfox/personnalisation` ou   
  `/opt/usr/share/tor-browser/Browser/TorBrowser/Data/Browser/profile.default/personnalisation`
   * l'url (première ligne) peut y être modifiée
   * la version est indiquée en seconde ligne
* pour figer des fichiers de personnalisations et ne pas les mettre à jour, il suffit d'effacer les fichiers
  `personnalisation`

* contenu de [user.js](https://framagit.org/sdeb/getFirefox/blob/master/user.js)
* contenu de [userChrome.css](https://framagit.org/sdeb/getFirefox/blob/master/userChrome.css)

pour Tor Browser

* contenu de [user.js](https://framagit.org/sdeb/getXfox/blob/master/userTor.js)
* contenu de [userChrome.css](https://framagit.org/sdeb/getXfox/blob/master/userChromeTor.css)



## personnalisation (upgrade)

```shell
getXfox pu
```

* les personnalisations, si présentes, de Waterfox **et** Tor Browser sont mise à jour
* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être lancée 
  manuellement


## personnalisation (suppression)

```shell
getXfox pr-soft
```



## désinstallation d'un programme

```shell
getXfox r-soft
```

* privilèges **root** requis
* le profil pour Waterfox **n'est pas supprimé**, il sera donc utilisable en cas de réinstallation
* le profil de Tor Browser est effacé, comme il est placé dans le répertoire du programme
* si les programmes sont ouverts, ils seront fermés
* update-alternatives, si supportée, sera supprimée
* les lanceurs desktop et console seront supprimés


## installation manuelle d'une archive

```shell
getXfox m-soft archive.tar.xxx
```

* privilèges **root** requis
* installe une archive téléchargée manuellement   


## profil Waterfox

* les caractéristiques sont les mêmes que celles de Firefox
* opération à faire manuellement, mais sauf si l"écart de version était trop grand, une copie du profil Firefox
  pourrait être placée dans le profil Waterfox
* emplacement profil: `/home/<user>/.waterfox/waterfox`



## mise à jour script & personnalisation

```shell
getXfox -u
```

* test toutes les **semaines**
* mise à jour du **script** si une nouvelle version est disponible en ligne. cela n'influe pas sur les
  programmes installés.
* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être lancée manuellement
* _anacron_ est utilisé, c'est à dire que la mise à jour sera testée, dès le redémarrage du Pc
* si une **personnalisation** est mise en place, une mise à jour possible sera elle aussi testée, pour tous les  
  programmes concernés


## logs

```shell
pager /var/log/sdeb_getXfox.log
```

tous les évènements importants sont consignés dans le fichier _/var/log/sdeb_getXfox.log_   


## supprimer le script

```shell
getXfox -r
```

* privilèges **root** requis
* effacement du script dans le système (_/opt/bin_)
* effacement de l'inscription dans crontab/anacron utilisateur
* cela ne **supprime pas** les éventuels programmes installés


## sources

sur [framagit](https://framagit.org/sdeb/getXfox/blob/master/getXfox)


## changelog

sur [framagit](https://framagit.org/sdeb/getXfox/blob/master/CHANGELOG.md)


## contact

pour tout problème ou suggestion concernant ce script, n'hésitez pas à ouvrir une issue 
[Framagit](https://framagit.org/sdeb/getXfox/issues)

IRC: ##sdeb@freenode.net


## license

[LPRAB/WTFPL](https://framagit.org/sdeb/getXfox/blob/master/LICENSE.md)


![compteur](https://framaclic.org/h/getxfox-gif)
