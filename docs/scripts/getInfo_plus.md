# getInfo avancé

Si le script est installé dans le système, les commandes `./getInfo ...` (script appelé depuis le répertoire
de téléchargement) seront alors sous la forme `getInfo ...`.

 
## catégories

`/getInfo -c` permet d'obtenir un menu de sélection de quatre catégories d'analyse pour limiter la taille du rapport ou cibler une analyse
```text
              _   ___        __       
    __ _  ___| |_|_ _|_ __  / _| ___  
   / _' |/ _ \ __|| || '_ \| |_ / _ \ 
  | (_| |  __/ |_ | || | | |  _| (_) |
   \__, |\___|\__|___|_| |_|_|  \___/  $ -c
   |___/  version 4.12.0 - 26/06/2018

            getInfo -h  : afficher l'aide 

  getInfo -cs : catégorie système                 getInfo -cc : catégorie configuration
        (matériel détecté, disque,...)                  (locale, pakager, ...)
  getInfo -cr : catégorie réseau                  getInfo -ca : catégorie analyse
        (config réseau, Network Manager)                (journaux Xorg, système)

    les catégories peuvent être cumulées: 
     getInfo -csa générera un rapport sur le système & l'analyse

  ( ne pas saisir le préfixe -c, all par défaut) 

``` 


## options principales

* `getInfo -h` affiche l'aide
* `getInfo -j` ou `./getInfo -ca` isole les journaux Xorg, kernel et autres du systeme. 
  Si `journalctl` n'est pas accessible, `dmesg` est utilisé (seulement pour le kernel).  
    * Priorités extraites: emergency, alerte, critique, erreur, warning
    * Les droits **superutilisateur** ou **root* sont requis, à défaut, les journaux système (`journalctl` 
      ou `dmesg`) ne seront pas extraits
* `getInfo -l` affiche un rapport _getInfo_rapport.md_ existant
* `getInfo -p` exporte un rapport _getInfo_rapport.md_ existant sur le pastebin 
    * l'option supplémentaire `-t n` permet de remplacer la durée de conservation **standard de 7 jours** par 
     **n** jours (par exemple -t 1: le paste sera effacé au bout de 1 jour)
* `getInfo -us` (update spécial), le script est mis à jour là où il est, sans installation pré-requise
* `gfetch` lanceur de `getInfo --rc`, si script installé


## options diverses

* `getInfo --debug` les messages d'erreurs sont redirigés dans _/tmp/getInfo.log_ qui sera ajouté au rapport lors de l'export
* `getInfo --ip` affiche les IPs publiques (ipv4 et/ou ipv6)
* `getInfo --mac` affiche les adresses Mac
* `getInfo --serial` affiche seulement (pas de rapport construit) les numéros de série des disques, 
  des batteries ou du châssis si présents. le n° de châssis demandera le mot de passe administrateur 
  pour s'afficher
* `getInfo --ssid` affiche seulement (pas de rapport construit) la configuration des ssid de NetworkManager. 
  Attention, cela affichera probablement les mot de passe des réseaux wifi configurés. 
  les **droits root** sont requis
* `getInfo --rc` affiche  un résumé rapide en console. 


## gfetch, remarques 

À l'installation de getinfo, un lanceur `gfetch` est créé. Lancé manuellement, il affichera un résumé système dans le terminal.   

Avec `getInfo --irc`, `gfetch` est inscrit dans le .bashrc système. A l'ouverture d'un terminal, le résumé système est affiché.   

Pour enlever cette inscription: `getInfo -rrc`

Avec une philosophie différente, le principe est celui de [neofetch](https://github.com/dylanaraps/neofetch), lui même un fork 
très actif de [screenfetch](https://github.com/KittyKatt/screenFetch).


## installation du script

Une fois installé, le script est accessible à tous les utilisateurs. Un test hebdomadaire est effectué et le
script est mis à jour si une nouvelle version est disponible.
 
* `./getInfo -i`  **droits root** requis. Installation du script dans le système. Le script téléchargé sera 
  effacé du répertoire courant. Le lancement se fera donc par `getInfo`. le lanceur `gfetch` est ajouté au système
* `getInfo -r`    **droits root** requis. Effacement du script dans le système (_/opt/bin_)

* `getInfo --irc` **droits root** requis. Inscription de `gfetch` dans bashrc: `/etc/bash.bashrc` (debian) ou `/etc/bashrc` (redhat) ou `/etc/bash.bashrc.local` (suse)
* `getInfo --rrc` **droits root** requis. Désinscription `gfetch` dans .bashrc

* `getInfo -u` Mise à jour du script installé dans le système. Cette opération est automatiquement lancée toutes les semaines.
* `getInfo -v` Affiche la version du script, installé et en ligne.
`
tous les évènements importants sont consignés dans le fichier _/var/log/sdeb_getInfo.log_   
```shell
pager /var/log/sdeb_getInfo.log
```
