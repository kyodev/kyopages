# d-i console

en cas de soucis majeurs, ou pour récupérer une capture d'écran, on peut accéder à une console et 
reprendre après l'installation en cours.   
cliquer sur _Revenir en arrière_ autant de fois qu'il le faut pour obtenir le _Menu principal du 
programme d'installation Debian_   
presque tout en bas: _Exécuter un shell_
![debian-installer_main-menu](debian-installer_main-menu.png)

![di-utils-shell_do-shell](di-utils-shell_do-shell.png)


## capture ou collecte infos

on est dans une busybox. les captures sont dans `/var/log`   
on insère une clé:
```shell
	# on vérifie le device de la clé
fdisk -l

	# on la monte (adapter si besoin)
mount /dev/sdc1 /mnt

	# récupère le dmesg par exemple
dmesg > /var/log/dmesg.txt

	# firmwares manquants
cp /tmp/check-missing-firmware-dmesg.list /var/log/
	# on copie le contenu de /var/log sur la clé
cp /var/log/ /mnt/

	#on démonte la clé
umount /mnt

	# on sort de la console
exit
```
on revient au menu _Menu principal du programme d'installation Debian_
