# boot à partir de grub

** utilité? **

![Clavier Qwerty](clavier_qwerty.svg)
mise en page des sorties de certaines commandes longues
```shell
grub> set pager=1
````
on liste les partitions pour identification
```shell
grub> ls
````
```text
	
(hd0) (hd0,msdos3) (hd0,msdos2) (hd0,msdos1)		# table partition mbr:
(hd0) (hd0,gpt4) (hd0,gpt3) (hd0,gpt2) (hd0,gpt1)	# table partition gpt (& uefi):
```

**attention**   

 * la numérotation des **disques** commence à **0**
 * la numérotation des **partitions** à **1**

on liste la partition **efi**
```shell
grub> ls (hd0,1)
```
```text
Partition hd0,1 : Type de système de fichiers fat, UUID C0E3-2598 - La partition commence à 1024 Kio - 
Taille totale 248832 Kio
```
on liste la partition système
```shell
grub> ls (hd0,gpt2)	# on peut utiliser gpt2 ou lieu du seul numéro de partition
```
```text
Partition hd0,2 : Type de système de fichiers ext* - Étiquette "sys" - Dernière date de modification 
samedi 20/04/2017 18:36:49, UUID 4496a30e-e370-47b9-b699-f4c149f0efa7 - La partition commence à 249856 Kio - 
Taille totale 10485760 Kio
```
on identifie une Debian
```shell
grub> cat (hd0,2)/etc/issue
  Debian GNU/Linux 9 \n \l    #système identifié
```

à adapter selon le système visé, à ce niveau la complétion Tab fonctionne
```shell
grub> ls (hd0,2)/
  lost+found/ boot/ home/ etc/ media/ vmzlinuz.old.var/ var/ usr/ lib/ lib64/ bin/ dev/ proc/ root/ run/ sbin/ sys/ 
  tmp/ mnt/ srv/ opt/ initrd.img.old vmlinuz initrd.img
```
```shell
grub> set root=(hd0,2)	# cible du système
grub> linux /boot/vmlinuz-4.9.0-2-amd64 root=/dev/sda2
grub> initrd /boot/initrd.img-4.9.0-2-amd64
grub> boot
```
démarrage

## réparer le grub:

```shell
update-grub
grub-install /dev/sda	#vérifier que le grub doit être installé sur ce disque, cas e plus courant
reboot
```

test: 04/2017
