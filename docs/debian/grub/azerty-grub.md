# azerty en grub

fonctionne avec grub2+, vérifier la version `grub-install --version`
```text
grub-install (GRUB) 2.02~beta3-5
```

 * générer un layout grub du clavier en-cours.   
   voir les locales possibles: `ls /usr/share/X11/xkb/symbols/`   
   en root:
```
su
grub-kbdcomp -o /boot/grub/azerty.gkb fr
```

 * éditer `/etc/default/grub` pour ajouter 
```text
GRUB_TERMINAL_INPUT="at_keyboard"
```
 * éditer `/etc/grub.d/40_custom` pour ajouter
```text
insmod keylayouts
keymap /boot/grub/azerty.gkb
```
 * re-créer un grub.cfg
```shell
update-grub
```

testé: 17/06/2017

