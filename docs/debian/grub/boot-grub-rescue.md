# boot à partir de grub rescue

A ce niveau, le clavier est en qwerty:  
![Clavier Qwerty](clavier_qwerty.svg)

on est dans **_grub rescue_**, le prompt ressemble à ça: `grub rescue>`

on liste les partitions
```shell
ls
```
```text
	(hd0) (hd0,msdos3) (hd0,msdos2) (hd0,msdos1)		=> table partitions mbr
	(hd0) (hd0,gpt4) (hd0,gpt3) (hd0,gpt2) (hd0,gpt1)	=> table partitions gpt (& uefi)
```

**attention**   

 * la numérotation des **disques** commence à **0**
 * la numérotation des **partitions** à **1**

choisir ou extrapoler à partir de ces 2 exemples:  

partition **mbr** |  | partition **gpt**
-- | :--: | --
disk système sur partition 1 |   | système sur partition 2 (efi sur partition 1)
`set prefix=(hd0,1)/boot/grub` | cible grub.cfg | `set prefix=(hd0,2)/boot/grub`
`set root=(hd0,1)` | cible le système | `set root=(hd0,2)`


on insère le(s) module(s):

```shell
insmod normal
```
remarque: 

 * certains insèrent les modules suivants (pas eu besoin pour ma part): `insmod linux` `insmod boot`
 * si soucis, essayer `lsmod` (disponible dans grub rescue?) pour voir des modules intéressant

on active le module normal
```shell
normal
```
démarrage habituel :)

## réparer le grub

voir [grub install](chroot-grub-install#grub-install)


test: 04/2017
