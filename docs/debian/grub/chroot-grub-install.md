# Réparer Grub en chroot

 * démarrer à partir d'un live en même architecture que le grub à réparer (amd64 ou 686)
 * ouvrir un terminal, en **root*
    * pass inconnu ou pas de root?: `sudo passwd root` mot de passe toto par exemple


## identifier les partitions

chercher si EFI:
```shell
[ -d /sys/firmware/efi ] && echo "EFI boot" || echo "MBR/Legacy boot"
```

lister les partitions
```shell
fdisk -l` pour identifier votre système
```

### exemple MBR

```text
 Type d'étiquette de disque : dos
Périphérique Amorçage    Début       Fin  Secteurs Taille Id Type
/dev/sda1    *            2048  29296639  29294592    14G 83 Linux
/dev/sda2             29298686  95705087  66406402  31,7G  5 Étendue
/dev/sda3             95705088 312580095 216875008 103,4G 83 Linux
/dev/sda5             29298688  87891967  58593280    28G 83 Linux
/dev/sda6             87894016  95705087   7811072   3,7G 82 partition d'échange Linux / Solaris
```
 * la partition root (*) est ici **sda1**

### exemple EFI

```text
Type d'étiquette de disque : gpt
Périphérique    Début       Fin Secteurs Taille Type
/dev/sda1        2048   1050623  1048576   512M Système EFI
/dev/sda2     1050624  42993663 41943040    20G Système de fichiers Linux
/dev/sda3    42993664  50515967  7522304   3,6G Partition d'échange Linux
/dev/sda4    50515968 113430527 62914560    30G Système de fichiers Linux
```
 * la partition efi est en **sda1**, la partition root est ici **sda2**

## montage chroot

sur le système live, on crée le répertoire de montage chroot
```shell
mkdir -p /mnt/chroot
```

monter les partitions systemes  

MBR | type | EFI
--- | :--: | ---
`mount /dev/sda1 /mnt/chroot`	| root | `mount /dev/sda2 /mnt/chroot`
_sans objet_					| EFI | `mount /dev/sda1 /mnt/chroot/boot/efi`
si `/boot` est sur une partition séparée, la monter | /boot | _sans objet_

monter les périphériques
```shell
for i in /sys /proc /dev; do mount --bind "$i" /mnt/chroot"$i"; done
```
chrooter
```shell
chroot /mnt/chroot
```
état des lieux
```shell
lsblk
```
```text
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 111,8G  0 disk 
├─sda1   8:1    0   512M  0 part /boot/efi
├─sda2   8:2    0    20G  0 part /
├─sda3   8:3    0   3,6G  0 part 
└─sda4   8:4    0    30G  0 part
```
si besoin pour autre chose qu'un grub repair, on monte les partition du fstab
```shell
mount -a
```

état des lieux
```shell
lsblk
```
```text
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 111,8G  0 disk 
├─sda1   8:1    0   512M  0 part /boot/efi
├─sda2   8:2    0    20G  0 part /
├─sda3   8:3    0   3,6G  0 part 
└─sda4   8:4    0    30G  0 part /home
```


## grub chroot réparation

réinstaller le paquet grub-efi

MBR | type | EFI
--- | :--: | ---
`apt-get install --reinstall grub-pc` | (ré)install si nécessaire | `apt-get install --reinstall grub-efi`


vérfier et installer os-prober si absent
```shell
which os-prober || apt install os-prober
```

install du bootloader
```shell
grub-install /dev/sda
```
**: /dev/sda optionnel si la partition EFI est bien montée en /boot/efi

on re-crée un grub.cfg
```shell
update-grub
```

on peut sortir du chroot et rebooter
```shell
exit
reboot
```

### vérifier grub
```shell
dd if=/dev/sda bs=512 count=1 | strings | grep -i grub
```
```text
1+0 enregistrements lus
1+0 enregistrements écrits
512 bytes copied, 0,00104559 s, 490 kB/s
GRUB 
```


### vérifier EFI 

 * que le bootloader est en place
```shell
file /boot/efi/EFI/debian/grubx64.efi
```
```text
/boot/efi/EFI/debian/grubx64.efi: PE32+ executable (EFI application) x86-64 (stripped to external PDB), for MS Windows
```

 * l'entrée NVRAM
```shell
efibootmgr --verbose | grep debian
```
```text
Boot0000* debian HD(1,GPT,d82668a0-175f-487f-b843-8bd6abb090d8,0x800,0x100000)/File(\EFI\debian\grubx64.efi)
```
 * voir tout NVRAM
```shell
efibootmgr --verbose
```
```text
BootCurrent: 0001
Timeout: 0 seconds
BootOrder: 0000,0001
Boot0000* debian HD(1,GPT,d82668a0-175f-487f-b843-8bd6abb090d8,0x800,0x100000)/File(\EFI\debian\grubx64.efi)
Boot0001* UEFI: PNY USB 2.0 FD 8192 PciRoot(0x0)/Pci(0x10,0x0)/USB(3,0)/HD(1,MBR,0x4294967250,0x13e4,0x580)AMBO
```

un fichier _.efi_ doit être visible sur le disque, à priori boot0000, avec une chemin correct
```text
HD(1,GPT,d82668a0-175f-487f-b843-8bd6abb090d8,0x800,0x100000)/File(\EFI\debian\grubx64.efi)
```

## essais

 * à tester dans la console de D-I
 * il semblerait que parfois, sur une machine 64bits, un EFI en 32bits doit être installé, ce que ne sait 
  pas faire d-i. dans ce cas, en console d-i?,: `apt-get install grub-efi-ia32 grub-efi-ia32-bin`   
  au pire, après refaire une install dans retoucher au grub


## plus d'infos:

 * [debian grub recover](https://wiki.debian.org/GrubLegacyRecover)
 * [man grub](https://www.gnu.org/software/grub/manual/grub.html)
 * [rodsbook](http://www.rodsbooks.com/efi-bootloaders/principles.html)
 * [Super Grub Disk](http://www.supergrubdisk.org/index.php?pid=5)
