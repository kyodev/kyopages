# apt préférences, pinning, hold

emplacement des fichiers:   
`/etc/apt/preferences` ou `/etc/apt/preferences.d/perso` ou `/etc/apt/preferences.d/perso.pref`


## préférences sur branche

* par défaut, un dépôt a une priorité de **500** sauf expérimental (1) ou backports (100)
* un paquet installé à une priorité **100** (release: a=now)
* un dépôt marqué target release (APT::Default-Release "stable";) a une priorité 990, comme celui indiqué temporairement par l'option `-t`
* une priorité >1000 permet un retour en arrière (downgrade) comme l'option `--allow-downgrades`
* une priorité 100: `NotAutomatic: yes`, `ButAutomaticUpgrades: yes`: mise à jour d'un paquet installé dans la branche possible, mais pas de mise à niveau hors branche (un noyau sid ne sera pas utilisé par exemple, sauf si install auparavant). épiglage par défaut de backports, induit par le fichier `releases` du dépôt, mais l'automatisme n'est pas (ou n'a pas toujours été?) fiable, épinglage préférable
* une priorité < -1  interdit l'installation automatique (recommends) (depends?), permet l'installation manuelle (apt -t ou paquet/branche). très strict


## suivi testing

```text
	### stable en fallback, cet épinglage n'est pas fonctionnellement utile
Package: *
Pin: release a=stable,n=stretch
Pin-Priority: 400

	### sid TRES STRICT uniquement manuel, pas d'upgrade auto
Package: *
Pin: release a=unstable,n=sid
Pin-Priority: -10
```

```text
	### sid en dépôt annexe, install manuelle, upgrade des paquets installés
Package: *
Pin: release a=unstable,n=sid
Pin-Priority: 100
```

## test simulation

jouer avec des mélanges de dépôts n'est pas anodin, bien tester le comportement de l'épinglage avec: `apt policy` et `apt policy <paquet>`


`getInfo -cc` permet de voir facilement l'origine des upgrades à faire


## installation d'une "origin" différente 

* installer un paquet de sid|unstable
```shell
apt -t unstable install <paquet>
 # ou
apt -t sid install <paquet>
 # ou
apt <paquet>/unstable
```

## préférence sur une version de paquet 

* maintenir version 1  
```text
Package: <paquet>
Pin: version <version1 paquet>*
Pin-Priority: 1001
```

* maintenir version 1, équivalent
```text
Package: <paquet>
Pin: release a=now
Pin-Priority: 1001
```
(release a=now indique les paquets installés)

* éviter une installation version 2
```text
Package: <paquet>
Pin: version <version2>*
Pin-Priority: -10
```

## info backports

sans réglages, le fichier `release` de la branche backports induira une priorité de 100, mais comme il y a eu de nombreux incidents à l'époque de jessie en stable, il est péférable de "_l'inscrire en dur_"

```text
	### backports, règles en du du comportement normalement par défaut
Package: *
Pin: release a=stretch-backports
Pin-Priority: 100
```

## hold

hold permet de figer un paquet, sans discernement comme la version par exemple. plus de mise à jour possible sans marquage **unhold**. fonctionnement différent de pinning, ici pour info:

* figeage (hold) d'un paquet en place
```shell
apt-mark hold <paquet>
```

* libérer un paquet
```shell
apt-mark unhold <paquet>
```

* voir tous les paquets marqués hold
```shell
apt-mark showhold
```

* combiner unhold et maj
```shell
apt --ignore-hold upgrade
```
équivalent de `apt-mark unhold *` & `apt upgrade`


## syntaxe préférences APT

```text
Origin 		-> o=		(origin)
Label		-> l=		(label)
Suite		-> a=		(archive)
Version		-> v=		(version)
Components	-> c=		(components: main contrib non-free)
Codename	-> n=		(name)
```
voir codes à appliquer:


## dépôts/branches debian

* voir les caractéristiques des dépots: `pager /var/lib/apt/lists/...InRelease|Release`
* `apt policy`

 Suite (a=)			|  
 :--:				| :--:	
 now				| paquets installés
 
 
 Suite (a=)			| Codename (n=)		| Label (l=)		| Origin (o=)
 :--:				| :--:				| :--:				| :--:
 stable				| stretch			| Debian			| Debian
 stable				| stretch			| Debian-Security	| Debian
 stable-updates		| stretch-updates	| Debian			| Debian
 stretch-backports	| stretch-backports	| Debian Backports	| Debian Backports


 Suite (a=)			| Codename (n=)		| Label (l=)		| Origin (o=)
 :--:				| :--:				| :--:				| :--:
 testing			| buster			| Debian			| Debian
 testing			| buster			| Debian-Security	| Debian
 testing-updates	| buster-updates	| Debian			| Debian


 Suite (a=)			| Codename (n=)		| Label (l=)		| Origin (o=)
 :--:				| :--:				| :--:				| :--:
 unstable			| sid				| Debian			| Debian


* pour toutes les branches, Components: `main` `contrib` `non-free`


