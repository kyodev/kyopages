# apt.conf, configuration

* configuration APT, dans les fichiers:   
`/etc/apt/apt.conf` ou `/etc/apt/apt.conf.d/maConf` ou `/etc/apt/apt.conf.d/maConf.conf`

* voir une ou les options configurées
```shell
apt-config dump | grep -i '<motif>'
apt-config dump | less
```

* rechercher une option dans un fichier 
```shell
grep -ir 'APT::periodic' /etc/apt/apt.conf*
```

* les commentaires d'une ligne: `//` 
* les commentaires d'une partie: `/*  */`
* les options sont **insensibles** à la casse
* une option par ligne, terminée avec: `;`

* les réglages peuvent se faire dans un fichier de conf, mais toutes les commandes `apt` ont une option `-o option` qui permet ponctuellement un réglage conf

par exemple:
```shell
apt -o APT::Install-Recommends=0
```
est l'équivalent d'une configuration
```shell
echo "APT::Install-Recommends "0";" >> /etc/apt/apt.conf.d/99maconfig
```
à noter que parfois une option existe directement pour une commande, par exemple:
```shell
apt --no-install-recommends <paquet>
```
 
## configuration basique

### conservation des archives avec le binaire apt

* `apt-get cmd` conserve les paquets chargés dans `/var/cache/apt/archives` (dossier par défaut)

* depuis apt 1.2~exp1, 01/2016, `apt` (binaire) ne conserve pas les paquets, ce qui ne simplifie pas, la récupération d'une archive de version précédente (testing|unstable) ou la création d'un dépot local. sont concernés notamment `apt install` `apt upgrade` `apt dist-upgrade`

* revenir à un comportement traditionnel:
```shell
echo -e "\t// les paquets chargés sont conservés, comme avec apt-get, dans /var/cache/apt/archives" \
	>> /etc/apt/apt.conf.d/99sdeb-niveau1.conf
echo -e "Binary::apt::APT::Keep-Downloaded-Packages "1"; \n" \
	>> /etc/apt/apt.conf.d/99sdeb-niveau1.conf
```

* confirmation du comportement par défaut _suggests_:
```shell
echo -e "\t// les paquets Suggests ne sont pas automatiquement installés, défaut" \
	>> /etc/apt/apt.conf.d/99sdeb-niveau1.conf
echo -e "APT::Install-Suggests "0"; \n" \
	>> /etc/apt/apt.conf.d/99sdeb-niveau1.conf
```


## configuration avancée

### Ignorer les recommends (recommandés)

Par défaut, sur une debian:

* les paquets depends (dépendances) sont bien sûr installés
* les paquets recommandés (recommandés) sont installés
* les paquets suggests (suggérés) ne sont pas installés


* ne pas installer automatiquement les _recommends_:
```shell
echo -e "\t// les paquets recommends ne sont pas automatiquement installés" \
	>> /etc/apt/apt.conf.d/99sdeb-niveau2.conf
echo -e "APT::Install-Recommends "0"; \n" \
	>> /etc/apt/apt.conf.d/99sdeb-niveau2.conf
```

