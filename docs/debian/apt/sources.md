# sources liste, format deb822

notes: 

* pour des composants uniquement **free**, effacer _contrib non-free_
* à partir de apt 1.5, soit buster/unstable, le protocole **https** est directement pris en charge. le paquet _apt-transport-https_ est inutile. le mélange de protocole http & https est possible
* seuls à ce jour (debian 9.3), les dépôts **deb.debian.org** servent en https


## emplacement

`/etc/apt/sources.list.d/`

   * nom sans importance
   * extension obligatoire
      * extension `.sources`: format deb822
      * extension `.list`: ancien format  'Une Ligne'


`/etc/apt`

   * obligatoire: `sources.list`, pas de format deb822, donc emplacement à oublier


## branche stretch ( stable )

```shell
cat /etc/apt/sources.list.d/sources.sources
```
```text
	# si besoin de compiler des paquets: utiliser: Types: deb deb-src 

Description: Stretch + update (volatile)
Types: deb 
URIs: https://deb.debian.org/debian/
Suites: stretch stretch-updates 
Components: main contrib non-free

Description: security
Types: deb
URIs: https://deb.debian.org/debian-security
Suites: stretch/updates
Components: main contrib non-free

Enabled: no
Description: backports, pas activés par défaut, pour activer: Enabled: yes
Types: deb
URIs: https://deb.debian.org/debian/
Suites: stretch-backports
Components: main contrib non-free
```

toutes options, au plus compact, sources & binaires + toutes les branches:
```text
Description: Stretch + update (volatile) + backports
Types: deb deb-src
URIs: https://deb.debian.org/debian/
Suites: stretch stretch-updates stretch-backports
Components: main contrib non-free

Description: security
Types: deb deb-src
URIs: https://deb.debian.org/debian-security
Suites: stretch/updates
Components: main contrib non-free
```

## suivi buster ( testing )

```shell
cat /etc/apt/sources.list.d/sources.sources
```
```text
Description: buster stable & sid (pin -10)
Types: deb 
URIs: https://deb.debian.org/debian/
Suites: buster stable sid
Components: main contrib non-free

Description: security
Types: deb
URIs: https://deb.debian.org/debian-security
Suites: buster/updates
Components: main contrib non-free

Enabled: no
Description: sources, pour activer: Enabled: yes
Types: deb-src
URIs: https://deb.debian.org/debian/
Suites: stable testing sid
Components: main contrib non-free

	# voir 99sdeb-niveau2.pref 
```
**ABSOLUMENT** régler les priorités avec le [fichier des préférences](https://kyodev.frama.io/kyopages/debian/apt/apt_preferences-hold/#suivi-testing)


## notes deb822

* <https://manpages.debian.org/testing/apt/sources.list.5.fr.html#FORMAT_DEB822-STYLE>
* nouveau format depuis 2015, dépréciant à terme l'ancien format de type "Une Ligne"
* fichier uniquement avec extension **.sources** (.list ne fonctionne pas avec ce format)
* format soi-disant plus facile à parser ... :/
* chaque paragraphe séparé par une **ligne vide** décrit une entrée
   * donc pas de commentaires # séparés par des lignes vides (sera considéré comme des paragraphes en erreur) 
* commentaire: 
   * `#` sur une ligne COMPLÈTE (Types: deb # deb-src -> Erreur !)
   * désactiver une entrée complète (paragraphe) avec `Enabled: no`
* chaque propriété peut avoir plusieurs valeurs, séparées par des espaces, sauf URIs
* Suites: peut comporter des noms de suite (stretch) ou d'archive (stable)
* format activé depuis apt 1.1, novembre 2015
* si souci, vérifier `apt-config dump | grep -i 'APT::Sources::Use-Deb822=false'` n'est pas à "0" ou "false" 
  (développement entre apt 0.9.14.3~exp5 et apt 1.1)
* exemple:

```text
	Types: deb
	Description: binaires stretch testing sid (suivi stretch)
	URIs: https://deb.debian.org/debian/ 
	Suites: stretch testing sid
	Components: main contrib non-free
	OptionX:
	CeQueJeVeux: n'importe Quoi, ne provoquera pas d'erreur
```


## format historique, suivi buster

```shell
cat /etc/apt/sources.list
```
```text
	# branche de base (testing)
deb https://deb.debian.org/debian/			buster			main contrib non-free
deb https://deb.debian.org/debian-security	buster/updates	main contrib non-free
 # deb-src https://deb.debian.org/debian/			buster 			main contrib non-free
 # deb-src https://deb.debian.org/debian-security	buster/updates	main contrib non-free

	# fallback stable
deb	https://deb.debian.org/debian/			stretch			main contrib non-free  

	# unstable 	(pin -10)
deb	https://deb.debian.org/debian/			sid			main contrib non-free  
 # deb-src https://deb.debian.org/debian/	sid 		main contrib non-free

	# voir 99sdeb-niveau2.pref pour le pinning 
```

