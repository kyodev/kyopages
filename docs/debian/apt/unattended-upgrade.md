# unattended-upgrade


<https://wiki.debian.org/UnattendedUpgrades>


le programme est **unattended-upgrade**, l'orthographe avec un s final est un lien sur /usr/bin/unattended-upgrade  
le man est unattended-upgrade, le paquet et le fichier de config comporte un S final :(  


## installation

```shell
apt install unattended-upgrades
```
normalement, depuis debian9 stretch, unattended-upgrade est installé par défaut (mais pas forcément activé?)


## configuration 

`xdg-open /etc/apt/apt.conf.d/50unattended-upgrades`  
extrait config, exemple:  

```text
Unattended-Upgrade::Origins-Pattern {

	// Suivi du codename (stretch,buster,sid):
		//\\ complété pour info(tous les champs ne sont pas requis à partir du moment 
		//\\ ou un dépôt est identifiables sans confusion

	"o=Debian,n=stretch,l=Debian,c=main contrib non-free";
	"o=Debian,n=stretch-updates,l=Debian,c=main contrib non-free";
	"o=Debian,n=stretch,l=Debian-Security,c=main contrib non-free";

	// Suivi d'une archive (suite) (stable,testing,unstable):
	// Attention cela fera une migration silencieuse lors d'une nouvelle version Debian

//	"o=Debian,a=stable";
//	"o=Debian,a=stable-updates";
//	"o=Debian,a=proposed-updates";

	//\\exemples autres syntaxes possibles

//	"origin=Debian,codename=${distro_codename},label=Debian-Security";
//	"origin=Debian,archive=stable,label=Debian-Security,component=main contrib non-free,site=deb.debian.org";
//	"origin=Debian,codename=stretch,label=Debian,component=main contrib non-free,site=deb.debian.org";


	//\\ TESTING
//	"o=Debian,a=testing,l=Debian,c=main contrib non-free";

};
```

configuration personnalisée:

```text
Unattended-Upgrade::MinimalSteps "true";
Unattended-Upgrade::Mail "root";
Unattended-Upgrade::MailOnlyOnError "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Automatic-Reboot "false";
Unattended-Upgrade::Automatic-Reboot-WithUsers "false";
Unattended-Upgrade::SyslogEnable "true"; 
```


## test

simuler pour test, visualisation des erreurs, pas d'ugrade réel

```shell
unattended-upgrade -d
```


## activation 


```shell
apt-config dump | grep -i 'APT::Periodic::Unattended-Upgrade'
```
```text
APT::Periodic::Unattended-Upgrade "1";
```
si **1** ou **true**, unattended-upgrade est activé, au besoin:

```shell
dpkg-reconfigure -plow unattended-upgrades
```
(plow = priority low)

cela configurera le fichier `/etc/apt/apt.conf.d/20auto-upgrades` avec la ligne permettant la mise à jour automatique:
```text
APT::Periodic::Unattended-Upgrade "1";
```
dans les anciennes config(?), ces options pouvaient être mises dans /etc/apt/apt.conf.d/02periodic

  
## config apt du système


```shell
apt-config dump | grep 'APT::Periodic::Enable' 
```
* défaut: APT::Periodic "";
* pas de `APT::Periodic::Enable "1";` configuré par défaut (dans stable & testing)

```shell
AutoAptEnable="n/a"
eval $(apt-config shell AutoAptEnable APT::Periodic::Enable)
[ $? ] && echo ok || echo KO
ok
echo $AutoAptEnable
n/a
```


## suivi

fichier logs:
```text
 /var/log/unattended-upgrades/unattended-upgrades.log
 /var/log/unattended-upgrades/unattended-upgrades-dpkg.log
 /var/log/dpkg.log
```

