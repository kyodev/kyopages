# snapshot comme sources de paquets

<http://snapshot.debian.org/> stocke toutes les versions des paquets debian et permet de s'en servir comme source pour remonter une version précédente par exemple, celle en cours étant cassée sur sid ou testing. la gestion des dépendances se fait donc simplement

remarques: pas de https


## repérage

* chercher dans <http://snapshot.debian.org/package/paquet/version/> la date d'apparition d'une version de paquet, exemple:
```text
geany_1.32-1_alpha.deb
    Seen in debian-ports on 2017-12-25 01:48:23 in /pool-alpha/main/g/geany
```
* la date sera transformée au format yyyymmdd, soit **20171225**
* le format yyyymmddThhmmssZ est possible, mais sans intérêt


## sources

* ajouter comme source de dépôt
```shell
su
echo 'deb [check-valid-until=no] http://snapshot.debian.org/archive/debian/yyyymmdd/ sid main contrib non-free' \
	/etc/apt/sources.list.d/snapshot.list
```
* adapter main contrib non-free selon les besoins, mais laisser les trois composants n'est pas dangereux
* si besoin des sources, ajouter une ligne deb-src 
* les caractéristiques du dépôt snapshot sont les mêmes que sid (unstable)
  * Origin: Debian, Label: Debian, Suite: unstable, Codename: sid
* pour une version debian antérieure à stretch, l'option [check-valid-until=no] n'est pas reconnue, ne pas l'utiliser


## update

* mettre à jour les listes
```shell
apt update
```

* pour une version debian antérieure à stretch, option [check-valid-until=no] non reconnue:
```shell
apt -o 'Acquire::Check-Valid-Until=false' update
```


## install

* pour _downgrader_ un paquet
```shell
apt --allow-downgrades -t unstable install paquet[=version]
```

exemple:
```shell
apt --allow-downgrades -t unstable install geany=1.32-1 geany-common=1.32-1
```
