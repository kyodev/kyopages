# nettoyage d'une Debian

**toutes les commandes système qui suivent doivent être lancées en root**  
`su` ou `sudo -s`(à confirmer pour sudo)


## suppression du cache des paquets téléchargés (avant installation)
* voir la taille `du -h /var/cache/apt/archives`
* vider le cache entièrement `apt clean`
* vider le cache des seuls paquets qui ne peuvent plus être chargés `apt autoclean`

## paquets inutilisés
* supprimer les paquets à l'abandon `apt autoremove`
    *  plus de profondeur, supprimer les paquets orphelins   
    `apt install deborphan`  
    `apt remove $(deborphan)`

## vérifier la taille des logs
* `ls -lh /var/log/` `du -h /var/log/`
* vérifier que syslog `ls -l /var/log/syslog` ne dépasse pas le Mo, au besoin vérifier l'installation et 
  la configuration de **logrotate**
* éventuellement supprimer les anciens logs `rm /var/log/*.gz`

## suppression des traductions inutiles
* `apt install localepurge`  
     * conserver fr, fr_FR, fr_FR@UTF-8 et fr_FR@euro pour la france par exemple  
     * confirmer l'utilisation de "dpkg --path-exclude" pour les futures installation
* pour reconfigurer `dpkg-reconfigure localepurge`

## Repérez les fichiers de configuration "orphelins"
 * `dpkg -l | awk '/^rc/{print $2}'`
 * **si** présent(s), les purger: `dpkg --purge $(dpkg -l | awk '/^rc/{print $2}')`

## Répérez les fichiers qui ne seraient pas en état _installé_
 * `dpkg -l | grep -v '^ii'` analyser avec prudence si résultats.
 * purger avec précautions après avoir analyser les états des paquets:
```
 ‣ État souhaité
    h.. hold (à garder)
    i.. install (à installer)
    p.. purge (à purger)
    r.. remove (à supprimer)
    u.. unknown (inconnu)
 ‣ État du paquet
    .c. config-files (fichiers de configuration seuls)
    .f. halF-configured-configured (semi-configuré)
    .h. Half-installed (semi-installé)
    .i. installed (installé)
    .n. not-installed (non installé)
    .t. triggers-pending (actions différées en cours)
    .u. unpacked (décompressé seulement)
    .w. triggers-awaiting (attente actions différées)
 ‣ Drapeaux d'erreur**
    ..r (réinstallation requise)
```


## configurer des motifs pour exclure à l'installation
* [voir](https://raphaelhertzog.fr/2011/04/14/economisez-de-l-espace-disque-en-excluant-les-fichiers-inutiles-avec-dpkg/)

