# mise à jour Jessie en Stretch
(Debian **8.8** à 9)

>
condensé du brouillon des release notes au 1er Juin 2017 ou de bonnes pratiques d'ordre général pour assurer un mise 
à jour de version majeure Debian, en éliminant le plus de risques possibles
>

mais rien n'interdit de les lire complètement. Maintenant que Stretch est en stable, 
les release notes sont [disponible ici](https://www.debian.org/releases/stretch/releasenotes)

##préliminaires

* [Nettoyer son système](nettoyage_debian).
* Sauvegarder ses données et son système surtout les données et la configuration, notamment
    * `/etc`
    * `/var/lib/dpkg`
    * `/var/lib/apt/extended_states`
    * `dpkg --get-selections "*" > get_selections.txt`
    * `/home/users`
    * `/home/users/.*`
* le répertoire /home ne sera pas impacté par une mise à jour, sauf la configuration de certains logiciels 
  (mozilla, gnome, Kde,...).
* les mises à jour depuis un système **antérieur à Debian 8 Jessie** ne sont pas prise en charge. procéder à une mise à 
  niveau en *Jessie* si nécessaire.
* par sécurité le noyau doit être suffisamment à jour comme celui livré dans *Jessie* **version 8.8**  
  `cat /etc/debian_version` ou `lsb_release -d`  
  **mettre à jour en 8.8 si nécessaire**
* Uefi est amélioré et prend en charge l'installation sur un micrologiciel **UEFI 32 bits** avec un noyau **64 bits**. 
  cela ne concerne pas **Secure Boot**, qui **doit être désactivé**.

## incompatibilités connues ou changements importants 

* les *intel i586* (5e génération et précedentes) ne sont plus pris en charge (sauf exception), 
  c'est-à-dire les premiers pentium et équivalents [voir détails](https://framagit.org/dflinux/DFiso/snippets/513).
* OpenSSH v7 désactive certains anciens algorithmes et le protocole SSH1 par défaut. En cas d'accès SSH uniquement
  se reporter à la [documentation OpenSSH](http://www.openssh.com/legacy.html).
* MySQL est supprimé en faveur de MariaDB entièrement libre. La transition est assurée selon 
  [ces modalités](https://www.debian.org/releases/testing/i386/release-notes/ch-whats-new.fr.html#mariadb-replaces-mysql).
* Virtualbox n'est plus dans les dépôts pour des questions de vulnérabilités chroniques, 
  voir le [site Oracle](https://www.virtualbox.org/) directement.
* le gestionnaire de mot de passe fpm2 n'est plus maintenu, prévoir d'utiliser pass, keepassX ou keepass2
* Firefox et Thunderbird ne seront probablement pas à jour sur leur dernière version esr, voir comment installer 
  les versions du site mozilla officiel sur les tutos dflinux ou kyopages (liens à venir).
* net-tools n'est plus livré, le projet est à l'abandon. Seul iproute est installé, avec comme conséquence visible
  que certaines commandes (comme ifconfig) ne sont plus présentes. au lieu de réinstaller un paquet déprécié, voir 
  un [tableau comparatif](https://github.com/kyodev/kyodeb/wiki/iproute2-vs-net_tools).
* les interfaces réseau ont un [nouveau nommage](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/), 
  apprêtez à connaître leur nouveau nom (`ip a`). cela ne devrait pas se produire lors d'une mise à jour de *Jessie*.
  en cas de réinstallation, cela pourrait causer la perte de serveurs distants (prévoir un accès physique ou de secours). 
  problème plus ou moins directement lié, certains adaptateurs wifi en usb peuvent ne plus fonctionner, voir 
  [comment revenir à l'ancien nommage](https://github.com/kyodev/kyodeb/wiki/interfaces-r%C3%A9seau,-retour-aux-sch%C3%A9ma-classique-de-nommage).
* nouveau dépôt miroir `deb.debian.org` en remplacement de **httpredir**, celui-ci pourvant disparaitre à terme.   
  ce nouveau miroir prend en charge **security** et permet des transferts en **https**.
* pour améliorer la sécurité, apt rejette des sommes de contrôles trop faibles, comme SHA1. certains dépots tiers 
  peuvent être impactés. se poser la question sur le niveau d'exigences de tel dépôts...
* gnupg version 1 classique est dépréciée, supportée dans le paquet **gnupg1**, mais c'est la version 2 livrée maintenant 
  dans le paquet **gnupg**.
* le suivi des mises à jour de sécurité sont limités pour certains paquets. Plus d'info sur [Strech: support de sécurité limité](securite_limitee)
* les paquets de symboles de débogage sont amenés à être déplacés dans un nouveau dépôt   
  `deb http://debug.mirrors.debian.org/debian-debug/ stretch-debug main`.
* le moteur d'épinglage (pinning) APT a changé, notamment sur l'épinglage de la version ou lieu du paquet.   
  les **règles de pinning sont à revoir**
* les règles des dépots ont changés, dans le but d'améliorer la stabilité et la sécurité. Certains dépôts tiers pourraient
  être impactés [voir le wiki Debian RepositoryFormat](https://wiki.debian.org/RepositoryFormat#A.22Release.22_files).
* le serveur Xorg n'a plus le setuid root par défaut. logind, libpam-systemd et un pilote vidéo du noyau doivent 
  être installé. s'il est lancé comme utilisateur normal, le journal Xorg est ici: `~/.local/share/xorg/`. installer le 
  paquet xserver-xorg-legacy pour rétablir le setuid précédent en cas de soucis
* Upstart est supprimé pour cause d'abandon
* les changement OpenSSL sont à surveiller, voir la [documentation OpenSSL](https://wiki.openssl.org/index.php/1.1_API_Changes).
* les changement Perl sont à surveiller, voir 
  [perl522delta](https://metacpan.org/pod/release/RJBS/perl-5.22.0/pod/perldelta.pod)
  [perl524delta](https://metacpan.org/pod/release/RJBS/perl-5.24.0/pod/perldelta.pod).
* incompatibilité PostgreSQL PL / Perl. mettre à jour PostgreSQL en 9.6.


## préparer le système
**toutes les commandes système qui suivent doivent être lancées en root**  
`su` ou `sudo -s`(à confirmer pour sudo)

**à faire avec les sources sur Jessie**

* repérer les paquets qui ne sont pas d'origine Debian
```shell
apt install apt-forktracer
apt-forktracer
```
décision peut-être prise de désinstaller certains non-officiels avant le mise à jour
* aptitude n'etant pas entièrement compatible avec apt, s'assurer qu'il n'y a aucune opération 
 en attente si vous utilisez cette interface.
* désactiver l'épinglage APT de certains paquets éventuels, vérifier
```text
/etc/apt/preferences.d
```
* auditer les paquets pour corriger tous les paquets Half-Installed, Failed-Config ou en erreur
```shell
dpkg --audit
```
* répèrer et corriger les paquets en hold ou pinnés ou autre
```shell
cat /etc/apt/preferences
dpkg --get-selections \* | grep -v install
```
* enlever tous les paquets on hold pour ne pas bloquer la mise à jour.   
débloquer un paquet:
```shell
echo paquet install | dpkg --set-selections
```
* si l'espace disque est limite, désinstaller au besoin avec `apt remove paquet1 paquet2 etc` certains gros paquets 
qui seront réinstaller après.
* vérifier que le paquet __linux-image-*__ est installé
```shell
dpkg -l "linux-image*" | grep ^ii | grep -i meta
```
* en cas d'absence, l'installer
    * détecter l'architecture (686, 686-pae, amd64) avec `uname -r` -> <ARCHI>
    * voir les images disponibles `apt search linux-image-<ARCHI>`
    * `apt install linux-image-<ARCHI>`
    * si une mise à jour du noyau est effectuée, rebooter

## préparer ses sources
* supprimer la section proposed-updates
* vérifier dans les sources les dépôts non-officiels
* vérifier que les dépôts non-officiels éventuels proposent des paquets pour *Stretch*
* les versions non-officielles rétroportées peuvent causer des conflits de version et faire échouer la mise à jour  
  consulter [problèmes possibles](https://www.debian.org/releases/testing/i386/release-notes/ch-upgrading.fr.html#trouble).
* désactiver temporairement les dépots backport en commentant les lignes
* désactiver temporairement les dépots exotiques se trouvant dans `/etc/apt/sources.list.d/`
* modifier les sources de *Jessie* en **Stretch** (stable est à éviter)
```text
 ## Stretch avec deb.debian.org
deb http://deb.debian.org/debian/ stretch main contrib non-free
deb http://deb.debian.org/debian-security stretch/updates main contrib non-free
deb http://deb.debian.org/debian/ stretch-updates main contrib non-free
 ######## main contrib non-free à adpater selon souhaité
 # backports: deb http://deb.debian.org/debian/ stretch-backports main contrib non-free
 # les dépots de sources sont éventuellement à modifier de la même manière
```
```text
 ## Stretch avec miroir exemple
deb http://ftp.fr.debian.org/debian/ stretch main contrib non-free
deb http://security.debian.org/ stretch/updates main contrib
deb http://ftp.fr.debian.org/debian/ stretch-updates main contrib non-free
 ######## main contrib non-free à adpater selon souhaité
 # backports: deb http://ftp.fr.debian.org/debian/ stretch-backports main contrib non-free
 # les dépots de sources sont éventuellement à modifier de la même manière
```
Il ne doit y avoir **aucune source** pointant vers *Jessie*


## vérifier l'espace disque

* connaitre la capacitée de la *racine* `df -h /`

* pour avoir une idée de la taille des paquets téléchargés et la nouvelle taille installée, ou détecter une erreur éventuelle:
```shell
apt update
apt-get -o APT::Get::Trivial-Only=true dist-upgrade
```

## mise à jour *sûre*

**APT est recommandé** (apt est la commande raccourcie de apt-get, apt-cache... selon le contexte, 
existe depuis longtemps. c'est une facilité, pas une nouvelle commande)

**aptitude n'est pas recommandé** et se montre moins fiable

```shell
apt update
apt upgrade
```

## public key EF0F382A1A7B6500

lors des mise à jour, il apparaît (parfois?) un avertissement qui ne doit 
[pas être bloquant](https://micronews.debian.org/2017/1497791872.html) pour la mise à jour
en cas où:
```shell
apt install debian-archive-keyring debian-keyring
```
et recommencer

## mise à jour *profonde* de la distribution
démarrage en **console en mode recovery**, ssh ou screen. pas de **session graphique**  
gnome like *update-manager* est fortement **déconseillé**
```shell
su
apt dist-upgrade --ignore-hold
```
*remarque* `--ignore-hold` option à vérifier sur cette commande, mais devrait permettre de mettre à jour
les paquets gelés (hold).

en cas de problèmes pendant la mise à niveau, se référer aux 
[release notes $4.5](https://www.debian.org/releases/testing/i386/release-notes/ch-upgrading.fr.html#trouble)

en cas d'échec, se préparer à utiliser un live (Usb ou Cd) comme **DFLinux**

## vérifications, corrections sources

une fois la mise à niveau effectuée:
* vérifier les sources
* les compléter si vraiment nécessaire
* autres précautions?
