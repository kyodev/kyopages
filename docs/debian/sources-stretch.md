# sources.list rapide stretch

ancien format
pour le nouveau format deb822 et notes plus complètes, voir [sources](https://kyodev.frama.io/kyopages/debian/apt/sources/)

## dépot CDN deb

* **remplace httpredir** (obsolètes depuis mi-2016)
* permet le protocole https

`cat /etc/apt/sources.list`   
```text
deb	http://deb.debian.org/debian/			stretch			main contrib non-free  
deb	http://deb.debian.org/debian/			stretch-updates	main contrib non-free  
deb	http://deb.debian.org/debian-security/	stretch/updates	main contrib non-free  

		# src inutiles sauf à développer des paquets et vouloir ralentir apt update 
 # deb-src http://deb.debian.org/debian/			stretch main contrib non-free
 # deb-src http://deb.debian.org/debian/			stretch-updates main contrib non-free
 # deb-src http://deb.debian.org/debian-security/	stretch/updates main contrib non-free

	# backports, à activer en connaissance de cause... 
 # deb		http://deb.debian.org/debian/	stretch-backports main contrib non-free
 # deb-src	http://deb.debian.org/debian/	stretch-backports main contrib non-free
```

* vérifier qu'il n'y ait pas d'autres dépôts en collision dans: _/etc/apt/sources.list.d/_   
`grep -Ersv '^#|^$' /etc/apt/sources.list*`

* pour des composants uniquement free, effacer _contrib non-free_


## protocole htpps

* remplacer http:// par **https://**
* installer le paquet apt-transport-https sur les versions apt < 1.5 (debian 9.3 à ce jour) et les dépôts deb.debian.org
```shell
su
apt install apt-transport-https
```
