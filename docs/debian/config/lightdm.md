# LightDM

curieusement le wiki debian comporte quelques erreurs ou imprécisions à ce jour (01/2018). 

* debian modifie la configuration par défaut de LightDM, donc les réglages par défaut ne correspondent pas à la doc officielle (ni au wiki qui parle de config LightDM au lieu d'indiquer config debian)

* doc officielle:
  <https://www.freedesktop.org/wiki/Software/LightDM/>

* greeter est la partie graphique, l'écran d'accueil, avec un look et une interface de connexion

## configuration

* fichiers:

```text
/usr/share/lightdm/lightdm.conf.d/*.conf
/etc/lightdm/lightdm.conf.d/*.conf
/etc/lightdm/lightdm.conf
```

les fichiers sont lus et analysés dans cet ordre, donc en final, le fichier `/etc/lightdm/lightdm.conf` est prépondérant en remplaçant les réglages éventuellement précédents

* config debian: 
  * `/usr/share/lightdm/lightdm.conf.d/01_debian.conf`
  * `/usr/share/lightdm/lightdm-gtk-greeter.conf.d/01_debian.conf`

* config personnalisée:
  * contrairement au wiki debian, utiliser un fichier `/etc/lightdm/lightdm.conf.d/90MaConfig.conf` pour régler une option
  * alternativement, décommenter les options de `/etc/lightdm/lightdm.conf`, ce sont les options normalement par défaut de LightDM officiel


## options

pour des explications, voir:

* /etc/lightdm/lightdm.conf
* /etc/lightdm/lightdm-gtk-greeter.conf
* <https://www.freedesktop.org/wiki/Software/LightDM/CommonConfiguration/>


## vérification

voir les configs hors personnalisées avec leur origine:
```shel
/usr/sbin/lightdm --show-config
```

## dernier utilisateur connecté

LightDM peut rendre accessible la liste des utilisateurs possibles, avec le dernier utilisateur séclectionné, avec l'option `greeter-hide-users=false`. décommenter l'option dans /etc/lightdm/lightdm.conf ou ajouter le fichier
```shell
su
mkdir -p /etc/lightdm/lightdm.conf.d/
echo '[Seat:*]' > /etc/lightdm/lightdm.conf.d/90sdeb.conf
echo 'greeter-hide-users=false' >> /etc/lightdm/lightdm.conf.d/90sdeb.conf
```

## liens

* site officiel: <https://github.com/CanonicalLtd/lightdm>
