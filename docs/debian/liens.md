# liens Debian utiles

## Debian testing
* [Release notes testing](https://www.debian.org/releases/testing/releasenotes)
* [Infos testing](https://www.debian.org/releases/testing/)
* [Faq Debian](https://www.debian.org/doc/manuals/debian-faq/index.fr.html)
