# Stretch, suivi de sécurité limité

pour certains paquets, Debian ne peut plus assurer de rétroportage de sécurité. cela sous-entend qu'il vaudrait de mieux 
se passer de ces paquets ou les utiliser pour des usages **sûrs**.

## détection des paquets concernés

pour tracer ces paquets, installer `apt install debian-security-support`. exemple:
```text
* Source : qtwebkit-opensource-src
  Détails : No security support upstream and backports not feasible, 
  only for use on trusted content
Paquet binaire affecté :
  - libqt5webkit5
```
* libjavascriptcoregtk-1.0-0                                    │  
* libwebkitgtk-1.0-00

connaitre le paquet reponsable d'une librairie:   
`apt rdepends libqt5webkit5`

simuler la déinstallation de cette librairie pour voir les paquets affectés:   
`apt remove -s libqt5webkit5`

 
re-détecter en cours d'exploitation: ??


## principaux paquets concerné:

* les navigateurs basés sur les moteurs de rendu **webkit, qtwebkit et khtml** sont affectés par un flot continu 
de vulnérabilités et leurs nombreuses interdépendances compliquent les mise à de sécurité. ils sont disponibles
sous stretch, mais ne seront pas suivi. Debian recommande **Firefox** et **Chromium**.
* plate-forme Node.js et tout son écosystème


