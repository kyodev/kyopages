# interfaces réseau nommage classique

# adaptateur Wifi USB ne fonctionne pas 

* le nom d'interface est wlx..., wlx indiquant un échec du nouveau nommage

solution

* correction de l'échec du nouveau nommage
ou
* retour à l'ancien nommage, juste sur l'usb ou toutes les interfaces


## introduction rapide

> Depuis la version 197 systemd/udev a introduit un nouveau schéma de nommage: _Predictable Network Interface 
Names_ sensé stabiliser le nommage des interfaces réseau. Il fait disparaître les classiques noms 
("eth0", "wlan0", ...). Il s’appuie sur le nommage du  noyau pour lui appliquer des règles, qui par défaut 
se trouvent dans `/lib/systemd/network/99-default.link`, avec notamment la règle 
`NamePolicy=kernel database onboard slot path`.
[Plus d'infos sur freedesktop (En)](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/)


Ce nommage ne fonctionne pas (ou pas toujours) pour les périphériques USB, au moins sur debian,
le nom d'interface se retrouve sous la forme `wlx`00c0ca3f3f23


Ceci est peut-être dû à à une lenteur d'initialisation de l'adaptateur USB (hypothèse personnelle), 
mais normalement, une règle udev devrait se rabattre sur l'adresse MAC MAC (ID_NET_NAME_MAC) pour 
construire le nom (voir /lib/udev/rules.d/73-usb-net-by-mac.rules)


* **Mise à jour Décembre 2017**

J'ai été personnellement confronté au souci en voulant utiliser ma vieille alfa network, sur debian 9.2.
Je n'ai pas envie de revenir aux vieux nommages pour **toutes** les interfaces. Donc en fouinant un peu, de 
ce que j'ai compris ou testé (j'ai pas trouvé les bonnes docs détaillées)

* /etc/udev/rules.d/ surcharge les règles par défaut de /lib/udev/rules.d/
* /etc/systemd/network/ surcharge  /lib/systemd/network/
* /lib/udev/rules.d/73-usb-net-by-mac.rules  indique tester: /etc/udev/rules.d/80-net-setup-link.rules
	et /etc/systemd/network/99-default.link"


**tous les commandes qui suivent doivent être faites en root** 

pour vérifier le nom d'interface
```shell
ip -o add 
```

## nouveau nommage sur usb dans l'esprit systemd: wlp...

### cible usb

* vérifier que rien n'existe déjà sur le cible /etc/udev/rules.d/80-net-setup-link.rules 
* clé en main:
```shell
unlink /etc/udev/rules.d/80-net-setup-link.rules 2>/dev/null
mv /etc/udev/rules.d/80-net-setup-link.rules  /etc/udev/rules.d/80-net-setup-link.bak  2>/dev/null

ln -s /lib/udev/rules.d/80-net-setup-link.rules  /etc/udev/rules.d/80-net-setup-link.rules 
```
(ls -l /etc/udev/rules.d/80-net-setup-link.rules -> /dev/null)


* **bien** au branchement, l'interface est activée, mais son nom est: **wlp0s29f7u3**  (*)
	je dois bien avouer que là, c'est pas très sexy
* c'est conforme au nouveau nommage
* dois je en conclure que c'est un bug debian sur l'implantation de systemd où les règles devraient êtres 
liées sur /etc?


  (*) j'espère que ce nom à la con, est seulement dû à l'échec initial du nommage et un fallback basé sur la MAC



## ancien nommage sur usb,  wlan **pour USB**

### cible usb

* 2e essai, on détourne la prise en charge de 80-net-setup-link.rules inexistant sur /etc/udev/rules.d/ 
ce qui revient à désactiver la règle.
* vérifier qu'il n'y a pas de fichier (ou lien présent sur la cible)

```shell
unlink /etc/udev/rules.d/80-net-setup-link.rules  2>/dev/null
mv /etc/udev/rules.d/80-net-setup-link.rules  /etc/udev/rules.d/80-net-setup-link.bak  2>/dev/null

ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules
```
(ls -l /etc/udev/rules.d/80-net-setup-link.rules -> /dev/null)

* **SUPER** résultat: **wlan0**, c'est plus _net_
* redémarrage de udev, les interfaces non-usb conservent leur noms :)
* remarque, pour obtenir une ip, relancer NetworkManager (ou monter/démonter si 
	/etc/network/interfaces)
* je n'ai pas le temps de rebooter pour tester, je mettrai ça à jour dès que possible

> cerise sur le gateau, l'adresse MAC **change automatiquent**(spoofing), pas besoin de configurer NetworkManager


## ancien nommage, via règles Udev

### cible usb et pci 

* pas testé, ou à moitié en aidant un utilisateur sur un fil pollué par les charlots de Debian-Facile, 
	cantonnés aux drivers.
  à l'époque, j'avais fait faire une manip bien trop compliquée et pas dans les clous.
* vérifier qu'il n'y a pas de fichier (ou lien présent sur la cible), et dévier 99-default.link
* solution clé en main:
```shell
unlink /etc/systemd/network/99-default.link 2>/dev/null
mv /etc/systemd/network/99-default.link /etc/systemd/network/99-default.bak  2>/dev/null

ln -s /dev/null /etc/systemd/network/99-default.link
```

relancer udev (normalement suffisant)
```shell
systemctl restart udev
```

pour les interfaces ethernet, qui sont dans devfs du noyau, il faut reconstruire le initramfs 
```shell
update-initramfs -u
reboot
```


## ancien nommage, via Grub

### cible usb et pci 

pas testé, mais indiqué par freedesktop et vu sur une ArchLinux


* avec son éditeur préféré, modifier `/etc/default/grub`
* **ajouter** les paramètres: `net.ifnames=0 biosdevname=0` en commentant la ligne existante et 
en la dupliquant, par **exemple**:
```text
	#GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX_DEFAULT="quiet net.ifnames=0 biosdevname=0"
```
puis

```shell
update-grub
reboot
```


## retour en arrière

### règles Udev

```shell
unlink  /etc/udev/rules.d/80-net-setup-link.rules 2>/dev/null
mv /etc/udev/rules.d/80-net-setup-link.bak  /etc/udev/rules.d/80-net-setup-link.rules   2>/dev/null
```

```shell
unlink /etc/systemd/network/99-default.link 2>/dev/null
mv /etc/systemd/network/99-default.bak  /etc/systemd/network/99-default.link   2>/dev/null
```

```shell
unlink /etc/systemd/network/99-default.link 2>/dev/null
mv /etc/systemd/network/99-default.bak  /etc/systemd/network/99-default.link   2>/dev/null
```

### vieux bidouillage

pour ceux qui auraient suivis mes premières élucubrations (fonctionnelles mais pas dans les clous (difficile 
de tester à distance;))
```shell
unlink /lib/systemd/network/99-default.link 2>/dev/null
mv /lib/systemd/network/99-default.link.bak /lib/systemd/network/99-default.link   2>/dev/null
update-initramfs -u
reboot
```

### grub


* avec son éditeur préféré, modifier `/etc/default/grub`
* décommenter la ligne originale
* supprimer la ligne contenant `net.ifnames=0 biosdevname=0`
* par exemple
```text
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
```
puis

```shell
update-grub
reboot
```

