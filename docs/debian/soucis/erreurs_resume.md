# erreurs fichier resume

**/etc/initramfs-tools/conf.d/resume**

## fichier resume absent

théoriquement un fichier resume peut être absent (comme sur ArchLinux par exemple), dans ce cas, selon man, 
la plus grande partition swap devrait être utilisée.

sur **debian**, une erreur se produit et n'est pas corrigée lors de l'update:   

`update-initramfs -u`   
```text
update-initramfs: Generating /boot/initrd.img-4.11.6-towo.2-siduction-amd64
I: The initramfs will attempt to resume from /dev/sda3
I: (UUID=691f4861-ac9e-4b44-adaf-db28378d1d8f)
I: Set the RESUME variable to override this.
```

## Gave up waiting for suspend/resume device...

quand l'UUID de la partition `swap` est changé (classique avec l'installeur Debian 
sur un multi-boot par exemple), il ne suffit pas de modifier le fstab.   
si le système permet l'hibernation, il faut aussi veiller à configurer correctement    
`/etc/initramfs-tools/conf.d/resume`   
à défaut

* le boot subi un timeout en essayant de trouver une partition fantôme 
  (Gave up waiting for suspend/resume device...).
* cela peut aussi se traduire par une erreur lors d'un _update-initramfs_

```text
update-initramfs: Generating /boot/initrd.img-4.9.0-3-686-pae
W: initramfs-tools configuration sets RESUME=UUID=68c4a15c-e3c8-4afe-8ffe-feb3637a0875
W: but no matching swap device is available.
I: The initramfs will attempt to resume from /dev/sda6
I: (UUID=3264f09c-87ed-4731-8132-3e929d120c65)
I: Set the RESUME variable to override this.
```

## correction resume

**root** requis

### avec UUID

méthode fastidieuse  
```shell
su
	# identifier le swap et noter l'UUID
lsblk -f
	# vérifier le fstab si besoin
cat /etc/fstab
	# configurer resume (editor|geany|mousepad|pluma|kate|gedit...)
editor /etc/initramfs-tools/conf.d/resume
	# mise à jour de l'image initramfs
update-initramfs -u
reboot
```

```ini
		## /etc/fstab
	# swap was on /dev/sda3 during installation
UUID=422c2bac-a6c3-4774-afd2-bb53c366a27d none    swap    sw     0     0

		## /etc/initramfs-tools/conf.d/resume
RESUME=UUID=422c2bac-a6c3-4774-afd2-bb53c366a27d
```

### avec device 

pour éviter les surprises quand on joue avec des multi-boots, **pour le(s) swap**, 
utilisation de _/dev/partition_
```ini
		## /etc/fstab
	# swap was on /dev/sda3 during installation
/dev/sda3 none            swap    sw              0       0

		## /etc/initramfs-tools/conf.d/resume
RESUME=/dev/sda3
```

### avec _auto_ 

mais depuis peu, **encore plus simple** (après test):  
```ini
RESUME=auto
```

### options variable RESUME

* auto - sélectionne le périphérique (device) resume automatiquement.   
  sélection de la plus grande partition swap disponible (ou la première?). peu clair entre 
  man initramfs-tools et man initramfs.conf.
* none - désactive resume à partir du disque
* non défini (pas de fichier _/etc/initramfs-tools/conf.d/resume_): idem auto
* la variable de boot `noresume` écrase ces options

### Grub

ajouter le paramètre resume=UUID=

par exemple:
```text
GRUB_CMDLINE_LINUX_DEFAULT="quiet resume=UUID=691f4861-ac9e-4b44-adaf-db28378d1d8f"

```
 

### check après reboot:
```shell
/sbin/swapon -s
```
