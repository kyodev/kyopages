# la part du root


**root** requis

> hum j'ai un problème avec une partition, j'ai viré environ 10go mais elle apparaît tout le temps 100% utilisée...
```shell
df -h /mnt/SAVE
```
```text
Sys. de fichiers Taille Utilisé Dispo Uti% Monté sur
/dev/sda5          130G    124G     0 100% /mnt/SAVE
```
> curieux, 124/130=5%... environ

```shell
su
tune2fs -l /dev/sda5 | grep Reserved
```
```text
Reserved block count:     1727987
Reserved GDT blocks:      1015
Reserved blocks uid:      0 (user root)
Reserved blocks gid:      0 (group root)
```
1727987 blocks réservés:
```shell
tune2fs -m 0 /dev/sda5
```
```text
tune2fs 1.42.12 (29-Aug-2014)
Définition du pourcentage de blocs réservés à 0% (0 blocs)
```
> voilà, 0 block:
```shell
df -h /mnt/SAVE
"/dev/sda5          130G    124G  5,8G  96% /mnt/SAVE
```
opération déconseillée sur le système pour laisser au _root_ (et les applications fonctionnant 
en tant que tel) d'avoir un minimum de place si la partition système est pleine.
