# récupération de fichiers

exemple simple, brouillon non testé


## installation

```shell
su
apt install foremost
```

## audit

```shell
cd ~
su
	# enregistrer une liste des fichiers récupérables
	# pas d'option -w pour afficher à l'écran
	# adapter le nom du périphérique
foremost -vw /dev/sdX
```

le rapport d'audit se trouve dans le dossier créé `output/audit.txt`
```shell
cat output/audit.txt | more
```

pour refaire un rapport d'audit effacer le répertoire auparavant:
```shell
rm -Rf output
```

## récupération

```shell
	# option all pour tout, pour des options spécifiques voir le man
	# option -t jpg,png pour chercher les jpeg et png
foremost -t all /dev/sdX
```

les fichiers sont placés dans le répertoire `output`. pour les exploiter en user:

```shell
chown -R <user> output
```
