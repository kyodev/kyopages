# LibreOffice sous Debian / Linux


## épuration LibreOffice sous GTK

l'intégration dans GTK est une peu... _lourdaude_

concerne les paquets: `libreoffice-gtk` normalement remplacé par `libreoffice-gtk2`   
probablement `libreoffice-gtk3`  

avec ces paquets, classiquement installés, l'interface de LibreOffice, se retrouve comme _figée_

tester la saveur originelle en désinstallant:
```shell
su
# chercher la verstion installée:
dpkg -l libreoffice-gtk* | grep ^ii
# désinstaller la version installé, libreoffice-gtk2 par exemple
apt purge libreoffice-gtk2
exit
```
le style `libreoffice-style-tango` est conservé, mais vous pouvez tester et savourer des barres d'outils 
beaucoup plus fines

**Remarque:** cet *allègement* sera d'autant appréciable que la résolution d'écran est faible.

si ce nouveau comportement ne vous satisfait pas, réinstaller comme à l'origine:
```shell
su
apt-update
# réinstaller la version installé, libreoffice-gtk2 par exemple
apt install libreoffice-gtk2
exit
```

### complément version officielle

si on installe les deb officiels de LibreOffice, on se retrouve aussi avec cette apparence. dans ce cas:
```shell
su
dpkgt -P libobasis5.3-gnome-integration
exit
```


## LO Light

### Objectif
 * conserver que les paquets composants utiles pour l'utilisation de base
 * résultat final 385 Mo installé au lieu de 755 Mo (environ) sur Debian Stretch
    * libreoffice-calc
    * libreoffice-writer
    * libreoffice-draw
    * libreoffice-impress
    * libreoffice-base
    * pdfimport
    * dictionnaire fr
    * hypénation-fr
    * thésaurus-fr
    * help & translation-fr
    * quelques fontes


### désinstallation existant

* la procédure pourrait peut-être être simplifiée, mais il apparait assez difficile de se débarasser de java :(

* dans tous les cas, **si des paquets sont proposés à la réinstallation, arrêter la procédure** et 
 me contacter sur irc freenode ##DFLinux 

* si des paquet apparaissent comme non mis à jour, ne pas en tenir compte, et **ne pas chercher à corriger**, ex: 
 `0 mis à jour, 0 nouvellement installés, 2 à enlever et 227 non mis à jour`

```shell
su
apt purge icedtea*
apt purge ant default-jre default-jre-headless
apt purge libreoffice*
apt-get purge java-common
apt autoremove --purge
exit
```
**vous devez terminer avec 0 paquet non mis à jour**

#### procédure raccourcie
pour les aventuriers qui voudraient tenter direct:
```shell
su
apt remove ant icedtea* default-jre default-jre-headless libreoffice* java-common 
```
mais sans garantie, en cas de succès, vous pouvez aussi me l'indiquer


### installation des paquets de base

```shell
su
apt-get update
apt-get install libreoffice-calc libreoffice-writer libreoffice-math libreoffice-draw \
	libreoffice-impress libreoffice-style-tango libreoffice-l10n-fr libreoffice-help-fr \
	fonts-crosextra-carlito fonts-crosextra-caladea fonts-dejavu fonts-liberation \
	hunspell hunspell-fr hunspell-fr-classical mythes-fr hyphen-fr \
	libreoffice-pdfimport unixodbc
```
installation libre-office base, sans java
```shell
apt install --no-install-recommends libreoffice-base
exit
```
paquets valables pour stretch, à voir pour jessie.


### fonctionnalités non-installées

 * pas de composant ~~base de données (libreoffice-base)~~
 * pas de java
 * pas de clipart (_désuets_)
 * ~~pas de pdfimport~~ pas vraiment fonctionnel... mais conforme à LO

### libreoffice-gtk(X)

n'a volontairement pas été proposé pour se rapprocher de LibreOffice original, mais il est toujours possible de
de le faire pour retouver un style pesant, mais conforme? à l'esprit du DE ;)  
`libreoffice-gtk2`(pour Xfce ou Mate) ou `libreoffice-gtk3`
```shell
su
# réinstaller la version installé, 2 par exemple
apt-get install libreoffice-gtk2
exit
```

### mise à jour: installation complète avec java, base de données et autres

installation du métapaquet:
```shell
su
apt-get install libreoffice
exit
```
