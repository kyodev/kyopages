
## extensions

 * [Adblock Plus](https://addons.mozilla.org/fr/firefox/addon/adblock-plus/)
    * [Element Hiding Helper pour Adblock Plus](https://addons.mozilla.org/fr/firefox/addon/elemhidehelper/?src=ss)

### web

[web developer](https://addons.mozilla.org/fr/firefox/addon/web-developer/)

### spécifiques

 * [Debian buttons](https://addons.mozilla.org/fr/firefox/addon/debian-buttons/)
