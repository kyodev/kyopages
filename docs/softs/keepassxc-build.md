# appImage KeePassXC

* nécessaire pour 32bits
* utile pour builder l'appImage

## installation des outils de compilation

```shell
	# C++ toolchain
apt install build-essential cmake g++
	# git
apt install git gitk

	# dépendances
apt install libxi-dev libxtst-dev qtbase5-dev \
	libqt5x11extras5-dev qttools5-dev qttools5-dev-tools \
	libgcrypt20-dev zlib1g-dev libyubikey-dev libykpers-1-dev
```

## chargement des sources

```shell
git clone https://github.com/keepassxreboot/keepassxc.git

cd keepassxc
```


## build AppImage

* build for amd64, ~~bug AppImage~~, [PR252 acceptée](https://github.com/AppImage/AppImages/pull/252) 
* 32bits, **attendre** que le fix passe en _tag_ ou release (>2.2.1)

```shell
./release-tool build --version $(git tag | tail -n1)
```

* build on i386, ~~bug keepassxc~~ [PR737acceptée](https://github.com/keepassxreboot/keepassxc/pull/737) 

dans release, vous avez l'appImage. s'il reste d'autres répertoires, vous pouvez les supprimer
(bin-release, build-release)

### décompresser l'appImage

```shell
./KeePassXC-*.AppImage --appimage-extract
```
### montage temporaire
```shell
./KeePassXC-*.AppImage --appimage-mount
```

### options release-tool

à utiliser à bon escient...

```text
Options:
  -v, --version           Release version number or name (required)
  -a, --app-name          Application name (default: '${APP_NAME}')
  -s, --source-dir        Source directory (default: '${SRC_DIR}')
  -o, --output-dir        Output directory where to build the release
                          (default: '${OUTPUT_DIR}')
  -t, --tag-name          Release tag to check out (defaults to version number)
  -b, --build             Build sources after exporting release
  -d, --docker-image      Use the specified Docker image to compile the application.
                          The image must have all required build dependencies installed.
                          This option has no effect if --build is not set.
      --container-name    Docker container name (default: '${DOCKER_CONTAINER_NAME}')
                          The container must not exist already
  -c, --cmake-options     Additional CMake options for compiling the sources
      --compiler          Compiler to use (default: '${COMPILER}')
  -m, --make-options      Make options for compiling sources (default: '${MAKE_OPTIONS}')
  -i, --install-prefix    Install prefix (default: '${INSTALL_PREFIX}')
  -p, --plugins           Space-separated list of plugins to build
                          (default: ${BUILD_PLUGINS})
  -n, --no-source-tarball Don't build source tarball
  -h, --help              Show this help
```



## installation système

voir [keepassxc installation-systeme](softs/keepassxc/#installation-systeme)


# compilation & install

destiné au développement, une AppImage est préférable à l'utilisation

```shell
mkdir build
cd build
#cmake -DWITH_XC_AUTOTYPE=ON -DWITH_XC_HTTP=ON -DWITH_XC_YUBIKEY=ON -DCMAKE_BUILD_TYPE=Release ..
cmake -DWITH_XC_AUTOTYPE=ON -DWITH_XC_HTTP=OFF -DWITH_XC_YUBIKEY=ON -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc)

# failed: checkinstall --install=no     # voir https://wiki.debian.org/fr/CheckInstall

make DESTDIR=~/.local install
```


## install

intégration lanceur au système
```shell
cp /home/kyodev/.local/usr/local/share/applications/keepassxc.desktop /usr/share/applications/keepassxc.desktop
```


### lancer manuellement
```shell
~/.local/usr/local/bin/keepassxc
```

### construction lanceur desktop, plus utile
```shell
su
echo "[Desktop Entry]" >> /usr/share/applications/keepassxc.desktop
echo "Name=KeePassXC" >> /usr/share/applications/keepassxc.desktop
echo "Comment=Password Manager" >> /usr/share/applications/keepassxc.desktop
echo "Exec=/home/kyodev/.local/usr/local/bin/keepassxc"  >> /usr/share/applications/keepassxc.desktop
echo "Path=/home/kyodev/.local/usr/local/bin/"  >> /usr/share/applications/keepassxc.desktop
echo "Icon=keepassxc" >> /usr/share/applications/keepassxc.desktop
echo "Terminal=false" >> /usr/share/applications/keepassxc.desktop
echo "Type=Application" >> /usr/share/applications/keepassxc.desktop
echo "Categories=Utility;" >> /usr/share/applications/keepassxc.desktop
echo "MimeType=application/x-keepass2;" >> /usr/share/applications/keepassxc.desktop
exit
```
~~echo "Icon=/home/kyodev/.local/usr/local/share/keepassxc/icons/application/48x48/apps/keepassxc.png" >> /usr/share/applications/keepassxc.desktop~~
