# installation KeepassXC

* 64 bits:   
récupérer les liens Official AppImage sur [keepassxc.org](https://keepassxc.org/download)
* 32 bits: voir compil
* depuis la version 2.2.1, fin 2017, keepassxc est packagé dans les dépôts debian, sid & testing


## téléchargement

```shell
	# l'application
wget https://github.com/keepassxreboot/keepassxc/releases/download/2.2.0/KeePassXC-2.2.0-x86_64.AppImage
	# la somme de contrôle au format sha comme son nom ne l'indique pas... :(
wget https://github.com/keepassxreboot/keepassxc/releases/download/2.2.0/KeePassXC-2.2.0-x86_64.AppImage.digest
	# la signature
wget https://github.com/keepassxreboot/keepassxc/releases/download/2.2.0/KeePassXC-2.2.0-x86_64.AppImage.sig
```

## contrôle de l'intégrité du chargement avec la somme de contrôle

```shell
shasum -c KeePassXC-2.2.0-x86_64.AppImage.DIGEST 
```
```text
KeePassXC-2.2.0-x86_64.AppImage: OK
```

## contrôle de la signature (la bonne identité du chargement)

#### 1- premier essai, quand la clé n'est pas connue

on vérifie, si la clé a déjà été chargée, on passe directement à 3-
```shell
gpg --verify KeePassXC-2.2.0-x86_64.AppImage.sig 
```
```text
gpg: les données signées sont supposées être dans « KeePassXC-2.2.0-x86_64.AppImage »
gpg: Signature faite le lun. 26 juin 2017 16:58:28 CEST
gpg:                avec la clef RSA C1E4CBA3AD78D3AFD894F9E0B7A66F03B59076A8
gpg: Impossible de vérifier la signature : Pas de clef publique
```
**`Impossible de vérifier la signature : Pas de clef publique`**: normal, on a jamais utilisé cette clé donc:

#### 2- recherche clé: quand la clé n'est pas connue

la commande précédente nous a donné la clé `C1E4CBA3AD78D3AFD894F9E0B7A66F03B59076A8`, on va la chercher dans les serveurs pour vérification
```shell
gpg --keyserver hkp://keys.gnupg.net --recv-keys C1E4CBA3AD78D3AFD894F9E0B7A66F03B59076A8
```
```text
gpg: /home/kyodev/.gnupg/trustdb.gpg : base de confiance créée
gpg: clef CFB4C2166397D0D2 : clef publique « KeePassXC Release <release@keepassxc.org> » importée
gpg: aucune clef de confiance ultime n'a été trouvée
gpg:       Quantité totale traitée : 1
gpg:                     importées : 1
```

#### 3- on peut (re)vérifier

```shell
gpg --verify KeePassXC-2.2.0-x86_64.AppImage.sig 
```
```text
gpg: les données signées sont supposées être dans « KeePassXC-2.2.0-x86_64.AppImage »
gpg: Signature faite le lun. 26 juin 2017 16:58:28 CEST
gpg:                avec la clef RSA C1E4CBA3AD78D3AFD894F9E0B7A66F03B59076A8
gpg: Bonne signature de « KeePassXC Release <release@keepassxc.org> » [inconnu]
gpg: Attention : cette clef n'est pas certifiée avec une signature de confiance.
gpg:             Rien n'indique que la signature appartient à son propriétaire.
Empreinte de clef principale : BF5A 669F 2272 CF43 24C1  FDA8 CFB4 C216 6397 D0D2
   Empreinte de la sous-clef : C1E4 CBA3 AD78 D3AF D894  F9E0 B7A6 6F03 B590 76A8
```

 * **`Bonne signature de « KeePassXC Release <release@keepassxc.org> » `** : c'est bon, la clé est bien celle qui a été publiée  
 * **`Attention : cette clef n'est pas certifiée avec une signature de confiance.`** : par contre, elle n'a été pas été certifiées par des signatures de confiance, ce qui n'est pas forcément un drame, c'est selon le contexte.

### 4- lister les signataires de la clé pour info

```shell
gpg --list-sigs C1E4CBA3AD78D3AFD894F9E0B7A66F03B59076A8
```
```text
pub   rsa4096 2017-01-03 [SC]
      BF5A669F2272CF4324C1FDA8CFB4C2166397D0D2
uid           [ unknown] KeePassXC Release <release@keepassxc.org>
sig          5C1B8E82B8030FCA 2017-05-15  [User ID not found]
sig 3        CFB4C2166397D0D2 2017-01-03  KeePassXC Release <release@keepassxc.org>
sub   rsa2048 2017-01-03 [S] [expires: 2019-01-03]
sig          CFB4C2166397D0D2 2017-01-03  KeePassXC Release <release@keepassxc.org>
sub   rsa2048 2017-01-03 [S] [expires: 2019-01-03]
sig          CFB4C2166397D0D2 2017-01-03  KeePassXC Release <release@keepassxc.org>
sub   rsa2048 2017-01-03 [S] [expires: 2019-01-03]
sig          CFB4C2166397D0D2 2017-01-03  KeePassXC Release <release@keepassxc.org>
```

## rendre exécutable l'image

```shell
chmod +x KeePassXC-2.2.0-x86_64.AppImage
```

l'application dans l'image est dès maintenant utilisable avec:
```shell
./KeePassXC-2.2.0-x86_64.AppImage
```
mais....

## installation système

```shell
	# on passe en root
su
	# on crée le répertoire de destination
mkdir -p /opt/KeePassXC

	# on déplace l'image exécutable dans ce répertoire
mv KeePassXC-2.2.0-x86_64.AppImage /opt/KeePassXC/

	# on sort du root
exit

	# on lance pour la dernière fois manuellement
/opt/KeePassXC/KeePassXC-2.2.0-x86_64.AppImage
	# on efface les fichiers accessoires
rm KeePassXC-2.2.0-x86_64.AppImage.*
```

Le programme se lance, et demande s'il doit s'intégrer au système. 
```text
Would you like to integrate /opt/KeePassXC-2.2.0-x86_64.AppImage with your system? 

This will add it to your applications menu and install icons.  
If you don't do this you can still launch the application by double-clicking on the AppImage.  
```
répondre Oui, Quitter.

en cas de réponse par non, pour revenir en arrière, effacer   
`~/.local/share/appimagekit/KeePassXC_no_desktopintegration` (à vérifier)

voilà, le lanceur de KeepassXC est dans le menu, **catégorie : Accessoires**


