# liens en vrac

## GNU Privacy Guard (gpg)
* [Offciel](https://gnupg.org/documentation/guides.html)
* [Doc sympa rancoz](https://www.francoz.net/doc/gpg/gpg.html)
* [Doc sympa riseup](https://riseup.net/fr/security/message-security/openpgp/best-practices)
* [Ubuntu](https://doc.ubuntu-fr.org/gnupg)
