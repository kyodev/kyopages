# Enregistrer une session Terminal pour afficher dans une page html

## installation

```shell
su
apt install python-pip
pip install TermRecord 
exit
```


## enregistrement

```shell
	# le fichier où sauvegarder
TermRecord -o /home/user/exemple.html
	...
#saisie de commandes
	...
	# fin

exit
```

## exploitation

maintenant il est possible d'ouvrir exemple.html dans un navigateur avec js, et partager 
cette **seule** et simple page
  
Plus d'infos: [TermRecord script](https://github.com/theonewolf/TermRecord)

## aussi

voir, similaire?: <https://asciinema.org/>
