# mkdocs

## liens

 * [gitlab.com/pages/mkdocs](https://gitlab.com/pages/mkdocs)
 * [Les Gitlab Pages débarquent dans Framagit !](https://framablog.org/2017/03/20/les-gitlab-pages-debarquent-dans-framagit/)
 * [www.mkdocs.org](http://www.mkdocs.org/)
 * [Change log à partir 0.16](http://www.mkdocs.org/about/release-notes/#version-016-2016-11-04)


## install en local pour dev

** ne pas utiliser apt, paquet périmé et localisation incompatible avec les thèmes additionnels** (2017)
```shell
su
apt install python-pip
pip install mkdocs
mkdocs --version
	# mkdocs, version 0.17.3
exit
```

## pour info


```shell
pip uninstall paquet
pip install -U paquet 
	# -U = --upgrade
pip download paquet
```

## localisation du programme

```text
	/usr/local/lib/python2.7/dist-packages/mkdocs
	/usr/local/lib/python2.7/dist-packages/mkdocs/assets/search/mkdocs
	/usr/local/lib/python2.7/dist-packages/mkdocs/themes/mkdocs
	/usr/local/bin/mkdocs
	/usr/share/mkdocs
```

récup thème du paquet:
```shell
cp -r /usr/local/lib/python2.7/dist-packages/mkdocs/themes/mkdocs/ ../
```

## installation d'un thème

```shell
pip install mkdocs-material
	#localisation: /usr/local/lib/python2.7/dist-packages/material
```


## création d'un nouveau site

```shell
mkdocs new kyopages
cd kyopages
```

 * toutes la configuation et la déclaration des pages dans:
```text
mkdocs.yml
```


## organisation pages

```text
mkdocs.yml
docs/
	autreDirectory/
		autre.md
	index.md
	...
sites/
	# pages html générées
```


## développement, build auto

 * démarrer un serveur local, build et rafraîchissement automatique du navigateur
```shell
mkdocs serve
```
on peut utiliser 
```shell
mkdocs serve  --dirtyreload
```
mais le build sera sur la page uniquement, sans rechargement profond du navigateur (css notamment?). réserver au dev donc.

 * lancer **firefox** sur http://localhost:8000


## build manuel

dans ce cas, un répertoire de pages html (site/) est créé à côté du répertoire des pages markdown (docs/ par défaut).
ce répertoire _site_ est à poussé une site de pages statiques, à la différence de _docs_ où les pages doivent être 
générées en ligne.   
dans le même ordre d'idée, on peut utiliser:
```shell
mkdocs build --dirty
```
incidences à voir


## thème mkdocs
 * [allègement hightlight.js](https://highlightjs.org/download/) avec seulement les langages:   
  `bash css diff html/xml http ini json javascript makefile markdown python shell(-session)`   
  `text` désactive la coloration syntaxique
 * [manuel](https://highlightjs.org/usage/)
