# traductions

## Weblate

[hosted.weblate.org](https://hosted.weblate.org)

projet | statut
 :--: | :--:
[FreedomBox](https://wiki.debian.org/fr/FreedomBox) | <a href="https://hosted.weblate.org/languages/fr/freedombox/"><img src="https://hosted.weblate.org/widgets/freedombox/fr/svg-badge.svg" alt="Freedombox" /></a>
[peek](https://github.com/peek/peek) | <a href="https://hosted.weblate.org/projects/peek/translations/fr/"><img src="https://hosted.weblate.org/widgets/peek/fr/svg-badge.svg" alt="Peek" /></a>
[weblate](https://hosted.weblate.org) | <a href="https://hosted.weblate.org/languages/fr/weblate/"><img src="https://hosted.weblate.org/widgets/weblate/fr/svg-badge.svg" alt="Weblate" /></a>
[gammu](https://fr.wammu.eu/gammu/) | <a href="https://hosted.weblate.org/languages/fr/gammu/"><img src="https://hosted.weblate.org/widgets/gammu/fr/svg-badge.svg" alt="Gammu" /></a>


## Transifex

[transifex](https://www.transifex.com/)

projet | plateforme
 :--: | :--:
[KeePassXC](https://keepassxc.org/) | [KeePassXC](https://www.transifex.com/keepassxc/keepassxc/language/fr/)
[SolydXK](https://solydxk.com/) | [SolydXK](https://www.transifex.com/solydxk/dashboard/all_projects/fr/)
