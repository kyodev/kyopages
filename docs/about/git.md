# Command line instructions


# création dépot distant en cli

nouveau projet `ipupdate`
```shell
cd ipupdate
git init
git add <liste files>
git commit -m "Initial commit"
git remote add origin git@framagit.org:sdeb/ipupdate.git
git push --set-upstream origin master

 # remote answser

git remote add origin git@framagit.org:sdeb/ipupdate.git
```
attention, le dépôt créé est privé sur gitlab

## Git global setup

```shell
git config --global user.name "kyodev"
git config --global user.email "kyodev@monMail.fr"
git config --global core.editor nano
git config --global color.ui true
git config --global format.pretty oneline
```

## Existing folder

```shell
cd kyopages
git init
git add .
git commit -m "Initial commit"
git remote add origin git@framagit.org:kyodev/kyopages.git
git push --set-upstream origin master
```

## Existing Git repository

```shell
cd kyopages
git remote add origin git@framagit.org:kyodev/kyopages.git
git push --set-upstream origin master
```

## Create a new repository

```shell
git clone git@framagit.org:kyodev/kyopages.git
cd kyopages
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin master
```
