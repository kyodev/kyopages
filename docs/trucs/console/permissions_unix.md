# permission unix

## chmod symbolique

 cible 	| operation 	| droit 
 --- 	| :---: 		| ---
u user 	| + ajout 		| r read
g group | - suppression | w write
o other | = inchangé 	| x exécution ou  listage
a all 	| 				| X listage uniquement si répertoire ou une catégorie user a déjà un x
		|				| s suid ou sgid exécutable (x)	 chmod 777 & chmod ugo+s : rws rws rws
		|				| S suid ou sgid sur un droit non exécutable (pas de x caché par S)	 chmod 7OO & chmod ugo+s : rws rwS rwS
		|				| t sticky bit	 chmod 777 & ugo+t ou a+t
		|				| T sticky bit sur un droit non exécutable (pas de x caché par T)	 chmod 770 & ugo+t ou a+t 

exemple: `chmod ug+rwx,o-rwx,a-t xyz/` 

## chmod octal


 --x	| -w-	| -wx	| r--	| r-x	| rw-	| rwx	| ---	| symbolique
 :---:	| :---:	| :---:	| :---:	| :---:	| :---:	| :---:	| :---:	| ---
		| 		| -ws	| 		| r-s	| rw-	| rws	| 		| 
 001	| 010	| 011	| 100	| 101	| 110	| 111	| 000	| binaire
  1		|  2	|  3	|  4	|  5	|  6	|  7	|  0	| octal


* 0xxx	standard, rien
* 1xxx	sticky bit		un fichier reste en mémoire. Dans un répertoire, suppression restreinte, seul le propriétaire peut effacer
* 2xxx	sgid			un fichier est exécuté avec les droits du groupe
* 3xxx	sticky bit & sgid
* 4xxx	suid			un fichier est exécuté avec les droits du propriétaire
* 6xxx	suid & sgid
* 7xxx	sticky bit & suid & sgid

suid est null généralement sur un répertoire sur linux, unix, pas freebsd   
sgid sur un répertoire: à la **création**, un fichier ou sous-répertoire appartient au groupe du répertoire parent.  
sgid s'hérite à la création, pas sur les répertoires ou fichiers existants.  

exemple: `chmod 0770 xyz`
 

## attribution des droits

 type	| proprio (user)	| groupe (group)	| autres (other)	| extension
 :---:	|	:---:			|	:---:			|	:---: 			| :---:
		| rwx				|	r-x				| ---				| 
		|  7				|	 5				|  0				| 

type	| extension
 --- 	| ---
* - : fichier standard					| + : acl surchargent les permissions de base
* d : répertoire (directory)			| . : SELinux est présent (ls -Z)
* l : lien symbolique (link)			| @ : attributs de fichier étendus présent
* c : périphérique de type caractère	| 
* b : périphérique de type bloc			| 
* p : pipe (FIFO)						| 
* s : socket							| 




## recette de cuisine

lecture=	4   
écriture=	2  
exécution=	1  
		_______   
	somme	7  


## droits

* -	aucun  
* r	read, lecture  
* w	write, écriture  
* x	exécution (fichier), listage et accès (répertoire)  
* 		exécutable (fichier) avec s ou S suid (user) ou sgid (group), set user id, ou set group id  


## voir les droits:

symbolique: `ls -l xyz`   
symbolique: `stat -c %A xyz`  
  
octal: `stat -c %a xyz`   

les 2: `stat -c '%A %a' xyz`   


## umask

nombre octal soustrait au nombre octal **max** des droits par défaut (666 pour un fichier, 777 pour un répertoire) lors de la **création** d'un fichier ou d'un répertoire

afficher umask: `umask -p`


exemple: 

* umask 022, 
  * touch file: rw- r-- r-- file	(644)
  * mkdir dir:  rwx r-x r-x dir 	(755)


* umask 007 (à utiliser de préférence si UPG, User Private Groups activé)
  * touch file: rw- rw- --- file	(660)
  * mkdir dir:  rwx rwx --- dir 	(770)


---

sgid, adapté pour les répertoires de groupes de travail, changer les répertoires:   
`find /path/to/directory -type d -exec chmod g+s '{}' \;`
