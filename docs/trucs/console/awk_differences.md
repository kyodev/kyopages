# mawk gawk

* par défaut mawk est installé sur debian, pas Gnu awk (gawk)
* awk s'appuie sur update-alternatives pour lancer les paquets awk installés
* mawk serait plus rapide dans _certains_ cas
* original-awk est la version décrite dans « The AWK Programming Language » (Al Aho, Brian Kernighan et Peter Weinberger)


## différences 

* KO indique une commande ou une variable non implémentée 

```text
test :         mawk gawk original-awk 
FILENAME      ok   ok   ok
NF            ok   ok   ok
FS            ok   ok   ok
NR            ok   ok   ok
FNR           ok   ok   ok
RS            ok   ok   ok
OFS           ok   ok   ok
ORS           ok   ok   ok
ARGC          ok   ok   ok
->ARGIND      KO   ok   KO
ARGV          ok   ok   ok
OFMT          ok   ok   ok
ENVIRON       ok   ok   ok
->IGNORECASE  KO   ok   KO
ERRNO         ok   ok   ok
match         ok   ok   ok
RSTART        ok   ok   ok
RLENGTH       ok   ok   ok
sub           ok   ok   ok
gsub          ok   ok   ok
toupper       ok   ok   ok
tolower       ok   ok   ok
->systime     KO   ok   KO
->strftime    KO   ok   KO
```
