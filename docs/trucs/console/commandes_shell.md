# commandes shell

## coreutils (gnu)

<https://www.gnu.org/software/coreutils/coreutils.html>

 cmd | objet | origine
 :-- | :-- | :--
	b2sum		| affiche BLAKE2 digests
basename | Renvoie le nom du fichier passé en paramètre | coreutils gnu
	base32		|  base32: encodage, décodage, affichage
	base64		|  base64: encodage, décodage, affichage
cal | affiche un calendrier | coreutils gnu
	cat			| affiche et concatène le contenu d'un ou plusieurs fichier (inverse tac)
chmod | Change les permissions d'accès | coreutils gnu
chown | Change le propriétaire et le groupe au quel appartient un fichier | coreutils gnu
chroot | Lance une commande avec un répertoire racine différent | coreutils gnu
	cksum		| affiche la somme de contrôle CRC
	comm		| compare deux fichiers triés ligne par ligne
	cp			| copie de fichiers
	csplit		| découpe un fichier en parties (selon un contexte déterminé) (split)
	cut			| supprimer ou affiche une partie de chaque ligne d'un fichier
date | affiche ou modifie la date | coreutils gnu
	dd			| (data dump) convertit et copie un fichier
df | affiche l'espace disque disponible dans chaque partition | coreutils gnu
	dir			| affiche le contenu d'un répertoire (raccourci ls) (vidr, ls)
	dircolors	| définit les couleurs pour $LS_COLORS
dirname | Convertit un chemin complet en juste un chemin | coreutils gnu
du | Estime de l'espace occupé par des fichiers | coreutils gnu
echo | affiche un message à l'écran | coreutils gnu
env | affiche, défini ou efface les variables d'environnement | coreutils gnu
	expand		| convertit les tabulations en espaces (inverse unexpand)
expr | Évalue un expression (utile pour les traitements conditionnels) | coreutils gnu
factor | affiche les facteur premiers | coreutils gnu
false | Retourne un état de sortie en échec | coreutils gnu
	fmt			| formate un paragraphe
	fold		| formate un texte pour une largeur spécifiée
groups | af	fiche le nom des groupes de l'utilisateur | coreutils gnu
	head		| affiche le début d'un ou plusieurs fichiers (inverse tail)
hostid | affiche l'ID de l'hôte courant | coreutils gnu
hostname | affiche ou défini le nom du système | coreutils gnu
id | affiche identifiant de l'utilisateur et de ses groupes | coreutils gnu
	install		| copie des fichiers et définit les attributs
	join		| fusionne les lignes de deux fichiers sur des champs communs
	link		| crée un lien dur	
	ln			| crée un lien dur ou symbolique
logname | affiche le nom de connexion courant | coreutils gnu
	ls			| liste le contenu de répertoire (dir, vdir)
	md5sum		| affiche MD5 digests
mkdir | Crée de nouveaux dossiers | coreutils gnu
mkfifo | Crée un FIFOs (tubes nommés) | coreutils gnu
mknod | Crée un fichier spécial de type bloque ou caractère | coreutils gnu
	mv			| déplace ou renomme des fichiers
nohup | Exécute une commande à l'abri des signaux hangups | coreutils gnu
nice | régle la priorité d'une commande ou d'un processus | coreutils gnu
	nl			| affiche un fichier en numérotant les lignes
	od			| affiche le contenu d'un fichier en octal ou autre format
	paste		| regroupe les lignes de différents fichiers
pathchk | Vérifie la validité et la portabilité d’un nom de fichier | coreutils gnu
pinky | Équivalent plus léger de finger | coreutils gnu
	pr			| met en forme des fichiers pour l’impression	
printenv | affiche les variables d'environnement | coreutils gnu
printf | Formate et affiche des données | coreutils gnu
	ptx			| génére un index croisé du contenu de fichiers
pwd | affiche le répertoire de travail (Print Working Directory) | coreutils gnu
	rm			| supprime un fichier ou répertoire
rmdir | Supprime des dossiers | coreutils gnu
seq | affiche une séquence numérique | coreutils gnu
	sha1sum		| affiche SHA-1 digests
	sha224sum sha256sum sha384sum sha512sum | SHA2 digests
	shred		| suppression plus sécuritaire 
	shuf		| mix des fichiers textes (shuffle)
sleep | Attend une délai spécifié | coreutils gnu
	sort		| trie des fichiers texte
	split		| découpe un fichier en plusieurs fichiers de taille fixée (csplit)
stty | Modifie et afficher la configuration de la ligne de terminal | coreutils gnu
su | Change l'identité de l'utilisateur | coreutils gnu
	sum			| affiche la somme de contrôle d'un fichier
sync | Synchronise les données entre le disque dur et la mémoire | coreutils gnu
	tac			| affiche et concatène  le contenu d'un ou plusieurs fichier dans l'ordre inverse (inverse cat)
	tail		| affiche la dernière partie d'un ou plusieurs fichiers (inverse head)
tee | Redirige la sortie vers plusieurs fichiers | coreutils gnu
test | Évalue une expression conditionnel | coreutils gnu
touch | Change la date d'un fichier (timestamps) ou crée un fichier vide | coreutils gnu
	tr			| convertit ou élimine des caractères
true | Ne rien faire, et réussir | coreutils gnu
	tsort		| Effectue un tri topologique
tty | affiche le nom de fichier du terminal associé à l’entrée standard | coreutils gnu
uname | affiche les informations systèmes | coreutils gnu
	unexpand	| convertit les espaces en tabulations (inverse expand)
	uniq		| élimine les lignes répétées
unlink | Supprime un fichier ou un lien | coreutils gnu
users | Liste les utilisateurs connectés au système | coreutils gnu
	vdir		| affiche le contenu de répertoires (dir, ls)
	wc			| affiche stats (lignes, mots, octets) d’un fichier
who | Montre qui est connecté | coreutils gnu
whoami | affiche l’identifiant d’utilisateur (`id -un') | coreutils gnu
yes | affiche indéfiniment une chaîne de caractères jusqu’à ce que le processus soit tué. | coreutils gnu


 cmd | objet | origine
 :-- | :-- | :--
adduser | Crée un nouveau compte utilisateur | debian
addgroup | Crée un nouveau groupe | debian
alias | Crée un alias | bash
awk | Cherche et remplace du texte dans des fichiers | voir gawk (mawk: défaut debian, pas entièrement compatible gawk)
bzip2/bunzip2 | Compresse et décompresse un ou des fichiers | ext (bzip2)
cd | Change de répertoire | bash
chgrp | Change le groupe qui possède un fichier donné |  coreutils gnu
clear | Nettoie l'écran du terminal | ext (debian: ncurses-bin)
cmp | Compare deux fichiers | diffutils gnu
dc | Calculatrice de bureau | gnu
diff | affiche la différence entre deux fichiers | diffutils gnu
diff3 | affiche les différences entre trois fichiers | diffutils gnu
dmesg | affiche la sortie du noyau | util-linux kernel.org
exit | Sortir du shell en cours | bash
export | Défini une variable d'environnement | bash
fdisk | Manipulateur de table de partition pour Linux | util-linux kernel.org
file | Indique le type de chaque fichier sur la ligne de commande | ext (file)
find | Cherche des fichiers qui correspondent à des critères choisis | findutils gnu
free | affiche la mémoire vive libre | procps
fsck | Vérifie et répare la cohérence d'un système de fichier | util-linux kernel.org
gawk | Langages d'analyse et de traitement par motif | gawk fsf
grep | affiche les lignes qui correspondent au motif de recherche | grep fsf
gzip/gunzip | Compresse ou décompresse des fichiers | fsf
history | Historiques des commandes | bash
import | Effectue une capture d'écran du serveur X | ImageMagick ou libc6
kill | Envoyer un signal à un processus | bash ou procps
killall | Envoyer un signal à un processus grâce à son nom | psmisc
less | affiche le contenu d'une fichier à l'écran et permet de le parcourir | less gnu
let | Effectuer des opérations arithmétiques sur les variables du shell | bash
logout | Quitte le shell de connexion | bash
m4 | Processeur de macro | m4 gnu
man | Manuel d'aide | debian?
more | affiche le contenu d'une fichier à l'écran et permet de le parcourir | util-linux kernel.org
mount | Monter un système de fichier | util-linux kernel.org
passwd | Modifier le mot de passe d’un utilisateur | shadow-utils
ps | Liste les processus lancés (Process Status) | procps
rsync | Copie de fichier à distance en utilisant son propre protocole | rsync
scp | Copie des fichiers entre deux machines au travers d'une connexion ssh | openssh-client
sdiff | Fusionne deux fichiers de manière interactive | diffutils gnu
sed | Éditeur de flux | sed gnu
sftp | Transfert de fichier sécurisé (FTP à travers SSH) | openssh-client
set | Manipulation de variables et fonction shell | bash
ssh | Shell à distance sécurisé | openssh-client
tar | Créateur d'archive | tar gnu
time | Mesure les ressources utilisées par un programme | shell
times | Statistique des temps utilisateur et système | bash
top | Liste les processus lancé sur le système | procps
traceroute | Trace la route vers un serveur | ext (traceroute)
type | Décris une commande | shell
umask | Fixer le masque de création de fichiers | bash
umount | Démonter un périphérique | util-linux kernel.org
unalias | Supprime un alias | bash
usermod | Modifie un compte utilisateur | shadow-utils
watch | Exécuter un programme périodiquement en affichant le résultat à l’écran | procps
whereis | Rechercher les fichiers exécutables, les sources et les pages de manuel d’une commande | util-linux kernel.org
which | Localise une commande | ext (debian: script debian)
xargs | Construire et exécuter des lignes de commandes à partir de l'entrée standard | findutils gnu


une introduction utile aux commandes linux: [abs.traduc.org](https://abs.traduc.org/abs-5.1-fr/pt04.html)

## shell

job_spec [&]                                                                             
(( expression ))                                                                         
. nom_fichier [arguments]                                                                
:                                                                                        
[ arg... ]                                                                               
[[ expression ]]                                                                         
alias [-p] [nom[=valeur] ... ]                                                           
bg [job_spec ...]                                                                        
bind [-lpvsPSVX] [-m keymap] [-f nomfichier] [-q nom] [-u nom] [-r seqtouche] [-x seqt>  
break [n]                                                                                
builtin [shell-builtin [arg ...]]                                                        
caller [expr]                                                                            
case MOT in [MOTIF [| MOTIF]...) COMMANDES ;;]... esac                                   
cd [-L|[-P [-e]] [-@]] [rép]                                                             
command [-pVv] commande [arg ...]                                                        
compgen [-abcdefgjksuv] [-o option] [-A action] [-G motif_glob] [-W liste_mots]  [-F f>  
complete [-abcdefgjksuv] [-pr] [-DE] [-o option] [-A action] [-G motif_glob] [-W liste>  
compopt [-o|+o option] [-DE] [nom ...]                                                   
continue [n]                                                                             
coproc [NOM] commande [redirections]                                                     
declare [-aAfFgilnrtux] [-p] [nom[=valeur] ...]                                          
dirs [-clpv] [+N] [-N]                                                                   
disown [-h] [-ar] [jobspec ... | pid ...]                                                
echo [-neE] [arg ...]                                                                    
enable [-a] [-dnps] [-f nomfichier] [nom ...]                                            
eval [arg ...]                                                                           
exec [-cl] [-a nom] [commande [arguments ...]] [redirection ...]                         
exit [n]                                                                                 
export [-fn] [nom[=valeur] ...] ou export -p                                             
false                                                                                    
fc [-e ename] [-lnr] [premier] [dernier] ou fc -s [motif=nouveau] [commande]             
fg [job_spec]                                                                            
for NOM [in MOTS ... ] ; do COMMANDES; done                                              
for (( exp1; exp2; exp3 )); do COMMANDES; done                                           
function nom { COMMANDES ; } ou nom () { COMMANDES ; }                                   
getopts chaineopts nom [arg]                                                             
hash [-lr] [-p nomchemin] [-dt] [nom ...]                                                
help [-dms] [motif ...]                                                                  

history [-c] [-d décalage] [n] ou history -anrw [nomfichier] ou history -ps arg [arg.>
if COMMANDES; then COMMANDES; [ elif COMMANDES; then COMMANDES; ]... [ else COMMANDES>
jobs [-lnprs] [jobspec ...] ou jobs -x commande [args]
kill [-s sigspec | -n signum | -sigspec] pid | jobspec ... ou kill -l [sigspec]
let arg [arg ...]
local [option] nom[=valeur] ...
logout [n]
mapfile [-d délim] [-n nombre] [-O origine] [-s nombre] [-t] [-u fd] [-C callback] [->
popd [-n] [+N | -N]
printf [-v var] format [arguments]
pushd [-n] [+N | -N | rép]
pwd [-LP]
read [-ers] [-a tableau] [-d delim] [-i texte] [-n ncars] [-N ncars] [-p prompt] [-t >
readarray [-n nombre] [-O origine] [-s nombre] [-t] [-u fd] [-C callback] [-c quantum>
readonly [-aAf] [nom[=valeur] ...] ou readonly -p
return [n]
select NOM [in MOTS ... ;] do COMMANDES; done
set [-abefhkmnptuvxBCHP] [-o nom-option] [--] [arg ...]
shift [n]
shopt [-pqsu] [-o] [nom_opt ...]
source nom_fichier [arguments]
suspend [-f]
test [expr]
time [-p] pipeline
times
trap [-lp] [[arg] signal_spec ...]
true
type [-afptP] nom [nom ...]
typeset [-aAfFgilnrtux] [-p] nom[=valeur] ...
ulimit [-SHabcdefiklmnpqrstuvxPT] [limite]
umask [-p] [-S] [mode]
unalias [-a] nom [nom ...]
unset [-f] [-v] [-n] [nom ...]
until COMMANDES; do COMMANDES; done
variables - Noms et significations de certaines variables du shell
wait [-n] [id ...]
while COMMANDES; do COMMANDES; done
{ COMMANDES ; }


## bsd

column
