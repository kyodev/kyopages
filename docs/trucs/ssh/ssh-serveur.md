# serveur ssh, authentification par clé


* chmod 700 sur ~/.ssh (droits pas défaut, propriété user)
* chmod 600 sur les fichiers key (obligatoire, vérification par openssh, ou 644, propriétaire user)
* root requis pour la configuration du serveur ssh


## installation debian

si nécessaire

```shell
apt install openssh-server
```
les clés du serveur sont générées automatiquement à l'installation du serveur



## configuration

```text
editor /etc/ssh/sshd_config
```

par défaut:

* PubkeyAuthentication yes 
* AuthorizedKeysFile  .ssh/authorized_keys .ssh/authorized_keys2
* ChallengeResponseAuthentication no
* PermitRootLogin prohibit-password (synonyme de without-password)


modification:

* `PasswordAuthentication no` pour désactiver l'accès par pass (**important**)
* `Port 5555` pour éviter le port standard 22, voir [ports iana](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml)


pour faire simple, ajout bloc suivant en fin de fichier :
```text
	# modifications ultérieures éventuelles facilitées
 #LogLevel DEBUG
 #AuthorizedKeysFile  .ssh/authorized_keys

	# standard répété
ChallengeResponseAuthentication no
PermitEmptyPasswords no
PubkeyAuthentication yes

	# changement port d'écoute
Port 5555
	# suppression accès par mot de passe
PasswordAuthentication no
	# même pas avec une clef !
PermitRootLogin no

	# facultatif ipv4
AddressFamily inet

	# facultatif, restreindre l'accès à certains
AllowUsers <user>
```

relancer le service: `systemctl restart sshd`



## connexion client

le client a une clé autorisée sur le serveur. pour la génération et la publication, voir 
<https://kyodev.frama.io/kyopages/trucs/ssh/ssh-client/>


```shell
ssh <user>@SRV

```
le **mot de passe de la clé** n'est pas demandé et la connexion se fait correctement



## ça marche

* commenter `LogLevel DEBUG` pour désactiver debug éventuel



## ça marche pas

* activer `LogLevel DEBUG` sur le serveur 
* surveiller `tail -n 20 -f /var/log/auth.log`
* côté client, se connecter avec `ssh -vv user@SRV`


* vérifier les droits
* vérifier les **droits**
* **vérifier les droits**
  * chmod 700 sur ~/.ssh
  * chmod 600 sur les fichiers
* droits sur le serveur, du côté user 600 sur les keys


* **MAIS AUSSI**, un /home/user correct, 777 proscrit, 755 c'est un minimum mieux

* journalctl -xe
* systemctl status sshd.service

* systemctl list-units | grep ssh
* systemctl list-unit-files | grep ssh



## config serveur rapide

```text
	# modifications ultérieures éventuelles facilitées
 #LogLevel DEBUG
 #AuthorizedKeysFile  .ssh/authorized_keys

	# standard
ChallengeResponseAuthentication no
PermitEmptyPasswords no
PubkeyAuthentication yes

	# modifications
Port 5555
PasswordAuthentication no
PermitRootLogin no
```

de manière plus ou moins aléatoire?, la connexion inactive provoque une erreur genre:
```shell
packet_write_wait: Connection to <ip server> port <port>: Broken pipe
```

selon [cette page](https://patrickmn.com/aside/how-to-keep-alive-ssh-sessions/):
```text
ClientAliveInterval 300
ClientAliveCountMax 2
```

## fail2ban

```shell
su
apt install fail2ban
systemctl enable fail2ban.service
systemctl start fail2ban.service

echo [sshd]               > /etc/fail2ban/jail.d/90sdeb.local
echo "enabled = true"    >> /etc/fail2ban/jail.d/90sdeb.local
echo "filter  = sshd"    >> /etc/fail2ban/jail.d/90sdeb.local
echo "#port = ssh, sftp, 5555"    >> /etc/fail2ban/jail.d/90sdeb.local
echo "mode = extra"      >> /etc/fail2ban/jail.d/90sdeb.local
echo "maxretry = 3"      >> /etc/fail2ban/jail.d/90sdeb.local
echo "findtime = 3600"   >> /etc/fail2ban/jail.d/90sdeb.local
echo "bantime = 86400"   >> /etc/fail2ban/jail.d/90sdeb.local

fail2ban-client reload
```

* par défaut, port = 0:65535
* si pas de syslog (journal systemd): `echo "backend = systemd" >> /etc/fail2ban/jail.d/90sdeb.local`
* enabled = true dans /etc/fail2ban/jail.d/defaults-debian.conf
* .local surcharge .conf, fail2ban/jail.d/ surcharge fail2ban/


vérifier:
```shell
systemctl status fail2ban.service

fail2ban-client -d

su

iptables -S | grep f2b
	-N f2b-sshd
	-A INPUT -p tcp -m multiport --dports 22 -j f2b-sshd
	-A f2b-sshd -s <xyz> -j REJECT --reject-with icmp-port-unreachable
	-A f2b-sshd -j RETURN

iptables -L
	target     prot opt source               destination 
	REJECT     all  --  <xyz>                  anywhere             reject-with icmp-port-unreachable
```

suivre les logs:
```shell
tail -f /var/log/fail2ban.log
```

unban:
```shell
fail2ban-client status sshd
	Status for the jail: sshd
	|- Filter
	|  |- Currently failed:	0
	|  |- Total failed:	0
	|  `- File list:	/var/log/auth.log
	`- Actions
	   |- Currently banned:	1
	   |- Total banned:	1
	   `- Banned IP list:	<xyz>

fail2ban-client unban <IP>
```
mais ne résiste pas à un restart du service fail2ban


## liens divers

* <https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml>
* <https://github.com/fail2ban/fail2ban/wiki>
* <https://wiki.debian-fr.xyz/Fail2ban>
* <https://doc.ubuntu-fr.org/fail2ban>
* <https://wiki.archlinux.org/index.php/SSH_keys>
* <https://github.com/arthepsy/ssh-audit>
* <https://github.com/jtesta/ssh-audit>
