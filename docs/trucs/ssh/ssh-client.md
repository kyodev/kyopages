# utilisation ssh côté client


## gestion des clés

* ouvrir sa clé, le temps de la session ou jusqu'à déchargement
```shell
	# la clé a un nom standard id_rsa
ssd-add 

	# le clé s'appelle toto_rsa
ssh-add toto_rsa
```

* si erreur, démarrer l'agent ssh
```shell
eval $(ssh-agent -s)
```
```text
Agent pid 12345
```
l' ajouter au démarrage du système

* voir le(s) clef(s)
```shell
ssh-add  -l
```

* décharger le(s) clef(s)
```shell
ssh-add  -D
```

## sftp

explorateur, par exemple: `sftp://<user>@SRV/` ou `sftp://<user>@ip:port/`


## export display

dans la config du serveur, sur debian, `X11Forwarding yes`, on peut donc se connecter et obtenir un export du serveur X:

```shell
ssh -X <user>@SRV

mousepad  		# exemple application X
```
l'application est sur le serveur, l'affichage sur le client



## scp (secure cp)

```shell
scp  -P 'port' /chemin/fichierSource <user>@SRV:/chemin/cible
```

## sshfs

voir <https://www.linuxtricks.fr/wiki/autour-du-protocole-ssh-utiliser-le-canal-de-communication#paragraph_sshfs-systeme-de-fichiers-sur-ssh>


## génération clé

si pas déjà exixtante :

```shell
ssh-keygen -t rsa -b 4096 -C mail ou host ou commentaire

```
* à lancer en utilisateur
* -t par défaut rsa 
* -b taille en bits (par défaut: 2048b)
* -C commentaire
* DSA, déprécié par openssl

* un commentaire permet d'identifier clairement le client, par nom ou host ou machine ou mail...
* mot de passe non obligatoire, mais ÇA DEVRAIT. le pass protège l'ouverture de la **clé privée**

* par défaut, 2 fichiers générés dans `~/.ssh` pour le client
  * id_rsa : **clé privée**, confidentielle, à ne jamais donner, à protéger
  * id_rsa.pub: **clé publique**, à communiquer au service requis
* pour le serveur, clé dans /etc/ssh


## config client

* pour faire simple: chmod 600 sur les fichiers key (au moins la clé privée)


* étape **non obligatoire**, mais augmente le confort d'accès
* **SRV** désigne l'ip 192.168.1.110 du serveur ( rappel connaître ipv4: `ip -4 a` ), majuscules non obligatoires
* **SRV** a un serveur ssh écoutant sur le port 5555 (pas de port 22 standard que kevin essaye d'emblée)
```shell
xdg-open ~/.ssh/config
```
```text
Host SRV
 HostName 192.168.1.110
 User client
 Port 5555
 # defaut: IdentityFile ~/.ssh/id_rsa
 IdentityFile ~/.ssh/essai_rsa
```


## envoi clé publique sur un serveur

### méthode rapide

```shell
 	# hypothèse que .ssh/ sera créé si besoin
ssh-copy-id -p 5555 -i ~/.ssh/id_rsa.pub  <user>@192.168.1.110
```

* crée le répertoire `~/.ssh/` si besoin, comme le fichier `authorized_keys`  
* ajoute la clé au fichier `authorized_keys`  
* si la connexion par mot de passe sur le serveur distant est encore possible, il sera demandé.  
* si la connexion par mot de passe n'est plus possible, passer par un intermédiaire autorisé qui utilisera par exemple 
  la commande `ssh-copy-id -f -p 5555 -i .id_rsa.pub  <user>@192.168.1.110` 'option -f force, sans vérification si clé privée 



### méthode _traditionnelle_

* se connecter en user sur le serveur, en debian d'origine, `<user>/.ssh/` n'existe pas
```shell
ssh -p '5555' <user>@SRV

mkdir -p .ssh
chmod 700 .ssh
exit
```
* copier la clé sur le serveur, du client:
```shell
scp  -P '5555' ~/.ssh/id_rsa.pub <user>@SRV:/home/user

	# sur le serveur SRV
cat id_rsa.pub >> .ssh/authorized_keys
rm id_rsa.pub
```

* vérifier les droits en final
```text
ls -ld .ssh/
	drwx------ 2 user user 4096 déc.  26 22:23 .ssh/

ls -l .ssh/
	-rw-r--r-- 1 user user 1472 janv.  9 21:17 authorized_keys
```
