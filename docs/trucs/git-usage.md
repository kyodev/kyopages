# Git en utilisation courante

```
git commit -m 'une fote de frappe'
git commit --amend
	## l'éditeur configuré par défaut s'ouvre pour modifier le message de commit
```
```
git commit -m 'message'
	## oups fichier oublié
git add nouveauFichier
git commit --amend
	## l'éditeur configuré par défaut s'ouvre pour modifier le message de commit
	## le nouveau fichier est pris en compte
```

https://help.github.com/articles/fork-a-repo/
