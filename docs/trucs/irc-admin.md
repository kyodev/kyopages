# IRC Freenode admin

* [freenode.net](https://freenode.net/) est un réseau administré par des volontaires
* les chans # sont chez freenode, les chans réservés aux [groupes enregistrés](https://freenode.net/groupreg) (voir staff #freenode) intervenants dans le développement et l'utilisation de logiciels libres
* les chans ## sont pour les chans hors sujets, ou sans enregistrement formaliste.
* dans tous les cas, utiliser freenode est un privilège, pas un droit.  

* voir: 
   * <http://freenode.net/changuide>
   * <http://freenode.net/policies#channel-ownership>

* <text> : à remplacer
* [text] : optionnel

## cmd opérateur (op)

cmd | objet
--- | -----
`/msg chanserv op ##chan`				| s'élever opérateur sur le chan (provisoire)
`/msg chanserv deop ##chan`				| se retirer opérateur sur le chan (provisoire)
`/msg chanserv op ##chan <pseudo>`		| élever opérateur pseudo sur le chan (provisoire)
`/msg chanserv deop ##chan <pseudo>`	| retirer opérateur pseudo sur le chan (provisoire)
`/op <pseudo>`							| élever pseudo en op (provisoire) (cmd alternative)
`/deop <pseudo>`						| retirer pseudo en op (provisoire) (cmd alternative)
`/msg chanserv voice ##chan`			| s'élever en voice (provisoire)
`/msg chanserv devoice ##chan`			| se retirer en voice (provisoire)
`/msg chanserv voice ##chan <pseudo>`	| élever pseudo en voice (provisoire)
`/msg chanserv devoice ##chan <pseudo>`	| retirer pseudo en voice (provisoire)
`/voice <pseudo>`						| élever pseudo en voice (provisoire) (cmd alternative)
`/devoice <pseudo>`				| retirer pseudo en voice (provisoire) (cmd alternative)
`/ban <pseudo> ou <mask>`		| bannir pseudo ou masque (pseudo construit le masque automatiquement)
`/kick <pseudo> [raison]`		| expulser pseudo (avec raison facultative) (pseudo construit le masque automatiquement)
`/kickban <pseudo> [raison]`	| bannir puis expulser pseudo (raison facultative) (pseudo construit le masque automatiquement)
`/unban <pseudo> ou mask`		| dé-bannir <pseudo> ou mask (pseudo construit le masque automatiquement)


## opérations sur le chan

on fait appel à ChanServ, le serveur de chan

cmd | objet
--- | -----
`/msg chanserv register ##chan` 						| enregistrement chan
`/msg chanserv topic ##chan <messageTopic>`				| définir le _topic_ du chan (toujours affiché en haut de la fenêtre)
`/msg chanserv set ##chan entrymsg <messageAccueil>`	| définir le message d'accueil, affiché à la connexion (défile dans la zone des messages)
`/msg chanserv set ##chan entrymsg`						| efface le message d'accueil
`/msg ChanServ set ##chan URL <http://url/>`			| définir une url à l'accueil, affiché à la connexion (défile dans la zone des messages)
`/msg chanserv set ##chan guard on`						| activation du service de garde chanserv
`/msg chanserv access ##chan add <pseudo> +o`			| donner statut op à pseudo
`/msg chanserv access ##chan add <pseudo> +flag(s)`		| attribuer flag(s) à pseudo
`/msg chanserv access ##chan add <pseudo> -flag(s)`		| enlever flag(s) à pseudo
`/msg chanserv access ##chan del <pseudo>		`		| enlèver pseudo de la liste d'accès
`/msg chanserv access ##chan list`						| consulter les droits des users
`/msg ChanServ SET ##chan verbose on|off|ops`			| annonces changement liste accès sur chan ou ops
`/msg ChanServ flags ##chan <pseudo>`					| attribuer des permissions (flags) à des pseudos
`/msg ChanServ drop ##chan`								| dé-enregistrer un chan, le message sera à confirmer
`/msg ChanServ flags ##chan`							| lister des flags des users
`/msg chanserv info ##chan` 							| informations sur le chan


## flags

**+** donner / **-** retirer

flag | objet
:--: | -----
+A	| Enables viewing of channel access lists
+b	| Enables automatic kickban
+i	| Enables use of the invite and getkey commands
+e	| Exempts from +b and enables unbanning self
+f	| Enables modification of channel access lists
+F	| Grants full founder access
+o	| Enables use of the op/deop commands
+O	| Enables automatic op
+s	| Enables use of the set command
+r	| Enables use of the unban command
+R	| Enables use of the recover, sync and clear commands
+S	| Marks the user as a successor
+t	| Enables use of the topic and topicappend commands
+v	| Enables use of the voice/devoice commands
+V	| Enables automatic voice
+*	| all permissions except +b, +S, and +F -> +AORVefiorstv
-*	| removes all permissions including +b and +F


## indésirables, spambot

freenode depuis fin juillet est la cible de spambots. en cas de d'attaque sur un channel:

* `/mode ##chan +r`  active le blocage des utilisateurs non identifiés
* `/mode ##chan -r`  désactive le blocage des utilisateurs non identifiés

par défaut, un chan sur freenode:   
/mode ##chan   
   Canal ##chan mode : +cnt   

pour les chans avec plus de 30 utilisateurs, on peut aussi se protéger en invitant le robot:   
`/invite Sigyn`

les [modes possibles](https://freenode.net/kb/answer/channelmodes)   

* m (moderated) 	Only opped and voiced users can send to the channel. This mode does not prevent users from changing nicks.
* s (secret) 		This channel will not appear on channel lists or WHO or WHOIS output unless you are on it.


mais ces modes sont aussi être pris en charge par une politique de restriction active MLOCK   
`/msg chanserv info ##chan`   
	Mode lock  : +ntc-slk   

`/msg ChanServ SET ##sdeb MLOCK <modes>`   

les modes ntcslk doivent être définis par    
`/msg ChanServ SET ##sdeb MLOCK +ntc-slk`   
