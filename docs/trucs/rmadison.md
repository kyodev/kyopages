# `rmadison`

Objectif: récupérer la dernière version du script perl `rmadison` sans installer le meta-paquet `devscripts` 

**rmadison** permet d'interroger à distance les bases des dépôts debian sur les paquets disponibles

 * dépendances `liburi-perl` et `wget` ou `curl`, normalement installées par défaut
 * le script est installé à son emplacement habituel, et une préférence sera placée dans `/etc/devscripts.conf` 
(créé au besoin) pour n'afficher que les architectures `source,i386,amd64`
 * le meta-paquet `devscripts` permet d'installer beaucoup d'outils pour le développement (>260Mo avec les recommandés)
 * `apt install --no-install-recommends devscripts` ne permet d'installer que les outils du maintainer (35Mo)


## installation

```shell
su
wget -O /usr/bin/rmadison "https://salsa.debian.org/debian/devscripts/raw/master/scripts/rmadison.pl"
chmod +x /usr/bin/rmadison
exit
```


## utilisation

`rmadison paquet`, exemple:
```shell
rmadison blender
```
```text
blender    | 2.63a-1+deb7u1      | oldoldstable       | source, amd64, armel, armhf, i386, ia64, kfreebsd-amd64, kfreebsd-i386, mips, mipsel, powerpc, s390, s390x, sparc
blender    | 2.72.b+dfsg0-3      | oldstable          | source, amd64, arm64, armel, armhf, i386, mips, mipsel, powerpc, ppc64el, s390x
blender    | 2.72.b+dfsg0-3      | oldstable-kfreebsd | source, kfreebsd-amd64, kfreebsd-i386
blender    | 2.74+dfsg0-4~bpo8+1 | jessie-backports   | source, amd64, arm64, armel, armhf, i386, kfreebsd-amd64, kfreebsd-i386, mips, mipsel, powerpc, ppc64el, s390x
blender    | 2.78.a+dfsg0-4      | stable             | source, amd64, arm64, armel, armhf, i386, mips, mips64el, mipsel, ppc64el, s390x
blender    | 2.78.a+dfsg0-4      | testing            | source, amd64, arm64, armel, armhf, i386, mips, mips64el, mipsel, ppc64el, s390x
blender    | 2.78.a+dfsg0-4      | unstable           | source, arm64, armel, armhf
blender    | 2.78.c+dfsg0-2      | buildd-unstable    | source, amd64, armel, hurd-i386, i386, kfreebsd-amd64, kfreebsd-i386, mips, mips64el, mipsel, powerpc, ppc64el, s390x
blender    | 2.78.c+dfsg0-2      | unstable           | source, amd64, armel, hurd-i386, i386, kfreebsd-amd64, kfreebsd-i386, mips, mips64el, mipsel, powerpc, ppc64el, s390x
```

## configuration

la configuration peut être modifiée en ajoutant un fichier `/etc/devscripts.conf`, par défaut:
```text
 ##### rmadison
 #
 # Add a custom URL to the default list of shorthands so one
 # can use it with -u without having to specify the full URL
 #
 # RMADISON_URL_MAP_EXAMPLE=http://example.com/madison.cgi
 #
 # Default URL to use if none is specified on the command line.
 # RMADISON_DEFAULT_URL=debian
 # debian	"https://api.ftp-master.debian.org/madison",
 # new		"https://api.ftp-master.debian.org/madison?s=new",
 # qa		"https://qa.debian.org/madison.php",
 # ubuntu	"http://people.canonical.com/~ubuntu-archive/madison.cgi",
 # udd		'https://qa.debian.org/cgi-bin/madison.cgi',
 # use --url=ubuntu to query ubuntu instead debian 
 #
 # Default architecture to use if none is specified on the command line.
 # use --architecture='*' to run an unrestricted query when
 # RMADISON_ARCHITECTURE is set.
```

## config adaptée

pour éviter un affichage de toutes les architectures possibles:

```shell
su
echo 'RMADISON_ARCHITECTURE=source,i386,amd64,all' >> /etc/devscripts.conf
exit
```
seules les **sources**, et les architectures **i386**, **amd64** et **all** seront affichées. 
pas d'espaces après les virgules.
`rmadison paquet`, exemple:
```shell
rmadison blender
```
```text
blender    | 2.63a-1+deb7u1      | oldoldstable       | source, amd64, i386
blender    | 2.72.b+dfsg0-3      | oldstable          | source, amd64, i386
blender    | 2.72.b+dfsg0-3      | oldstable-kfreebsd | source
blender    | 2.74+dfsg0-4~bpo8+1 | jessie-backports   | source, amd64, i386
blender    | 2.78.a+dfsg0-4      | stable             | source, amd64, i386
blender    | 2.78.a+dfsg0-4      | testing            | source, amd64, i386
blender    | 2.78.c+dfsg0-2      | buildd-unstable    | source
blender    | 2.78.c+dfsg0-2      | unstable           | source
blender    | 2.78.c+dfsg0-2+b1   | buildd-unstable    | amd64, i386
blender    | 2.78.c+dfsg0-2+b1   | unstable           | amd64, i386
```

## exemples

```shell
    # interroge les dépôts ubuntu
rmadison --url=ubuntu blender

    # limite à l'architecture amd64
rmadison --architecture=amd64 blender

    # interroge les dépôts ubuntu pour l'architecture i386 seule
rmadison --url=ubuntu --architecture=i386 blender
```

## notes options 

* les architectureS et les suiteS peuvent être séparées par des `,`

* `-s` ou `--suite`, signifie 'Suite' ou 'archive', affiche 'archive' (selon conventions apt). Exemple: 
  * `rmadison -s stable <paquet>` idem `rmadison -s stretch <paquet>`  
  * `rmadison -s stable,sid <paquet>` 

* `-r` 'regex': par sécurité, pour éviter DDoS, n'est pas actif en web externe

* `-a` ou `--architecture` amd64, i386, source, all  ....
  * `rmadison -s amd64,i386 <paquet>` 
