# microcode processeurs

* <https://wiki.archlinux.org/index.php/Microcode>
* <https://wiki.debian.org/fr/Microcode>
* <https://wiki.debian.org/Microcode>
* <https://www.kernel.org/doc/Documentation/x86/early-microcode.txt>
* <http://metadata.ftp-master.debian.org/changelogs/non-free/i/intel-microcode/unstable_README.Debian>


**Contrairement** à ce qui est dit dans le wiki debian Fr (pas le En), un **reboot** est nécessaire, 
les microcodes sont chargés dans le kernel au boot. il est possible de le faire manuellement en marche, 
mais c'est plus aventureux

ROOT requis

```shell
su
```  

## installation

les dépôts **non-free** et **contrib** doivent être accessibles:
```text
	# testing
deb     deb https://deb.debian.org/debian/ testing main contrib non-free
deb     deb https://deb.debian.org/debian-security testing/updates main contrib non-free
	# stretch (stable en cours)
deb     deb https://deb.debian.org/debian/ stretch main contrib non-free
deb     deb https://deb.debian.org/debian-security stretch/updates main contrib non-free
deb     deb https://deb.debian.org/debian/ stretch-updates main contrib non-free
```

```shell
apt install amd64-microcode
	## ou
apt install intel-microcode

reboot
```

pour intel (pas amd):
```txt
Les NOUVEAUX paquets suivants seront installés :
  intel-microcode iucode-tool
```

## désactiver

en cas de soucis au boot:

* accéder au menu grub (si besoin maintenir le shift gauche pour l'obtenir)
* si besoin, voir cette [page similaire](https://kyodev.frama.io/kyopages/trucs/kernel/kernel-nomodeset/) 
  pour éditer ce menu
* à la ligne `linux /boot ....`, ajouter l'option **dis_ucode_ldr**   
  exemple: `linux /boot .... [splash] [quiet] nomodeset`
* `ctrl+x` ou `F10` pour enregistrer et démarrer
* après démarrage, 
    * `apt purge amd64-microcode`
    * `apt purge intel-microcode`
        * ne pas oublier un `update-initramfs -u` (automatique sur debian, donc inutile)
    * si souci après mise à jour, ré-installer une version plus ancienne:
        * à partir des [snapshots debian](http://snapshot.debian.org/) par exemple
        * sur [AMD](http://www.amd64.org/microcode.html)
        * sur [intel](https://downloadcenter.intel.com/download/27337/Linux-Processor-Microcode-Data-File)


## initial

exemples
```shell
dmesg | grep 'microcode:'
	## ou 
journalctl -k --boot 0 --no-hostname  | grep 'microcode:'
```  
```text
	# exemple 1: I5
[    0.000000] [Firmware Bug]: TSC_DEADLINE disabled due to Errata; please update microcode to version: 0x20 (or later)
[    3.111358] microcode: sig=0x40651, pf=0x40, revision=0x1c
[    3.111593] microcode: Microcode Update Driver: v2.2.
```  
Noter: **Firmware Bug...update microcode to version: 0x20**

```text
	# exemple 2: Atom
[    1.871952] microcode: sig=0x106c2, pf=0x1, revision=0x211
[    1.872197] microcode: Microcode Update Driver: v2.01 <tigran@aivazian.fsnet.co.uk>, Peter Oruba

	# exemple 3: Amd
[    2.309457] microcode: CPU0: patch_level=0x0500010d
[    2.309469] microcode: CPU1: patch_level=0x0500010d
[    2.309599] microcode: Microcode Update Driver: v2.2.
```


`getInfo -cs`   
```text
1 x Intel Core i5-4202Y  (2cores, 4threads) {0.60/1.60/2.00GHz} microcode:0x1c
1 x Intel Atom Z520 (1core, 2threads) {0.80/1.07/1.33GHz} microcode:0x211
1 x AMD E2-1800 APU with Radeon HD Graphics (2cores, 2threads) {0.85/1.36/1.70GHz} microcode:0x500010d
```


## après install


`dmesg | grep 'microcode: microcode updated early'`   
```text
	# exemple 1: I5
[    0.000000] microcode: microcode updated early to revision 0x20, date = 2017-01-27
[    3.107301] microcode: sig=0x40651, pf=0x40, revision=0x20
[    3.107536] microcode: Microcode Update Driver: v2.2.

	# exemple 2: Atom:
[    0.000000] microcode: microcode updated early to revision 0x217, date = 2009-04-10
[    1.870174] microcode: sig=0x106c2, pf=0x1, revision=0x217
[    1.870368] microcode: Microcode Update Driver: v2.01 <tigran@aivazian.fsnet.co.uk>, Peter Oruba

	# exemple 3: Amd
[    2.347856] microcode: microcode updated early to new patch_level=0x05000119
[    2.347933] microcode: CPU0: patch_level=0x05000119
[    2.347986] microcode: CPU1: patch_level=0x05000119
[    2.348209] microcode: Microcode Update Driver: v2.2.
```
Noter: **microcode updated early**

Note Atom
* l'atom n'a pas le flag cpu 'rep_good' ()rep microcode works well)
* il est indiqué dans les liste des procs chez Intel
* microcode est quand même mis à jour au boot


`getInfo -cs`   
```text
1 x Intel Core i5-4202Y  (2cores, 4threads) {0.60/1.60/2.00GHz} microcode:0x20
1 x Intel Atom Z520 (1core, 2threads) {0.80/1.07/1.33GHz} microcode:0x217
1 x AMD E2-1800 APU with Radeon HD Graphics (2cores, 2threads) {0.85/1.36/1.70GHz} microcode:0x5000119
```
noter: la version affichée du microcode en cours


## divers

état microcode (intérêt?) `iucode_tool`     
```text
iucode_tool: nothing to do...
```

signature du processeur `iucode_tool -S`     
```text
iucode_tool: system has processor(s) with signature 0x000106c2
```

voir les firmwares possibles:   
`iucode_tool -S -l /lib/firmware/intel-ucode/*`   
```text
iucode_tool: system has processor(s) with signature 0x000106c2
microcode bundle 1: /lib/firmware/intel-ucode/06-01-01
microcode bundle 2: /lib/firmware/intel-ucode/06-01-02
...
microcode bundle 104: /lib/firmware/intel-ucode/0f-06-05
microcode bundle 105: /lib/firmware/intel-ucode/0f-06-08
selected microcodes:
  048/001: sig 0x000106c1, pf_mask 0x01, 2007-12-03, rev 0x0109, size 5120
  049/001: sig 0x000106c2, pf_mask 0x08, 2009-04-10, rev 0x0219, size 5120
...
  050/003: sig 0x000106ca, pf_mask 0x04, 2009-08-25, rev 0x0107, size 5120
  050/004: sig 0x000106ca, pf_mask 0x01, 2009-08-25, rev 0x0107, size 5120
```


## options kernel 

pour compiler, voir
```text
Processor type and features  --->
    <*> CPU microcode loading support
    [*]   Intel microcode loading support

Device Drivers  --->
  Generic Driver Options  --->
    [*]   Include in-kernel firmware blobs in kernel binary

CONFIG_BLK_DEV_INITRD=Y
CONFIG_MICROCODE=y
CONFIG_MICROCODE_INTEL=Y
```  

## intel supportés

ça ressemble à la liste de tous les processeurs au 11/2017, malgré que certains processeurs n'aient pas 
le flag cpu:   
`rep_good ⟷ rep microcode works well`

pas trouvé de doc officielle sur ce [flag kernel](https://kyodev.frama.io/kyopages/trucs/kernel/cpu_flags/),
mais il me semble qu'il indiquerait un cpu supportant une correction de microcode


```text
Atom 230 (512K Cache, 1.60 GHz, 533 MHz FSB)
Atom 330 (1M Cache, 1.60 GHz, 533 MHz FSB)
Atom C2308 (1M Cache, 1.25 GHz)
Atom C2316 (1M Cache, 1.50 GHz)
Atom C2338 (1M Cache, 1.70 GHz)
Atom C2350 (1M Cache, 1.70 GHz)
Atom C2358 (1M Cache, 1.70 GHz)
Atom C2508 (2M Cache, 1.25 GHz)
Atom C2516 (2M Cache, 1.40 GHz)
Atom C2518 (2M Cache, 1.70 GHz)
Atom C2530 (2M Cache, 1.70 GHz)
Atom C2538 (2M Cache, 2.40 GHz)
Atom C2550 (2M Cache, 2.40 GHz)
Atom C2558 (2M Cache, 2.40 GHz)
Atom C2718 (4M Cache, 2.00 GHz)
Atom C2730 (4M Cache, 1.70 GHz)
Atom C2738 (4M Cache, 2.40 GHz)
Atom C2750 (4M Cache, 2.40 GHz)
Atom C2758 (4M Cache, 2.40 GHz)
Atom C3308 (4M Cache, up to 2.10 GHz)
Atom C3338 (4M Cache, up to 2.20 GHz)
Atom C3508 (8M Cache, up to 1.60 GHz)
Atom C3538 (8M Cache, up to 2.10 GHz)
Atom C3558 (8M Cache, up to 2.20 GHz)
Atom C3708 (16M Cache, up to 1.70 GHz)
Atom C3750 (16M Cache, up to 2.40 GHz)
Atom C3758 (16M Cache, up to 2.20 GHz)
Atom C3808 (12M Cache, up to 2.0 GHz)
Atom C3830 (12M Cache, up to 2.30 GHz)
Atom C3850 (12M Cache, up to 2.40 GHz)
Atom C3858 (12M Cache, up to 2.0 GHz)
Atom C3950 (16M Cache, up to 2.20 GHz)
Atom C3955 (16M Cache, up to 2.40 GHz)
Atom C3958 (16M Cache, up to 2.0 GHz)
Atom D2500 (1M Cache, 1.86 GHz)
Atom D2550 (1M Cache, 1.86 GHz)
Atom D2560 (1M Cache, 2.00 GHz)
Atom D2700 (1M Cache, 2.13 GHz)
Atom D410 (512K Cache, 1.66 GHz)
Atom D425 (512K Cache, 1.80 GHz)
Atom D510 (1M Cache, 1.66 GHz)
Atom D525 (1M Cache, 1.80 GHz)
Atom E3805 (1M Cache, 1.33 GHz)
Atom E3815 (512K Cache, 1.46 GHz)
Atom E3825 (1M Cache, 1.33 GHz)
Atom E3826 (1M Cache, 1.46 GHz)
Atom E3827 (1M Cache, 1.75 GHz)
Atom E3845 (2M Cache, 1.91 GHz)
Atom E620 (512K Cache, 600 MHz)
Atom E620T (512K Cache, 600 MHz)
Atom E640 (512K Cache, 1.00 GHz)
Atom E640T (512K Cache, 1.00 GHz)
Atom E645C (512K Cache, 1.0 GHz)
Atom E645CT (512K Cache, 1.0 GHz)
Atom E660 (512K Cache, 1.30 GHz)
Atom E660T (512K Cache, 1.30 GHz)
Atom E665C (512K Cache, 1.3 GHz)
Atom E665CT (512K Cache, 1.3 GHz)
Atom E680 (512K Cache, 1.60 GHz)
Atom E680T (512K Cache, 1.60 GHz)
Atom N2600 (1M Cache, 1.6 GHz)
Atom N270 (512K Cache, 1.60 GHz, 533 MHz FSB)
Atom N2800 (1M Cache, 1.86 GHz)
Atom N280 (512K Cache, 1.66 GHz, 667 MHz FSB)
Atom N450 (512K Cache, 1.66 GHz)
Atom N455 (512K Cache, 1.66 GHz)
Atom N470 (512K Cache, 1.83 GHz)
Atom N475 (512K Cache, 1.83 GHz)
Atom N550 (1M Cache, 1.50 GHz)
Atom N570 (1M Cache, 1.66 GHz)
Atom S1220 (1M Cache, 1.60 GHz)
Atom S1240 (1M Cache, 1.60 GHz)
Atom S1260 (1M Cache, 2.00 GHz)
Atom S1269 (1M Cache, 1.60 GHz)
Atom S1279 (1M Cache, 1.60 GHz)
Atom S1289 (1M Cache, 2.00 GHz)
Atom x3-C3130 (512K Cache, 1.00 GHz)
Atom x3-C3200RK (1M Cache, up to 1.10 GHz)
Atom x3-C3205RK (1M Cache, up to 1.20 GHz)
Atom x3-C3230RK (1M Cache, up to 1.10 GHz)
Atom x3-C3235RK (1M Cache, up to 1.20 GHz)
Atom x3-C3265RK (1M Cache, up to 1.1 Ghz)
Atom x3-C3295RK (1M Cache, up to 1.1 GHz)
Atom x3-C3405 (1M Cache, up to 1.40 GHz)
Atom x3-C3445 (1M Cache, up to 1.40 GHz)
Atom x5-E3930 (2M Cache, up to 1.80 GHz)
Atom x5-E3940 (2M Cache, up to 1.80 GHz)
Atom x5-E8000 (2M Cache, up to 2.00 GHz)
Atom x5-Z8300 (2M Cache, up to 1.84 GHz)
Atom x5-Z8330 (2M Cache, up to 1.92 GHz)
Atom x5-Z8350 (2M Cache, up to 1.92 GHz)
Atom x5-Z8500 (2M Cache, up to 2.24 GHz)
Atom x5-Z8550 (2M Cache, up to 2.40 GHz)
Atom x7-E3950 (2M Cache, up to 2.00 GHz)
Atom x7-Z8700 (2M Cache, up to 2.40 GHz)
Atom x7-Z8750 (2M Cache, up to 2.56 GHz)
Atom Z2420 (512K Cache, up to 1.20 GHZ)
Atom Z2460 (512K Cache, up to 1.60 GHz)
Atom Z2480 (512K Cache, up to 2.00 GHz)
Atom Z2520 (1M Cache, 1.20 GHz)
Atom Z2560 (1M Cache, 1.60 GHz)
Atom Z2580 (1M Cache, 2.00 GHz)
Atom Z2760 (1M Cache, 1.80 GHz)
Atom Z3460 (1M Cache, up to 1.60 GHz)
Atom Z3480 (1M Cache, up to 2.13 GHz)
Atom Z3530 (2M Cache, up to 1.33 GHz)
Atom Z3560 (2M Cache, up to 1.83 GHz)
Atom Z3570 (2M Cache, up to 2.00 GHz)
Atom Z3580 (2M Cache, up to 2.33 GHz)
Atom Z3590 (2M Cache, up to 2.50 GHz)
Atom Z3735D (2M Cache, up to 1.83 GHz)
Atom Z3735E (2M Cache, up to 1.83 GHz)
Atom Z3735F (2M Cache, up to 1.83 GHz)
Atom Z3735G (2M Cache, up to 1.83 GHz)
Atom Z3736F (2M Cache, up to 2.16 GHz)
Atom Z3736G (2M Cache, up to 2.16 GHz)
Atom Z3740 (2M Cache, up to 1.86 GHz)
Atom Z3740D (2M Cache, up to 1.83 GHz)
Atom Z3745 (2M Cache, up to 1.86 GHz)
Atom Z3745D (2M Cache, up to 1.83 GHz)
Atom Z3770 (2M Cache, up to 2.39 GHz)
Atom Z3770D (2M Cache, up to 2.41 GHz)
Atom Z3775 (2M Cache, up to 2.39 GHz)
Atom Z3775D (2M Cache, up to 2.41 GHz)
Atom Z3785 (2M Cache, up to 2.41 GHz)
Atom Z3795 (2M Cache, up to 2.39 GHz)
Atom Z500 (512K Cache, 800 MHz, 400 MHz FSB)
Atom Z510 (512K Cache, 1.10 GHz, 400 MHz FSB)
Atom Z510P (512K Cache, 1.10 GHz, 400 MHz FSB)
Atom Z510PT (512K Cache, 1.10 GHz, 400 MHz FSB)
Atom Z515 (512K Cache, 1.20 GHz, 400 MHz FSB)
Atom Z520 (512K Cache, 1.33 GHz, 533 MHz FSB)
Atom Z520PT (512K Cache, 1.33 GHz, 533 MHz FSB)
Atom Z530 (512K Cache, 1.60 GHz, 533 MHz FSB)
Atom Z530P (512K Cache, 1.60 GHz, 533 MHz FSB)
Atom Z540 (512K Cache, 1.86 GHz, 533 MHz FSB)
Atom Z550 (512K Cache, 2.00 GHz, 533 MHz FSB)
Atom Z560 (512K Cache, 2.13 GHz, 533 MHz FSB)
Atom Z600 (512K Cache, 1.20 GHz)
Atom Z615 (512K Cache, 1.60 GHz)
Atom Z625 (512K Cache, 1.90 GHz)
Atom Z650 (512K Cache, 1.20 GHz)
Atom Z670 (512K Cache, 1.50 GHz)
Celeron 1000M (2M Cache, 1.80 GHz)
Celeron 1005M (2M Cache, 1.90 GHz)
Celeron 1007U (2M Cache, 1.50 GHz)
Celeron 1.00 GHz, 128K Cache, 100 MHz FSB
Celeron 1.00 GHz, 256K Cache, 100 MHz FSB
Celeron 1017U (2M Cache, 1.60 GHz)
Celeron 1019Y (2M Cache, 1.00 GHz)
Celeron 1020E (2M Cache, 2.20 GHz)
Celeron 1020M (2M Cache, 2.10 GHz)
Celeron 1037U (2M Cache, 1.80 GHz)
Celeron 1047UE (2M Cache, 1.40 GHz)
Celeron 1.10 GHz, 128K Cache, 100 MHz FSB
Celeron 1.10 GHz, 256K Cache, 100 MHz FSB
Celeron 1.20 GHz, 256K Cache, 100 MHz FSB
Celeron 1.30 GHz, 256K Cache, 100 MHz FSB
Celeron 1.40 GHz, 256K Cache, 100 MHz FSB
Celeron 1.66 GHz, 1M Cache, 667 MHz FSB
Celeron 1.70 GHz, 128K Cache, 400 MHz FSB
Celeron 1.80 GHz, 128K Cache, 400 MHz FSB
Celeron 1.83 GHz, 1M Cache, 667 MHz FSB
Celeron 2000E (2M Cache, 2.20 GHz)
Celeron 2002E (2M Cache, 1.50 GHz)
Celeron 2.00 GHz, 128K Cache, 400 MHz FSB
Celeron 2.10 GHz, 128K Cache, 400 MHz FSB
Celeron 220 (512K Cache, 1.20 GHz, 533 MHz FSB)
Celeron 2.20 GHz, 128K Cache, 400 MHz FSB
Celeron 2.30 GHz, 128K Cache, 400 MHz FSB
Celeron 2.40 GHz, 128K Cache, 400 MHz FSB
Celeron 2.40 GHz, 256K Cache, 533 MHz FSB
Celeron 2.50 GHz, 128K Cache, 400 MHz FSB
Celeron 2.60 GHz, 128K Cache, 400 MHz FSB
Celeron 266 MHz, 66 MHz FSB
Celeron 2.70 GHz, 128K Cache, 400 MHz FSB
Celeron 2.80 GHz, 128K Cache, 400 MHz FSB
Celeron 2950M (2M Cache, 2.00 GHz)
Celeron 2955U (2M Cache, 1.40 GHz)
Celeron 2957U (2M Cache, 1.40 GHz)
Celeron 2961Y (2M Cache, 1.10 GHz)
Celeron 2970M (2M Cache, 2.20 GHz)
Celeron 2980U (2M Cache, 1.60 GHz)
Celeron 2981U (2M Cache, 1.60 GHz)
Celeron 300 MHz, 128K Cache, 66 MHz FSB
Celeron 300 MHz, 66 MHz FSB
Celeron 3205U (2M Cache, 1.50 GHz)
Celeron 3215U (2M Cache, 1.70 GHz)
Celeron 333 MHz, 128K Cache, 66 MHz FSB
Celeron 366 MHz, 128K Cache, 66 MHz FSB
Celeron 3755U (2M Cache, 1.70 GHz)
Celeron 3765U (2M Cache, 1.90 GHz)
Celeron 3855U (2M Cache, 1.60 GHz)
Celeron 3865U (2M Cache, 1.80 GHz)
Celeron 3955U (2M Cache, 2.00 GHz)
Celeron 3965U (2M Cache, 2.20 GHz)
Celeron 3965Y (2M Cache, 1.50 GHz)
Celeron 400 MHz, 128K Cache, 66 MHz FSB
Celeron 420 (512K Cache, 1.60 GHz, 800 MHz FSB)
Celeron 430 (512K Cache, 1.80 GHz, 800 MHz FSB)
Celeron 433 MHz, 128K Cache, 66 MHz FSB
Celeron 440 (512K Cache, 2.00 GHz, 800 MHz FSB)
Celeron 445 (512K Cache, 1.86 GHz, 1066 MHz FSB)
Celeron 450 (512K Cache, 2.20 GHz, 800 MHz FSB)
Celeron 466 MHz, 128K Cache, 66 MHz FSB
Celeron 500 MHz, 128K Cache, 66 MHz FSB
Celeron 530 (1M Cache, 1.73 GHz, 533 MHz FSB) Socket P
Celeron 533 MHz, 128K Cache, 66 MHz FSB
Celeron 540 (1M Cache, 1.86 GHz, 533 MHz FSB)
Celeron 550 (1M Cache, 2.00 GHz, 533 MHz FSB)
Celeron 560 (1M Cache, 2.13 GHz, 533 MHz FSB)
Celeron 566 MHz, 128K Cache, 66 MHz FSB
Celeron 570 (1M Cache, 2.26 GHz, 533 MHz FSB)
Celeron 575 (1M Cache, 2.00 GHz, 667 MHz FSB)
Celeron 585 (1M Cache, 2.16 GHz, 667 MHz FSB)
Celeron 600 MHz, 128K Cache, 66 MHz FSB
Celeron 633 MHz, 128K Cache, 66 MHz FSB
Celeron 667 MHz, 128K Cache, 66 MHz FSB
Celeron 700 MHz, 128K Cache, 66 MHz FSB
Celeron 725C (1.5M Cache, 1.30 GHz)
Celeron 733 MHz, 128K Cache, 66 MHz FSB
Celeron 766 MHz, 128K Cache, 66 MHz FSB
Celeron 787 (1.5M Cache, 1.30 GHz)
Celeron 797 (1.5M Cache, 1.50 GHz)
Celeron 800 MHz, 128K Cache, 100 MHz FSB
Celeron 807 (1.5M Cache, 1.50 GHz)
Celeron 807UE (1M Cache, 1.00 GHz)
Celeron 827E (1.5M Cache, 1.40 GHz)
Celeron 847 (2M Cache, 1.10 GHz)
Celeron 847E (2M Cache, 1.10 GHz)
Celeron 850 MHz, 128K Cache, 100 MHz FSB
Celeron 857 (2M Cache, 1.20 GHz)
Celeron 867 (2M Cache, 1.30 GHz)
Celeron 877 (2M Cache, 1.40 GHz)
Celeron 887 (2M Cache, 1.50 GHz)
Celeron 900 (1M Cache, 2.20 GHz, 800 MHz FSB)
Celeron 900 MHz, 128K Cache, 100 MHz FSB
Celeron 900 MHz, 256K Cache, 100 MHz FSB
Celeron 925 (1M Cache, 2.30 GHz, 800 MHz FSB)
Celeron 927UE (1M Cache, 1.50 GHz)
Celeron 950 MHz, 128K Cache, 100 MHz FSB
Celeron B710 (1.5M Cache, 1.60 GHz)
Celeron B720 (1.5M Cache, 1.70 GHz)
Celeron B800 (2M Cache, 1.50 GHz)
Celeron B810 (2M Cache, 1.60 GHz)
Celeron B810E (2M Cache, 1.60 GHz)
Celeron B815 (2M Cache, 1.60 GHz)
Celeron B820 (2M Cache, 1.70 GHz)
Celeron B830 (2M Cache, 1.80 GHz)
Celeron B840 (2M Cache, 1.90 GHz)
Celeron D 310 (256K Cache, 2.13 GHz, 533 MHz FSB)
Celeron D 315 (256K Cache, 2.26 GHz, 533 MHz FSB)
Celeron D 315/315J (256K Cache, 2.26 GHz, 533 MHz FSB)
Celeron D 320 (256K Cache, 2.40 GHz, 533 MHz FSB)
Celeron D 325 (256K Cache, 2.53 GHz, 533 MHz FSB)
Celeron D 325/325J (256K Cache, 2.53 GHz, 533 MHz FSB)
Celeron D 325J (256K Cache, 2.53 GHz, 533 MHz FSB)
Celeron D 326 (256K Cache, 2.53 GHz, 533 MHz FSB)
Celeron D 330 (256K Cache, 2.66 GHz, 533 MHz FSB)
Celeron D 330/330J (256K Cache, 2.66 GHz, 533 MHz FSB)
Celeron D 330J (256K Cache, 2.66 GHz, 533 MHz FSB)
Celeron D 331 (256K Cache, 2.66 GHz, 533 MHz FSB)
Celeron D 335 (256K Cache, 2.80 GHz, 533 MHz FSB)
Celeron D 335/335J (256K Cache, 2.80 GHz, 533 MHz FSB)
Celeron D 335J (256K Cache, 2.80 GHz, 533 MHz FSB)
Celeron D 336 (256K Cache, 2.80 GHz, 533 MHz FSB)
Celeron D 340 (256K Cache, 2.93 GHz, 533 MHz FSB)
Celeron D 340J (256K Cache, 2.93 GHz, 533 MHz FSB)
Celeron D 341 (256K Cache, 2.93 GHz, 533 MHz FSB)
Celeron D 345 (256K Cache, 3.06 GHz, 533 MHz FSB)
Celeron D 345J (256K Cache, 3.06 GHz, 533 MHz FSB)
Celeron D 346 (256K Cache, 3.06 GHz, 533 MHz FSB)
Celeron D 347 (512K Cache, 3.06 GHz, 533 MHz FSB)
Celeron D 350 (256K Cache, 3.20 GHz, 533 MHz FSB)
Celeron D 350/350J (256K Cache, 3.20 GHz, 533 MHz FSB)
Celeron D 351 (256K Cache, 3.20 GHz, 533 MHz FSB)
Celeron D 352 (512K Cache, 3.20 GHz, 533 MHz FSB)
Celeron D 355 (256K Cache, 3.33 GHz, 533 MHz FSB)
Celeron D 356 (512K Cache, 3.33 GHz, 533 MHz FSB)
Celeron D 360 (512K Cache, 3.46 GHz, 533 MHz FSB)
Celeron D 365 (512K Cache, 3.60 GHz, 533 MHz FSB)
Celeron E1200 (512K Cache, 1.60 GHz, 800 MHz FSB)
Celeron E1400 (512K Cache, 2.00 GHz, 800 MHz FSB)
Celeron E1500 (512K Cache, 2.20 GHz, 800 MHz FSB)
Celeron E1600 (512K Cache, 2.40 GHz, 800 MHz FSB)
Celeron E3200 (1M Cache, 2.40 GHz, 800 MHz FSB)
Celeron E3300 (1M Cache, 2.50 GHz, 800 MHz FSB)
Celeron E3400 (1M Cache, 2.60 GHz, 800 MHz FSB)
Celeron E3500 (1M Cache, 2.70 GHz, 800 MHz FSB)
Celeron G1101 (2M Cache, 2.26 GHz)
Celeron G1610 (2M Cache, 2.60 GHz)
Celeron G1610T (2M Cache, 2.30 GHz)
Celeron G1620 (2M Cache, 2.70 GHz)
Celeron G1620T (2M Cache, 2.40 GHz)
Celeron G1630 (2M Cache, 2.80 GHz)
Celeron G1820 (2M Cache, 2.70 GHz)
Celeron G1820T (2M Cache, 2.40 GHz)
Celeron G1820TE (2M Cache, 2.20 GHz)
Celeron G1830 (2M Cache, 2.80 GHz)
Celeron G1840 (2M Cache, 2.80 GHz)
Celeron G1840T (2M Cache, 2.50 GHz)
Celeron G1850 (2M Cache, 2.90 GHz)
Celeron G3900 (2M Cache, 2.80 GHz)
Celeron G3900E (2M Cache, 2.40 GHz)
Celeron G3900T (2M Cache, 2.60 GHz)
Celeron G3900TE (2M Cache, 2.30 GHz)
Celeron G3902E (2M Cache, 1.60 GHz)
Celeron G3920 (2M Cache, 2.90 GHz)
Celeron G3930 (2M Cache, 2.90 GHz)
Celeron G3930E (2M Cache, 2.90 GHz)
Celeron G3930T (2M Cache, 2.70 GHz)
Celeron G3930TE (2M Cache, 2.70 GHz)
Celeron G3950 (2M Cache, 3.00 GHz)
Celeron G440 (1M Cache, 1.60 GHz)
Celeron G460 (1.5M Cache, 1.80 GHz)
Celeron G465 (1.5M Cache, 1.90 GHz)
Celeron G470 (1.5M Cache, 2.00 GHz)
Celeron G530 (2M Cache, 2.40 GHz)
Celeron G530T (2M Cache, 2.00 GHz)
Celeron G540 (2M Cache, 2.50 GHz)
Celeron G540T (2M Cache, 2.10 GHz)
Celeron G550 (2M Cache, 2.60 GHz)
Celeron G550T (2M Cache, 2.20 GHz)
Celeron G555 (2M Cache, 2.70 GHz)
Celeron J1750 (1M Cache, 2.41 GHz)
Celeron J1800 (1M Cache, up to 2.58 GHz)
Celeron J1850 (2M Cache, 2.00 GHz)
Celeron J1900 (2M Cache, up to 2.42 GHz)
Celeron J3060 (2M Cache, up to 2.48 GHz)
Celeron J3160 (2M Cache, up to 2.24 GHz)
Celeron J3355 (2M Cache, up to 2.5 GHz)
Celeron J3455 (2M Cache, up to 2.3 GHz)
Celeron M 215 (512K Cache, 1.33 GHz, 533 MHz FSB)
Celeron M 310 (512K Cache, 1.20 GHz, 400 MHz FSB)
Celeron M 320 (512K Cache, 1.30 GHz, 400 MHz FSB)
Celeron M 330 (512K Cache, 1.40 GHz, 400 MHz FSB)
Celeron M 340 (512K Cache, 1.50 GHz, 400 MHz FSB)
Celeron M 350 (1M Cache, 1.30 GHz, 400 MHz FSB)
Celeron M 360 (1M Cache, 1.40 GHz, 400 MHz FSB)
Celeron M 370 (1M Cache, 1.50 GHz, 400 MHz FSB)
Celeron M 380 (1M Cache, 1.60 GHz, 400 MHz FSB)
Celeron M 390 (1M Cache, 1.70 GHz, 400 MHz FSB)
Celeron M 410 (1M Cache, 1.46 GHz, 533 MHz FSB)
Celeron M 420 (1M Cache, 1.60 GHz, 533 MHz FSB)
Celeron M 430 (1M Cache, 1.73 GHz, 533 MHz FSB)
Celeron M 440 (1M Cache, 1.86 GHz, 533 MHz FSB)
Celeron M 450 (1M Cache, 2.00 GHz, 533 MHz FSB)
Celeron M 520 (1M Cache, 1.60 GHz, 533 MHz FSB)
Celeron M 530 (1M Cache, 1.73 GHz, 533 MHz FSB) Socket M
Celeron M 600 MHz, 512K Cache, 400 MHz FSB
Celeron Mobile 1.00 GHz, 256K Cache, 133 MHz FSB
Celeron Mobile 1.06 GHz, 256K Cache, 133 MHz FSB
Celeron Mobile 1.13 GHz, 256K Cache, 133 MHz FSB
Celeron Mobile 1.20 GHz, 256K Cache, 133 MHz FSB
Celeron Mobile 1.20 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 1.33 GHz, 256K Cache, 133 MHz FSB
Celeron Mobile 1.40 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 1.50 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 1.60 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 1.70 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 1.80 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 2.00 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 2.20 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 2.40 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 2.50 GHz, 256K Cache, 400 MHz FSB
Celeron Mobile 266 MHz, 128K Cache, 66 MHz FSB
Celeron Mobile 300 MHz, 128K Cache, 66 MHz FSB
Celeron Mobile 366 MHz, 128K Cache, 66 MHz FSB
Celeron Mobile 400 MHz, 128K Cache, 66 MHz FSB
Celeron Mobile 433 MHz, 128K Cache, 66 MHz FSB
Celeron Mobile 450 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 466 MHz, 128K Cache, 66 MHz FSB
Celeron Mobile 500 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 550 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 600 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 650 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 700 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 733 MHz, 128K Cache, 133 MHz FSB
Celeron Mobile 733 MHz, 256K Cache, 133 MHz FSB
Celeron Mobile 750 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 800 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 800 MHz, 128K Cache, 133 MHz FSB
Celeron Mobile 850 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 866 MHz, 128K Cache, 133 MHz FSB
Celeron Mobile 900 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile 933 MHz, 128K Cache, 133 MHz FSB
Celeron Mobile LV 400 MHz, 128K Cache, 100 MHz FSB
Celeron Mobile LV 650 MHz, 256K Cache, 100 MHz FSB
Celeron M ULV 333 (512K Cache, 900 MHz, 400 MHz FSB)
Celeron M ULV 353 (512K Cache, 900 MHz, 400 MHz FSB)
Celeron M ULV 373 (512K Cache, 1.00 GHz, 400 MHz FSB)
Celeron M ULV 383 (1M Cache, 1.00 GHz, 400 MHz FSB)
Celeron M ULV 423 (1M Cache, 1.06 GHz, 533 MHz FSB)
Celeron M ULV 443 (1M Cache, 1.20 GHz, 533 MHz FSB)
Celeron M ULV 523 (1M Cache, 933 MHz, 533 MHz FSB)
Celeron M ULV 722 (1M Cache, 1.20 GHz, 800 MHz FSB)
Celeron M ULV 723 (1M Cache, 1.20 GHz, 800 MHz FSB)
Celeron M ULV 743 (1M Cache, 1.30 GHz, 800 MHz FSB)
Celeron M ULV 800 MHz, 512K Cache, 400 MHz FSB
Celeron N2805 (1M Cache, 1.46 GHz)
Celeron N2806 (1M Cache, up to 2.00 GHz)
Celeron N2807 (1M Cache, up to 2.16 GHz)
Celeron N2808 (1M Cache, up to 2.25 GHz)
Celeron N2810 (1M Cache, 2.00 GHz)
Celeron N2815 (1M Cache, up to 2.13 GHz)
Celeron N2820 (1M Cache, up to 2.39 GHz)
Celeron N2830 (1M Cache, up to 2.41 GHz)
Celeron N2840 (1M Cache, up to 2.58 GHz)
Celeron N2910 (2M Cache, 1.60 GHz)
Celeron N2920 (2M Cache, up to 2.00 GHz)
Celeron N2930 (2M Cache, up to 2.16 GHz)
Celeron N2940 (2M Cache, up to 2.25 GHz)
Celeron N3000 (2M Cache, up to 2.08 GHz)
Celeron N3010 (2M Cache, up to 2.24 GHz)
Celeron N3050 (2M Cache, up to 2.16 GHz)
Celeron N3060 (2M Cache, up to 2.48 GHz)
Celeron N3150 (2M Cache, up to 2.08 GHz)
Celeron N3160 (2M Cache, up to 2.24 GHz)
Celeron N3350 (2M Cache, up to 2.4 GHz)
Celeron N3450 (2M Cache, up to 2.2 GHz)
Celeron P1053 (2M Cache, 1.33 GHz)
Celeron P4500 (2M Cache, 1.86 GHz)
Celeron P4505 (2M Cache, 1.86 GHz)
Celeron P4600 (2M Cache, 2.00 GHz)
Celeron SU2300 (1M Cache, 1.20 GHz, 800 MHz FSB)
Celeron T1600 (1M Cache, 1.66 GHz, 667 MHz FSB)
Celeron T1700 (1M Cache, 1.83 GHz, 667 MHz FSB)
Celeron T3000 (1M Cache, 1.80 GHz, 800 MHz FSB)
Celeron T3100 (1M Cache, 1.90 GHz, 800 MHz FSB)
Celeron T3300 (1M Cache, 2.00 GHz, 800 MHz FSB)
Celeron T3500 (1M Cache, 2.10 GHz, 800 MHz FSB)
Celeron U3400 (2M Cache, 1.06 GHz)
Celeron U3405 (2M Cache, 1.07 GHz)
Celeron U3600 (2M Cache, 1.20 GHz)
Celeron ULV 573 (512K Cache, 1.00 GHz, 533 MHz FSB)
Celeron ULV 763 (1M Cache, 1.40 GHz, 800 MHz FSB)
Core2 Duo E4300 (2M Cache, 1.80 GHz, 800 MHz FSB)
Core2 Duo E4400 (2M Cache, 2.00 GHz, 800 MHz FSB)
Core2 Duo E4500 (2M Cache, 2.20 GHz, 800 MHz FSB)
Core2 Duo E4600 (2M Cache, 2.40 GHz, 800 MHz FSB)
Core2 Duo E4700 (2M Cache, 2.60 GHz, 800 MHz FSB)
Core2 Duo E6300 (2M Cache, 1.86 GHz, 1066 MHz FSB)
Core2 Duo E6305 (2M Cache, 1.86 GHz, 1066 MHz FSB)
Core2 Duo E6320 (4M Cache, 1.86 GHz, 1066 MHz FSB)
Core2 Duo E6400 (2M Cache, 2.13 GHz, 1066 MHz FSB)
Core2 Duo E6405 (2M Cache, 2.13 GHz, 1066 MHz FSB)
Core2 Duo E6420 (4M Cache, 2.13 GHz, 1066 MHz FSB)
Core2 Duo E6540 (4M Cache, 2.33 GHz, 1333 MHz FSB)
Core2 Duo E6550 (4M Cache, 2.33 GHz, 1333 MHz FSB)
Core2 Duo E6600 (4M Cache, 2.40 GHz, 1066 MHz FSB)
Core2 Duo E6700 (4M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Duo E6750 (4M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Duo E6850 (4M Cache, 3.00 GHz, 1333 MHz FSB)
Core2 Duo E7200 (3M Cache, 2.53 GHz, 1066 MHz FSB)
Core2 Duo E7300 (3M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Duo E7400 (3M Cache, 2.80 GHz, 1066 MHz FSB)
Core2 Duo E7500 (3M Cache, 2.93 GHz, 1066 MHz FSB)
Core2 Duo E7600 (3M Cache, 3.06 GHz, 1066 MHz FSB)
Core2 Duo E8190 (6M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Duo E8200 (6M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Duo E8300 (6M Cache, 2.83 GHz, 1333 MHz FSB)
Core2 Duo E8400 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Core2 Duo E8500 (6M Cache, 3.16 GHz, 1333 MHz FSB)
Core2 Duo E8600 (6M Cache, 3.33 GHz, 1333 MHz FSB)
Core2 Duo L7200 (4M Cache, 1.33 GHz, 667 MHz FSB)
Core2 Duo L7300 (4M Cache, 1.40 GHz, 800 MHz FSB)
Core2 Duo L7400 (4M Cache, 1.50 GHz, 667 MHz FSB)
Core2 Duo L7500 (4M Cache, 1.60 GHz, 800 MHz FSB)
Core2 Duo L7700 (4M Cache, 1.80 GHz, 800 MHz FSB)
Core2 Duo P7350 (3M Cache, 2.00 GHz, 1066 MHz FSB)
Core2 Duo P7370 (3M Cache, 2.00 GHz, 1066 MHz FSB)
Core2 Duo P7450 (3M Cache, 2.13 GHz, 1066 MHz FSB)
Core2 Duo P7550 (3M Cache, 2.26 GHz, 1066 MHz FSB)
Core2 Duo P7570 (3M Cache, 2.26 GHz, 1066 MHz FSB)
Core2 Duo P8400 (3M Cache, 2.26 GHz, 1066 MHz FSB)
Core2 Duo P8600 (3M Cache, 2.40 GHz, 1066 MHz FSB)
Core2 Duo P8700 (3M Cache, 2.53 GHz, 1066 MHz FSB)
Core2 Duo P8800 (3M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Duo P9500 (6M Cache, 2.53 GHz, 1066 MHz FSB)
Core2 Duo P9600 (6M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Duo P9700 (6M Cache, 2.80 GHz, 1066 MHz FSB)
Core2 Duo SL9300 (6M Cache, 1.60 GHz, 1066 MHz FSB)
Core2 Duo SL9380 (6M Cache, 1.80 GHz, 800 MHz FSB)
Core2 Duo SL9400 (6M Cache, 1.86 GHz, 1066 MHz FSB)
Core2 Duo SL9600 (6M Cache, 2.13 GHz, 1066 MHz FSB)
Core2 Duo SP9300 (6M Cache, 2.26 GHz, 1066 MHz FSB)
Core2 Duo SP9400 (6M Cache, 2.40 GHz, 1066 MHz FSB)
Core2 Duo SP9600 (6M Cache, 2.53 GHz, 1066 MHz FSB)
Core2 Duo SU7300 (3M Cache, 1.30 GHz, 800 MHz FSB)
Core2 Duo SU9300 (3M Cache, 1.20 GHz, 800 MHz FSB)
Core2 Duo SU9400 (3M Cache, 1.40 GHz, 800 MHz FSB)
Core2 Duo SU9600 (3M Cache, 1.60 GHz, 800 MHz, FSB)
Core2 Duo T5200 (2M Cache, 1.60 GHz, 533 MHz FSB)
Core2 Duo T5250 (2M Cache, 1.50 GHz, 667 MHz FSB)
Core2 Duo T5270 (2M Cache, 1.40 GHz, 800 MHz FSB)
Core2 Duo T5300 (2M Cache, 1.73 GHz, 533 MHz FSB)
Core2 Duo T5450 (2M Cache, 1.66 GHz, 667 MHz FSB)
Core2 Duo T5470 (2M Cache, 1.60 GHz, 800 MHz FSB)
Core2 Duo T5500 (2M Cache, 1.66 GHz, 667 MHz FSB)
Core2 Duo T5550 (2M Cache, 1.83 GHz, 667 MHz FSB)
Core2 Duo T5600 (2M Cache, 1.83 GHz, 667 MHz FSB)
Core2 Duo T5670 (2M Cache, 1.80 GHz, 800 MHz FSB)
Core2 Duo T5750 (2M Cache, 2.00 GHz, 667 MHz FSB)
Core2 Duo T5800 (2M Cache, 2.00 GHz, 800 MHz FSB)
Core2 Duo T5870 (2M Cache, 2.00 GHz, 800 MHz FSB)
Core2 Duo T6400 (2M Cache, 2.00 GHz, 800 MHz FSB)
Core2 Duo T6500 (2M Cache, 2.10 GHz, 800 MHz FSB)
Core2 Duo T6570 (2M Cache, 2.10 GHz, 800 MHz FSB)
Core2 Duo T6600 (2M Cache, 2.20 GHz, 800 MHz FSB)
Core2 Duo T6670 (2M Cache, 2.20 GHz, 800 MHz FSB)
Core2 Duo T7100 (2M Cache, 1.80 GHz, 800 MHz FSB)
Core2 Duo T7200 (4M Cache, 2.00 GHz, 667 MHz FSB)
Core2 Duo T7250 (2M Cache, 2.00 GHz, 800 MHz FSB)
Core2 Duo T7300 (4M Cache, 2.00 GHz, 800 MHz FSB)
Core2 Duo T7400 (4M Cache, 2.16 GHz, 667 MHz FSB)
Core2 Duo T7500 (4M Cache, 2.20 GHz, 800 MHz FSB)
Core2 Duo T7600 (4M Cache, 2.33 GHz, 667 MHz FSB)
Core2 Duo T7700 (4M Cache, 2.40 GHz, 800 MHz FSB)
Core2 Duo T7800 (4M Cache, 2.60 GHz, 800 MHz FSB)
Core2 Duo T8100 (3M Cache, 2.10 GHz, 800 MHz FSB)
Core2 Duo T8300 (3M Cache, 2.40 GHz, 800 MHz FSB)
Core2 Duo T9300 (6M Cache, 2.50 GHz, 800 MHz FSB)
Core2 Duo T9400 (6M Cache, 2.53 GHz, 1066 MHz FSB)
Core2 Duo T9500 (6M Cache, 2.60 GHz, 800 MHz FSB)
Core2 Duo T9550 (6M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Duo T9600 (6M Cache, 2.80 GHz, 1066 MHz FSB)
Core2 Duo T9800 (6M Cache, 2.93 GHz, 1066 MHz FSB)
Core2 Duo T9900 (6M Cache, 3.06 GHz, 1066 MHz FSB)
Core2 Duo U7500 (2M Cache, 1.06 GHz, 533 MHz FSB) Socket M
Core2 Duo U7500 (2M Cache, 1.06 GHz, 533 MHz FSB) Socket P
Core2 Duo U7600 (2M Cache, 1.20 GHz, 533 MHz FSB) Socket M
Core2 Duo U7600 (2M Cache, 1.20 GHz, 533 MHz FSB) Socket P
Core2 Duo U7700 (2M Cache, 1.33 GHz, 533 MHz FSB) Socket P
Core2 Extreme QX6700 (8M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Extreme QX6800 (8M Cache, 2.93 GHz, 1066 MHz FSB)
Core2 Extreme QX6850 (8M Cache, 3.00 GHz, 1333 MHz FSB)
Core2 Extreme QX9300 (12M Cache, 2.53 GHz, 1066 MHz FSB)
Core2 Extreme QX9650 (12M Cache, 3.00 GHz, 1333 MHz FSB)
Core2 Extreme QX9770 (12M Cache, 3.20 GHz, 1600 MHz FSB)
Core2 Extreme QX9775 (12M Cache, 3.20 GHz, 1600 MHz FSB)
Core2 Extreme X6800 (4M Cache, 2.93 GHz, 1066 MHz FSB)
Core2 Extreme X7800 (4M Cache, 2.60 GHz, 800 MHz FSB)
Core2 Extreme X7900 (4M Cache, 2.80 GHz, 800 MHz FSB)
Core2 Extreme X9000 (6M Cache, 2.80 GHz, 800 MHz FSB)
Core2 Extreme X9100 (6M Cache, 3.06 GHz, 1066 MHz FSB)
Core2 Quad Q6600 (8M Cache, 2.40 GHz, 1066 MHz FSB)
Core2 Quad Q6700 (8M Cache, 2.66 GHz, 1066 MHz FSB)
Core2 Quad Q8200 (4M Cache, 2.33 GHz, 1333 MHz FSB)
Core2 Quad Q8200S (4M Cache, 2.33 GHz, 1333 MHz FSB)
Core2 Quad Q8300 (4M Cache, 2.50 GHz, 1333 MHz FSB)
Core2 Quad Q8400 (4M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Quad Q8400S (4M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Quad Q9000 (6M Cache, 2.00 GHz, 1066 MHz FSB)
Core2 Quad Q9100 (12M Cache, 2.26 GHz, 1066 MHz FSB)
Core2 Quad Q9300 (6M Cache, 2.50 GHz, 1333 MHz FSB)
Core2 Quad Q9400 (6M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Quad Q9400S (6M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Quad Q9450 (12M Cache, 2.66 GHz, 1333 MHz FSB)
Core2 Quad Q9500 (6M Cache, 2.83 GHz, 1333 MHz FSB)
Core2 Quad Q9505 (6M Cache, 2.83 GHz, 1333 MHz FSB)
Core2 Quad Q9505S (6M Cache, 2.83 GHz, 1333 MHz FSB)
Core2 Quad Q9550 (12M Cache, 2.83 GHz, 1333 MHz FSB)
Core2 Quad Q9550S (12M Cache, 2.83 GHz, 1333 MHz FSB)
Core2 Quad Q9650 (12M Cache, 3.00 GHz, 1333 MHz FSB)
Core2 Solo SL3400 (3M Cache, 1.86 GHz, 1066 MHz FSB)
Core2 Solo SU3500 (3M Cache, 1.30 GHz, 800 MHz FSB)
Core2 Solo U2100 (1M Cache, 1.06 GHz, 533 MHz FSB)
Core2 Solo U2200 (1M Cache, 1.20 GHz, 533 MHz FSB)
Core2 Solo ULV SU3300 (3M Cache, 1.20 GHz, 800 MHz FSB)
Core2 Solo ULV SU3500 (3M Cache, 1.40 GHz, 800 MHz FSB)
Core Duo L2300 (2M Cache, 1.50 GHz, 667 MHz FSB)
Core Duo L2400 (2M Cache, 1.66 GHz, 667 MHz FSB)
Core Duo L2500 (2M Cache, 1.83 GHz, 667 MHz FSB)
Core Duo T2050 (2M Cache, 1.60 GHz, 533 MHz FSB)
Core Duo T2250 (2M Cache, 1.73 GHz, 533 MHz FSB)
Core Duo T2300 (2M Cache, 1.66 GHz, 667 MHz FSB)
Core Duo T2300E (2M Cache, 1.66 GHz, 667 MHz FSB)
Core Duo T2350 (2M Cache, 1.86 GHz, 533 MHz FSB)
Core Duo T2400 (2M Cache, 1.83 GHz, 667 MHz FSB)
Core Duo T2450 (2M Cache, 2.00 GHz, 533 MHz FSB)
Core Duo T2500 (2M Cache, 2.00 GHz, 667 MHz FSB)
Core Duo T2600 (2M Cache, 2.16 GHz, 667 MHz FSB)
Core Duo T2700 (2M Cache, 2.33 GHz, 667 MHz FSB)
Core Duo U2400 (2M Cache, 1.06 GHz, 533 MHz FSB)
Core Duo U2500 (2M Cache, 1.20 GHz, 533 MHz FSB)
Core i3-2100 (3M Cache, 3.10 GHz)
Core i3-2100T (3M Cache, 2.50 GHz)
Core i3-2102 (3M Cache, 3.10 GHz)
Core i3-2105 (3M Cache, 3.10 GHz)
Core i3-2115C (3M Cache, 2.00 GHz)
Core i3-2120 (3M Cache, 3.30 GHz)
Core i3-2120T (3M Cache, 2.60 GHz)
Core i3-2125 (3M Cache, 3.30 GHz)
Core i3-2130 (3M Cache, 3.40 GHz)
Core i3-2310E (3M Cache, 2.10 GHz)
Core i3-2310M (3M Cache, 2.10 GHz)
Core i3-2312M (3M Cache, 2.10 GHz)
Core i3-2328M (3M Cache, 2.20 GHz)
Core i3-2330E (3M Cache, 2.20 GHz)
Core i3-2330M (3M Cache, 2.20 GHz)
Core i3-2340UE (3M Cache, 1.30 GHz)
Core i3-2348M (3M Cache, 2.30 GHz)
Core i3-2350M (3M Cache, 2.30 GHz)
Core i3-2357M (3M Cache, 1.30 GHz)
Core i3-2365M (3M Cache, 1.40 GHz)
Core i3-2367M (3M Cache, 1.40 GHz)
Core i3-2370M (3M Cache, 2.40 GHz)
Core i3-2375M (3M Cache, 1.50 GHz)
Core i3-2377M (3M Cache, 1.50 GHz)
Core i3-3110M (3M Cache, 2.40 GHz)
Core i3-3115C (4M Cache, 2.50 GHz)
Core i3-3120M (3M Cache, 2.50 GHz)
Core i3-3120ME (3M Cache, 2.40 GHz)
Core i3-3130M (3M Cache, 2.60 GHz)
Core i3-3210 (3M Cache, 3.20 GHz)
Core i3-3217U (3M Cache, 1.80 GHz)
Core i3-3217UE (3M Cache, 1.60 GHz)
Core i3-3220 (3M Cache, 3.30 GHz)
Core i3-3220T (3M Cache, 2.80 GHz)
Core i3-3225 (3M Cache, 3.30 GHz)
Core i3-3227U (3M Cache, 1.90 GHz)
Core i3-3229Y (3M Cache, 1.40 GHz)
Core i3-3240 (3M Cache, 3.40 GHz)
Core i3-3240T (3M Cache, 2.90 GHz)
Core i3-3245 (3M Cache, 3.40 GHz)
Core i3-3250 (3M Cache, 3.50 GHz)
Core i3-3250T (3M Cache, 3.00 GHz)
Core i3-330E (3M Cache, 2.13 GHz)
Core i3-330M (3M Cache, 2.13 GHz)
Core i3-330UM (3M cache, 1.20 GHz)
Core i3-350M (3M Cache, 2.26 GHz)
Core i3-370M (3M cache, 2.40 GHz)
Core i3-380M (3M Cache, 2.53 GHz)
Core i3-380UM (3M Cache, 1.33 GHz)
Core i3-390M (3M Cache, 2.66 GHz)
Core i3-4000M (3M Cache, 2.40 GHz)
Core i3-4005U (3M Cache, 1.70 GHz)
Core i3-4010U (3M Cache, 1.70 GHz)
Core i3-4010Y (3M Cache, 1.30 GHz)
Core i3-4012Y (3M Cache, 1.50 GHz)
Core i3-4020Y (3M Cache, 1.50 GHz)
Core i3-4025U (3M Cache, 1.90 GHz)
Core i3-4030U (3M Cache, 1.90 GHz)
Core i3-4030Y (3M Cache, 1.60 GHz)
Core i3-4100E (3M Cache, 2.40 GHz)
Core i3-4100M (3M Cache, 2.50 GHz)
Core i3-4100U (3M Cache, 1.80 GHz)
Core i3-4102E (3M Cache, 1.60 GHz)
Core i3-4110E (3M Cache, 2.60 GHz)
Core i3-4110M (3M Cache, 2.60 GHz)
Core i3-4112E (3M Cache, 1.80 GHz)
Core i3-4120U (3M Cache, 2.00 GHz)
Core i3-4130 (3M Cache, 3.40 GHz)
Core i3-4130T (3M Cache, 2.90 GHz)
Core i3-4150 (3M Cache, 3.50 GHz)
Core i3-4150T (3M Cache, 3.00 GHz)
Core i3-4158U (3M Cache, 2.00 GHz)
Core i3-4160 (3M Cache, 3.60 GHz)
Core i3-4160T (3M Cache, 3.10 GHz)
Core i3-4170 (3M Cache, 3.70 GHz)
Core i3-4170T (3M Cache, 3.20 GHz)
Core i3-4330 (4M Cache, 3.50 GHz)
Core i3-4330T (4M Cache, 3.00 GHz)
Core i3-4330TE (4M Cache, 2.40 GHz)
Core i3-4340 (4M Cache, 3.60 GHz)
Core i3-4340TE (4M Cache, 2.60 GHz)
Core i3-4350 (4M Cache, 3.60 GHz)
Core i3-4350T (4M Cache, 3.10 GHz)
Core i3-4360 (4M Cache, 3.70 GHz)
Core i3-4360T (4M Cache, 3.20 GHz)
Core i3-4370 (4M Cache, 3.80 GHz)
Core i3-4370T (4M Cache, 3.30 GHz)
Core i3-5005U (3M Cache, 2.00 GHz)
Core i3-5010U (3M Cache, 2.10 GHz)
Core i3-5015U (3M Cache, 2.10 GHz)
Core i3-5020U (3M Cache, 2.20 GHz)
Core i3-5157U (3M Cache, 2.50 GHz)
Core i3-530 (4M Cache, 2.93 GHz)
Core i3-540 (4M Cache, 3.06 GHz)
Core i3-550 (4M Cache, 3.20 GHz)
Core i3-560 (4M Cache, 3.33 GHz)
Core i3-6006U (3M Cache, 2.00 GHz)
Core i3-6098P (3M Cache, 3.60 GHz)
Core i3-6100 (3M Cache, 3.70 GHz)
Core i3-6100E (3M Cache, 2.70 GHz)
Core i3-6100H (3M Cache, 2.70 GHz)
Core i3-6100T (3M Cache, 3.20 GHz)
Core i3-6100TE (4M Cache, 2.70 GHz)
Core i3-6100U (3M Cache, 2.30 GHz)
Core i3-6102E (3M Cache, 1.90 GHz)
Core i3-6157U (3M Cache, 2.40 GHz)
Core i3-6167U (3M Cache, 2.70 GHz)
Core i3-6300 (4M Cache, 3.80 GHz)
Core i3-6300T (4M Cache, 3.30 GHz)
Core i3-6320 (4M Cache, 3.90 GHz)
Core i3-7100 (3M Cache, 3.90 GHz)
Core i3-7100E (3M Cache, 2.90 GHz)
Core i3-7100H (3M Cache, 3.00 GHz)
Core i3-7100T (3M Cache, 3.40 GHz)
Core i3-7100U (3M Cache, 2.40 GHz)
Core i3-7101E (3M Cache, 3.90 GHz)
Core i3-7101TE (3M Cache, 3.40 GHz)
Core i3-7102E (3M Cache, 2.10 GHz)
Core i3-7130U (3M Cache, 2.70 GHz)
Core i3-7167U (3M Cache, 2.80 GHz)
Core i3-7300 (4M Cache, 4.00 GHz)
Core i3-7300T (4M Cache, 3.50 GHz)
Core i3-7320 (4M Cache, 4.10 GHz)
Core i3-7350K (4M Cache, 4.20 GHz)
Core i3-8100 (6M Cache, 3.60 GHz)
Core i3-8350K (8M Cache, 4.00 GHz)
Core i5-2300 (6M Cache, up to 3.10 GHz)
Core i5-2310 (6M Cache, up to 3.20 GHz)
Core i5-2320 (6M Cache, up to 3.30 GHz)
Core i5-2380P (6M Cache, up to 3.40 GHz)
Core i5-2390T (3M Cache, up to 3.50 GHz)
Core i5-2400 (6M Cache, up to 3.40 GHz)
Core i5-2400S (6M Cache, up to 3.30 GHz)
Core i5-2405S (6M Cache, up to 3.30 GHz)
Core i5-2410M (3M Cache, up to 2.90 GHz)
Core i5-2415M (3M Cache, up to 2.90 GHz)
Core i5-2430M (3M Cache, up to 3.00 GHz)
Core i5-2435M (3M Cache, up to 3.00 GHz)
Core i5-2450M (3M Cache, up to 3.10 GHz)
Core i5-2450P (6M Cache, up to 3.50 GHz)
Core i5-2467M (3M Cache, up to 2.30 GHz)
Core i5-2500 (6M Cache, up to 3.70 GHz)
Core i5-2500K (6M Cache, up to 3.70 GHz)
Core i5-2500S (6M Cache, up to 3.70 GHz)
Core i5-2500T (6M Cache, up to 3.30 GHz)
Core i5-2510E (3M Cache, up to 3.10 GHz)
Core i5-2515E (3M Cache, up to 3.10 GHz)
Core i5-2520M (3M Cache, up to 3.20 GHz)
Core i5-2537M (3M Cache, up to 2.30 GHz)
Core i5-2540M (3M Cache, up to 3.30 GHz)
Core i5-2550K (6M Cache, up to 3.80 GHz)
Core i5-2557M (3M Cache, up to 2.70 GHz)
Core i5-3210M (3M Cache, up to 3.10 GHz) BGA
Core i5-3210M (3M Cache, up to 3.10 GHz, rPGA
Core i5-3230M (3M Cache, up to 3.20 GHz) BGA
Core i5-3230M (3M Cache, up to 3.20 GHz) rPGA
Core i5-3317U (3M Cache, up to 2.60 GHz)
Core i5-3320M (3M Cache, up to 3.30 GHz)
Core i5-3330 (6M Cache, up to 3.20 GHz)
Core i5-3330S (6M Cache, up to 3.20 GHz)
Core i5-3337U (3M Cache, up to 2.70 GHz)
Core i5-3339Y (3M Cache, up to 2.00 GHz)
Core i5-3340 (6M Cache, up to 3.30 GHz)
Core i5-3340M (3M Cache, up to 3.40 GHz)
Core i5-3340S (6M Cache, up to 3.30 GHz)
Core i5-3350P (6M Cache, up to 3.30 GHz)
Core i5-3360M (3M Cache, up to 3.50 GHz)
Core i5-3380M (3M Cache, up to 3.60 GHz)
Core i5-3427U (3M Cache, up to 2.80 GHz)
Core i5-3437U (3M Cache, up to 2.90 GHz)
Core i5-3439Y (3M Cache, up to 2.30 GHz)
Core i5-3450 (6M Cache, up to 3.50 GHz)
Core i5-3450S (6M Cache, up to 3.50 GHz)
Core i5-3470 (6M Cache, up to 3.60 GHz)
Core i5-3470S (6M Cache, up to 3.60 GHz)
Core i5-3470T (3M Cache, up to 3.60 GHz)
Core i5-3475S (6M Cache, up to 3.60 GHz)
Core i5-3550 (6M Cache, up to 3.70 GHz)
Core i5-3550S (6M Cache, up to 3.70 GHz)
Core i5-3570 (6M Cache, up to 3.80 GHz)
Core i5-3570K (6M Cache, up to 3.80 GHz)
Core i5-3570S (6M Cache, up to 3.80 GHz)
Core i5-3570T (6M Cache, up to 3.30 GHz)
Core i5-3610ME (3M Cache, up to 3.30 GHz)
Core i5-4200H (3M Cache, up to 3.40 GHz)
Core i5-4200M (3M Cache, up to 3.10 GHz)
Core i5-4200U (3M Cache, up to 2.60 GHz)
Core i5-4200Y (3M Cache, up to 1.90 GHz)
Core i5-4202Y (3M Cache, up to 2.00 GHz)
Core i5-4210H (3M Cache, up to 3.50 GHz)
Core i5-4210M (3M Cache, up to 3.20 GHz)
Core i5-4210U (3M Cache, up to 2.70 GHz)
Core i5-4210Y (3M Cache, up to 1.90 GHz)
Core i5-4220Y (3M Cache, up to 2.00 GHz)
Core i5-4250U (3M Cache, up to 2.60 GHz)
Core i5-4258U (3M Cache, up to 2.90 GHz)
Core i5-4260U (3M Cache, up to 2.70 GHz)
Core i5-4278U (3M Cache, up to 3.10 GHz)
Core i5-4288U (3M Cache, up to 3.10 GHz)
Core i5-4300M (3M Cache, up to 3.30 GHz)
Core i5-4300U (3M Cache, up to 2.90 GHz)
Core i5-4300Y (3M Cache, up to 2.30 GHz)
Core i5-4302Y (3M Cache, up to 2.30 GHz)
Core i5-4308U (3M Cache, up to 3.30 GHz)
Core i5-430M (3M Cache, 2.26 GHz)
Core i5-430UM (3M cache, 1.20 GHz)
Core i5-4310M (3M Cache, up to 3.40 GHz)
Core i5-4310U (3M Cache, up to 3.00 GHz)
Core i5-4330M (3M Cache, up to 3.50 GHz)
Core i5-4340M (3M Cache, up to 3.60 GHz)
Core i5-4350U (3M Cache, up to 2.90 GHz)
Core i5-4360U (3M Cache, up to 3.00 GHz)
Core i5-4400E (3M Cache, up to 3.30 GHz)
Core i5-4402E (3M Cache, up to 2.70 GHz)
Core i5-4402EC (4M Cache, up to 2.50 GHz)
Core i5-4410E (3M Cache, up to 2.90 GHz)
Core i5-4422E (3M Cache, up to 2.90 GHz)
Core i5-4430 (6M Cache, up to 3.20 GHz)
Core i5-4430S (6M Cache, up to 3.20 GHz)
Core i5-4440 (6M Cache, up to 3.30 GHz)
Core i5-4440S (6M Cache, up to 3.30 GHz)
Core i5-4460 (6M Cache, up to 3.40 GHz)
Core i5-4460S (6M Cache, up to 3.40 GHz)
Core i5-4460T (6M Cache, up to 2.70 GHz)
Core i5-450M (3M cache, 2.40 GHz)
Core i5-4570 (6M Cache, up to 3.60 GHz)
Core i5-4570R (4M Cache, up to 3.20 GHz)
Core i5-4570S (6M Cache, up to 3.60 GHz)
Core i5-4570T (4M Cache, up to 3.60 GHz)
Core i5-4570TE (4M Cache, up to 3.30 GHz)
Core i5-4590 (6M Cache, up to 3.70 GHz)
Core i5-4590S (6M Cache, up to 3.70 GHz)
Core i5-4590T (6M Cache, up to 3.00 GHz)
Core i5-460M (3M Cache, 2.53 GHz)
Core i5-4670 (6M Cache, up to 3.80 GHz)
Core i5-4670K (6M Cache, up to 3.80 GHz)
Core i5-4670R (4M Cache, up to 3.70 GHz)
Core i5-4670S (6M Cache, up to 3.80 GHz)
Core i5-4670T (6M Cache, up to 3.30 GHz)
Core i5-4690 (6M Cache, up to 3.90 GHz)
Core i5-4690K (6M Cache, up to 3.90 GHz)
Core i5-4690S (6M Cache, up to 3.90 GHz)
Core i5-4690T (6M Cache, up to 3.50 GHz)
Core i5-470UM (3M Cache, 1.33 GHz)
Core i5-480M (3M Cache, 2.66 GHz)
Core i5-5200U (3M Cache, up to 2.70 GHz)
Core i5-520E (3M Cache, 2.40 GHz)
Core i5-520M (3M Cache, 2.40 GHz)
Core i5-520UM (3M Cache, 1.06 GHz)
Core i5-5250U (3M Cache, up to 2.70 GHz)
Core i5-5257U (3M Cache, up to 3.10 GHz)
Core i5-5287U (3M Cache, up to 3.30 GHz)
Core i5-5300U (3M Cache, up to 2.90 GHz)
Core i5-5350H (4M Cache, up to 3.50 GHz)
Core i5-5350U (3M Cache, up to 2.90 GHz)
Core i5-540M (3M Cache, 2.53 GHz)
Core i5-540UM (3M Cache, 1.20 GHz)
Core i5-5575R (4M Cache, up to 3.30 GHz)
Core i5-560M (3M Cache, 2.66 GHz)
Core i5-560UM (3M Cache, 1.33 GHz)
Core i5-5675C (4M Cache, up to 3.60 GHz)
Core i5-5675R (4M Cache, up to 3.60 GHz)
Core i5-580M (3M Cache, 2.66 GHz)
Core i5-6198DU (3M Cache, up to 2.80 GHz)
Core i5-6200U (3M Cache, up to 2.80 GHz)
Core i5-6260U (4M Cache, up to 2.90 GHz)
Core i5-6267U (4M Cache, up to 3.30 GHz)
Core i5-6287U (4M Cache, up to 3.50 GHz)
Core i5-6300HQ (6M Cache, up to 3.20 GHz)
Core i5-6300U (3M Cache, up to 3.00 GHz)
Core i5-6350HQ (6M Cache, up to 3.20 GHz)
Core i5-6360U (4M Cache, up to 3.10 GHz)
Core i5-6400 (6M Cache, up to 3.30 GHz)
Core i5-6400T (6M Cache, up to 2.80 GHz)
Core i5-6402P (6M Cache, up to 3.40 GHz)
Core i5-6440EQ (6M Cache, up to 3.40 GHz)
Core i5-6440HQ (6M Cache, up to 3.50 GHz)
Core i5-6442EQ (6M Cache, up to 2.70 GHz)
Core i5-6500 (6M Cache, up to 3.60 GHz)
Core i5-6500T (6M Cache, up to 3.10 GHz)
Core i5-6500TE (6M Cache, up to 3.30 GHz)
Core i5-650 (4M Cache, 3.20 GHz)
Core i5-655K (4M Cache, 3.20 GHz)
Core i5-6585R (6M Cache, up to 3.60 GHz)
Core i5-6600 (6M Cache, up to 3.90 GHz)
Core i5-6600K (6M Cache, up to 3.90 GHz)
Core i5-6600T (6M Cache, up to 3.50 GHz)
Core i5-660 (4M Cache, 3.33 GHz)
Core i5-661 (4M Cache, 3.33 GHz)
Core i5-6685R (6M Cache, up to 3.80 GHz)
Core i5-670 (4M Cache, 3.46 GHz)
Core i5-680 (4M Cache, 3.60 GHz)
Core i5-7200U (3M Cache, up to 3.10 GHz)
Core i5-7260U (4M Cache, up to 3.40 GHz)
Core i5-7267U (4M Cache, up to 3.50 GHz)
Core i5-7287U (4M Cache, up to 3.70 GHz)
Core i5-7300HQ (6M Cache, up to 3.50 GHz)
Core i5-7300U (3M Cache, up to 3.50 GHz)
Core i5-7360U (4M Cache, up to 3.60 GHz)
Core i5-7400 (6M Cache, up to 3.50 GHz)
Core i5-7400T (6M Cache, up to 3.00 GHz)
Core i5-7440EQ (6M Cache, up to 3.60 GHz)
Core i5-7440HQ (6M Cache, up to 3.80 GHz)
Core i5-7442EQ (6M Cache, up to 2.90 GHz)
Core i5-7500 (6M Cache, up to 3.80 GHz)
Core i5-7500T (6M Cache, up to 3.30 GHz)
Core i5-750 (8M Cache, 2.66 GHz)
Core i5-750S (8M Cache, 2.40 GHz)
Core i5-7600 (6M Cache, up to 4.10 GHz)
Core i5-7600K (6M Cache, up to 4.20 GHz)
Core i5-7600T (6M Cache, up to 3.70 GHz)
Core i5-760 (8M Cache, 2.80 GHz)
Core i5-7640X X-series (6M Cache, up to 4.20 GHz)
Core i5-7Y54 (4M Cache, up to 3.20 GHz)
Core i5-7Y57 (4M Cache, up to 3.30 GHz)
Core i5-8250U (6M Cache, up to 3.40 GHz)
Core i5-8350U (6M Cache, up to 3.60 GHz)
Core i5-8400 (9M Cache, up to 4.00 GHz)
Core i5-8600K (9M Cache, up to 4.30 GHz)
Core i7-2600 (8M Cache, up to 3.80 GHz)
Core i7-2600K (8M Cache, up to 3.80 GHz)
Core i7-2600S (8M Cache, up to 3.80 GHz)
Core i7-2610UE (4M Cache, up to 2.40 GHz)
Core i7-2617M (4M Cache, up to 2.60 GHz)
Core i7-2620M (4M Cache, up to 3.40 GHz)
Core i7-2629M (4M Cache, up to 3.00 GHz)
Core i7-2630QM (6M Cache, up to 2.90 GHz)
Core i7-2635QM (6M Cache, up to 2.90 GHz)
Core i7-2637M (4M Cache, up to 2.80 GHz)
Core i7-2640M (4M Cache, up to 3.50 GHz)
Core i7-2649M (4M Cache, up to 3.20 GHz)
Core i7-2655LE (4M Cache, up to 2.90 GHz)
Core i7-2657M (4M Cache, up to 2.70 GHz)
Core i7-2670QM (6M Cache, up to 3.10 GHz)
Core i7-2675QM (6M Cache, up to 3.10 GHz)
Core i7-2677M (4M Cache, up to 2.90 GHz)
Core i7-2700K (8M Cache, up to 3.90 GHz)
Core i7-2710QE (6M Cache, up to 3.00 GHz)
Core i7-2715QE (6M Cache, up to 3.00 GHz)
Core i7-2720QM (6M Cache, up to 3.30 GHz)
Core i7-2760QM (6M Cache, up to 3.50 GHz)
Core i7-2820QM (8M Cache, up to 3.40 GHz)
Core i7-2860QM (8M Cache, up to 3.60 GHz)
Core i7-2920XM Extreme Edition (8M Cache, up to 3.50 GHz)
Core i7-2960XM Extreme Edition (8M Cache, up to 3.70 GHz)
Core i7-3517U (4M Cache, up to 3.00 GHz)
Core i7-3517UE (4M Cache, up to 2.80 GHz)
Core i7-3520M (4M Cache, up to 3.60 GHz)
Core i7-3537U (4M Cache, up to 3.10 GHz)
Core i7-3540M (4M Cache, up to 3.70 GHz)
Core i7-3555LE (4M Cache, up to 3.20 GHz)
Core i7-3610QE (6M Cache, up to 3.30 GHz)
Core i7-3610QM (6M Cache, up to 3.30 GHz)
Core i7-3612QE (6M Cache, up to 3.10 GHz)
Core i7-3612QM (6M Cache, up to 3.10 GHz) BGA
Core i7-3612QM (6M Cache, up to 3.10 GHz) rPGA
Core i7-3615QE (6M Cache, up to 3.30 GHz)
Core i7-3615QM (6M Cache, up to 3.30 GHz)
Core i7-3630QM (6M Cache, up to 3.40 GHz)
Core i7-3632QM (6M Cache, up to 3.20 GHz) BGA
Core i7-3632QM (6M Cache, up to 3.20 GHz) rPGA
Core i7-3635QM (6M Cache, up to 3.40 GHz)
Core i7-3667U (4M Cache, up to 3.20 GHz)
Core i7-3687U (4M Cache, up to 3.30 GHz)
Core i7-3689Y (4M Cache, up to 2.60 GHz)
Core i7-3720QM (6M Cache, up to 3.60 GHz)
Core i7-3740QM (6M Cache, up to 3.70 GHz)
Core i7-3770 (8M Cache, up to 3.90 GHz)
Core i7-3770K (8M Cache, up to 3.90 GHz)
Core i7-3770S (8M Cache, up to 3.90 GHz)
Core i7-3770T (8M Cache, up to 3.70 GHz)
Core i7-3820 (10M Cache, up to 3.80 GHz)
Core i7-3820QM (8M Cache, up to 3.70 GHz)
Core i7-3840QM (8M Cache, up to 3.80 GHz)
Core i7-3920XM Extreme Edition (8M Cache, up to 3.80 GHz)
Core i7-3930K (12M Cache, up to 3.80 GHz)
Core i7-3940XM Extreme Edition (8M Cache, up to 3.90 GHz)
Core i7-3960X Extreme Edition (15M Cache, up to 3.90 GHz)
Core i7-3970X Extreme Edition (15M Cache, up to 4.00 GHz)
Core i7-4500U (4M Cache, up to 3.00 GHz)
Core i7-4510U (4M Cache, up to 3.10 GHz)
Core i7-4550U (4M Cache, up to 3.00 GHz)
Core i7-4558U (4M Cache, up to 3.30 GHz)
Core i7-4578U (4M Cache, up to 3.50 GHz)
Core i7-4600M (4M Cache, up to 3.60 GHz)
Core i7-4600U (4M Cache, up to 3.30 GHz)
Core i7-4610M (4M Cache, up to 3.70 GHz)
Core i7-4610Y (4M Cache, up to 2.90 GHz)
Core i7-4650U (4M Cache, up to 3.30 GHz)
Core i7-4700EC (8M Cache, up to 2.70 GHz)
Core i7-4700EQ (6M Cache, up to 3.40 GHz)
Core i7-4700HQ (6M Cache, up to 3.40 GHz)
Core i7-4700MQ (6M Cache, up to 3.40 GHz)
Core i7-4701EQ (6M Cache, up to 3.40 GHz)
Core i7-4702EC (8M Cache, up to 2.00 GHz)
Core i7-4702HQ (6M Cache, up to 3.20 GHz)
Core i7-4702MQ (6M Cache, up to 3.20 GHz)
Core i7-4710HQ (6M Cache, up to 3.50 GHz)
Core i7-4710MQ (6M Cache, up to 3.50 GHz)
Core i7-4712HQ (6M Cache, up to 3.30 GHz)
Core i7-4712MQ (6M Cache, up to 3.30 GHz)
Core i7-4720HQ (6M Cache, up to 3.60 GHz)
Core i7-4722HQ (6M Cache, up to 3.40 GHz)
Core i7-4750HQ (6M Cache, up to 3.20 GHz)
Core i7-4760HQ (6M Cache, up to 3.30 GHz)
Core i7-4765T (8M Cache, up to 3.00 GHz)
Core i7-4770 (8M Cache, up to 3.90 GHz)
Core i7-4770HQ (6M Cache, up to 3.40 GHz)
Core i7-4770K (8M Cache, up to 3.90 GHz)
Core i7-4770R (6M Cache, up to 3.90 GHz)
Core i7-4770S (8M Cache, up to 3.90 GHz)
Core i7-4770T (8M Cache, up to 3.70 GHz)
Core i7-4770TE (8M Cache, up to 3.30 GHz)
Core i7-4771 (8M Cache, up to 3.90 GHz)
Core i7-4785T (8M Cache, up to 3.20 GHz)
Core i7-4790 (8M Cache, up to 4.00 GHz)
Core i7-4790K (8M Cache, up to 4.40 GHz)
Core i7-4790S (8M Cache, up to 4.00 GHz)
Core i7-4790T (8M Cache, up to 3.90 GHz)
Core i7-4800MQ (6M Cache, up to 3.70 GHz)
Core i7-4810MQ (6M Cache, up to 3.80 GHz)
Core i7-4820K (10M Cache, up to 3.90 GHz)
Core i7-4850EQ (6M Cache, up to 3.20 GHz)
Core i7-4850HQ (6M Cache, up to 3.50 GHz)
Core i7-4860EQ (6M Cache, up to 3.20 GHz)
Core i7-4860HQ (6M Cache, up to 3.60 GHz)
Core i7-4870HQ (6M Cache, up to 3.70 GHz)
Core i7-4900MQ (8M Cache, up to 3.80 GHz)
Core i7-4910MQ (8M Cache, up to 3.90 GHz)
Core i7-4930K (12M Cache, up to 3.90 GHz)
Core i7-4930MX Extreme Edition (8M Cache, up to 3.90 GHz)
Core i7-4940MX Extreme Edition (8M Cache, up to 4.00 GHz)
Core i7-4950HQ (6M Cache, up to 3.60 GHz)
Core i7-4960HQ (6M Cache, up to 3.80 GHz)
Core i7-4960X Extreme Edition (15M Cache, up to 4.00 GHz)
Core i7-4980HQ (6M Cache, up to 4.00 GHz)
Core i7-5500U (4M Cache, up to 3.00 GHz)
Core i7-5550U (4M Cache, up to 3.00 GHz)
Core i7-5557U (4M Cache, up to 3.40 GHz)
Core i7-5600U (4M Cache, up to 3.20 GHz)
Core i7-5650U (4M Cache, up to 3.20 GHz)
Core i7-5700EQ (6M Cache, up to 3.40 GHz)
Core i7-5700HQ (6M Cache, up to 3.50 GHz)
Core i7-5750HQ (6M Cache, up to 3.40 GHz)
Core i7-5775C (6M Cache, up to 3.70 GHz)
Core i7-5775R (6M Cache, up to 3.80 GHz)
Core i7-5820K (15M Cache, up to 3.60 GHz)
Core i7-5850EQ (6M Cache, up to 3.40 GHz)
Core i7-5850HQ (6M Cache, up to 3.60 GHz)
Core i7-5930K (15M Cache, up to 3.70 GHz)
Core i7-5950HQ (6M Cache, up to 3.80 GHz)
Core i7-5960X Extreme Edition (20M Cache, up to 3.50 GHz)
Core i7-610E (4M Cache, 2.53 GHz)
Core i7-620LE (4M Cache, 2.00 GHz)
Core i7-620LM (4M Cache, 2.00 GHz)
Core i7-620M (4M Cache, 2.66 GHz)
Core i7-620UE (4M Cache, 1.06 GHz)
Core i7-620UM (4M Cache, 1.06 GHz)
Core i7-640LM (4M Cache, 2.13 GHz)
Core i7-640M (4M Cache, 2.80 GHz)
Core i7-640UM (4M Cache, 1.20 GHz)
Core i7-6498DU (4M Cache, up to 3.10 GHz)
Core i7-6500U (4M Cache, up to 3.10 GHz)
Core i7-6560U (4M Cache, up to 3.20 GHz)
Core i7-6567U (4M Cache, up to 3.60 GHz)
Core i7-6600U (4M Cache, up to 3.40 GHz)
Core i7-660LM (4M Cache, 2.26 GHz)
Core i7-660UE (4M Cache, 1.33 GHz)
Core i7-660UM (4M Cache, 1.33 GHz)
Core i7-6650U (4M Cache, up to 3.40 GHz)
Core i7-6660U (4M Cache, up to 3.40 GHz)
Core i7-6700 (8M Cache, up to 4.00 GHz)
Core i7-6700HQ (6M Cache, up to 3.50 GHz)
Core i7-6700K (8M Cache, up to 4.20 GHz)
Core i7-6700T (8M Cache, up to 3.60 GHz)
Core i7-6700TE (8M Cache, up to 3.40 GHz)
Core i7-6770HQ (6M Cache, up to 3.50 GHz)
Core i7-6785R (8M Cache, up to 3.90 GHz)
Core i7-6800K (15M Cache, up to 3.60 GHz)
Core i7-680UM (4M Cache, 1.46 GHz)
Core i7-6820EQ (8M Cache, up to 3.50 GHz)
Core i7-6820HK (8M Cache, up to 3.60 GHz)
Core i7-6820HQ (8M Cache, up to 3.60 GHz)
Core i7-6822EQ (8M Cache, up to 2.80 GHz)
Core i7-6850K (15M Cache, up to 3.80 GHz)
Core i7-6870HQ (8M Cache, up to 3.60 GHz)
Core i7-6900K (20M Cache, up to 3.70 GHz)
Core i7-6920HQ (8M Cache, up to 3.80 GHz)
Core i7-6950X Extreme Edition (25M Cache, up to 3.50 GHz)
Core i7-6970HQ (8M Cache, up to 3.70 GHz)
Core i7-720QM (6M Cache, 1.60 GHz)
Core i7-740QM (6M cache, 1.73 GHz)
Core i7-7500U (4M Cache, up to 3.50 GHz )
Core i7-7560U (4M Cache, up to 3.80 GHz)
Core i7-7567U (4M Cache, up to 4.00 GHz)
Core i7-7600U (4M Cache, up to 3.90 GHz)
Core i7-7660U (4M Cache, up to 4.00 GHz)
Core i7-7700 (8M Cache, up to 4.20 GHz)
Core i7-7700HQ (6M Cache, up to 3.80 GHz)
Core i7-7700K (8M Cache, up to 4.50 GHz)
Core i7-7700T (8M Cache, up to 3.80 GHz)
Core i7-7740X X-series (8M Cache, up to 4.50 GHz)
Core i7-7800X X-series (8.25M Cache, up to 4.00 GHz)
Core i7-7820EQ (8M Cache, up to 3.70 GHz)
Core i7-7820HK (8M Cache, up to 3.90 GHz)
Core i7-7820HQ (8M Cache, up to 3.90 GHz)
Core i7-7820X X-series (11M Cache, up to 4.30 GHz)
Core i7-7920HQ (8M Cache, up to 4.10 GHz)
Core i7-7Y75 (4M Cache, up to 3.60 GHz)
Core i7-820QM (8M Cache, 1.73 GHz)
Core i7-840QM (8M Cache, 1.86 GHz)
Core i7-8550U (8M Cache, up to 4.00 GHz)
Core i7-860 (8M Cache, 2.80 GHz)
Core i7-860S (8M Cache, 2.53 GHz)
Core i7-8650U (8M Cache, up to 4.20 GHz)
Core i7-8700 (12M Cache, up to 4.60 GHz)
Core i7-8700K (12M Cache, up to 4.70 GHz)
Core i7-870 (8M Cache, 2.93 GHz)
Core i7-870S (8M Cache, 2.66 GHz)
Core i7-875K (8M Cache, 2.93 GHz)
Core i7-880 (8M Cache, 3.06 GHz)
Core i7-920 (8M Cache, 2.66 GHz, 4.80 GT/s QPI)
Core i7-920XM Extreme Edition (8M Cache, 2.00 GHz)
Core i7-930 (8M Cache, 2.80 GHz, 4.80 GT/s QPI)
Core i7-940 (8M Cache, 2.93 GHz, 4.80 GT/s QPI)
Core i7-940XM Extreme Edition (8M Cache, 2.13 GHz)
Core i7-950 (8M Cache, 3.06 GHz, 4.80 GT/s QPI)
Core i7-960 (8M Cache, 3.20 GHz, 4.80 GT/s QPI)
Core i7-965 Extreme Edition (8M Cache, 3.20 GHz, 6.40 GT/s QPI)
Core i7-970 (12M Cache, 3.20 GHz, 4.80 GT/s QPI)
Core i7-975 Extreme Edition (8M Cache, 3.33 GHz, 6.40 GT/s QPI)
Core i7-980 (12M Cache, 3.33 GHz, 4.8 GT/s QPI)
Core i7-980X Extreme Edition (12M Cache, 3.33 GHz, 6.40 GT/s QPI)
Core i7-990X Extreme Edition (12M Cache, 3.46 GHz, 6.40 GT/s QPI)
Core i9-7900X X-series (13.75M Cache, up to 4.30 GHz)
Core i9-7920X X-series (16.50M Cache, up to 4.30 GHz)
Core i9-7940X X-series (19.25M Cache, up to 4.30 GHz)
Core i9-7960X X-series (22M Cache, up to 4.20 GHz)
Core i9-7980XE Extreme Edition (24.75M Cache, up to 4.20 GHz)
Core m3-6Y30 (4M Cache, up to 2.20 GHz)
Core m3-7Y30 (4M Cache, 2.60 GHz )
Core m3-7Y32 (4M Cache, up to 3.00 GHz)
Core m5-6Y54 (4M Cache, up to 2.70 GHz)
Core m5-6Y57 (4M Cache, up to 2.80 GHz)
Core M-5Y10 (4M Cache, up to 2.00 GHz)
Core M-5Y10a (4M Cache, up to 2.00 GHz)
Core M-5Y10c (4M Cache, up to 2.00 GHz)
Core M-5Y31 (4M Cache, up to 2.40 GHz)
Core M-5Y51 (4M Cache, up to 2.60 GHz)
Core M-5Y70 (4M Cache, up to 2.60 GHz)
Core M-5Y71 (4M Cache, up to 2.90 GHz)
Core m7-6Y75 (4M Cache, up to 3.10 GHz)
Core Solo T1250 (2M Cache, 1.73 GHz, 533 MHz FSB)
Core Solo T1300 (2M Cache, 1.66 GHz, 667 MHz FSB)
Core Solo T1350 (2M Cache, 1.86 GHz, 533 MHz FSB)
Core Solo T1400 (2M Cache, 1.83 GHz, 667 MHz FSB)
Core Solo U1300 (2M Cache, 1.06 GHz, 533 MHz FSB)
Core Solo U1400 (2M Cache, 1.20 GHz, 533 MHz FSB)
Core Solo U1500 (2M Cache, 1.33 GHz, 533 MHz FSB)
EP80579 Integrated Processor, 1066 MHz
EP80579 Integrated Processor, 1200 MHz
EP80579 Integrated Processor, 600 MHz
EP80579 Integrated with QuickAssist Technology, 1066 MHz
EP80579 Integrated with QuickAssist Technology, 1200 MHz
EP80579 Integrated with QuickAssist Technology, 600 MHz
IOC340 I/O Controller (1.2 GHz, 8 SAS/SATA, PCIe or PCI-X)
IOC340 I/O Controller (800 MHz, 8 SAS/SATA, PCIe or PCI-X)
IOP341 I/O (1.20 GHz)
IOP341 I/O (800 MHz)
IOP342 I/O (1.20 GHz)
IOP342 I/O (800 MHz)
IOP348 I/O (1.2 GHz, PCIe and PCI-X)
IOP348 I/O (1.2 GHz, PCIe or PCI-X)
IOP348 I/O (667 MHz, PCIe and PCI-X)
IOP348 I/O (667 MHz, PCIe or PCI-X)
IOP348 I/O (800 MHz, PCIe and PCI-X)
IOP348 I/O (800 MHz, PCIe or PCI-X)
Itanium 1.00 GHz, 3M Cache, 400 MHz FSB
Itanium 1.30 GHz, 3M Cache, 400 MHz FSB
Itanium 1.40 GHz, 4M Cache, 400 MHz FSB
Itanium 1.50 GHz, 4M Cache, 400 MHz FSB
Itanium 1.50 GHz, 6M Cache, 400 MHz FSB
Itanium 1.60 GHz, 6M Cache, 533 MHz FSB
Itanium 1.60 GHz, 9M Cache, 533 MHz FSB
Itanium 1.66 GHz, 6M Cache, 667 MHz FSB
Itanium 1.66 GHz, 9M Cache, 667 MHz FSB
Itanium 900 MHz, 1.5M Cache, 400 MHz FSB
Itanium 9010 (6M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9015 (12M Cache, 1.40 GHz, 400 MHz FSB)
Itanium 9020 (12M Cache, 1.42 GHz, 533 MHz FSB)
Itanium 9030 (8M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9040 (18M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9050 (24M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9110N (12M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9120N (12M Cache, 1.42 GHz, 533 MHz FSB)
Itanium 9130M (8M Cache, 1.66 GHz, 667 MHz FSB)
Itanium 9140M (18M Cache, 1.66 GHz, 667 MHz FSB)
Itanium 9140N (18M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9150M (24M Cache, 1.66 GHz, 667 MHz FSB)
Itanium 9150N (24M Cache, 1.60 GHz, 533 MHz FSB)
Itanium 9152M (24M Cache, 1.66 GHz, 667 MHz FSB)
Itanium 9310 (10M Cache, 1.60 GHz, 4.80 GT/s QPI)
Itanium 9320 (16M Cache, 1.33 GHz, 4.80 GT/s QPI)
Itanium 9330 (20M Cache, 1.46 GHz, 4.80 GT/s QPI)
Itanium 9340 (20M Cache, 1.60 GHz, 4.80 GT/s QPI)
Itanium 9350 (24M Cache, 1.73 GHz, 4.80 GT/s QPI)
Itanium 9520 (20M Cache, 1.73 GHz)
Itanium 9540 (24M Cache, 2.13 GHz)
Itanium 9550 (32M Cache, 2.40 GHz)
Itanium 9560 (32M Cache, 2.53 GHz)
Itanium 9720 (20M Cache, 1.73 GHz)
Itanium 9740 (24M Cache, 2.13 GHz)
Itanium 9750 (32M Cache, 2.53 GHz)
Itanium 9760 (32M Cache, 2.66 GHz)
Pentium 100 MHz, 50 MHz FSB
Pentium 120 MHz, 60 MHz FSB
Pentium 1403 (5M Cache, 2.60GHz)
Pentium 1403 v2 (6M Cache, 2.60 GHz)
Pentium 1405 (5M Cache, 1.2 GHz)
Pentium 1405 v2 (6M Cache, 1.40 GHz)
Pentium 1407 (5M Cache, 2.80GHz)
Pentium 150 MHz, 60 MHz FSB
Pentium 2020M (2M Cache, 2.40 GHz)
Pentium 2030M (2M Cache, 2.50 GHz)
Pentium 2117U (2M Cache, 1.80 GHz)
Pentium 2127U (2M Cache, 1.90 GHz)
Pentium 2129Y (2M Cache, 1.10 GHz)
Pentium 350 (3M Cache, 1.2 GHz)
Pentium 3550M (2M Cache, 2.30 GHz)
Pentium 3556U (2M Cache, 1.70 GHz)
Pentium 3558U (2M Cache, 1.70 GHz)
Pentium 3560M (2M Cache, 2.40 GHz)
Pentium 3560Y (2M Cache, 1.20 GHz)
Pentium 3561Y (2M Cache, 1.20 GHz)
Pentium 3805U (2M Cache, 1.90 GHz)
Pentium 3825U (2M Cache, 1.90 GHz)
Pentium 4 1.30 GHz, 256K Cache, 400 MHz FSB
Pentium 4 1.40 GHz, 256K Cache, 400 MHz FSB
Pentium 4 1.50 GHz, 256K Cache, 400 MHz FSB
Pentium 4 1.60 GHz, 256K Cache, 400 MHz FSB
Pentium 4 1.60 GHz, 512K Cache, 400 MHz FSB
Pentium 4 1.70 GHz, 256K Cache, 400 MHz FSB
Pentium 4 1.70 GHz, 512K Cache, 400 MHz FSB
Pentium 4 1.80 GHz, 256K Cache, 400 MHz FSB
Pentium 4 1.80 GHz, 512K Cache, 400 MHz FSB
Pentium 4 1.90 GHz, 256K Cache, 400 MHz FSB
Pentium 4 2.00 GHz, 256K Cache, 400 MHz FSB
Pentium 4 2.00 GHz, 512K Cache, 400 MHz FSB
Pentium 4 2.20 GHz, 512K Cache, 400 MHz FSB
Pentium 4 2.26 GHz, 512K Cache, 533 MHz FSB
Pentium 4 2.40 GHz, 1M Cache, 533 MHz FSB
Pentium 4 2.40 GHz, 512K Cache, 400 MHz FSB
Pentium 4 2.40 GHz, 512K Cache, 533 MHz FSB
Pentium 4 2.50 GHz, 512K Cache, 400 MHz FSB
Pentium 4 2.53 GHz, 512K Cache, 533 MHz FSB
Pentium 4 2.60 GHz, 512K Cache, 400 MHz FSB
Pentium 4 2.66 GHz, 1M Cache, 533 MHz FSB
Pentium 4 2.66 GHz, 512K Cache, 533 MHz FSB
Pentium 4 2.80A GHz, 1M Cache, 533 MHz FSB
Pentium 4 2.80 GHz, 1M Cache, 533 MHz FSB
Pentium 4 2.80 GHz, 512K Cache, 400 MHz FSB
Pentium 4 2.80 GHz, 512K Cache, 533 MHz FSB
Pentium 4405U (2M Cache, 2.10 GHz)
Pentium 4405Y (2M Cache, 1.50 GHz)
Pentium 4410Y (2M Cache, 1.50 GHz)
Pentium 4415U (2M Cache, 2.30 GHz)
Pentium 4415Y (2M Cache, 1.60 GHz)
Pentium 4 505 (1M Cache, 2.66 GHz, 533 MHz FSB)
Pentium 4 505/505J (1M Cache, 2.66 GHz, 533 MHz FSB)
Pentium 4 506 (1M Cache, 2.66 GHz, 533 MHz FSB)
Pentium 4 511 (1M Cache, 2.80A GHz, 533 MHz FSB)
Pentium 4 511 (1M Cache, 2.80 GHz, 533 MHz FSB)
Pentium 4 515/515J (1M Cache, 2.93 GHz, 533 MHz FSB)
Pentium 4 516 (1M Cache, 2.93 GHz, 533 MHz FSB)
Pentium 4 517 supporting HT Technology (1M Cache, 2.93 GHz, 533 MHz FSB)
Pentium 4 519J (1M Cache, 3.06 GHz, 533 MHz FSB)
Pentium 4 519K (1M Cache, 3.06 GHz, 533 MHz FSB)
Pentium 4 520/520J supporting HT Technology (1M Cache, 2.80 GHz, 800 MHz FSB)
Pentium 4 520/521 supporting HT Technology (1M Cache, 2.80E GHz, 800 MHz FSB)
Pentium 4 520/521 supporting HT Technology (1M Cache, 2.80 GHz, 800 MHz FSB)
Pentium 4 520J supporting HT Technology (1M Cache, 2.80 GHz, 800 MHz FSB)
Pentium 4 520 supporting HT Technology (1M Cache, 2.80 GHz, 800 MHz FSB)
Pentium 4 521 supporting HT Technology (1M Cache, 2.80 GHz, 800 MHz FSB)
Pentium 4 524 supporting HT Technology (1M Cache, 3.06 GHz, 533 MHz FSB)
Pentium 4 530/530J supporting HT Technology (1M Cache, 3.00 GHz, 800 MHz FSB)
Pentium 4 530J supporting HT Technology (1M Cache, 3.00 GHz, 800 MHz FSB)
Pentium 4 531 supporting HT Technology (1M Cache, 3.00 GHz, 800 MHz FSB)
Pentium 4 540/540J supporting HT Technology (1M Cache, 3.20 GHz, 800 MHz FSB)
Pentium 4 540J supporting HT Technology (1M Cache, 3.20 GHz, 800 MHz FSB)
Pentium 4 541 supporting HT Technology (1M Cache, 3.20 GHz, 800 MHz FSB)
Pentium 4 550/550J supporting HT Technology (1M Cache, 3.40 GHz, 800 MHz FSB)
Pentium 4 550J supporting HT Technology (1M Cache, 3.40 GHz, 800 MHz FSB)
Pentium 4 550 supporting HT Technology (1M Cache, 3.40 GHz, 800 MHz FSB)
Pentium 4 551 supporting HT Technology (1M Cache, 3.40 GHz, 800 MHz FSB)
Pentium 4 560/560J supporting HT Technology (1M Cache, 3.60 GHz, 800 MHz FSB)
Pentium 4 560J supporting HT Technology (1M Cache, 3.60 GHz, 800 MHz FSB)
Pentium 4 561 supporting HT Technology (1M Cache, 3.60 GHz, 800 MHz FSB)
Pentium 4 570J supporting HT Technology (1M Cache, 3.80 GHz, 800 MHz FSB)
Pentium 4 571 supporting HT Technology (1M Cache, 3.80 GHz, 800 MHz FSB)
Pentium 4 620 supporting HT Technology (2M Cache, 2.80 GHz, 800 MHz FSB)
Pentium 4 630 supporting HT Technology (2M Cache, 3.00 GHz, 800 MHz FSB)
Pentium 4 631 supporting HT Technology (2M Cache, 3.00 GHz, 800 MHz FSB)
Pentium 4 640 supporting HT Technology (2M Cache, 3.20 GHz, 800 MHz FSB)
Pentium 4 641 supporting HT Technology (2M Cache, 3.20 GHz, 800 MHz FSB)
Pentium 4 650 supporting HT Technology (2M Cache, 3.40 GHz, 800 MHz FSB)
Pentium 4 651 supporting HT Technology (2M Cache, 3.40 GHz, 800 MHz FSB)
Pentium 4 660 supporting HT Technology (2M Cache, 3.60 GHz, 800 MHz FSB)
Pentium 4 661 supporting HT Technology (2M Cache, 3.60 GHz, 800 MHz FSB)
Pentium 4 662 supporting HT Technology (2M Cache, 3.60 GHz, 800 MHz FSB)
Pentium 4 670 supporting HT Technology (2M Cache, 3.80 GHz, 800 MHz FSB)
Pentium 4 672 supporting HT Technology (2M Cache, 3.80 GHz, 800 MHz FSB)
Pentium 4 745 supporting HT Technology (512K Cache, 3.00 GHz, 800 MHz FSB)
Pentium 4 Extreme Edition supporting HT Technology 3.20 GHz, 2M Cache, 800 MHz FSB
Pentium 4 Extreme Edition supporting HT Technology 3.40 GHz, 2M Cache, 800 MHz FSB
Pentium 4 Extreme Edition supporting HT Technology 3.46 GHz, 2M Cache, 1066 MHz FSB
Pentium 4 Extreme Edition supporting HT Technology 3.73 GHz, 2M Cache, 1066 MHz FSB
Pentium 4 Mobile 518 supporting HT Technology (1M Cache, 2.80 GHz, 533 MHz FSB)
Pentium 4 Mobile 532 supporting HT Technology (1M Cache, 3.06 GHz, 533 MHz FSB)
Pentium 4 Mobile 538 supporting HT Technology (1M Cache, 3.20 GHz, 533 MHz FSB)
Pentium 4 Mobile 548 supporting HT Technology (1M Cache, 3.33 GHz, 533 MHz FSB)
Pentium 4 Mobile 552 supporting HT Technology (1M Cache, 3.46 GHz, 533 MHz FSB)
Pentium 4 Mobile - M 1.40 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 1.50 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 1.60 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 1.70 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 1.80 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 1.90 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 2.00 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 2.20 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 2.30 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 2.40 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 2.50 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile - M 2.60 GHz, 512K Cache, 400 MHz FSB
Pentium 4 Mobile supporting HT Technology 2.40 GHz, 512K Cache, 533 MHz FSB
Pentium 4 Mobile supporting HT Technology 2.66 GHz, 512K Cache, 533 MHz FSB
Pentium 4 Mobile supporting HT Technology 2.80 GHz, 512K Cache, 533 MHz FSB
Pentium 4 Mobile supporting HT Technology 3.06 GHz, 512K Cache, 533 MHz FSB
Pentium 4 Mobile supporting HT Technology 3.20 GHz, 512K Cache, 533 MHz FSB
Pentium 4 supporting HT Technology 2.40 GHz, 512K Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 2.60 GHz, 512K Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 2.80E GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 2.80 GHz, 512K Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.00E GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.00 GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.00 GHz, 512K Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.06 GHz, 512K Cache, 533 MHz FSB
Pentium 4 supporting HT Technology 3.20E GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.20 GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.20 GHz, 512K Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.40E GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.40 GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.40 GHz, 512K Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 3.80 GHz, 1M Cache, 800 MHz FSB
Pentium 4 supporting HT Technology 4.00 GHz, 2M Cache, 1066 MHz FSB
Pentium 75 MHz, 50 MHz FSB
Pentium 90 MHz, 60 MHz FSB
Pentium 957 (2M Cache, 1.20 GHz)
Pentium 967 (2M Cache, 1.30 GHz)
Pentium 977 (2M Cache, 1.40 GHz)
Pentium 987 (2M Cache, 1.50 GHz)
Pentium 997 (2M Cache, 1.60 GHz)
Pentium A1018 (1M Cache, 2.10 GHz)
Pentium A1020 (2M Cache, up to 2.66 GHz)
Pentium B915C (3M Cache, 1.50 GHz)
Pentium B925C (4M Cache, 2.00 GHz)
Pentium B940 (2M Cache, 2.00 GHz)
Pentium B950 (2M Cache, 2.10 GHz)
Pentium B960 (2M Cache, 2.20 GHz)
Pentium B970 (2M Cache, 2.30 GHz)
Pentium B980 (2M Cache, 2.40 GHz)
Pentium D1507 (3M Cache, 1.20 GHz)
Pentium D1508 (3M Cache, 2.20 GHz)
Pentium D1509 (3M Cache, 1.50 GHz)
Pentium D1517 (6M Cache, 1.60 GHz)
Pentium D1519 (6M Cache, 1.50 GHz)
Pentium D 805 (2M Cache, 2.66 GHz, 533 MHz FSB)
Pentium D 820 (2M Cache, 2.80 GHz, 800 MHz FSB)
Pentium D 830 (2M Cache, 3.00 GHz, 800 MHz FSB)
Pentium D 840 (2M Cache, 3.20 GHz, 800 MHz FSB)
Pentium D 915 (4M Cache, 2.80 GHz, 800 MHz FSB)
Pentium D 920 (4M Cache, 2.80 GHz, 800 MHz FSB)
Pentium D 920 (4M Cache, 2.8 GHz, 800 MHz FSB)
Pentium D 925 (4M Cache, 3.00 GHz, 800 MHz FSB)
Pentium D 930 (4M Cache, 3.00 GHz, 800 MHz FSB)
Pentium D 935 (4M Cache, 3.20 GHz, 800 MHz FSB)
Pentium D 940 (4M Cache, 3.20 GHz, 800 MHz FSB)
Pentium D 945 (4M Cache, 3.40 GHz, 800 MHz FSB)
Pentium D 950 (4M Cache, 3.40 GHz, 800 MHz FSB)
Pentium D 960 (4M Cache, 3.60 GHz, 800 MHz FSB)
Pentium E2140 (1M Cache, 1.60 GHz, 800 MHz FSB)
Pentium E2160 (1M Cache, 1.80 GHz, 800 MHz FSB)
Pentium E2180 (1M Cache, 2.00 GHz, 800 MHz FSB)
Pentium E2200 (1M Cache, 2.20 GHz, 800 MHz FSB)
Pentium E2210 (1M Cache, 2.20 GHz, 800 MHz FSB)
Pentium E2220 (1M Cache, 2.40 GHz, 800 MHz FSB)
Pentium E5200 (2M Cache, 2.50 GHz, 800 MHz FSB)
Pentium E5300 (2M Cache, 2.60 GHz, 800 MHz FSB)
Pentium E5400 (2M Cache, 2.70 GHz, 800 MHz FSB)
Pentium E5500 (2M Cache, 2.80 GHz, 800 MHz FSB)
Pentium E5700 (2M Cache, 3.00 GHz, 800 MHz FSB)
Pentium E5800 (2M Cache, 3.20 GHz, 800 MHz FSB)
Pentium E6300 (2M Cache, 2.80 GHz, 1066 MHz FSB)
Pentium E6500 (2M Cache, 2.93 GHz, 1066 FSB)
Pentium E6500K (2M Cache, 2.93 GHz, 1066 MHz FSB)
Pentium E6600 (2M Cache, 3.06 GHz, 1066 FSB)
Pentium E6700 (2M Cache, 3.20 GHz, 1066 FSB)
Pentium E6800 (2M Cache, 3.33 GHz, 1066 FSB)
Pentium Extreme Edition 840 (2M Cache, 3.20 GHz, 800 MHz FSB)
Pentium Extreme Edition 955 (4M Cache, 3.46 GHz, 1066 MHz FSB)
Pentium Extreme Edition 965 (4M Cache, 3.73 GHz, 1066 MHz FSB)
Pentium G2010 (3M Cache, 2.80 GHz)
Pentium G2020 (3M Cache, 2.90 GHz)
Pentium G2020T (3M Cache, 2.50 GHz)
Pentium G2030 (3M Cache, 3.00 GHz)
Pentium G2030T (3M Cache, 2.60 GHz)
Pentium G2100T (3M Cache, 2.60 GHz)
Pentium G2120 (3M Cache, 3.10 GHz)
Pentium G2120T (3M Cache, 2.70 GHz)
Pentium G2130 (3M Cache, 3.20 GHz)
Pentium G2140 (3M Cache, 3.30 GHz)
Pentium G3220 (3M Cache, 3.00 GHz)
Pentium G3220T (3M Cache, 2.60 GHz)
Pentium G3240 (3M Cache, 3.10 GHz)
Pentium G3240T (3M Cache, 2.70 GHz)
Pentium G3250 (3M Cache, 3.20 GHz)
Pentium G3250T (3M Cache, 2.80 GHz)
Pentium G3258 (3M Cache, 3.20 GHz)
Pentium G3260 (3M Cache, 3.30 GHz)
Pentium G3260T (3M Cache, 2.90 GHz)
Pentium G3320TE (3M Cache, 2.30 GHz)
Pentium G3420 (3M Cache, 3.20 GHz)
Pentium G3420T (3M Cache, 2.70 GHz)
Pentium G3430 (3M Cache, 3.30 GHz)
Pentium G3440 (3M Cache, 3.30 GHz)
Pentium G3440T (3M Cache, 2.80 GHz)
Pentium G3450 (3M Cache, 3.40 GHz)
Pentium G3450T (3M Cache, 2.90 GHz)
Pentium G3460 (3M Cache, 3.50 GHz)
Pentium G3460T (3M Cache, 3.00 GHz)
Pentium G3470 (3M Cache, 3.60 GHz)
Pentium G4400 (3M Cache, 3.30 GHz)
Pentium G4400T (3M Cache, 2.90 GHz)
Pentium G4400TE (3M Cache, 2.40 GHz)
Pentium G4500 (3M Cache, 3.50 GHz)
Pentium G4500T (3M Cache, 3.00 GHz)
Pentium G4520 (3M Cache, 3.60 GHz)
Pentium G4560 (3M Cache, 3.50 GHz)
Pentium G4560T (3M Cache, 2.90 GHz)
Pentium G4600 (3M Cache, 3.60 GHz)
Pentium G4600T (3M Cache, 3.00 GHz)
Pentium G4620 (3M Cache, 3.70 GHz)
Pentium G620 (3M Cache, 2.60 GHz)
Pentium G620T (3M Cache, 2.20 GHz)
Pentium G622 (3M Cache, 2.60 GHz)
Pentium G630 (3M Cache, 2.70 GHz)
Pentium G630T (3M Cache, 2.30 GHz)
Pentium G632 (3M Cache, 2.70 GHz)
Pentium G640 (3M Cache, 2.80 GHz)
Pentium G640T (3M Cache, 2.40 GHz)
Pentium G645 (3M Cache, 2.90 GHz)
Pentium G645T (3M Cache, 2.50 GHz)
Pentium G6950 (3M Cache, 2.80 GHz)
Pentium G6951 (3M Cache, 2.80 GHz)
Pentium G6960 (3M Cache, 2.93 GHz)
Pentium G840 (3M Cache, 2.80 GHz)
Pentium G850 (3M Cache, 2.90 GHz)
Pentium G860 (3M Cache, 3.00 GHz)
Pentium G860T (3M Cache, 2.60 GHz)
Pentium G870 (3M Cache, 3.10 GHz)
Pentium II 233 MHz, 512K Cache, 66 MHz FSB
Pentium II 266 MHz, 512K Cache, 66 MHz FSB
Pentium II 300 MHz, 512K Cache, 66 MHz FSB
Pentium II 333 MHz, 512K Cache, 66 MHz FSB
Pentium II 350 MHz, 512K Cache, 100 MHz FSB
Pentium II 400 MHz, 512K Cache, 100 MHz FSB
Pentium II 450 MHz, 512K Cache, 100 MHz FSB
Pentium III 1.00 GHz, 256K Cache, 100 MHz FSB
Pentium III 1.00 GHz, 256K Cache, 133 MHz FSB
Pentium III 1.10 GHz, 256K Cache, 100 MHz FSB
Pentium III 1.13 GHz, 256K Cache, 133 MHz FSB
Pentium III 1.20 GHz, 256K Cache, 133 MHz FSB
Pentium III 1.33 GHz, 256K Cache, 133 MHz FSB
Pentium III 1.40 GHz, 256K Cache, 133 MHz FSB
Pentium III 450 MHz, 512K Cache, 100 MHz FSB
Pentium III 500 MHz, 256K Cache, 100 MHz FSB
Pentium III 500 MHz, 512K Cache, 100 MHz FSB
Pentium III 533 MHz, 256K Cache, 133 MHz FSB
Pentium III 533 MHz, 512K Cache, 133 MHz FSB
Pentium III 550 MHz, 256K Cache, 100 MHz FSB
Pentium III 550 MHz, 512K Cache, 100 MHz FSB
Pentium III 600 MHz, 256K Cache, 100 MHz FSB
Pentium III 600 MHz, 256K Cache, 133 MHz FSB
Pentium III 600 MHz, 512K Cache, 100 MHz FSB
Pentium III 600 MHz, 512K Cache, 133 MHz FSB
Pentium III 650 MHz, 256K Cache, 100 MHz FSB
Pentium III 667 MHz, 256K Cache, 133 MHz FSB
Pentium III 700 MHz, 256K Cache, 100 MHz FSB
Pentium III 733 MHz, 256K Cache, 133 MHz FSB
Pentium III 750 MHz, 256K Cache, 100 MHz FSB
Pentium III 800 MHz, 256K Cache, 100 MHz FSB
Pentium III 800 MHz, 256K Cache, 133 MHz FSB
Pentium III 850 MHz, 256K Cache, 100 MHz FSB
Pentium III 866 MHz, 256K Cache, 133 MHz FSB
Pentium III 900 MHz, 256K Cache, 100 MHz FSB
Pentium III 933 MHz, 256K Cache, 133 MHz FSB
Pentium III Mobile 1.00 GHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 1.06 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile 400 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 450 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 500 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 600 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 650 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 700 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 700 MHz, 512K Cache, 100 MHz FSB
Pentium III Mobile 750 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 750 MHz, 512K Cache, 100 MHz FSB
Pentium III Mobile 800 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 800 MHz, 512K Cache, 100 MHz FSB
Pentium III Mobile 800 MHz, 512K Cache, 133 MHz FSB
Pentium III Mobile 850 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile 900 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile LV 600 MHz, 256K Cache, 100 MHz FSB
Pentium III Mobile - M 1.00 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 1.06 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 1.13 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 1.20 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 1.26 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 1.33 GHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 866 MHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M 933 MHz, 512K Cache, 133 MHz FSB
Pentium III Mobile - M ULV 700 MHz, 512K Cache, 100 MHz FSB
Pentium III - S 1.00 GHz, 512K Cache, 133 MHz FSB
Pentium III - S 1.13 GHz, 512K Cache, 133 MHz FSB
Pentium III - S 1.26 GHz, 512K Cache, 133 MHz FSB
Pentium III - S 1.40 GHz, 512K Cache, 133 MHz FSB
Pentium III - S 800 MHz, 512K Cache, 133 MHz FSB
Pentium III - S 933 MHz, 512K Cache, 133 MHz FSB
Pentium III Xeon 1.00 GHz, 256K Cache, 133 MHz FSB
Pentium III Xeon 500 MHz, 1M Cache, 100 MHz FSB
Pentium III Xeon 500 MHz, 2M Cache, 100 MHz FSB
Pentium III Xeon 500 MHz, 512K Cache, 100 MHz FSB
Pentium III Xeon 550 MHz, 1M Cache, 100 MHz FSB
Pentium III Xeon 550 MHz, 2M Cache, 100 MHz FSB
Pentium III Xeon 550 MHz, 512K Cache, 100 MHz FSB
Pentium III Xeon 600 MHz, 256K Cache, 100 MHz FSB
Pentium III Xeon 600 MHz, 256K Cache, 133 MHz FSB
Pentium III Xeon 667 MHz, 256K Cache, 133 MHz FSB
Pentium III Xeon 700 MHz, 1M Cache, 100 MHz FSB
Pentium III Xeon 700 MHz, 2M Cache, 100 MHz FSB
Pentium III Xeon 733 MHz, 256K Cache, 133 MHz FSB
Pentium III Xeon 800 MHz, 256K Cache, 133 MHz FSB
Pentium III Xeon 866 MHz, 256K Cache, 133 MHz FSB
Pentium III Xeon 900 MHz, 2M Cache, 100 MHz FSB
Pentium III Xeon 933 MHz, 256K Cache, 133 MHz FSB
Pentium II Mobile 233 MHz, 512K Cache, 66 MHz FSB
Pentium II Mobile 266 MHz, 256K Cache, 66 MHz FSB
Pentium II Mobile 266 MHz, 512K Cache, 66 MHz FSB
Pentium II Mobile 300 MHz, 256K Cache, 66 MHz FSB
Pentium II Mobile 300 MHz, 512K Cache, 66 MHz FSB
Pentium II Mobile 333 MHz, 256K Cache, 66 MHz FSB
Pentium II Mobile 366 MHz, 256K Cache, 66 MHz FSB
Pentium II Mobile 400 MHz, 256K Cache, 66 MHz FSB
Pentium II Xeon 400 MHz, 1M Cache, 100 MHz FSB
Pentium II Xeon 400 MHz, 512K Cache, 100 MHz FSB
Pentium II Xeon 450 MHz, 1M Cache, 100 MHz FSB
Pentium II Xeon 450 MHz, 2M Cache, 100 MHz FSB
Pentium II Xeon 450 MHz, 512K Cache, 100 MHz FSB
Pentium J2850 (2M Cache, 2.41 GHz)
Pentium J2900 (2M Cache, up to 2.67 GHz)
Pentium J3710 (2M Cache, up to 2.64 GHz)
Pentium J4205 (2M Cache, up to 2.6 GHz)
Pentium M 1.30 GHz, 1M Cache, 400 MHz FSB
Pentium M 1.40 GHz, 1M Cache, 400 MHz FSB
Pentium M 1.50 GHz, 1M Cache, 400 MHz FSB
Pentium M 1.60 GHz, 1M Cache, 400 MHz FSB
Pentium M 1.70 GHz, 1M Cache, 400 MHz FSB
Pentium M 705 (1M Cache, 1.50 GHz, 400 MHz FSB)
Pentium M 715 (2M Cache, 1.50 GHz, 400 MHz FSB)
Pentium M 725 (2M Cache, 1.60A GHz, 400 MHz FSB)
Pentium M 730 (2M Cache, 1.60B GHz, 533 MHz FSB)
Pentium M 735 (2M Cache, 1.70A GHz, 400 MHz FSB)
Pentium M 740 (2M Cache, 1.73 GHz, 533 MHz FSB)
Pentium M 745 (2M Cache, 1.80 GHz, 400 MHz FSB)
Pentium M 745A (2M Cache, 1.80 GHz, 400 MHz FSB)
Pentium M 750 (2M Cache, 1.86 GHz, 533 MHz FSB)
Pentium M 755 (2M Cache, 2.00 GHz, 400 MHz FSB)
Pentium M 760 (2M Cache, 2.00A GHz, 533 MHz FSB)
Pentium M 765 (2M Cache, 2.10 GHz, 400 MHz FSB)
Pentium M 770 (2M Cache, 2.13 GHz, 533 MHz FSB)
Pentium M 780 (2M Cache, 2.26 GHz, 533 MHz FSB)
Pentium M LV 1.10 GHz, 1M Cache, 400 MHz FSB
Pentium M LV 1.20 GHz, 1M Cache, 400 MHz FSB
Pentium M LV 718 (1M Cache, 1.30 GHz, 400 MHz FSB)
Pentium M LV 738 (2M Cache, 1.40 GHz, 400 MHz FSB)
Pentium M LV 758 (2M Cache, 1.50 GHz, 400 MHz FSB)
Pentium M LV 778 (2M Cache, 1.60 GHz, 400 MHz FSB)
Pentium M ULV 1.00 GHz, 1M Cache, 400 MHz FSB
Pentium M ULV 713 (1M Cache, 1.10 GHz, 400 MHz FSB)
Pentium M ULV 723 (2M Cache, 1.00 GHz, 400 MHz FSB)
Pentium M ULV 733/733J (2M Cache, 1.10A GHz, 400 MHz FSB)
Pentium M ULV 753 (2M Cache, 1.20 GHz, 400 MHz FSB)
Pentium M ULV 773 (2M Cache, 1.30 GHz, 400 MHz FSB)
Pentium M ULV 900 MHz, 1M Cache, 400 MHz FSB
Pentium N3510 (2M Cache, 2.00 GHz)
Pentium N3520 (2M Cache, up to 2.42 GHz)
Pentium N3530 (2M Cache, up to 2.58 GHz)
Pentium N3540 (2M Cache, up to 2.66 GHz)
Pentium N3700 (2M Cache, up to 2.40 GHz)
Pentium N3710 (2M Cache, up to 2.56 GHz)
Pentium N4200 (2M Cache, up to 2.5 GHz)
Pentium P6000 (3M Cache, 1.86 GHz)
Pentium P6100 (3M Cache, 2.00 GHz)
Pentium P6200 (3M Cache, 2.13 GHz)
Pentium P6300 (3M Cache, 2.27 GHz)
Pentium Pro 150 MHz, 256K Cache, 60 MHz FSB
Pentium Pro 166 MHz, 512K Cache, 66 MHz FSB
Pentium Pro 180 MHz, 256K Cache, 60 MHz FSB
Pentium Pro 200 MHz, 1M Cache, 66 MHz FSB
Pentium Pro 200 MHz, 256K Cache, 66 MHz FSB
Pentium Pro 200 MHz, 512K Cache, 66 MHz FSB
Pentium SU2700 (2M Cache, 1.30 GHz, 800 MHz FSB)
Pentium SU4100 (2M Cache, 1.30 GHz, 800 MHz FSB)
Pentium T2060 (1M Cache, 1.60 GHz, 533 MHz FSB)
Pentium T2080 (1M Cache, 1.73 GHz, 533 MHz FSB)
Pentium T2130 (1M Cache, 1.86 GHz, 533 MHz FSB)
Pentium T2310 (1M Cache, 1.46 GHz, 533 MHz FSB)
Pentium T2330 (1M Cache, 1.60 GHz, 533 MHz FSB)
Pentium T2370 (1M Cache, 1.73 GHz, 533 MHz FSB)
Pentium T2390 (1M Cache, 1.86 GHz, 533 MHz FSB)
Pentium T3200 (1M Cache, 2.00 GHz, 667 MHz FSB) Socket P
Pentium T3400 (1M Cache, 2.16 GHz, 667 MHz FSB) Socket P
Pentium T4200 (1M Cache, 2.00 GHz, 800 MHz FSB)
Pentium T4300 (1M Cache, 2.10 GHz, 800 MHz FSB)
Pentium T4400 (1M Cache, 2.20 GHz, 800 MHz FSB) Socket P
Pentium T4500 (1M Cache, 2.30 GHz, 800 MHz FSB)
Pentium U5400 (3M Cache, 1.20 GHz)
Pentium U5600 (3M Cache, 1.33 GHz)
Pentium with MMX Technology 133 MHz, 66 MHz FSB
Pentium with MMX Technology 150 MHz, 66 MHz FSB
Pentium with MMX Technology 166 MHz, 66 MHz FSB
Pentium with MMX Technology 200 MHz, 66 MHz FSB
Pentium with MMX Technology 233 MHz, 66 MHz FSB
Pentium with MMX Technology 266 MHz, 66 MHz FSB
Pentium with MMX Technology 300 MHz, 66 MHz FSB
Quark Microcontroller D1000
Quark Microcontroller D2000
Quark SE C1000 Microcontroller
Quark SoC X1000 (16K Cache, 400 MHz)
Quark SoC X1001 (16K Cache, 400 MHz)
Quark SoC X1010 (16K Cache, 400 MHz)
Quark SoC X1011 (16K Cache, 400 MHz)
Quark SoC X1020 (16K Cache, 400 MHz)
Quark SoC X1020D (16K Cache, 400 MHz)
Quark SoC X1021 (16K Cache, 400 MHz)
Quark SoC X1021D (16K Cache, 400 MHz)
Xeon 1.40 GHz, 256K Cache, 400 MHz FSB
Xeon 1.40 GHz, 512K Cache, 400 MHz FSB
Xeon 1.50 GHz, 1M Cache, 400 MHz FSB
Xeon 1.50 GHz, 256K Cache, 400 MHz FSB
Xeon 1.50 GHz, 512K Cache, 400 MHz FSB
Xeon 1.60 GHz, 1M Cache, 400 MHz FSB
Xeon 1.70 GHz, 256K Cache, 400 MHz FSB
Xeon 1.80 GHz, 512K Cache, 400 MHz FSB
Xeon 1.90 GHz, 1M Cache, 400 MHz FSB
Xeon 2.00 GHz, 1M Cache, 400 MHz FSB
Xeon 2.00 GHz, 256K Cache, 400 MHz FSB
Xeon 2.00 GHz, 2M Cache, 400 MHz FSB
Xeon 2.00 GHz, 512K Cache, 400 MHz FSB
Xeon 2.00 GHz, 512K Cache, 533 MHz FSB
Xeon 2.20 GHz, 2M Cache, 400 MHz FSB
Xeon 2.20 GHz, 512K Cache, 400 MHz FSB
Xeon 2.40 GHz, 1M Cache, 533 MHz FSB
Xeon 2.40 GHz, 512K Cache, 400 MHz FSB
Xeon 2.40 GHz, 512K Cache, 533 MHz FSB
Xeon 2.50 GHz, 1M Cache, 400 MHz FSB
Xeon 2.60 GHz, 512K Cache, 400 MHz FSB
Xeon 2.66 GHz, 512K Cache, 533 MHz FSB
Xeon 2.70 GHz, 2M Cache, 400 MHz FSB
Xeon 2.80D GHz, 1M Cache, 800 MHz FSB
Xeon 2.80E GHz, 2M Cache, 800 MHz FSB
Xeon 2.80 GHz, 1M Cache, 533 MHz FSB
Xeon 2.80 GHz, 1M Cache, 800 MHz FSB
Xeon 2.80 GHz, 2M Cache, 400 MHz FSB
Xeon 2.80 GHz, 2M Cache, 800 MHz FSB
Xeon 2.80 GHz, 4M Cache, 800 MHz FSB
Xeon 2.80 GHz, 512K Cache, 400 MHz FSB
Xeon 2.80 GHz, 512K Cache, 533 MHz FSB
Xeon 2.83 GHz, 4M Cache, 667 MHz FSB
Xeon 3.00D GHz, 1M Cache, 800 MHz FSB
Xeon 3.00E GHz, 2M Cache, 800 MHz FSB
Xeon 3.00 GHz, 1M Cache, 667 MHz FSB
Xeon 3.00 GHz, 1M Cache, 800 MHz FSB
Xeon 3.00 GHz, 2M Cache, 800 MHz FSB
Xeon 3.00 GHz, 4M Cache, 400 MHz FSB
Xeon 3.00 GHz, 512K Cache, 400 MHz FSB
Xeon 3.00 GHz, 8M Cache, 667 MHz FSB
Xeon 3040 (2M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon 3050 (2M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon 3060 (4M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon 3065 (4M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon 3.06 GHz, 1M Cache, 533 MHz FSB
Xeon 3.06 GHz, 512K Cache, 533 MHz FSB
Xeon 3070 (4M Cache, 2.66 GHz, 1066 MHz FSB)
Xeon 3.16 GHz, 1M Cache, 667 MHz FSB
Xeon 3.20E GHz, 2M Cache, 800 MHz FSB
Xeon 3.20 GHz, 1M Cache, 533 MHz FSB
Xeon 3.20 GHz, 1M Cache, 800 MHz FSB
Xeon 3.20 GHz, 2M Cache, 533 MHz FSB
Xeon 3.20 GHz, 2M Cache, 800 MHz FSB
Xeon 3.33 GHz, 8M Cache, 667 MHz FSB
Xeon 3.40E GHz, 2M Cache, 800 MHz FSB
Xeon 3.40 GHz, 1M Cache, 800 MHz FSB
Xeon 3.40 GHz, 2M Cache, 800 MHz FSB
Xeon 3.50 GHz, 1M Cache, 667 MHz FSB
Xeon 3.60E GHz, 2M Cache, 800 MHz FSB
Xeon 3.60 GHz, 1M Cache, 800 MHz FSB
Xeon 3.60 GHz, 2M Cache, 800 MHz FSB
Xeon 3.66 GHz, 1M Cache, 667 MHz FSB
Xeon 3.80E GHz, 2M Cache, 800 MHz FSB
Xeon 3.80 GHz, 2M Cache, 800 MHz FSB
Xeon 5030 (4M Cache, 2.66 GHz, 667 MHz FSB)
Xeon 5040 (4M Cache, 2.83 GHz, 667 MHz FSB)
Xeon 5050 (4M Cache, 3.00 GHz, 667 MHz FSB)
Xeon 5060 (4M Cache, 3.20 GHz, 1066 MHz FSB)
Xeon 5063 (4M Cache, 3.20 GHz, 1066 MHz FSB)
Xeon 5070 (4M Cache, 3.46 GHz, 1066 MHz FSB)
Xeon 5080 (4M Cache, 3.73 GHz, 1066 MHz FSB)
Xeon 5110 (4M Cache, 1.60 GHz, 1066 MHz FSB)
Xeon 5120 (4M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon 5130 (4M Cache, 2.00 GHz, 1333 MHz FSB)
Xeon 5140 (4M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon 5150 (4M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon 5160 (4M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon 7020 (2M Cache, 2.66 GHz, 667 MHz FSB)
Xeon 7030 (2M Cache, 2.80 GHz, 800 MHz FSB)
Xeon 7040 (4M Cache, 3.00 GHz, 667 MHz FSB)
Xeon 7041 (4M Cache, 3.00 GHz, 800 MHz FSB)
Xeon 7110M (4M Cache, 2.60 GHz, 800 MHz FSB)
Xeon 7110N (4M Cache, 2.50 GHz, 667 MHz FSB)
Xeon 7120M (4M Cache, 3.00 GHz, 800 MHz FSB)
Xeon 7120N (4M Cache, 3.00 GHz, 667 MHz FSB)
Xeon 7130M (8M Cache, 3.20 GHz, 800 MHz FSB)
Xeon 7130N (8M Cache, 3.16 GHz, 667 MHz FSB)
Xeon 7140M (16M Cache, 3.40 GHz, 800 MHz FSB)
Xeon 7140N (16M Cache, 3.33 GHz, 667 MHz FSB)
Xeon 7150N (16M Cache, 3.50 GHz, 667 MHz FSB)
Xeon Bronze 3104 (8.25M Cache, 1.70 GHz)
Xeon Bronze 3106 (11M Cache, 1.70 GHz)
Xeon D-1513N (6M Cache, 1.60 GHz)
Xeon D-1518 (6M Cache, 2.20 GHz)
Xeon D-1520 (6M Cache, 2.20 GHz)
Xeon D-1521 (6M Cache, 2.40 GHz)
Xeon D-1523N (6M Cache, 2.00 GHz)
Xeon D-1527 (6M Cache, 2.20 GHz)
Xeon D-1528 (9M Cache, 1.90 GHz)
Xeon D-1529 (6M Cache, 1.30 GHz)
Xeon D-1531 (9M Cache, 2.20 GHz)
Xeon D-1533N (9M Cache, 2.10 GHz)
Xeon D-1537 (12M Cache, 1.70 GHz)
Xeon D-1539 (12M Cache, 1.60 GHz)
Xeon D-1540 (12M Cache, 2.00 GHz)
Xeon D-1541 (12M Cache, 2.10 GHz)
Xeon D-1543N (12M Cache, 1.90 GHz)
Xeon D-1548 (12M Cache, 2.00 GHz)
Xeon D-1553N (12M Cache, 2.30 GHz)
Xeon D-1557 (18M Cache, 1.50 GHz)
Xeon D-1559 (18M Cache, 1.50 GHz)
Xeon D-1567 (18M Cache, 2.10 GHz)
Xeon D-1571 (24M Cache, 1.30 GHz)
Xeon D-1577 (24M Cache, 1.30 GHz)
Xeon D-1581 (24M Cache, 1.80 GHz)
Xeon D-1587 (24M Cache, 1.70 GHz)
Xeon E3-1105C (6M Cache, 1.00 GHz)
Xeon E3-1105C v2 (8M Cache, 1.80 GHz)
Xeon E3110 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon E3-1125C (8M Cache, 2.00 GHz)
Xeon E3-1125C v2 (8M Cache, 2.50 GHz)
Xeon E3-1135C v2 (8M Cache, 3.00 GHz)
Xeon E3113 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon E3-1205 v6 (8M Cache, 3.00 GHz)
Xeon E3120 (6M Cache, 3.16 GHz, 1333 MHz FSB)
Xeon E3-1220 (8M Cache, 3.10 GHz)
Xeon E3-1220L (3M Cache, 2.20 GHz)
Xeon E3-1220L v2 (3M Cache, 2.30 GHz)
Xeon E3-1220L v3 (4M Cache, 1.10 GHz)
Xeon E3-1220 v2 (8M Cache, 3.10 GHz)
Xeon E3-1220 v3 (8M Cache, 3.10 GHz)
Xeon E3-1220 v5 (8M Cache, 3.00 GHz)
Xeon E3-1220 v6 (8M Cache, 3.00 GHz)
Xeon E3-1225 (6M Cache, 3.10 GHz)
Xeon E3-1225 v2 (8M Cache, 3.20 GHz)
Xeon E3-1225 v3 (8M Cache, 3.20 GHz)
Xeon E3-1225 v5 (8M Cache, 3.30 GHz)
Xeon E3-1225 v6 (8M Cache, 3.30 GHz)
Xeon E3-1226 v3 (8M Cache, 3.30 GHz)
Xeon E3-1230 (8M Cache, 3.20 GHz)
Xeon E3-1230L v3 (8M Cache, 1.80 GHz)
Xeon E3-1230 v2 (8M Cache, 3.30 GHz)
Xeon E3-1230 v3 (8M Cache, 3.30 GHz)
Xeon E3-1230 v5 (8M Cache, 3.40 GHz)
Xeon E3-1230 v6 (8M Cache, 3.50 GHz)
Xeon E3-1231 v3 (8M Cache, 3.40 GHz)
Xeon E3-1235 (8M Cache, 3.20 GHz)
Xeon E3-1235L v5 (8M Cache, 2.00 GHz)
Xeon E3-1240 (8M Cache, 3.30 GHz)
Xeon E3-1240L v3 (8M Cache, 2.00 GHz)
Xeon E3-1240L v5 (8M Cache, 2.10 GHz)
Xeon E3-1240 v2 (8M Cache, 3.40 GHz)
Xeon E3-1240 v3 (8M Cache, 3.40 GHz)
Xeon E3-1240 v5 (8M Cache, 3.50 GHz)
Xeon E3-1240 v6 (8M Cache, 3.70 GHz)
Xeon E3-1241 v3 (8M Cache, 3.50 GHz)
Xeon E3-1245 (8M Cache, 3.30 GHz)
Xeon E3-1245 v2 (8M Cache, 3.40 GHz)
Xeon E3-1245 v3 (8M Cache, 3.40 GHz)
Xeon E3-1245 v5 (8M Cache, 3.50 GHz)
Xeon E3-1245 v6 (8M Cache, 3.70 GHz)
Xeon E3-1246 v3 (8M Cache, 3.50 GHz)
Xeon E3-1258L v4 (6M Cache, 1.80 GHz)
Xeon E3-1260L (8M Cache, 2.40 GHz)
Xeon E3-1260L v5 (8M Cache, 2.90 GHz)
Xeon E3-1265L v2 (8M Cache, 2.50 GHz)
Xeon E3-1265L v3 (8M Cache, 2.50 GHz)
Xeon E3-1265L v4 (6M Cache, 2.30 GHz)
Xeon E3-1268L v3 (8M Cache, 2.30 GHz)
Xeon E3-1268L v5 (8M Cache, 2.40 GHz)
Xeon E3-1270 (8M Cache, 3.40 GHz)
Xeon E3-1270L v4 (6M Cache, 3.00 GHz)
Xeon E3-1270 v2 (8M Cache, 3.50 GHz)
Xeon E3-1270 v3 (8M Cache, 3.50 GHz)
Xeon E3-1270 v5 (8M Cache, 3.60 GHz)
Xeon E3-1270 v6 (8M Cache, 3.80 GHz)
Xeon E3-1271 v3 (8M Cache, 3.60 GHz)
Xeon E3-1275 (8M Cache, 3.40 GHz)
Xeon E3-1275L v3 (8M Cache, 2.70 GHz)
Xeon E3-1275 v2 (8M Cache, 3.50 GHz)
Xeon E3-1275 v3 (8M Cache, 3.50 GHz)
Xeon E3-1275 v5 (8M Cache, 3.60 GHz)
Xeon E3-1275 v6 (8M Cache, 3.80 GHz)
Xeon E3-1276 v3 (8M Cache, 3.60 GHz)
Xeon E3-1278L v4 (6M Cache, 2.00 GHz)
Xeon E3-1280 (8M Cache, 3.50 GHz)
Xeon E3-1280 v2 (8M Cache, 3.60 GHz)
Xeon E3-1280 v3 (8M Cache, 3.60 GHz)
Xeon E3-1280 v5 (8M Cache, 3.70 GHz)
Xeon E3-1280 v6 (8M Cache, 3.90 GHz)
Xeon E3-1281 v3 (8M Cache, 3.70 GHz)
Xeon E3-1284L v3 (6M Cache, 1.80 GHz)
Xeon E3-1284L v4 (6M Cache, 2.90 GHz)
Xeon E3-1285L v3 (8M Cache, 3.10 GHz)
Xeon E3-1285L v4 (6M Cache, 3.40 GHz)
Xeon E3-1285 v3 (8M Cache, 3.60 GHz)
Xeon E3-1285 v4 (6M Cache, 3.50 GHz)
Xeon E3-1285 v6 (8M Cache, 4.10 GHz)
Xeon E3-1286L v3 (8M Cache, 3.20 GHz)
Xeon E3-1286 v3 (8M Cache, 3.70 GHz)
Xeon E3-1290 (8M Cache, 3.60 GHz)
Xeon E3-1290 v2 (8M Cache, 3.70 GHz)
Xeon E3-1501L v6 (6M Cache, 2.10 GHz)
Xeon E3-1501M v6 (6M Cache, 2.90 GHz)
Xeon E3-1505L v5 (8M Cache, 2.00 GHz)
Xeon E3-1505L v6 (8M Cache, 2.20 GHz)
Xeon E3-1505M v5 (8M Cache, 2.80 GHz)
Xeon E3-1505M v6 (8M Cache, 3.00 GHz)
Xeon E3-1515M v5 (8M Cache, 2.80 GHz)
Xeon E3-1535M v5 (8M Cache, 2.90 GHz)
Xeon E3-1535M v6 (8M Cache, 3.10 GHz)
Xeon E3-1545M v5 (8M Cache, 2.90 GHz)
Xeon E3-1558L v5 (8M Cache, 1.90 GHz)
Xeon E3-1565L v5 (8M Cache, 2.50 GHz)
Xeon E3-1575M v5 (8M Cache, 3.00 GHz)
Xeon E3-1578L v5 (8M Cache, 2.00 GHz)
Xeon E3-1585L v5 (8M Cache, 3.00 GHz)
Xeon E3-1585 v5 (8M Cache, 3.50 GHz)
Xeon E5-1410 (10M Cache, 2.8 GHz)
Xeon E5-1410 v2 (10M Cache, 2.80 GHz)
Xeon E5-1428L (15M Cache, 1.8 GHz)
Xeon E5-1428L v2 (15M Cache, 2.20 GHz)
Xeon E5-1428L v3 (20M Cache, 2.00 GHz)
Xeon E5-1603 (10M Cache, 2.80 GHz, 0.0 GT/s QPI)
Xeon E5-1603 v3 (10M Cache, 2.80 GHz)
Xeon E5-1603 v4 (10M Cache, 2.80 GHz)
Xeon E5-1607 (10M Cache, 3.00 GHz, 0.0 GT/s QPI)
Xeon E5-1607 v2 (10M Cache, 3.00 GHz)
Xeon E5-1607 v3 (10M Cache, 3.10 GHz)
Xeon E5-1607 v4 (10M Cache, 3.10 GHz)
Xeon E5-1620 (10M Cache, 3.60 GHz, 0.0 GT/s QPI)
Xeon E5-1620 v2 (10M Cache, 3.70 GHz)
Xeon E5-1620 v3 (10M Cache, 3.50 GHz)
Xeon E5-1620 v4 (10M Cache, 3.50 GHz)
Xeon E5-1630 v3 (10M Cache, 3.70 GHz)
Xeon E5-1630 v4 (10M Cache, 3.70 GHz)
Xeon E5-1650 (12M Cache, 3.20 GHz, 0.0 GT/s QPI)
Xeon E5-1650 v2 (12M Cache, 3.50 GHz)
Xeon E5-1650 v3 (15M Cache, 3.50 GHz)
Xeon E5-1650 v4 (15M Cache, 3.60 GHz)
Xeon E5-1660 (15M Cache, 3.30 GHz, 0.0 GT/s QPI)
Xeon E5-1660 v2 (15M Cache, 3.70 GHz)
Xeon E5-1660 v3 (20M Cache, 3.00 GHz)
Xeon E5-1660 v4 (20M Cache, 3.20 GHz)
Xeon E5-1680 v2 (25M Cache, 3.00 GHz)
Xeon E5-1680 v3 (20M Cache, 3.20 GHz)
Xeon E5-1680 v4 (20M Cache, 3.40 GHz)
Xeon E5205 (6M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon E5220 (6M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon E5-2403 (10M Cache, 1.80 GHz, 6.40 GT/s QPI)
Xeon E5-2403 v2 (10M Cache, 1.80 GHz)
Xeon E5240 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon E5-2407 (10M Cache, 2.20 GHz, 6.40 GT/s QPI)
Xeon E5-2407 v2 (10M Cache, 2.40 GHz)
Xeon E5-2408L v3 (10M Cache, 1.80 GHz)
Xeon E5-2418L (10M, 2.0 GHz, 6.4 GT/s QPI)
Xeon E5-2418L v2 (15M Cache, 2.00 GHz)
Xeon E5-2418L v3 (15M Cache, 2.00 GHz)
Xeon E5-2420 (15M Cache, 1.90 GHz, 7.20 GT/s QPI)
Xeon E5-2420 v2 (15M Cache, 2.20 GHz)
Xeon E5-2428L (15M, 1.8 GHz, 7.2 GT/s QPI)
Xeon E5-2428L v2 (20M Cache, 1.80 GHz)
Xeon E5-2428L v3 (20M Cache, 1.80 GHz)
Xeon E5-2430 (15M Cache, 2.20 GHz, 7.20 GT/s QPI)
Xeon E5-2430L (15M Cache, 2.00 GHz, 7.20 GT/s QPI)
Xeon E5-2430L v2 (15M Cache, 2.40 GHz)
Xeon E5-2430 v2 (15M Cache, 2.50 GHz)
Xeon E5-2438L v3 (25M Cache, 1.80 GHz)
Xeon E5-2440 (15M Cache, 2.40 GHz, 7.20 GT/s QPI)
Xeon E5-2440 v2 (20M Cache, 1.90 GHz)
Xeon E5-2448L (20M, 1.8 GHz, 8.0 GT/s QPI)
Xeon E5-2448L v2 (25M Cache, 1.80 GHz)
Xeon E5-2450 (20M Cache, 2.10 GHz, 8.00 GT/s QPI)
Xeon E5-2450L (20M Cache, 1.80 GHz, 8.00 GT/s QPI)
Xeon E5-2450L v2 (25M Cache, 1.70 GHz)
Xeon E5-2450 v2 (20M Cache, 2.50 GHz)
Xeon E5-2470 (20M Cache, 2.30 GHz, 8.00 GT/s QPI)
Xeon E5-2470 v2 (25M Cache, 2.40 GHz)
Xeon E5-2603 (10M Cache, 1.80 GHz, 6.40 GT/s QPI)
Xeon E5-2603 v2 (10M Cache, 1.80 GHz)
Xeon E5-2603 v3 (15M Cache, 1.60 GHz)
Xeon E5-2603 v4 (15M Cache, 1.70 GHz)
Xeon E5-2608L v3 (15M Cache, 2.00 GHz)
Xeon E5-2608L v4 (20M Cache, 1.60 GHz)
Xeon E5-2609 (10M Cache, 2.40 GHz, 6.40 GT/s QPI)
Xeon E5-2609 v2 (10M Cache, 2.50 GHz)
Xeon E5-2609 v3 (15M Cache, 1.90 GHz)
Xeon E5-2609 v4 (20M Cache, 1.70 GHz)
Xeon E5-2618L v2 (15M Cache, 2.00 GHz)
Xeon E5-2618L v3 (20M Cache, 2.30 GHz)
Xeon E5-2618L v4 (25M Cache, 2.20 GHz)
Xeon E5-2620 (15M Cache, 2.00 GHz, 7.20 GT/s QPI)
Xeon E5-2620 v2 (15M Cache, 2.10 GHz)
Xeon E5-2620 v3 (15M Cache, 2.40 GHz)
Xeon E5-2620 v4 (20M Cache, 2.10 GHz)
Xeon E5-2623 v3 (10M Cache, 3.00 GHz)
Xeon E5-2623 v4 (10M Cache, 2.60 GHz)
Xeon E5-2628L v2 (20M Cache, 1.90 GHz)
Xeon E5-2628L v3 (25M Cache, 2.00 GHz)
Xeon E5-2628L v4 (30M Cache, 1.90 GHz)
Xeon E5-2630 (15M Cache, 2.30 GHz, 7.20 GT/s QPI)
Xeon E5-2630L (15M Cache, 2.00 GHz, 7.20 GT/s QPI)
Xeon E5-2630L v2 (15M Cache, 2.40 GHz)
Xeon E5-2630L v3 (20M Cache, 1.80 GHz)
Xeon E5-2630L v4 (25M Cache, 1.80 GHz)
Xeon E5-2630 v2 (15M Cache, 2.60 GHz)
Xeon E5-2630 v3 (20M Cache, 2.40 GHz)
Xeon E5-2630 v4 (25M Cache, 2.20 GHz)
Xeon E5-2637 (5M Cache, 3.00 GHz, 8.00 GT/s QPI)
Xeon E5-2637 v2 (15M Cache, 3.50 GHz)
Xeon E5-2637 v3 (15M Cache, 3.50 GHz)
Xeon E5-2637 v4 (15M Cache, 3.50 GHz)
Xeon E5-2640 (15M Cache, 2.50 GHz, 7.20 GT/s QPI)
Xeon E5-2640 v2 (20M Cache, 2.00 GHz)
Xeon E5-2640 v3 (20M Cache, 2.60 GHz)
Xeon E5-2640 v4 (25M Cache, 2.40 GHz)
Xeon E5-2643 (10M Cache, 3.30 GHz, 8.00 GT/s QPI)
Xeon E5-2643 v2 (25M Cache, 3.50 GHz)
Xeon E5-2643 v3 (20M Cache, 3.40 GHz)
Xeon E5-2643 v4 (20M Cache, 3.40 GHz)
Xeon E5-2648L (20M, 1.80 GHz, 8.0 GT/s QPI)
Xeon E5-2648L v2 (25M Cache, 1.90 GHz)
Xeon E5-2648L v3 (30M Cache, 1.80 GHz)
Xeon E5-2648L v4 (35M Cache, 1.80 GHz)
Xeon E5-2650 (20M Cache, 2.00 GHz, 8.00 GT/s QPI)
Xeon E5-2650L (20M Cache, 1.80 GHz, 8.00 GT/s QPI)
Xeon E5-2650L v2 (25M Cache, 1.70 GHz)
Xeon E5-2650L v3 (30M Cache, 1.80 GHz)
Xeon E5-2650L v4 (35M Cache, 1.70 GHz)
Xeon E5-2650 v2 (20M Cache, 2.60 GHz)
Xeon E5-2650 v3 (25M Cache, 2.30 GHz)
Xeon E5-2650 v4 (30M Cache, 2.20 GHz)
Xeon E5-2658 (20M, 2.10 GHz, 8.0 GT/s QPI)
Xeon E5-2658A v3 (30M Cache, 2.20 GHz)
Xeon E5-2658 v2 (25M Cache, 2.40 GHz)
Xeon E5-2658 v3 (30M Cache, 2.20 GHz)
Xeon E5-2658 v4 (35M Cache, 2.30 GHz)
Xeon E5-2660 (20M Cache, 2.20 GHz, 8.00 GT/s QPI)
Xeon E5-2660 v2 (25M Cache, 2.20 GHz)
Xeon E5-2660 v3 (25M Cache, 2.60 GHz)
Xeon E5-2660 v4 (35M Cache, 2.00 GHz)
Xeon E5-2665 (20M Cache, 2.40 GHz, 8.00 GT/s QPI)
Xeon E5-2667 (15M Cache, 2.90 GHz, 8.00 GT/s QPI)
Xeon E5-2667 v2 (25M Cache, 3.30 GHz)
Xeon E5-2667 v3 (20M Cache, 3.20 GHz)
Xeon E5-2667 v4 (25M Cache, 3.20 GHz)
Xeon E5-2670 (20M Cache, 2.60 GHz, 8.00 GT/s QPI)
Xeon E5-2670 v2 (25M Cache, 2.50 GHz)
Xeon E5-2670 v3 (30M Cache, 2.30 GHz)
Xeon E5-2673 v2 (25M Cache, 3.30 GHz)
Xeon E5-2679 v4 (50M Cache, 2.50 GHz)
Xeon E5-2680 (20M Cache, 2.70 GHz, 8.00 GT/s QPI)
Xeon E5-2680 v2 (25M Cache, 2.80 GHz)
Xeon E5-2680 v3 (30M Cache, 2.50 GHz)
Xeon E5-2680 v4 (35M Cache, 2.40 GHz)
Xeon E5-2683 v3 (35M Cache, 2.00 GHz)
Xeon E5-2683 v4 (40M Cache, 2.10 GHz)
Xeon E5-2685 v3 (30M Cache, 2.60 GHz)
Xeon E5-2687W (20M Cache, 3.10 GHz, 8.00 GT/s QPI)
Xeon E5-2687W v2 (25M Cache, 3.40 GHz)
Xeon E5-2687W v3 (25M Cache, 3.10 GHz)
Xeon E5-2687W v4 (30M Cache, 3.00 GHz)
Xeon E5-2689 v4 (25M Cache, 3.10 GHz)
Xeon E5-2690 (20M Cache, 2.90 GHz, 8.00 GT/s QPI)
Xeon E5-2690 v2 (25M Cache, 3.00 GHz)
Xeon E5-2690 v3 (30M Cache, 2.60 GHz)
Xeon E5-2690 v4 (35M Cache, 2.60 GHz)
Xeon E5-2695 v2 (30M Cache, 2.40 GHz)
Xeon E5-2695 v3 (35M Cache, 2.30 GHz)
Xeon E5-2695 v4 (45M Cache, 2.10 GHz)
Xeon E5-2697A v4 (40M Cache, 2.60 GHz)
Xeon E5-2697 v2 (30M Cache, 2.70 GHz)
Xeon E5-2697 v3 (35M Cache, 2.60 GHz)
Xeon E5-2697 v4 (45M Cache, 2.30 GHz)
Xeon E5-2698 v3 (40M Cache, 2.30 GHz)
Xeon E5-2698 v4 (50M Cache, 2.20 GHz)
Xeon E5-2699A v4 (55M Cache, 2.40 GHz)
Xeon E5-2699R v4 (55M Cache, 2.20 GHz)
Xeon E5-2699 v3 (45M Cache, 2.30 GHz)
Xeon E5-2699 v4 (55M Cache, 2.20 GHz)
Xeon E5310 (8M Cache, 1.60 GHz, 1066 MHz FSB)
Xeon E5320 (8M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon E5335 (8M Cache, 2.00 GHz, 1333 MHz FSB)
Xeon E5345 (8M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon E5405 (12M Cache, 2.00 GHz, 1333 MHz FSB)
Xeon E5410 (12M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon E5420 (12M Cache, 2.50 GHz, 1333 MHz FSB)
Xeon E5430 (12M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon E5440 (12M Cache, 2.83 GHz, 1333 MHz FSB)
Xeon E5450 (12M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon E5-4603 (10M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon E5-4603 v2 (10M Cache, 2.20 GHz)
Xeon E5-4607 (12M Cache, 2.20 GHz, 6.40 GT/s QPI)
Xeon E5-4607 v2 (15M Cache, 2.60 GHz)
Xeon E5-4610 (15M Cache, 2.40 GHz, 7.20 GT/s QPI)
Xeon E5-4610 v2 (16M Cache, 2.30 GHz)
Xeon E5-4610 v3 (25M Cache, 1.70 GHz)
Xeon E5-4610 v4 (25M Cache, 1.80 GHz)
Xeon E5-4617 (15M Cache, 2.90 GHz, 7.20 GT/s QPI)
Xeon E5-4620 (16M Cache, 2.20 GHz, 7.20 GT/s QPI)
Xeon E5-4620 v2 (20M Cache, 2.60 GHz)
Xeon E5-4620 v3 (25M Cache, 2.00 GHz)
Xeon E5-4620 v4 (25M Cache, 2.10 GHz)
Xeon E5462 (12M Cache, 2.80 GHz, 1600 MHz FSB)
Xeon E5-4624L v2 (25M Cache, 1.90 GHz)
Xeon E5-4627 v2 (16M Cache, 3.30 GHz)
Xeon E5-4627 v3 (25M Cache, 2.60 GHz)
Xeon E5-4627 v4 (25M Cache, 2.60 GHz)
Xeon E5-4628L v4 (35M Cache, 1.80 GHz)
Xeon E5-4640 (20M Cache, 2.40 GHz, 8.00 GT/s QPI)
Xeon E5-4640 v2 (20M Cache, 2.20 GHz)
Xeon E5-4640 v3 (30M Cache, 1.90 GHz)
Xeon E5-4640 v4 (30M Cache, 2.10 GHz)
Xeon E5-4648 v3 (30M Cache, 1.70 GHz)
Xeon E5-4650 (20M Cache, 2.70 GHz, 8.00 GT/s QPI)
Xeon E5-4650L (20M Cache, 2.60 GHz, 8.00 GT/s QPI)
Xeon E5-4650 v2 (25M Cache, 2.40 GHz)
Xeon E5-4650 v3 (30M Cache, 2.10 GHz)
Xeon E5-4650 v4 (35M Cache, 2.20 GHz)
Xeon E5-4655 v3 (30M Cache, 2.90 GHz)
Xeon E5-4655 v4 (30M Cache, 2.50 GHz)
Xeon E5-4657L v2 (30M Cache, 2.40 GHz)
Xeon E5-4660 v3 (35M Cache, 2.10 GHz)
Xeon E5-4660 v4 (40M Cache, 2.20 GHz)
Xeon E5-4667 v3 (40M Cache, 2.00 GHz)
Xeon E5-4667 v4 (45M Cache, 2.20 GHz)
Xeon E5-4669 v3 (45M Cache, 2.10 GHz)
Xeon E5-4669 v4 (55M Cache, 2.20 GHz)
Xeon E5472 (12M Cache, 3.00 GHz, 1600 MHz FSB)
Xeon E5502 (4M Cache, 1.86 GHz, 4.80 GT/s QPI)
Xeon E5503 (4M Cache, 2.00 GHz, 4.80 GT/s QPI)
Xeon E5504 (4M Cache, 2.00 GHz, 4.80 GT/s QPI)
Xeon E5506 (4M Cache, 2.13 GHz, 4.80 GT/s QPI)
Xeon E5507 (4M Cache, 2.26 GHz, 4.80 GT/s QPI)
Xeon E5520 (8M Cache, 2.26 GHz, 5.86 GT/s QPI)
Xeon E5530 (8M Cache, 2.40 GHz, 5.86 GT/s QPI)
Xeon E5540 (8M Cache, 2.53 GHz, 5.86 GT/s QPI)
Xeon E5603 (4M Cache, 1.60 GHz, 4.80 GT/s QPI)
Xeon E5606 (8M Cache, 2.13 GHz, 4.80 GT/s QPI)
Xeon E5607 (8M Cache, 2.26 GHz, 4.80 GT/s QPI)
Xeon E5620 (12M Cache, 2.40 GHz, 5.86 GT/s QPI)
Xeon E5630 (12M Cache, 2.53 GHz, 5.86 GT/s QPI)
Xeon E5640 (12M Cache, 2.66 GHz, 5.86 GT/s QPI)
Xeon E5645 (12M Cache, 2.40 GHz, 5.86 GT/s QPI)
Xeon E5649 (12M Cache, 2.53 GHz, 5.86 GT/s QPI)
Xeon E6510 (12M Cache, 1.73 GHz, 4.80 GT/s QPI)
Xeon E6540 (18M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon E7210 (8M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon E7220 (8M Cache, 2.93 GHz, 1066 MHz FSB)
Xeon E7-2803 (18M Cache, 1.73 GHz, 4.80 GT/s QPI)
Xeon E7-2820 (18M Cache, 2.00 GHz, 5.86 GT/s QPI)
Xeon E7-2830 (24M Cache, 2.13 GHz, 6.40 GT/s QPI)
Xeon E7-2850 (24M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon E7-2850 v2 (24M Cache, 2.30 GHz)
Xeon E7-2860 (24M Cache, 2.26 GHz, 6.40 GT/s QPI)
Xeon E7-2870 (30M Cache, 2.40 GHz, 6.40 GT/s QPI)
Xeon E7-2870 v2 (30M Cache, 2.30 GHz)
Xeon E7-2880 v2 (37.5M Cache, 2.50 GHz)
Xeon E7-2890 v2 (37.5M Cache, 2.80 GHz)
Xeon E7310 (4M Cache, 1.60 GHz, 1066 MHz FSB)
Xeon E7320 (4M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon E7330 (6M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon E7340 (8M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon E7420 (8M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon E7430 (12M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon E7440 (16M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon E7450 (12M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon E7-4807 (18M Cache, 1.86 GHz, 4.80 GT/s QPI)
Xeon E7-4809 v2 (12M Cache, 1.90 GHz)
Xeon E7-4809 v3 (20M Cache, 2.00 GHz)
Xeon E7-4809 v4 (20M Cache, 2.10 GHz)
Xeon E7-4820 (18M Cache, 2.00 GHz, 5.86 GT/s QPI)
Xeon E7-4820 v2 (16M Cache, 2.00 GHz)
Xeon E7-4820 v3 (25M Cache, 1.90 GHz)
Xeon E7-4820 v4 (25M Cache, 2.00 GHz)
Xeon E7-4830 (24M Cache, 2.13 GHz, 6.40 GT/s QPI)
Xeon E7-4830 v2 (20M Cache, 2.20 GHz)
Xeon E7-4830 v3 (30M Cache, 2.10 GHz)
Xeon E7-4830 v4 (35M Cache, 2.00 GHz)
Xeon E7-4850 (24M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon E7-4850 v2 (24M Cache, 2.30 GHz)
Xeon E7-4850 v3 (35M Cache, 2.20 GHz)
Xeon E7-4850 v4 (40M Cache, 2.10 GHz)
Xeon E7-4860 (24M Cache, 2.26 GHz, 6.40 GT/s QPI)
Xeon E7-4860 v2 (30M Cache, 2.60 GHz)
Xeon E7-4870 (30M Cache, 2.40 GHz, 6.40 GT/s QPI)
Xeon E7-4870 v2 (30M Cache, 2.30 GHz)
Xeon E7-4880 v2 (37.5M Cache, 2.50 GHz)
Xeon E7-4890 v2 (37.5M Cache, 2.80 GHz)
Xeon E7520 (18M Cache, 1.86 GHz, 4.80 GT/s QPI)
Xeon E7530 (12M Cache, 1.86 GHz, 5.86 GT/s QPI)
Xeon E7540 (18M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon E7-8830 (24M Cache, 2.13 GHz, 6.40 GT/s QPI)
Xeon E7-8837 (24M Cache, 2.66 GHz, 6.40 GT/s QPI)
Xeon E7-8850 (24M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon E7-8850 v2 (24M Cache, 2.30 GHz)
Xeon E7-8855 v4 (35M Cache, 2.10 GHz)
Xeon E7-8857 v2 (30M Cache, 3.00 GHz)
Xeon E7-8860 (24M Cache, 2.26 GHz, 6.40 GT/s QPI)
Xeon E7-8860 v3 (40M Cache, 2.20 GHz)
Xeon E7-8860 v4 (45M Cache, 2.20 GHz)
Xeon E7-8867L (30M Cache, 2.13 GHz, 6.40 GT/s QPI)
Xeon E7-8867 v3 (45M Cache, 2.50 GHz)
Xeon E7-8867 v4 (45M Cache, 2.40 GHz)
Xeon E7-8870 (30M Cache, 2.40 GHz, 6.40 GT/s QPI)
Xeon E7-8870 v2 (30M Cache, 2.30 GHz)
Xeon E7-8870 v3 (45M Cache, 2.10 GHz)
Xeon E7-8870 v4 (50M Cache, 2.10 GHz)
Xeon E7-8880L v2 (37.5M Cache, 2.20 GHz)
Xeon E7-8880L v3 (45M Cache, 2.00 GHz)
Xeon E7-8880 v2 (37.5M Cache, 2.50 GHz)
Xeon E7-8880 v3 (45M Cache, 2.30 GHz)
Xeon E7-8880 v4 (55M Cache, 2.20 GHz)
Xeon E7-8890 v2 (37.5M Cache, 2.80 GHz)
Xeon E7-8890 v3 (45M Cache, 2.50 GHz)
Xeon E7-8890 v4 (60M Cache, 2.20 GHz)
Xeon E7-8891 v2 (37.5M Cache, 3.20 GHz)
Xeon E7-8891 v3 (45M Cache, 2.80 GHz)
Xeon E7-8891 v4 (60M Cache, 2.80 GHz)
Xeon E7-8893 v2 (37.5M Cache, 3.40 GHz)
Xeon E7-8893 v3 (45M Cache, 3.20 GHz)
Xeon E7-8893 v4 (60M Cache, 3.20 GHz)
Xeon E7-8894 v4 (60M Cache, 2.40 GHz)
Xeon E7-8895 v2 (37.5M Cache, 2.80 GHz)
Xeon E7-8895 v3 (45M Cache, 2.60 GHz)
Xeon EC3539 (8M Cache, 2.13 GHz)
Xeon EC5509 (8M Cache, 2.00 GHz, 4.80 GT/s QPI)
Xeon EC5539 (4M Cache, 2.27 GHz, 5.87 GT/s QPI)
Xeon EC5549 (8M Cache, 2.53 GHz, 5.87 GT/s QPI)
Xeon Gold 5115 (13.75M Cache, 2.40 GHz)
Xeon Gold 5117 (19.25M Cache, 2.00 GHz)
Xeon Gold 5117F (19.25M Cache, 2.00 GHz)
Xeon Gold 5118 (16.5M Cache, 2.30 GHz)
Xeon Gold 5119T (19.25M Cache, 1.90 GHz)
Xeon Gold 5120 (19.25M Cache, 2.20 GHz)
Xeon Gold 5120T (19.25M Cache, 2.20 GHz)
Xeon Gold 5122 (16.5M Cache, 3.60 GHz)
Xeon Gold 6126 (19.25M Cache, 2.60 GHz)
Xeon Gold 6126F (19.25M Cache, 2.60 GHz)
Xeon Gold 6126T (19.25M Cache, 2.60 GHz)
Xeon Gold 6128 (19.25M Cache, 3.40 GHz)
Xeon Gold 6130 (22M Cache, 2.10 GHz)
Xeon Gold 6130F (22M Cache, 2.10 GHz)
Xeon Gold 6130T (22M Cache, 2.10 GHz)
Xeon Gold 6132 (19.25M Cache, 2.60 GHz)
Xeon Gold 6134 (24.75M Cache, 3.20 GHz)
Xeon Gold 6134M (24.75M Cache, 3.20 GHz)
Xeon Gold 6136 (24.75M Cache, 3.00 GHz)
Xeon Gold 6138 (27.5M Cache, 2.00 GHz)
Xeon Gold 6138F (27.5M Cache, 2.00 GHz)
Xeon Gold 6138T (27.5M Cache, 2.00 GHz)
Xeon Gold 6140 (24.75M Cache, 2.30 GHz)
Xeon Gold 6140M (24.75M Cache, 2.30 GHz)
Xeon Gold 6142 (22M Cache, 2.60 GHz)
Xeon Gold 6142F (22M Cache, 2.60 GHz)
Xeon Gold 6142M (22M Cache, 2.60 GHz)
Xeon Gold 6144 (24.75M Cache, 3.50 GHz)
Xeon Gold 6146 (24.75M Cache, 3.20 GHz)
Xeon Gold 6148 (27.5M Cache, 2.40 GHz)
Xeon Gold 6148F (27.5M Cache, 2.40 GHz)
Xeon Gold 6150 (24.75M Cache, 2.70 GHz)
Xeon Gold 6152 (30.25M Cache, 2.10 GHz)
Xeon Gold 6154 (24.75M Cache, 3.00 GHz)
Xeon L3014 (3M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon L3110 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon L3360 (12M Cache, 2.83 GHz, 1333 MHz FSB)
Xeon L3406 (4M Cache, 2.26 GHz)
Xeon L3426 (8M Cache, 1.86 GHz)
Xeon L5215 (6M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon L5238 (6M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon L5240 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon L5248 (6M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon L5310 (8M Cache, 1.60 GHz, 1066 MHz FSB)
Xeon L5318 (8M Cache, 1.60 GHz, 1066 MHz FSB)
Xeon L5320 (8M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon L5335 (8M Cache, 2.00 GHz, 1333 MHz FSB)
Xeon L5408 (12M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon L5410 (12M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon L5420 (12M Cache, 2.50 GHz, 1333 MHz FSB)
Xeon L5430 (12M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon L5506 (4M Cache, 2.13 GHz, 4.80 GT/s QPI)
Xeon L5508 (8M Cache, 2.00 GHz, 5.86 GT/s QPI)
Xeon L5518 (8M Cache, 2.13 GHz, 5.86 GT/s QPI)
Xeon L5520 (8M Cache, 2.26 GHz, 5.86 GT/s QPI)
Xeon L5530 (8M Cache, 2.40 GHz, 5.86 GT/s QPI)
Xeon L5609 (12M Cache, 1.86 GHz, 4.80 GT/s QPI)
Xeon L5618 (12M Cache, 1.87 GHz, 5.86 GT/s QPI)
Xeon L5630 (12M Cache, 2.13 GHz, 5.86 GT/s QPI)
Xeon L5638 (12M Cache, 2.00 GHz, 5.86 GT/s QPI)
Xeon L5640 (12M Cache, 2.26 GHz, 5.86 GT/s QPI)
Xeon L7345 (8M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon L7445 (12M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon L7455 (12M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon L7545 (18M Cache, 1.86 GHz, 5.86 GT/s QPI)
Xeon L7555 (24M Cache, 1.86 GHz, 5.86 GT/s QPI)
Xeon LC3518 (2M Cache, 1.73 GHz)
Xeon LC3528 (4M Cache, 1.73 GHz)
Xeon LC5518 (8M Cache, 1.73 GHz, 4.80 GT/s QPI)
Xeon LC5528 (8M Cache, 2.13 GHz, 4.80 GT/s QPI)
Xeon LV 1.60 GHz, 512K Cache, 400 MHz FSB
Xeon LV 1.66 GHz, 2M Cache, 667 MHz FSB
Xeon LV 2.00 GHz, 2M Cache, 667 MHz FSB
Xeon LV 2.16 GHz, 2M Cache, 667 MHz FSB
Xeon LV 2.40 GHz, 512K Cache, 400 MHz FSB
Xeon LV 2.80 GHz, 1M Cache, 800 MHz FSB
Xeon LV 5113 (4M Cache, 1.60 GHz, 800 MHz FSB)
Xeon LV 5128 (4M Cache, 1.86 GHz, 1066 MHz FSB)
Xeon LV 5133 (4M Cache, 2.20 GHz, 800 MHz FSB)
Xeon LV 5138 (4M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon LV 5148 (4M Cache, 2.33 GHz, 1333 MHz FSB)
Xeon Phi 7210 (16GB, 1.30 GHz, 64 core)
Xeon Phi 7210F (16GB, 1.30 GHz, 64 core)
Xeon Phi 7230 (16GB, 1.30 GHz, 64 core)
Xeon Phi 7230F (16GB, 1.30 GHz, 64 core)
Xeon Phi 7250 (16GB, 1.40 GHz, 68 core)
Xeon Phi 7250F (16GB, 1.40 GHz, 68 core)
Xeon Phi 7290 (16GB, 1.50 GHz, 72 core)
Xeon Phi 7290F (16GB, 1.50 GHz, 72 core)
Xeon Phi Co3120A (6GB, 1.100 GHz, 57 core)
Xeon Phi Co3120P (6GB, 1.100 GHz, 57 core)
Xeon Phi Co31S1P (8GB, 1.100 GHz, 57 core)
Xeon Phi Co5110P (8GB, 1.053 GHz, 60 core)
Xeon Phi Co5120D (8GB, 1.053 GHz, 60 core)
Xeon Phi Co7120A (16GB, 1.238 GHz, 61 core)
Xeon Phi Co7120D (16GB, 1.238 GHz, 61 core)
Xeon Phi Co7120P (16GB, 1.238 GHz, 61 core)
Xeon Phi Co7120X (16GB, 1.238 GHz, 61 core)
Xeon Platinum 8153 (22M Cache, 2.00 GHz)
Xeon Platinum 8156 (16.5M Cache, 3.60 GHz)
Xeon Platinum 8158 (24.75M Cache, 3.00 GHz)
Xeon Platinum 8160 (33M Cache, 2.10 GHz)
Xeon Platinum 8160F (33M Cache, 2.10 GHz)
Xeon Platinum 8160M (33M Cache, 2.10 GHz)
Xeon Platinum 8160T (33M Cache, 2.10 GHz)
Xeon Platinum 8164 (35.75M Cache, 2.00 GHz)
Xeon Platinum 8168 (33M Cache, 2.70 GHz)
Xeon Platinum 8170 (35.75M Cache, 2.10 GHz)
Xeon Platinum 8170M (35.75M Cache, 2.10 GHz)
Xeon Platinum 8176 (38.5M Cache, 2.10 GHz)
Xeon Platinum 8176F (38.5M Cache, 2.10 GHz)
Xeon Platinum 8176M (38.5M Cache, 2.10 GHz)
Xeon Platinum 8180 (38.5M Cache, 2.50 GHz)
Xeon Platinum 8180M (38.5M Cache, 2.50 GHz)
Xeon Silver 4108 (11M Cache, 1.80 GHz)
Xeon Silver 4109T (11M Cache, 2.00 GHz)
Xeon Silver 4110 (11M Cache, 2.10 GHz)
Xeon Silver 4112 (8.25M Cache, 2.60 GHz)
Xeon Silver 4114 (13.75M Cache, 2.20 GHz)
Xeon Silver 4114T (13.75M Cache, 2.20 GHz)
Xeon Silver 4116 (16.5M Cache, 2.10 GHz)
Xeon Silver 4116T (16.5M cache, 2.10 GHz)
Xeon ULV 1.66 GHz, 2M Cache, 667 MHz FSB
Xeon W-2102 (8.25M Cache, 2.90 GHz)
Xeon W-2104 (8.25M Cache, 3.20 GHz)
Xeon W-2123 (8.25M Cache, 3.60 GHz)
Xeon W-2125 (8.25M Cache, 4.00 GHz)
Xeon W-2133 (8.25M Cache, 3.60 GHz)
Xeon W-2135 (8.25M Cache, 3.70 GHz)
Xeon W-2145 (11M Cache, 3.70 GHz)
Xeon W-2155 (13.75M Cache, 3.30 GHz)
Xeon W-2195 (24.75M Cache, 2.30 GHz)
Xeon W3503 (4M Cache, 2.40 GHz, 4.80 GT/s QPI)
Xeon W3505 (4M Cache, 2.53 GHz, 4.80 GT/s QPI)
Xeon W3520 (8M Cache, 2.66 GHz, 4.80 GT/s QPI)
Xeon W3530 (8M Cache, 2.80 GHz, 4.80 GT/s QPI)
Xeon W3540 (8M Cache, 2.93 GHz, 4.80 GT/s QPI)
Xeon W3550 (8M Cache, 3.06 GHz, 4.80 GT/s QPI)
Xeon W3565 (8M Cache, 3.20 GHz, 4.80 GT/s QPI)
Xeon W3570 (8M Cache, 3.20 GHz, 6.40 GT/s QPI)
Xeon W3580 (8M Cache, 3.33 GHz, 6.40 GT/s QPI)
Xeon W3670 (12M Cache, 3.20 GHz, 4.80 GT/s QPI)
Xeon W3680 (12M Cache, 3.33 GHz, 6.40 GT/s QPI)
Xeon W3690 (12M Cache, 3.46 GHz, 6.40 GT/s QPI)
Xeon W5580 (8M Cache, 3.20 GHz, 6.40 GT/s QPI)
Xeon W5590 (8M Cache, 3.33 GHz, 6.40 GT/s QPI)
Xeon X3210 (8M Cache, 2.13 GHz, 1066 MHz FSB)
Xeon X3220 (8M Cache, 2.40 GHz, 1066 MHz FSB)
Xeon X3230 (8M Cache, 2.66 GHz, 1066 MHz FSB)
Xeon X3320 (6M Cache, 2.50 GHz, 1333 MHz FSB)
Xeon X3323 (6M Cache, 2.50 GHz, 1333 MHz FSB)
Xeon X3330 (6M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon X3350 (12M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon X3353 (12M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon X3360 (12M Cache, 2.83 GHz, 1333 MHz FSB)
Xeon X3363 (12M Cache, 2.83 GHz, 1333 MHz FSB)
Xeon X3370 (12M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon X3380 (12M Cache, 3.16 GHz, 1333 MHz FSB)
Xeon X3430 (8M Cache, 2.40 GHz)
Xeon X3440 (8M Cache, 2.53 GHz)
Xeon X3450 (8M Cache, 2.66 GHz)
Xeon X3460 (8M Cache, 2.80 GHz)
Xeon X3470 (8M Cache, 2.93 GHz)
Xeon X3480 (8M Cache, 3.06 GHz)
Xeon X5260 (6M Cache, 3.33 GHz, 1333 MHz FSB)
Xeon X5270 (6M Cache, 3.50 GHz, 1333 MHz FSB)
Xeon X5272 (6M Cache, 3.40 GHz, 1600 MHz FSB)
Xeon X5355 (8M Cache, 2.66 GHz, 1333 MHz FSB)
Xeon X5365 (8M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon X5450 (12M Cache, 3.00 GHz, 1333 MHz FSB)
Xeon X5460 (12M Cache, 3.16 GHz, 1333 MHz FSB)
Xeon X5470 (12M Cache, 3.33 GHz, 1333 MHz FSB)
Xeon X5472 (12M Cache, 3.00 GHz, 1600 MHz FSB)
Xeon X5482 (12M Cache, 3.20 GHz, 1600 MHz FSB)
Xeon X5492 (12M Cache, 3.40 GHz, 1600 MHz FSB)
Xeon X5550 (8M Cache, 2.66 GHz, 6.40 GT/s QPI)
Xeon X5560 (8M Cache, 2.80 GHz, 6.40 GT/s QPI)
Xeon X5570 (8M Cache, 2.93 GHz, 6.40 GT/s QPI)
Xeon X5647 (12M Cache, 2.93 GHz, 5.86 GT/s QPI)
Xeon X5650 (12M Cache, 2.66 GHz, 6.40 GT/s QPI)
Xeon X5660 (12M Cache, 2.80 GHz, 6.40 GT/s QPI)
Xeon X5667 (12M Cache, 3.06 GHz, 6.40 GT/s QPI)
Xeon X5670 (12M Cache, 2.93 GHz, 6.40 GT/s QPI)
Xeon X5672 (12M Cache, 3.20 GHz, 6.40 GT/s QPI)
Xeon X5675 (12M Cache, 3.06 GHz, 6.40 GT/s QPI)
Xeon X5677 (12M Cache, 3.46 GHz, 6.40 GT/s QPI)
Xeon X5680 (12M Cache, 3.33 GHz, 6.40 GT/s QPI)
Xeon X5687 (12M Cache, 3.60 GHz, 6.40 GT/s QPI)
Xeon X5690 (12M Cache, 3.46 GHz, 6.40 GT/s QPI)
Xeon X6550 (18M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon X7350 (8M Cache, 2.93 GHz, 1066 MHz FSB)
Xeon X7460 (16M Cache, 2.66 GHz, 1066 MHz FSB)
Xeon X7542 (18M Cache, 2.66 GHz, 5.86 GT/s QPI)
Xeon X7550 (18M Cache, 2.00 GHz, 6.40 GT/s QPI)
Xeon X7560 (24M Cache, 2.26 GHz, 6.40 GT/s QPI)
```
