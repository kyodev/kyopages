# iproute2 / net-tools

| | net-tool | iproute | note |
|-| :-- | :-- | -- |
|$|`ifconfig [-a]`|`ip address [show\|list]`|lister les interfaces|
|$|`ifconfig [-a]`|`ip link [show\|list]`|lister les interfaces|
|#|`ifconfig eth1 up`|`ip link set up eth1`|activer une interface|
|#|`ifconfig eth1 down`|`ip link set down eth1`|désactiver une interface|
|#|`ifconfig eth1  10.0.0.1/24`|`ip address add  10.0.0.1/24  dev eth1`|set ip|
|#|`ifconfig eth1  0 `|`ip address del  10.0.0.1/24  dev eth1`|del ip|
|$|`ifconfig eth1`|`ip address show dev eth1 `|montrer ip|
|#|`ifconfig eth1 inet6 address 2002:0db5:0:f102::1/64`|`ip -6 address add 2002:0db5:0:f102::1/64 dev eth1`|set ipv6|
|$|`ifconfig eth1 `|`ip  -6  address show dev eth1`|montrer ipv6|
|#|`ifconfig eth1 inet6 del  2002:0db5:0:f102::1/64`|`ip -6 address del 2002:0db5:0:f102::1/64 dev eth1`|del ipv6|
|#|`ifconfig eth1 hw ether 08:00:27:75:2a:66`|`ip link set dev eth1 address 08:00:27:75:2a:67`|set MAC|
|$|`route -n`|`ip route show`|montrer route|
|$|`netstat -rn`|`ip route show`|montrer route|
|#|`route add default gw 192.168.1.2 eth0`|`ip route add default via 192.168.1.2 dev eth0`|+route défaut|
|#|`route del default gw 192.168.1.1 eth0`|`ip route replace default via 192.168.1.2 dev eth0`|-route défaut|
|#|`route add -net 172.16.32.0/24 gw 192.168.1.1 dev eth0`|`ip route add 172.16.32.0/24 via 192.168.1.1 dev eth0`|+route statique|
|#|`route del -net 172.16.32.0/24`|`ip route del 172.16.32.0/24`|-route statique|
|$|`netstat`|`ss`|stats|
|$|`netstat -l`|`ss -l`|stats|
|$|`arp -an`|`ip neigh`|montrer arp|
|#|`arp -s 192.168.1.100 00:0c:29:c0:5a:ef`|`ip neigh add 192.168.1.100 lladdr 00:0c:29:c0:5a:ef dev eth0`|+arp|
|#|`arp -d 192.168.1.100`|`ip neigh del 192.168.1.100 dev eth0`|-arp|
|#|`ipmaddr add 33:44:00:00:00:01 dev eth0`|`ip maddr add 33:44:00:00:00:01 dev eth0`|+multicast adresse|
|#|`ipmaddr del 33:44:00:00:00:01 dev eth0`|`ip maddr del 33:44:00:00:00:01 dev eth0`|-multicast adresse|
|$|`ipmaddr show dev eth0`|`ip maddr list dev eth0`|montrer multicast adresse|
|$|`netstat -g`|`ip maddr list dev eth0`|montrer multicast adresse|

doc de base: http://blog.jobbole.com/97270/   


## Notes  

`$` exécutable en user (se réfère à `ip`)   
`#` à exécuter en root (se réfère à `ip`)  

ethX ou wlanX sont à remplacer éventuellement par la nouvelle nomenclature:   
`ethX`: `en`(ethernet)`p0s`(p<bus>s) => ex: `enp0s25` (25?)   
`wlanX`: `wl`( wireless LAN)`X` (o<index>) => ex: `wlo1`  

Plusieurs ip pour une interface
````shell
ip address add   10.0.0.1 / 24   broadcast   10.0.0.255   dev  eth1  
ip address add   10.0.0.2 / 24   broadcast   10.0.0.255   dev  eth1  
ip address add   10.0.0.3 / 24   broadcast   10.0.0.255   dev  eth1
````

## Raccourcis

les objets (comme address, route, rule) ont des raccourcis, de mêmes pour les commandes, par exemple:  
`ip a` = `ip addr` = `ip address` = `ip a show` = `ip a s` = `ip a list` = `ip a l` = `ip addr s` = `ip addr list` = `ip addr l` ...

[Une doc complète](http://linux-ip.net/html/index.html)
