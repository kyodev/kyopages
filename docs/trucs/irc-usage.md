# IRC Freenode utilisation

* [freenode.net](https://freenode.net/) est un réseau administré par des volontaires


* si le logiciel ne gère pas ça automatiquement, utiliser le codage de caractères **UTF-8 sur freenode**.   
  inutile avec Hexchat, utile avec xchat: `/charset utf-8` 
* <text> : à remplacer
* [text] : optionnel

## utilisation courante

 cmd | objet 
 --- | ---
`/join ##chan`				| rejoindre un chan ##chan
`/nick <pseudo>`			| demander un pseudo
`/msg nickserv identify <pseudo> <motDePasse>`	| identification, à faire dans les 30s après avoir réclamé un nick enregistré, si le client est configuré avec le mot de passe, opération inutile
`/msg nickserv identify <motDePasse>`	| identification, à faire dans les 30s après avoir réclamé un nick enregistré, si le client est configuré avec le mot de passe, opération inutile
`/me <blabla>`				| affiche pseudo  blablabla (affichage dans la zone de texte avec /me remplacé par pseudo), commande propre au client IRC
`/query <pseudo2>`			| engager une discussion **privée** avec pseudo2
`/away [raison]`			| signaler son départ et la raison (facultative)
`/back`						| signaler son retour
`/charset utf-8` 			| retourne ou modifie le jeu de caractère utilisé
`/part [#salon] [raison]`	| quitter le chan en cours ou autre, raison (facultative)
`/quit [raison]` 			| se déconnecter du serveur actuel, raison (facultative)
`/discon`					| se déconnecter du serveur


## enregister son pseudo

on fait appel à NickServ, le serveur de nick (pseudo)

cmd | objet
--- | ---
`/msg nickserv register <motDePasse> <email>`		| enregistrement pseudo après avoir obtenu un nick. une confirmation sera demandée par mail
`/msg nickserv identify <motDePasse>`				| identification, à faire dans les 30s après avoir réclamé son nick
`/msg nickserv ghost <pseudo> <motDePasse>`			| tuer son pseudo fantôme
`/msg nickserv set password <nouveauMotDePasse>`	| changer de mot de passe
`/msg nickserv info [pseudo]`						| obtenir des infos sur son nick [ou un pseudo]
`/msg NickServ sendpass <pseudo>`					| une clé reçue par mail pour initialiser un nouveau mot de passe
`/msg nickserv drop <pseudo> <pass>`				| supprimer pseudo (inutile si pas enregistré initialement)


## grouper un autre pseudo

cmd | objet
--- | ---
`/msg nickserv group` 					| ajouter pseudo en cours (pseudo2) au groupe
`/msg nickserv ungroup <pseudo2>`		| retirer pseudo2 du groupe


### exemple rapide avec un 2e pseudo non enregistré

```shell
/nick <pseudo2>
/msg NickServ IDENTIFY <pseudo1> <motPasse>
/msg NickServ group
```
```text
  Nick pseudo2 is now registered to your account.
```
si besoin
```shell
/nick <pseudo1>
```


## utilisation possible

 cmd | objet
 --- | ---
`/ALLCHAN <cmd>`					| envoyer une commande à tous les canaux sur lesquels vous êtes présent
`/ALLSERV <cmd>`					| envoyer une commande à tous les serveurs auxquels vous êtes connecté
`/cycle [##chan]`					| quitte et rejoint à nouveau le chan donné
`/dns <pseudo> ou <hôte> ou <IP>` 	| résout le pseudo ou une adresse IP ou un nom d'hôte
`/exec -o cmd`						| exécuter une commande, sortie sur le canal actuel (hexchat)
`/IGNORE <pseudo> ALL QUIET`		| ignorer quelqu'un, pseudo créera un masque adapté



## indésirables

en cas de spam en private: `/mode <pseudo> +R`, bloque les users non enregistrés 
