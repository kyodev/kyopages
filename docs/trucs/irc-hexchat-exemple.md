# exemple configuration hexchat

site officiel: [hexchat.github.io](https://hexchat.github.io/)


## install debian

```
su 
apt install hexchat
```

## configuration

au lancement, ou `Ctrl + S`
![choix des réseaux](/img/hexchat1.png)  

les serveurs: 
![serveurs IRC](/img/hexchat3.png)  


joindre les canaux automatiquement: 
![serveurs IRC](/img/hexchat5.png)  

si pseudo authentifié, pour faire simple  

* méthode authentification: default
* mot de passe: <celuiQuiVaBien\>

manuellement, dans la zone de saisie: /join <canal\>


## utilisation irc

voir: [irc-usage](https://kyodev.frama.io/kyopages/trucs/irc-usage/)  


