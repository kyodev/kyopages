# Compilation d'un kernel.org

* destination debian
* Création originale: naguam 03/01/2017

## Dépendances

Installer les paquets:
```shell
su
apt update
apt install build-essential fakeroot libncurses5-dev dpkg-dev libssl-dev bc gnupg dirmngr flex bison libelf-dev
```

* depuis 4.16, nouvelles dépendances flex bison libelf-dev
* à voir dans les sources: Documentation/process/changes.rst, tous les softs/versions nécessaires
* en ligne [sur kernel.org](https://www.kernel.org/doc/html/latest/process/changes.html#current-minimal-requirements)

Pour avoir un menu graphique de configuration des options:  

 * pour kde (Qt):
```shell
	# permet make xconfig
apt install libqt4-dev
```

 * pour les autres DE:
```shell
	# permet make gconfig
apt install libgtk2.0-dev libglade2-dev
```

```shell
exit
```

## Préparation des dossiers

tout est à faire en **user**

```shell
	# création dossier kernel dans le home si inexistant
mkdir -p ~/kernel

	# on va dans le dossier
cd ~/kernel
```

## Chargement

**les versions rc** (releases candidates), sont destinées aux beta-testeurs ou aux aventuriers. elles ne sont pas signées, pas mainline, les liens ci après devront être adaptés.

```shell
	# **/!\** modifier la version du kernel à charger selon la disponibilité
kversion=4.16.3

	# la signature des sources
wget https://www.kernel.org/pub/linux/kernel/v${kversion:0:1}.x/linux-$kversion.tar.sign
	# les sources du kernel
wget https://www.kernel.org/pub/linux/kernel/v${kversion:0:1}.x/linux-$kversion.tar.xz
```

vérification signature des sources:
```shell
xz -cd linux-$kversion.tar.xz | gpg --verify linux-$kversion.tar.sign
```
```text
	gpg: Signature made mer. 03 mai 2017 17:38:11 CEST
	gpg: using RSA key 647F28654894E3BD457199BE38DBBDC86092693E
	gpg: Can't check signature: Pas de clef publique
```
Remarque: extraction clé, signature inconnue (Can't check signature).

recherche clé:
```shell
gpg --keyserver hkp://keys.gnupg.net --recv-keys 647F28654894E3BD457199BE38DBBDC86092693E
```
```text
	gpg: key 38DBBDC86092693E: public key "Greg Kroah-Hartman (Linux kernel stable release signing key) 
	           greg@kroah.com" imported
	gpg: no ultimately trusted keys found
	gpg: Total number processed: 1
	gpg: imported: 1
```

re-vérification:
```shell
xz -cd linux-$kversion.tar.xz | gpg --verify linux-$kversion.tar.sign
```
```text
	gpg: Signature made mer. 03 mai 2017 17:38:11 CEST
	gpg: using RSA key 647F28654894E3BD457199BE38DBBDC86092693E
	gpg: Good signature from "Greg Kroah-Hartman (Linux kernel stable release signing key) greg@kroah.com" [unknown]
	gpg: WARNING: This key is not certified with a trusted signature!
	gpg: There is no indication that the signature belongs to the owner.
	Primary key fingerprint: 647F 2865 4894 E3BD 4571 99BE 38DB BDC8 6092 693E
``` 
Remarque : 

* **Good signature** from "Greg Kroah-Hartman (Linux kernel stable release signing key) greg@kroah.com" [unknown]
* signature ok 
* WARNING: This key is not certified with a trusted signature!   
  la clé n'est pas signée avec une signature de confiance, mais on s'arrête là.
* **/!\\** si **BAD signature**, vérifier:
   * la procédure
   * le chargement
   * contacter ftpadmin@kernel.org immédiatement pour investigation

_éventuellement pour voir/contacter les signataires de cette clé:_
```shell
gpg --list-sigs ABAF11C65A2970B130ABE3C479BE3E4300411886
```

## Décompression

```shell
	# on décompresse
tar -xaf linux-$kversion.tar.xz

	# on va dans le nouveau dossier de l'archive
cd linux-$kversion
```

## Configuration des options

à voir `make help`

```shell
make clean 		# effacement des fichiers générés mais maintien de .config
make mrproper	# effacement des fichiers généréss + .config + divers fichiers sauvegardés
make distclean	# comme _mrproper_ + effacement sauvegarde editeur et fichier patch
```


### reprise des options actuelles ou nouvelles configs

```shell
make olddefconfig		# réponse par défaut aux nouvelles options sans demander

make oldconfig 			# config existant + demande pour les nouvelles options du kernel
make silentoldconfig 	# comme ci-dessus, moins verbeux, mise à jour des dépendances
make oldnoconfig 		# config existant + réponse **N** à tous les nouvelles options.
make localmodconfig 	# config courante avec les modules chargés (lsmod) en désactivant les modules non chargés  
						# pour créer un autre Pc, enregistrer lsmod de cet autre Pc et le passer comme 
						# paramètre LSMOD
						# autrePc$  lsmod > pclsmod  
						# make LSMOD=pclsmod localmodconfig  
make localyesconfig 	# comme localmodconfig, mais convertit tous les modules en core (=y options)

make allyesconfig 	# nouvelle config avec toutes les options à **Yes** autant que possible
make allnoconfig 	# nouvelle config avec toutes les options à **No** autant que possible
make alldefconfig 	# nouvelle config avec toutes les options par **défaut**
make allmodconfig 	# nouvelle config avec toutes les options à **"m"** autant que possible
make tinyconfig 	# configure le plus petit noyau possible
make kvmconfig 		# active options additionnelles pour la prise en charge des invités kvm
make xenconfig 		# active options additionnelles pour xen dom0 la prise en charge des invités kernel
make defconfig 		# création de `./.config` avec les valeurs par défaut de `arch/$ARCH/defconfig`  
					# et `arch/$ARCH/configs/${PLATFORM}_defconfig`, selon l'architecture
make ${PLATFORM}_defconfig	# création de `./.config` avec les valeurs par défaut 
							# de `arch/$ARCH/configs/${PLATFORM}_defconfig`

make savedefconfig 	#sauve config courante comme `./defconfig` (config minima)
```

Remarque, voir aussi `Documentation/kbuild/kconfig.txt`

si les options ne sont pas récupérées automatiquement pour former `.config`  
voir dans ce cas `cp /boot/config-kernelPrecedant .config` ou `/usr/src/linux_headers-kernelPrecedant`

### parcourir ou modifier les options manuellement

```shell
make config 	# en console pure
make menuconfig # en console ncurse 
make nconfig 	# en console avec texte couleur étendu
make gconfig 	# en graphique GTK+
make xconfig 	# en graphique Qt
```

A voir:

 * ~/kernel/linux-4.X.X/README  
 * Documentation/admin-guide/README.rst  
 * [documentation en ligne](https://www.kernel.org/doc/html/latest/admin-guide/index.html)
 
 
### désactiver les clés existantes du kernel

voir: https://lists.debian.org/debian-kernel/2016/04/msg00579.html

```shell
./scripts/config -d CONFIG_MODULE_SIG_ALL -d CONFIG_MODULE_SIG_KEY -d CONFIG_SYSTEM_TRUSTED_KEYS
```

à la compilation, questions posées pour trouver où sont les clés pour signer, ou valider pour défaut, ex:
```text
* Certificates for signature checking
*
File name or PKCS#11 URI of module signing key (MODULE_SIG_KEY) [certs/signing_key.pem] (NEW) 
Provide system-wide ring of trusted keys (SYSTEM_TRUSTED_KEYRING) [Y/?] y
  Additional X.509 keys for default system keyring (SYSTEM_TRUSTED_KEYS) [] (NEW) 
  Reserve area for inserting a certificate without recompiling (SYSTEM_EXTRA_CERTIFICATE) [N/y/?] n
  Provide a keyring to which extra trustable keys may be added (SECONDARY_TRUSTED_KEYRING) [N/y/?] n
```

### éviter de compiler debug

```shell
./scripts/config -d CONFIG_DEBUG_INFO
```

## Compilation

```shell
make bindeb-pkg -j"$(nproc)" LOCALVERSION=-"$(dpkg --print-architecture)"
cd ..
```

> avec certaines anciennes version de kernels, il fallait lancer la compilation avec fakeroot, mais plus maintenant



Rq:

 * `-j"$(nproc)"` tous les threads possibles
 * `-LOCALVERSION=-"$(dpkg --print-architecture)"` optionnel
 * `-KDEB_PKGVERSION="$(make kernelversion)"-1`, optionnel, surcharge la version du paquet avec une révision qui est incrémentée et stockée dans le fichier .version

## Fichiers créés

fichiers créés:
```text
linux...changes				# changelog
linux-firmware-image-...deb 		# microcodes pour certains pilotes  
					# ( absent quand on compile à partir des sources Debian) 
linux-image-...deb			# noyau Linux et modules
linux-headers-...deb			# entêtes pour construire des modules kernel externes
linux-libc-dev_...deb			# entêtes pour certaines bibliothèques de code en espace utilisateur
					# pour le noyau comme GNU C (glibc)
```

## Installation

```shell
su
dpkg -i linux-headers-x.x.x-architecture
dpkg -i linux-image-x.x.x-architecture
exit
```

## Désinstallation
 
```shell
su
dpkg -P linux-image-x.x.x-architechure
dpkg -P linux-headers-x.x.x-architecture
exit
```
Rq: la partie `_....deb` est supprimée  , juste le nom du paquet   
vérifier avec `apt search "linux-image-4.11|linux-headers-4.11"` par exemple

recherche genérique :
```shell
dpkg -l linux-image*  linux-headers* | grep ^ii
```

## Remarques

### problème souvent rencontrés
au reboot

> pcspkr is already registered

```shell
su
echo blacklist pcspkr > /etc/modprobe.d/blacklist-pcspkr.conf
```

### conservation des noyaux installés

dans `/etc/kernel/postinst.d/apt-auto-removal` :
> \# Mark as not-for-autoremoval those kernel packages that are:  
> \#  - the currently booted version  
> \#  - the kernel version we've been called for  
> \#  - the latest kernel version (as determined by debian version number)  
> \#  - the second-latest kernel version  
> \#  
> \# In the common case this results in **two** kernels saved (booted into the  
> \# second-latest kernel, we install the latest kernel in an upgrade), but  
> \# can save up to four. Kernel refers here to a distinct release, which can  
> \# potentially be installed in multiple flavours counting as one kernel.  

 * désinstallez un noyau qui ne convient pas avant d'installer un nouveau
 * annuller ce script apt-auto-removal

```shell
su
mv /etc/kernel/postinst.d/apt-auto-removal /etc/kernel/postinst.d/apt-auto-removal.bak
exit
```

### Pour info

_compilation paquet standard_ :

```shell
make deb-pkg -j"$(nproc)" LOCALVERSION=-"$(dpkg --print-architecture)" KDEB_PKGVERSION="$(make kernelversion)"-1
```
produit tous les fichiers additionnels pour publier sur un dépôt Debian
```text
linux...debian.tar.gz			# control paquet, accessoire pour dépôt debian
linux...dsc				# description, accessoire pour dépôt debian
linux...orig.tar.gz			# sources, accessoire pour dépôt debian
```

### fichiers additionnels:

```text
linux-image-...-dbg_...deb		# symboles de débogage pour l'image du noyau et des modules
```
non présent avec -d CONFIG_DEBUG_INFO


### A voir
 * <https://debian-handbook.info/browse/fr-FR/stable/sect.kernel-compilation.html>
 * <https://kernel-team.pages.debian.net/kernel-handbook/ch-common-tasks.html#s-common-official>
 * <https://kernelnewbies.org/>
 * <https://www.kernel.org/>
