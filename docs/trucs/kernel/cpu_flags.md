
# cpu flags du kernel (cpufeatures.h) au 11/2017

* <https://github.com/torvalds/linux/blob/master/arch/x86/include/asm/cpufeatures.h>

* informations: <https://en.wikipedia.org/wiki/CPUID>

## Intel-defined CPU features

* CPUID level 0x00000001 (edx), word 0

```text
ACPI	⟷ ACPI via MSR
APIC	⟷ Onboard APIC
CLFLUSH	⟷ CLFLUSH instruction
CMOV	⟷ CMOV instructions (plus FCMOVcc, FCOMI with FPU)
CX8	⟷ CMPXCHG8 instruction
DE	⟷ Debugging Extensions
DTS	⟷ Debug Store
FPU	⟷ Onboard FPU
FXSR	⟷ FXSAVE/FXRSTOR, CR4.OSFXSR
HT	⟷ Hyper-Threading
IA64	⟷ IA-64 processor
MCA	⟷ Machine Check Architecture
MCE	⟷ Machine Check Exception
MMX	⟷ Multimedia Extensions
MSR	⟷ Model-Specific Registers
MTRR	⟷ Memory Type Range Registers
PAE	⟷ Physical Address Extensions
PAT	⟷ Page Attribute Table
PBE	⟷ Pending Break Enable
PGE	⟷ Page Global Enable
PN		⟷ Processor serial number
PSE36	⟷ 36-bit PSEs
PSE	⟷ Page Size Extensions
SEP	⟷ SYSENTER/SYSEXIT
SS	⟷ CPU self snoop
SSE2	⟷ sse2
SSE	⟷ sse
TM	⟷ Automatic clock control
TSC	⟷ Time Stamp Counter
VME	⟷ Virtual Mode Extensions
```

## AMD-defined CPU features

* CPUID level 0x80000001, word 1 no duplicate feature flags which are redundant with Intel!

```text
3DNOW	⟷ 3DNow!
3DNOWEXT	⟷ AMD 3DNow! extensions
FXSR_OPT	⟷ FXSAVE/FXRSTOR optimizations
LM	⟷ Long Mode (x86-64)
MMXEXT	⟷ AMD MMX extensions
MP	⟷ MP Capable.
NX	⟷ Execute Disable
PDPE1GB	⟷ GB pages
RDTSCP	⟷ RDTSCP
SYSCALL	⟷ SYSCALL/SYSRET
```

## Transmeta-defined CPU features

* CPUID level 0x80860001, word 2

```text
LONGRUN	⟷ Longrun power control
LRTI	⟷ LongRun table interface
RECOVERY	⟷ CPU in recovery mode
```


## Other features, Linux-defined mapping

* word 3 This range is used for feature bits which conflict or are synthesized

```text
CENTAUR_MCR	⟷ Centaur MCRs (= MTRRs)
CXMMX	⟷ Cyrix MMX extensions
CYRIX_ARR	⟷ Cyrix ARRs (= MTRRs)
```

## cpu types for specific tunings

```text
ACC_POWERAMD	⟷ Accumulated Power Mechanism
ALWAYS	⟷ Always-present feature
AMD_DCM	⟷ multi-node processor
APERFMPERF	⟷ APERFMPERF
ARCH_PERFMON	⟷ Intel Architectural PerfMon
ART	⟷ Platform has always running timer (ART)
BTS	⟷ Branch Trace Store
CONSTANT_TSC	⟷ TSC ticks at a constant rate
CPUID	⟷ CPU has CPUID instruction itself
EXTD_APICID 	⟷ has extended APICID (8 bits)
K7	⟷ Athlon
K8	⟷ Opteron, Athlon64
LFENCE_RDTSC	⟷ Lfence synchronizes RDTSC
MFENCE_RDTSC	⟷ Mfence synchronizes RDTSC
NONSTOP_TSC_S3	⟷ TSC doesn't stop in S3 state
NONSTOP_TSC	⟷ TSC does not stop in C states
NOPL	⟷ instructions
P3	⟷ P3
P4	⟷ P4
PEBS	⟷ Precise-Event Based Sampling
REP_GOOD	⟷ rep microcode works well
SYSCALL32	⟷ syscall in ia32 userspace
SYSENTER32	⟷ sysenter in ia32 userspace
TSC_KNOWN_FREQ	⟷ TSC has known frequency
TSC_RELIABLE	⟷ TSC is known to be reliable
UP	⟷ smp kernel running on up
XTOPOLOGY	⟷ cpu topology enum extensions
```


## Intel-defined CPU features

* CPUID level 0x00000001 (ecx), word 4

```text
AES	⟷ AES instructions
AVX	⟷ Advanced Vector Extensions
CID	⟷ Context ID
CX16	⟷ CMPXCHG16B
DCA	⟷ Direct Cache Access
DS_CPL	⟷ CPL Qual. Debug Store
DTES64	⟷ 64-bit Debug Store
EST	⟷ Enhanced SpeedStep
F16C	⟷ 16-bit fp conversions
FMA	⟷ Fused multiply-add
HYPERVISOR	⟷ Running on a hypervisor
MONITOR	⟷ Monitor/Mwait support
MOVBE	⟷ MOVBE instruction
OSXSAVE	⟷ XSAVE enabled in the OS
PCID	⟷ Process Context Identifiers
PCLMULQDQ	⟷ PCLMULQDQ instruction
PDCM	⟷ Performance Capabilities
PNI	⟷ SSE-3
POPCNT	⟷ POPCNT instruction
RDRAND	⟷ The RDRAND instruction
SDBG	⟷ Silicon Debug
SMX	⟷ Safer mode
SSE4_1	⟷ SSE-4.1
SSE4_2	⟷ SSE-4.2
SSSE3	⟷ Supplemental SSE-3
TM2	⟷ Thermal Monitor 2
TSC_DEADLINE_TIMER	⟷ Tsc deadline timer
VMX	⟷ Hardware virtualization
X2APIC	⟷ x2APIC
XSAVE	⟷ XSAVE/XRSTOR/XSETBV/XGETBV
XTPR	⟷ Send Task Priority Messages
```


## VIA/Cyrix/Centaur-defined CPU features

* CPUID level 0xC0000001, word 5

```text
ACE2	⟷ Advanced Cryptography Engine v2
ACE2_EN	⟷ ACE v2 enabled
ACE_EN	⟷ on-CPU crypto enabled
ACE	⟷ on-CPU crypto (xcrypt)
PHE	⟷ PadLock Hash Engine
PHE_EN	⟷ PHE enabled
PMM_EN	⟷ PMM enabled
PMM	⟷ PadLock Montgomery Multiplier
RNG_EN	⟷ RNG enabled
RNG	⟷ RNG present (xstore)
```


## More extended AMD flags

* CPUID level 0x80000001, ecx, word 6

```text
3DNOWPREFETCH	⟷ 3DNow prefetch instructions
ABM	⟷ Advanced bit manipulation
BPEXT	⟷ data breakpoint extension
CMP_LEGACY	⟷ If yes HyperThreading not valid
CR8_LEGACY	⟷ CR8 in 32-bit mode
EXTAPIC	⟷ Extended APIC space
FMA44	⟷ operands MAC instructions
IBS	⟷ Instruction Based Sampling
LAHF_LM	⟷ LAHF/SAHF in long mode
LWP	⟷ Light Weight Profiling
MISALIGNSSE	⟷ Misaligned SSE mode
MWAITX	⟷ MWAIT extension (MONITORX/MWAITX)
NODEID_MSR	⟷ NodeId MSR
OSVWOS	⟷ Visible Workaround
PERFCTR_CORE	⟷ core performance counter extensions
PERFCTR_LLC	⟷ Last Level Cache performance counter extensions
PERFCTR_NB	⟷ NB performance counter extensions
PTSC	⟷ performance time-stamp counter
SKINIT	⟷ SKINIT/STGI instructions
SSE4A	⟷ SSE-4A
SVM	⟷ Secure virtual machine
TBM	⟷ trailing bit manipulations
TCE	⟷ translation cache extension
TOPOEXT	⟷ topology extensions CPUID leafs
WDT	⟷ Watchdog timer
XOP	⟷ extended AVX instructions
```


## Auxiliary flags: Linux defined

* For features scattered in various CPUID levels like 0x6, 0xA etc, word 7.

```text
AVX512_4FMAPS	⟷ AVX-512 Multiply Accumulation Single precision
AVX512_4VNNIW	⟷ AVX-512 Neural Network Instructions
CAT_L2	⟷ Cache Allocation Technology L2
CAT_L3	⟷ Cache Allocation Technology L3
CDP_L3	⟷ Code and Data Prioritization L3
CPB	⟷ AMD Core Performance Boost
CPUID_FAULT	⟷ Intel CPUID faulting
EPB	⟷ IA32_ENERGY_PERF_BIAS support
HW_PSTATE	⟷ AMD HW-PState
INTEL_PPIN	⟷ Intel Processor Inventory Number
INTEL_PT	⟷ Intel Processor Trace
MBA 	⟷ Memory Bandwidth Allocation
PROC_FEEDBACK	⟷ AMD ProcFeedbackInterface
RING3MWAIT	⟷ Ring 3 MONITOR/MWAIT
SME	⟷ AMD Secure Memory Encryption
```


## Virtualization flags

* Linux defined, word 8

```text
EPT	⟷ Intel Extended Page Table
FLEXPRIORITY	⟷ Intel FlexPriority
TPR_SHADOW	⟷ Intel TPR Shadow
VMMCALL	⟷ Prefer vmmcall to vmcall
VNMI	⟷ Intel Virtual NMI
VPID	⟷ Intel Virtual Processor ID
XENPV	⟷ Xen paravirtual guest
```


## Intel-defined CPU features

* CPUID level 0x00000007:0 (ebx), word 9

```text
ADX	⟷ The ADCX and ADOX instructions
AVX2	⟷ AVX2 instructions
AVX512BW	⟷ AVX-512 BW (Byte/Word granular) Instructions
AVX512CD	⟷ AVX-512 Conflict Detection
AVX512DQ	⟷ AVX-512 DQ (Double/Quad granular) Instructions 
AVX512ER	⟷ AVX-512 Exponential and Reciprocal
AVX512F	⟷ AVX-512 Foundation
AVX512IFMA	⟷ AVX-512 Integer Fused Multiply-Add instructions
AVX512PF	⟷ AVX-512 Prefetch
AVX512VL	⟷ AVX-512 VL (128/256 Vector Length) Extensions
BMI1	⟷ 1st group bit manipulation extensions
BMI2	⟷ 2nd group bit manipulation extensions
CLFLUSHOPT	⟷ CLFLUSHOPT instruction
CLWB	⟷ CLWB instruction
CQM	⟷ Cache QoS Monitoring
ERMS	⟷ Enhanced REP MOVSB/STOSB
FSGSBASE	⟷ {RD/WR}{FS/GS}BASE instructions
HLE	⟷ Hardware Lock Elision
INVPCID	⟷ Invalidate Processor Context ID
MPX	⟷ Memory Protection Extension
RDSEED	⟷ The RDSEED instruction
RDT_A	⟷ Resource Director Technology Allocation
RTM	⟷ Restricted Transactional Memory
SHA_NI	⟷ SHA1/SHA256 Instruction Extensions
SMAP	⟷ Supervisor Mode Access Prevention
SMEP	⟷ Supervisor Mode Execution Protection
TSC_ADJUST	⟷ TSC adjustment MSR 0x3b
```


#Extended state features

* CPUID level 0x0000000d:1 (eax), word 10

```text
XGETBV1	⟷ XGETBV with ECX = 1
XSAVEC	⟷ XSAVEC
XSAVEOPT	⟷ XSAVEOPT
XSAVES	⟷ XSAVES/XRSTORS
```


## Intel-defined CPU QoS Sub-leaf

* CPUID level 0x0000000F:0 (edx), word 11

```text
CQM_LLC	⟷ LLC QoS if 1
```


## Intel-defined CPU QoS Sub-leaf

* CPUID level 0x0000000F:1 (edx), word 12

```text
CQM_MBM_LOCAL	⟷ LLC Local MBM monitoring
CQM_MBM_TOTAL	⟷ LLC Total MBM monitoring
CQM_OCCUP_LLC	⟷ LLC occupancy monitoring if 1
```


## AMD-defined CPU features

* CPUID level 0x80000008 (ebx), word 13

```text
CLZERO	⟷ CLZERO instruction
IRPERF	⟷ Instructions Retired Count
```


## Thermal and Power Management Leaf

* CPUID level 0x00000006 (eax), word 14

```text
ARAT	⟷ Always Running APIC Timer
DTHERM	⟷ Digital Thermal Sensor
HWP_ACT_WINDOW	⟷ HWP Activity Window
HWP_EPP	⟷ HWP Energy Perf. Preference
HWP	⟷ Intel Hardware P-states
HWP_NOTIFY	⟷ HWP Notification
HWP_PKG_REQ	⟷ HWP Package Level Request
IDA	⟷ Intel Dynamic Acceleration
PLN	⟷ Intel Power Limit Notification
PTS	⟷ Intel Package Thermal Status
```


## AMD SVM Feature Identification

* CPUID level 0x8000000a (edx), word 15

```text
AVIC	⟷ Virtual Interrupt Controller
DECODEASSISTS	⟷ Decode Assists support
FLUSHBYASID	⟷ flush-by-ASID support
LBRV	⟷ LBR Virtualization support
NPT	⟷ Nested Page Table support
NRIP_SAVE	⟷ SVM next_rip save
PAUSEFILTER	⟷ filtered pause intercept
PFTHRESHOLD	⟷ pause filter threshold
SVM_LOCK	⟷ SVM locking MSR
TSC_SCALE	⟷ TSC scaling support
VGIF	⟷ Virtual GIF
VMCB_CLEAN	⟷ VMCB clean bits support
V_VMSAVE_VMLOAD	⟷ Virtual VMSAVE VMLOAD
```

## Intel-defined CPU features

* CPUID level 0x00000007:0 (ecx), word 16

```text
AVX512VBMI	⟷ AVX512 Vector Bit Manipulation instructions
AVX512_VPOPCNTDQ	⟷ POPCNT for vectors of DW/QW
LA57	⟷ 5-level page tables
OSPKEOS	⟷ Protection Keys Enable
PKU	⟷ Protection Keys for Userspace
RDPID	⟷ RDPIDinstruction
```


## AMD-defined CPU features

* CPUID level 0x80000007 (ebx), word 17

```text
OVERFLOW_RECOV	⟷ MCA overflow recovery support
SMCA	⟷ Scalable MCA
SUCCOR	⟷ Uncorrectable error containment and recovery
```

# TOTAL TRI

```text
3DNOW	⟷ 3DNow!
3DNOWEXT	⟷ AMD 3DNow! extensions
3DNOWPREFETCH	⟷ 3DNow prefetch instructions
ABM	⟷ Advanced bit manipulation
ACC_POWERAMD	⟷ Accumulated Power Mechanism
ACE2	⟷ Advanced Cryptography Engine v2
ACE2_EN	⟷ ACE v2 enabled
ACE_EN	⟷ on-CPU crypto enabled
ACE	⟷ on-CPU crypto (xcrypt)
ACPI	⟷ ACPI via MSR
ADX	⟷ The ADCX and ADOX instructions
AES	⟷ AES instructions
ALWAYS	⟷ Always-present feature
AMD_DCM	⟷ multi-node processor
APERFMPERF	⟷ APERFMPERF
APIC	⟷ Onboard APIC
ARAT	⟷ Always Running APIC Timer
ARCH_PERFMON	⟷ Intel Architectural PerfMon
ART	⟷ Platform has always running timer (ART)
AVIC	⟷ Virtual Interrupt /Controller
AVX2	⟷ AVX2 instructions
AVX512_4FMAPS	⟷ AVX-512 Multiply Accumulation Single precision
AVX512_4VNNIW	⟷ AVX-512 Neural Network Instructions
AVX512BW	⟷ AVX-512 BW (Byte/Word granular) Instructions
AVX512CD	⟷ AVX-512 Conflict Detection
AVX512DQ	⟷ AVX-512 DQ (Double/Quad granular) Instructions 
AVX512ER	⟷ AVX-512 Exponential and Reciprocal
AVX512F	⟷ AVX-512 Foundation
AVX512IFMA	⟷ AVX-512 Integer Fused Multiply-Add instructions
AVX512PF	⟷ AVX-512 Prefetch
AVX512VBMI	⟷ AVX512 Vector Bit Manipulation instructions
AVX512VL	⟷ AVX-512 VL (128/256 Vector Length) Extensions
AVX512_VPOPCNTDQ	⟷ POPCNT for vectors of DW/QW
AVX	⟷ Advanced Vector Extensions
BMI1	⟷ 1st group bit manipulation extensions
BMI2	⟷ 2nd group bit manipulation extensions
BPEXT	⟷ data breakpoint extension
BTS	⟷ Branch Trace Store
CAT_L2	⟷ Cache Allocation Technology L2
CAT_L3	⟷ Cache Allocation Technology L3
CDP_L3	⟷ Code and Data Prioritization L3
CENTAUR_MCR	⟷ Centaur MCRs (= MTRRs)
CID	⟷ Context ID
CLFLUSH	⟷ CLFLUSH instruction
CLFLUSHOPT	⟷ CLFLUSHOPT instruction
CLWB	⟷ CLWB instruction
CLZERO	⟷ CLZERO instruction
CMOV	⟷ CMOV instructions (plus FCMOVcc, FCOMI with FPU)
CMP_LEGACY	⟷ If yes HyperThreading not valid
CONSTANT_TSC	⟷ TSC ticks at a constant rate
CPB	⟷ AMD Core Performance Boost
CPUID	⟷ CPU has CPUID instruction itself
CPUID_FAULT	⟷ Intel CPUID faulting
CQM	⟷ Cache QoS Monitoring
CQM_LLC	⟷ LLC QoS if 1
CQM_MBM_LOCAL	⟷ LLC Local MBM monitoring
CQM_MBM_TOTAL	⟷ LLC Total MBM monitoring
CQM_OCCUP_LLC	⟷ LLC occupancy monitoring if 1
CR8_LEGACY	⟷ CR8 in 32-bit mode
CX16	⟷ CMPXCHG16B
CX8	⟷ CMPXCHG8 instruction
CXMMX	⟷ Cyrix MMX extensions
CYRIX_ARR	⟷ Cyrix ARRs (= MTRRs)
DCA	⟷ Direct Cache Access
DECODEASSISTS	⟷ Decode Assists support
DE	⟷ Debugging Extensions
DS_CPL	⟷ CPL Qual. Debug Store
DTES64	⟷ 64-bit Debug Store
DTHERM	⟷ Digital Thermal Sensor
DTS	⟷ Debug Store
EPB	⟷ IA32_ENERGY_PERF_BIAS support
EPT	⟷ Intel Extended Page Table
ERMS	⟷ Enhanced REP MOVSB/STOSB
EST	⟷ Enhanced SpeedStep
EXTAPIC	⟷ Extended APIC space
EXTD_APICID	⟷ has extended APICID (8 bits)
F16C	⟷ 16-bit fp conversions
FLEXPRIORITY	⟷ Intel FlexPriority
FLUSHBYASID	⟷ flush-by-ASID support
FMA44	⟷ operands MAC instructions
FMA	⟷ Fused multiply-add
FPU	⟷ Onboard FPU
FSGSBASE	⟷ {RD/WR}{FS/GS}BASE instructions
FXSR	⟷ FXSAVE/FXRSTOR, CR4.OSFXSR
FXSR_OPT	⟷ FXSAVE/FXRSTOR optimizations
HLE	⟷ Hardware Lock Elision
HT	⟷ Hyper-Threading
HWP_ACT_WINDOW	⟷ HWP Activity Window
HWP_EPP	⟷ HWP Energy Perf. Preference
HWP	⟷ Intel Hardware P-states
HWP_NOTIFY	⟷ HWP Notification
HWP_PKG_REQ	⟷ HWP Package Level Request
HW_PSTATE	⟷ AMD HW-PState
HYPERVISOR	⟷ Running on a hypervisor
IA64	⟷ IA-64 processor
IBS	⟷ Instruction Based Sampling
IDA	⟷ Intel Dynamic Acceleration
INTEL_PPIN	⟷ Intel Processor Inventory Number
INTEL_PT	⟷ Intel Processor Trace
INVPCID	⟷ Invalidate Processor Context ID
IRPERF	⟷ Instructions Retired Count
K7	⟷ Athlon
K8	⟷ Opteron, Athlon64
LA57	⟷ 5-level page tables
LAHF_LM	⟷ LAHF/SAHF in long mode
LBRV	⟷ LBR Virtualization support
LFENCE_RDTSC	⟷ Lfence synchronizes RDTSC
LM	⟷ Long Mode (x86-64)
LONGRUN	⟷ Longrun power control
LRTI	⟷ LongRun table interface
LWP	⟷ Light Weight Profiling
MBA 	⟷ Memory Bandwidth Allocation
MCA	⟷ Machine Check Architecture
MCE	⟷ Machine Check Exception
MFENCE_RDTSC	⟷ Mfence synchronizes RDTSC
MISALIGNSSE	⟷ Misaligned SSE mode
MMXEXT	⟷ AMD MMX extensions
MMX	⟷ Multimedia Extensions
MONITOR	⟷ Monitor/Mwait support
MOVBE	⟷ MOVBE instruction
MP	⟷ MP Capable.
MPX	⟷ Memory Protection Extension
MSR	⟷ Model-Specific Registers
MTRR	⟷ Memory Type Range Registers
MWAITX	⟷ MWAIT extension (MONITORX/MWAITX)
NODEID_MSR	⟷ NodeId MSR
NONSTOP_TSC_S3	⟷ TSC doesn't stop in S3 state
NONSTOP_TSC	⟷ TSC does not stop in C states
NOPL	⟷ instructions
NPT	⟷ Nested Page Table support
NRIP_SAVE	⟷ SVM next_rip save
NX	⟷ Execute Disable
OSPKEOS	⟷ Protection Keys Enable
OSVWOS	⟷ Visible Workaround
OSXSAVE	⟷ XSAVE enabled in the OS
OVERFLOW_RECOV	⟷ MCA overflow recovery support
P3	⟷ P3
P4	⟷ P4
PAE	⟷ Physical Address Extensions
PAT	⟷ Page Attribute Table
PAUSEFILTER	⟷ filtered pause intercept
PBE	⟷ Pending Break Enable
PCID	⟷ Process Context Identifiers
PCLMULQDQ	⟷ PCLMULQDQ instruction
PDCM	⟷ Performance Capabilities
PDPE1GB	⟷ GB pages
PEBS	⟷ Precise-Event Based Sampling
PERFCTR_CORE	⟷ core performance counter extensions
PERFCTR_LLC	⟷ Last Level Cache performance counter extensions
PERFCTR_NB	⟷ NB performance counter extensions
PFTHRESHOLD	⟷ pause filter threshold
PGE	⟷ Page Global Enable
PHE	⟷ PadLock Hash Engine
PHE_EN	⟷ PHE enabled
PKU	⟷ Protection Keys for Userspace
PLN	⟷ Intel Power Limit Notification
PMM_EN	⟷ PMM enabled
PMM	⟷ PadLock Montgomery Multiplier
PNI	⟷ SSE-3
PN		⟷ Processor serial number
POPCNT	⟷ POPCNT instruction
PROC_FEEDBACK	⟷ AMD ProcFeedbackInterface
PSE36	⟷ 36-bit PSEs
PSE	⟷ Page Size Extensions
PTSC	⟷ performance time-stamp counter
PTS	⟷ Intel Package Thermal Status
RDPID	⟷ RDPIDinstruction
RDRAND	⟷ The RDRAND instruction
RDSEED	⟷ The RDSEED instruction
RDT_A	⟷ Resource Director Technology Allocation
RDTSCP	⟷ RDTSCP
RECOVERY	⟷ CPU in recovery mode
REP_GOOD	⟷ rep microcode works well
RING3MWAIT	⟷ Ring 3 MONITOR/MWAIT
RNG_EN	⟷ RNG enabled
RNG	⟷ RNG present (xstore)
RTM	⟷ Restricted Transactional Memory
SDBG	⟷ Silicon Debug
SEP	⟷ SYSENTER/SYSEXIT
SHA_NI	⟷ SHA1/SHA256 Instruction Extensions
SKINIT	⟷ SKINIT/STGI instructions
SMAP	⟷ Supervisor Mode Access Prevention
SMCA	⟷ Scalable MCA
SME	⟷ AMD Secure Memory Encryption
SMEP	⟷ Supervisor Mode Execution Protection
SMX	⟷ Safer mode
SS	⟷ CPU self snoop
SSE2	⟷ sse2
SSE4_1	⟷ SSE-4.1
SSE4_2	⟷ SSE-4.2
SSE4A	⟷ SSE-4A
SSE	⟷ sse
SSSE3	⟷ Supplemental SSE-3
SUCCOR	⟷ Uncorrectable error containment and recovery
SVM_LOCK	⟷ SVM locking MSR
SVM	⟷ Secure virtual machine
SYSCALL32	⟷ syscall in ia32 userspace
SYSCALL	⟷ SYSCALL/SYSRET
SYSENTER32	⟷ sysenter in ia32 userspace
TBM	⟷ trailing bit manipulations
TCE	⟷ translation cache extension
TM2	⟷ Thermal Monitor 2
TM	⟷ Automatic clock control
TOPOEXT	⟷ topology extensions CPUID leafs
TPR_SHADOW	⟷ Intel TPR Shadow
TSC_ADJUST	⟷ TSC adjustment MSR 0x3b
TSC_DEADLINE_TIMER	⟷ Tsc deadline timer
TSC_KNOWN_FREQ	⟷ TSC has known frequency
TSC_RELIABLE	⟷ TSC is known to be reliable
TSC_SCALE	⟷ TSC scaling support
TSC	⟷ Time Stamp Counter
UP	⟷ smp kernel running on up
VGIF	⟷ Virtual GIF
VMCB_CLEAN	⟷ VMCB clean bits support
VME	⟷ Virtual Mode Extensions
VMMCALL	⟷ Prefer vmmcall to vmcall
VMX	⟷ Hardware virtualization
VNMI	⟷ Intel Virtual NMI
VPID	⟷ Intel Virtual Processor ID
V_VMSAVE_VMLOAD	⟷ Virtual VMSAVE VMLOAD
WDT	⟷ Watchdog timer
X2APIC	⟷ x2APIC
XENPV	⟷ Xen paravirtual guest
XGETBV1	⟷ XGETBV with ECX = 1
XOP	⟷ extended AVX instructions
XSAVEC	⟷ XSAVEC
XSAVEOPT	⟷ XSAVEOPT
XSAVES	⟷ XSAVES/XRSTORS
XSAVE	⟷ XSAVE/XRSTOR/XSETBV/XGETBV
XTOPOLOGY	⟷ cpu topology enum extensions
XTPR	⟷ Send Task Priority Messages
```
