# Compilation d'un kernel.org avec patch 

* destination debian

## préparation

dans kernel.org, on récupére la nouvelle version de la même branche du noyau :   
4.10.15 => patch incrémental = 4.10.14-15

**Rq**: 

 * **incr. patch**, patch incrémental, est pour une **même branche** (4.10.15 à partir de 4.10.14 par exemple), **version par version**
 * **patch** est pour upgrader une **branche inférieure** (4.10.15 à partir de 4.9.27)
 * un petit avantage de patcher au fur et à mesure est de voir quels fichiers sont impactés

```shell
	# répertoire racine des noyaux à compiler
	# adapter si besoin. Ex: en sous-dossier: linux-4.10.14
cd ~/kernel

	# **/!\** modifier la version du kernel à patcher et celle du patch
	# passer de version à version une par une
	# ne pas changer de branche
kversion=4.10.14
kpatch=4.10.15
```

## téléchargement

```shell
cd ~/kernel/linux-$kversion
kpatchSuff=$(echo $kpatch | awk -F "." '{print $3}')
kload=$kversion'-'$kpatchSuff
kcore=$(echo $kversion | awk -F "." '{print $1}')

wget "https://cdn.kernel.org/pub/linux/kernel/v$kcore.x/incr/patch-$kload.xz"

	# décompression
xz -d patch-$kload.xz

	# localisation patch
if [ -f "patch-$kload/data" ]; then patchDiff=patch-$kload/data; else patchDiff=patch-$kload; fi
if [ ! -f "patch-$kload" ]; then echo "Patch Introuvable, continuer manuellement"; fi
```

## patch / ménage

si patch localisé automatiquement, sinon chercher manuellement où est le patch
```shell
	# patch
patch -p1 -b -i $patchDiff

	# ménage, on remontre, renomme le dossier et redescend pour compiler
cd ..
mv linux-$kversion linux-$kpatch
cd linux-$kpatch
```

exemple sortie en patchant 4.10.14 avec 4.10.15
```text
patching file Makefile
patching file arch/arm/boot/dts/am57xx-idk-common.dtsi
patching file arch/arm/boot/dts/bcm958522er.dts
patching file arch/arm/boot/dts/bcm958525er.dts
patching file arch/arm/boot/dts/bcm958525xmc.dts
patching file arch/arm/boot/dts/bcm958622hr.dts
patching file arch/arm/boot/dts/bcm958623hr.dts
patching file arch/arm/boot/dts/bcm958625hr.dts
patching file arch/arm/boot/dts/bcm988312hr.dts
patching file arch/arm/boot/dts/imx6sx-udoo-neo.dtsi
patching file arch/arm/boot/dts/qcom-ipq8064.dtsi
patching file arch/arm/boot/dts/sun7i-a20-lamobo-r1.dts
patching file arch/arm/mach-omap2/omap-headsmp.S
patching file arch/arm/mach-omap2/omap_hwmod_3xxx_data.c
patching file arch/arm/mach-pxa/ezx.c
patching file arch/arm64/boot/dts/renesas/r8a7795.dtsi
patching file arch/arm64/include/asm/pgtable.h
patching file arch/arm64/kernel/topology.c
patching file arch/arm64/net/bpf_jit_comp.c
patching file arch/mips/kernel/mips-r2-to-r6-emul.c
patching file arch/powerpc/Kconfig
patching file arch/powerpc/include/asm/reg.h
patching file arch/powerpc/kernel/Makefile
patching file arch/powerpc/kvm/book3s_hv.c
patching file arch/powerpc/mm/mmu_context_iommu.c
patching file arch/powerpc/perf/core-book3s.c
patching file arch/powerpc/perf/isa207-common.c
patching file arch/powerpc/perf/isa207-common.h
patching file arch/powerpc/perf/power9-pmu.c
patching file arch/powerpc/platforms/powernv/opal-wrappers.S
patching file arch/sparc/kernel/head_64.S
patching file arch/sparc/lib/GENbzero.S
patching file arch/sparc/lib/NGbzero.S
patching file arch/x86/events/intel/pt.c
patching file arch/x86/include/asm/xen/events.h
patching file arch/x86/kernel/apic/io_apic.c
patching file arch/x86/kernel/kprobes/common.h
patching file arch/x86/kernel/kprobes/core.c
patching file arch/x86/kernel/kprobes/opt.c
patching file arch/x86/kernel/pci-calgary_64.c
patching file arch/x86/kvm/cpuid.c
patching file arch/x86/kvm/vmx.c
patching file arch/x86/pci/xen.c
patching file arch/x86/platform/intel-mid/device_libs/platform_mrfld_wdt.c
patching file arch/x86/xen/enlighten.c
patching file arch/x86/xen/smp.c
patching file arch/x86/xen/time.c
patching file block/blk-integrity.c
patching file block/partition-generic.c
patching file drivers/char/tpm/tpm-chip.c
patching file drivers/char/tpm/tpm.h
patching file drivers/char/tpm/tpm2-cmd.c
patching file drivers/clk/Makefile
patching file drivers/clk/rockchip/clk-rk3036.c
patching file drivers/crypto/caam/caamhash.c
patching file drivers/gpu/drm/hisilicon/hibmc/hibmc_drm_fbdev.c
patching file drivers/gpu/drm/mxsfb/mxsfb_drv.c
patching file drivers/gpu/drm/sti/sti_gdp.c
patching file drivers/gpu/drm/ttm/ttm_bo_vm.c
patching file drivers/hwmon/it87.c
patching file drivers/leds/leds-ktd2692.c
patching file drivers/mtd/nand/Kconfig
patching file drivers/net/ethernet/broadcom/bnxt/bnxt.c
patching file drivers/net/ethernet/cadence/macb.c
patching file drivers/net/geneve.c
patching file drivers/net/macsec.c
patching file drivers/net/phy/mdio-mux-bcm-iproc.c
patching file drivers/net/usb/qmi_wwan.c
patching file drivers/net/wireless/broadcom/brcm80211/brcmfmac/core.c
patching file drivers/net/wireless/intel/iwlwifi/iwl-6000.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/d3.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/debugfs.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/fw-dbg.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/fw.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/mac80211.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/rxmq.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/sta.c
patching file drivers/net/wireless/intel/iwlwifi/mvm/tx.c
patching file drivers/net/wireless/intel/iwlwifi/pcie/internal.h
patching file drivers/net/wireless/intel/iwlwifi/pcie/trans.c
patching file drivers/net/wireless/intel/iwlwifi/pcie/tx.c
patching file drivers/net/wireless/marvell/mwifiex/11n_aggr.c
patching file drivers/net/wireless/marvell/mwifiex/debugfs.c
patching file drivers/net/wireless/marvell/mwifiex/main.c
patching file drivers/net/wireless/marvell/mwifiex/sta_ioctl.c
patching file drivers/phy/Kconfig
patching file drivers/platform/x86/intel_pmc_core.c
patching file drivers/power/supply/bq24190_charger.c
patching file drivers/power/supply/lp8788-charger.c
patching file drivers/scsi/Kconfig
patching file drivers/scsi/qedi/qedi_debugfs.c
patching file drivers/scsi/qedi/qedi_fw.c
patching file drivers/scsi/qedi/qedi_gbl.h
patching file drivers/scsi/qedi/qedi_iscsi.c
patching file drivers/scsi/qla2xxx/qla_os.c
patching file drivers/scsi/smartpqi/smartpqi_init.c
patching file drivers/spi/spi-armada-3700.c
patching file drivers/staging/emxx_udc/emxx_udc.c
patching file drivers/staging/lustre/lustre/llite/lproc_llite.c
patching file drivers/staging/lustre/lustre/ptlrpc/pack_generic.c
patching file drivers/staging/wlan-ng/p80211netdev.c
patching file drivers/tty/serial/8250/8250_omap.c
patching file drivers/usb/chipidea/ci.h
patching file drivers/usb/chipidea/core.c
patching file drivers/usb/chipidea/otg.c
patching file drivers/usb/dwc2/core.c
patching file drivers/usb/host/ehci-exynos.c
patching file drivers/usb/host/ohci-exynos.c
patching file drivers/usb/serial/ark3116.c
patching file drivers/usb/serial/ch341.c
patching file drivers/usb/serial/digi_acceleport.c
patching file drivers/usb/serial/ftdi_sio.c
patching file drivers/usb/serial/io_edgeport.c
patching file drivers/usb/serial/keyspan_pda.c
patching file drivers/usb/serial/mct_u232.c
patching file drivers/usb/serial/quatech2.c
patching file drivers/usb/serial/ssu100.c
patching file drivers/usb/serial/ti_usb_3410_5052.c
patching file drivers/xen/events/events_base.c
patching file drivers/xen/platform-pci.c
patching file fs/9p/acl.c
patching file fs/block_dev.c
patching file fs/f2fs/super.c
patching file include/linux/f2fs_fs.h
patching file include/linux/genhd.h
patching file include/linux/usb/chipidea.h
patching file include/net/addrconf.h
patching file include/net/ip6_route.h
patching file include/xen/xen.h
patching file kernel/bpf/verifier.c
patching file lib/test_bpf.c
patching file net/core/rtnetlink.c
patching file net/core/skbuff.c
patching file net/ipv4/raw.c
patching file net/ipv4/tcp_lp.c
patching file net/ipv4/tcp_minisocks.c
patching file net/ipv4/tcp_output.c
patching file net/ipv6/addrconf.c
patching file net/ipv6/raw.c
patching file net/ipv6/route.c
patching file net/openvswitch/vport-internal_dev.c
patching file sound/pci/hda/hda_intel.c
patching file tools/power/cpupower/utils/helpers/cpuid.c
patching file tools/testing/selftests/bpf/test_verifier.c
patching file tools/testing/selftests/x86/Makefile
```

## compilation

voir [compilation kernel.org](kernel-compil)
