# démarrer avec nomodeset

## ligne de commande

 * au démarrage de Debian, lors de la sélection possible d'autres noyau, appuyer sur `e` pour _editer les 
  commandes avant de démarrer_
 * au clavier, aller sur la ligne commençant par
```text
linux /boot .... [splash] [quiet]
```
ajouter
```text
linux /boot .... [splash] [quiet] nomodeset
```
`Ctrl+x` ou `F10` pour enregistrer et démarrer


## grub
 
fixer ce réglage dans grub,  en **root**
```shell
su
nano /etc/default/grub
```

ajouter sur la ligne commençant par **GRUB_CMDLINE_LINUX_DEFAULT=**: `nomodeset`, ce qui pourrait donner:
```text
GRUB_CMDLINE_LINUX_DEFAULT="quiet nomodeset"
```

mettre à jour le grub: `update-grub`  

voilà, au prochain redémarrage, inutile de se pré-occuper des options de boot, ça se fera en nomodeset.

### remarques

* `GRUB_CMDLINE_LINUX_DEFAULT` concerne le démarrage normal
* `GRUB_CMDLINE_LINUX` concerne tous les démarrages, mode recovery compris
* pour test ou pour nerder, enlever `splash` `quiet` dans les options permet de voir en texte la séquence de 
  boot et d'observer éventuellement des erreurs au démarrage

pour tester ou reconfigurer, modifier la ligne **GRUB_CMDLINE_LINUX_DEFAULT**


## options kernel

* [kernel-boot-options](kernel-boot-options)
* [kernel-parameters](kernel-parameters)

à tester, option boot et/ou grub
 
```
    i915.modeset=1
    i915.modeset=0
    i915.modeset=0 xforcevesa

	i815.
	radeon.
```
