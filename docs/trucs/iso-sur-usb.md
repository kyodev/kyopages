# iso sur usb

## tester les sommes de contrôle

**exemple** avec md5, à adapter:
```shell
md5sum -c MD5SUMS
firmware-stretch-DI-rc5-amd64-netinst.iso: OK
```
ou avec shaX
```shell
shasum -c SHA256SUMS
firmware-stretch-DI-rc5-amd64-netinst.iso: OK
```

## déterminer le périphérique de la clé

```shell
lsblk -f
```
maintenant, on sait qu'elle s'appelle **sdX** (sdb, sdc, sdd...)

éventuellement on pourrait la détecter dans les montages: `cat /etc/mtab`

biensûr on ne navigue plus sur cette clé et on ne s'en sert plus, éventuellement, la **démonter**:   
`umount /dev/sdXy`


## copier l'iso hybride (live)
**ATTENTION: à chaque copie sur un _device_, _sdb_ par exemple, toutes les données et les partitions de 
la clé seront perdues car la table de partition est écrasée**

```shell
su
cp /chemin/image.iso /dev/sdX && sync
```
sync est utilisé pour s'assurer que toutes les données qui sont stockées dans la mémoire lors de la copie 
sont écrites sur la clé.

une méthode _traditionnelle_ **alternative** en cas où...
```shell
su
dd if=/chemin/image.iso of=/dev/sdX bs=10M && sync
```

## partition supplémentaire

### traditionnellement

après avoir copié `image.iso` sur une clé USB, pour utiliser l'espace libre
restant, utilisez un outil de partitionnement tel que _gparted_ afin de créer 
une nouvelle partition sur la clé et la **formater** en ext4

**remarque:** inutile de se casser la tête avec du FAT, windows n'est pas capable de lire
plusieurs partitions sur une clé. on n'abordera pas ici les exceptions tordues à cette
règle pour compenser des lacunes de ce système merdique.

### impossibilité actuelle

cette possibilité, est actuellement indisponible, la cause me semble provenir que les outils
communs, ne savent pas appréhender la construction des images iso hybrides


### amorce de réponse

exemple:
```shell
fdisk -l /dev/sdb
```
```text
Disque /dev/sdb : 7,6 GiB, 8103395328 octets, 15826944 secteurs
Type d'étiquette de disque : dos

Périphérique Amorçage Début     Fin Secteurs Taille Id Type
/dev/sdb1    *           64 2890719  2890656   1,4G  0 Vide
/dev/sdb2              5092    6499     1408   704K ef EFI (FAT-12/16/32)
```

```shell
gdisk -l /dev/sdb
```
```text
Using GPT and creating fresh protective MBR.
Disk /dev/sdb: 15826944 sectors, 7.5 GiB

Number  Start (sector)    End (sector)  Size       Code  Name
   1              64         2890663   1.4 GiB     0700  ISOHybrid
   2            5092            6499   704.0 KiB   0700  ISOHybrid1

```

une image iso hybride doit pouvoir répondre à 2 contraintes:

1. fonctionner aussi bien sur un DVD ou sur une clé USB.
2. booter aussi bien sur des Pc UEFI que BIOS.

ces 2 contraintes induisent 4 scénarios, auxquels une seule image doit répondre,:

2. boot d'un Bios sur une clé Usb: nécessite une table de partition MBR avec une partition taguée active, 
   et où le premier secteur est chargé et exécuté.
4. boot d'un UEFI sur une clé Usb: nécessite une table de partition GPT, avec une partition EFI dans un
   (FilesSystem) FS FAT, devant contenir lui aussi un fichier efi, dans `EFI/BOOT/`.
1. boot d'un Bios sur un DVD: nécessite un secteur de boot dans le FS ISO, qui sera ignoré sur une clé Usb.
3. boot d'un UEFI sur un DVD: nécessite un fichier efi, dans `EFI/BOOT/` dans le FS ISO, qui est chargé 
   et exécuté.

donc les images résultantes ont 2 problèmes à résoudre:

1. pour les scénarios 1 et 2, il y a 2 tables de partitions (MBR, GPT, ce qui violent le standard GPT, qui
   requiert qu'une table MBR de protection soit présente. une seule partition englobe l'espace entier, afin 
   d'empêcher les outils seulement MBR, de considérer le disque comme vide.
2. pour les scénarios 1 et 3, le répertoire EFI est inclus dans le FS ISO dans la première partition, et le 
   même répertoire est mappé aussi dans le FS FAT, dans la deuxième partition. On peut voir que la première 
   partition commence à 64 pour se terminer à 2890719, tandis que la seconde
   commence à 5092 pour se terminer à 6499. Donc la seconde partition est encapsulée dans la première, exposant
   le même répertoire EFI qui est contenu dans la première partition. Concernant les standards, c'est clairement
   invalide, mais les Bios ignore cela et bootent correctement avec cette méthode.   
   C'est la raison pour laquelle xyz-fdisk bute sur cette disposition. Il retient seulement la fin 
   de la dernière partition, soit 6499 dans notre exemple, et calcule le début de l'espace libre à cette limite.


```shell
sfdisk -l /dev/sdb
```
```text
Disque /dev/sdb : 7,6 GiB, 8103395328 octets, 15826944 secteurs
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : dos

Périphérique Amorçage Début     Fin Secteurs Taille Id Type
/dev/sdb1    *           64 2890719  2890656   1,4G  0 Vide
/dev/sdb2              5092    6499     1408   704K ef EFI (FAT-12/16/32)
```

sfdisk devrait être capable de s'accommoder de cette disposition, en tant qu'outil capable
de travailler sur la plupart des dispositions de disque _étranges_:

```shell
umount /dev/sdb1
sfdisk -a /dev/sdb
```
```text
Périphérique Amorçage Début     Fin Secteurs Taille Id Type
/dev/sdb1    *           64 2890719  2890656   1,4G  0 Vide
/dev/sdb2              5092    6499     1408   704K ef EFI (FAT-12/16/32)

Saisissez « help » pour obtenir des renseignements complémentaires.

/dev/sdb3: ,4G		## pour le fun, début dès que possible, 4Go

Une nouvelle partition 3 de type « Linux » et de taille 4 GiB a été créée.
   /dev/sdb3 :      2891776     11280383 (4G) Linux
 
/dev/sdb4: ,,		### début dès que possible, taille maxi

Une nouvelle partition 4 de type « Linux » et de taille 2,2 GiB a été créée.
   /dev/sdb4 :     11280384     15826943 (2,2G) Linux
Toutes les partition utilisées.

Nouvelle situation :

Périphérique Amorçage    Début      Fin Secteurs Taille Id Type
/dev/sdb1    *              64  2890719  2890656   1,4G  0 Vide
/dev/sdb2                 5092     6499     1408   704K ef EFI (FAT-12/16/32)
/dev/sdb3              2891776 11280383  8388608     4G 83 Linux
/dev/sdb4             11280384 15826943  4546560   2,2G 83 Linux

Voulez-vous écrire cela sur le disque ? [O]ui/[N]on : o
```

pour recharger la nouvelle table
```
partprobe
```

formater
```shell
mkfs.ext4 /dev/sdb3
mkfs.ext4 /dev/sdb4
```

l'explorateur de fichier fonctionne, mais  parted/gparted sont toujours à la rue:
```shell
 parted  /dev/sdb
 ```
 ```text
 (parted) print                                                            
Warning: The driver descriptor says the physical block size is 2048 bytes, but Linux says it is 512 bytes.
Ignore/Cancel? i                                                          
Model: PNY USB 2.0 FD (scsi)
Disk /dev/sdb: 32,4GB
Sector size (logical/physical): 2048B/512B
```

ces images iso hybrides sont peut-être  le résultat de hacks solutionnant boot UEFI/MBR, pouvant être 
prises en charge par certains outils, mais cette histoire de taille de secteur de 2048o (typique des iso)
ainsi que le type de partition iso non reconnue et indiquée comme vide, sauf par `gdisk` en GPT. 
