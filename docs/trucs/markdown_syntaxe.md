# Syntaxe Markdown

## formatage de texte

```md
un texte est mis en paragraphe automatiquement. un paragraphe s'arrête et un autre commence
quand il y a deux _newlines_ (retour à la ligne ou retour chariot), donc une ligne vide.

une _newline seule est ignorée
(equivalent d'un espace).

dans un paragraphe, si une ligne est terminée par au moins **deux** espaces,  
cela provoque un retour  
à la ligne.
```

un texte est mis en paragraphe automatiquement (`<p></p>`). un paragraphe s'arrête et un autre commence
quand il y a deux _newlines_ (retour à la ligne ou retour chariot), donc une ligne vide.

une _newline seule est ignorée
(équivalent d'un espace).

dans un paragraphe, si une ligne est terminée par au moins **deux** espaces,  
cela provoque un retour  
à la ligne (`<br>`).


```md
*italique* ou _italique_

**gras** ou __gras__

_l'un et l'autre **peuvent être** imbriqués_

~~barré~~  
```

*italique* ou _italique_

**gras** ou __gras__

_l'un et l'autre **peuvent être** imbriqués_

~~barré~~  (`<del></del>`)

quand la plateforme ou l'application qui convertit n'a pas implémenté une fonctionnalité, comme ici le strike
sur _mkdocs_, on doit pouvoir contourner en utilisant directement du code html, par exemple 
`<del>barré avec html</del>`: <del>barré avec html</del>

## Quote

```md
> quote (citation)

mais aussi

> Bash  est  un interpréteur de commandes (shell) compatible sh  
qui exécute les commandes lues depuis l'entrée standard ou depuis un fichier.  
Bash inclut aussi des fonctionnalités utiles des interpréteurs  
de  commandes Korn et C (ksh et csh).

>Bash  vise  à être une implémentation conforme à la partie relative aux interpréteurs de commandes et 
utilitaires des spécifications IEEE POSIX (norme IEEE 1003.1).
```


> quote (citation) (`<blockquote></blockquote>`)

mais aussi

> Bash  est  un interpréteur de commandes (shell) compatible sh  
qui exécute les commandes lues depuis l'entrée standard ou depuis un fichier.  
Bash inclut aussi des fonctionnalités utiles des interpréteurs  
de  commandes Korn et C (ksh et csh).

>Bash  vise  à être une implémentation conforme à la partie relative aux interpréteurs de commandes et 
utilitaires des spécifications IEEE POSIX (norme IEEE 1003.1).


## ligne horizontale (hr)

ligne horizontale (`hr`)

```md
 ---
ou 
 ***
```

3 tirets minimum entourés par lignes vides 

---

ou 3 *

***

## Titrage

```md
 # titre niveau 1

 ## titre niveau 2

...

 ###### titre niveau 6
```

# titre niveau 1

(`<h1></h1>`)

## titre niveau 2

(`<h2></h2>`)

...

###### titre niveau 6

(`<h6></h6>`)

il est préférable de laisser une ligne vide après un titre, selon plateformes/serveurs

Note: le markdown est converti en html. la sémantique recommande que les niveaux se suivent séquentiellement.
le niveau de titre NE DOIT DONC PAS être induit par la présentation, mais par la logique du contenu.


## Code

```md
du code inline peut être formé avec des simples backticks: `apt update`: mise à jour des paquets Debian 
possibles.
```

du code inline peut être formé avec des simples backticks: `apt update`: mise à jour des paquets Debian 
possibles. (`<code></code>`)


```md
  ```shell
  sudo -s
  apt update
  apt upgrade
  ```
**attention**, pas d'espaces avant les backticks, ici ils servent à éviter la conversion
```
un **bloc** de code peut être formé avec:
au moins 3 backticks, qui peuvent être suivis du type de code 
(utilisable par une éventuelle coloration syntaxique si implémentée) (`<pre><code></code></code>`)
```shell
sudo -s
apt update
apt upgrade

```
**attention**, pas d'espaces avant les backticks, ici ils servent à contourner la conversion

```text
un bloc de code peut être formé avec une indentation par tabulation:

	su
	apt update
	apt upgrade

ou une indentation avec au moins 4 espaces

    su
    apt update
    apt upgrade

```
un bloc de code peut être formé avec une indentation par tabulation:

	su
	apt update
	apt upgrade

ou une indentation avec au moins 4 espaces

    su
    apt update
    apt upgrade


## Listes

une liste peut être non-ordonnée (`<ul><li></li></ul>`), utiliser `*` ou  `-`:
```md
* ligne a
- ligne b
* ligne c
```

* ligne a
- ligne b
* ligne c

par sécurité, laisser une ligne vide avant pour que la liste soit convertie proprement

avec des sous-niveaux (ajouter au moins 4 espaces)
```md
* ligne a
* ligne b
    * ligne ba
    * ligne bb
    * ligne bc
* ligne c
```

* ligne a
* ligne b
    * ligne ba
    * ligne bb
    * ligne bc
* ligne c

(ou avec une tabulation)
```md
* ligne a
* ligne b
* ligne c
	* ligne ca
	* ligne cb
	* ligne cc
```

* ligne a
* ligne b
* ligne c
	* ligne ca
	* ligne cb
	* ligne cc

même principe pour une liste ordonnée (`<ol><li></li></ol>`):
```md
1. ligne 1
    1. ligne 11
    2. ligne 12
    3. ligne 13
2. ligne 2
3. ligne 3
```

1. ligne 1
    1. ligne 11
    2. ligne 12
    3. ligne 13
2. ligne 2
3. ligne 3

avec un mélange des types de listes:
```md
1. ligne 1
    1. ligne 11
    2. ligne 12
    3. ligne 13
2. ligne 2
    * ligne a
    * ligne b
    * ligne c
3. ligne 3
```

1. ligne 1
    1. ligne 11
    2. ligne 12
    3. ligne 13
2. ligne 2
    * ligne a
    * ligne b
    * ligne c
3. ligne 3


## Liens

### hyperliens

```md
des liens web peuvent être formés automatiquement, par exemple:

* https://www.debian.org/
* <https://www.debian.org/> parfois en les entourant de <>
```
des liens web peuvent être formés automatiquement, par exemple:

* https://www.debian.org/
* <https://www.debian.org/> parfois en les entourant de <>

mais ils peuvent aussi être construits:
```md
[le texte du lien Debian](https://www.debian.org/) par exemple
```
[le texte du lien Debian](https://www.debian.org/) par exemple  
équivalent à (`<a href="https://www.debian.org/">le texte du lien Debian</a>`)

### images

```md
une image: ![attribut alt: icone](/kglide_myTux.png) parmi d'autres
```
une image: ![icone curvy tyx](/kglide_myTux.png) parmi d'autres 
(`<img src="/kglide_myTux.png" alt="attribut alt: icone"`)


## Tables

```md
 prénom nom | âge | remarque 
--- | --- | ---
Debra Lynn  | ? | épouse murdock
Ian Murdock  | 42 | âge au décès
Debian | 24
```
**attention**: une ligne vide avant la table  
par sécurité (meilleure compatibilité), il vaut mieux utiliser 3 tirets pour former les colonnes même 
si la configuration d'une colonne est parfois assurée avec un ou deux tirets seulement

 prénom nom | âge | remarque 
--- | --- | ---
Debra Lynn  | ? | épouse murdock
Ian Murdock | 42 | âge au décès |
Debian | 24

les lignes peuvent aussi commencer et se terminer par des pipes (`|`)
```md
| nom | âge | remarque |
| --- | --- | --- |
| Debra Lynn  | ? | épouse murdock |
| Ian Murdock  | 42 | âge au décès |
```

| nom | âge | remarque |
| --- | --- | --- |
| Debra Lynn  | ? | épouse murdock |
| Ian Murdock  | 42 | âge au décès |

l'alignement dans les colonnes peut être configurer
```md
| alignement à gauche (défaut) | alignement centré | alignement à droite |
| :--- | :---: | ---: |
|debra | ian | debian    |
```

| alignement à gauche (par défaut) | alignement centré | alignement à droite |
| :--- | :---: | ---: |
|debra | ian | debian    |


## Divers

### todo

parfois, ce marquage sera joliment rendu (github, gitlab?)
```md
* [x] tâche effectuée
* [ ] tâche à faire
```
* [x] tâche effectuée
* [ ] tâche à faire

### échappement

le caractère `\`, ailleurs que dans du _code_ permet d'échapper le caractère suivant et contourne 
la syntaxe mardown
```md
\*\*ne sera pas en gras, ni en italique\*\*
```
\*\*ne sera pas en gras, ni en italique\*\*

## autres docs

* [Markdown de base](http://daringfireball.net/projects/markdown/syntax)
* [saveurs Github](https://help.github.com/articles/basic-writing-and-formatting-syntax/).
* [index aide Github](https://help.github.com/categories/writing-on-github/).
