# DNS Publics


## OpenNic

OpenNic est un groupe de volontaires proposant:

* des domaines de premier niveau (TLD) alternatifs, en dehors du monopole de l'ICANN américain ( ?bbs, .chan, .dyn, .fur, .geek, .gopher, .indy, .libre, .neo, .null, .o, .oss, .oz, .parody, .pirate),
* interconnexions avec d'autres réseaux alternatifs, comme _Namecoin_, _Emercoin_, _New Nations_
* une gouvernance démocratique
* un réseau de serveurs DNS pour ces domaines alternatifs interconnecté avec le réseau ICANN. Les TLD traditionnels sont donc utilisables avce les avantages des DNS OpenNic
* des serveurs DNS non censurants, non indiscrets (pas de logs ou logs anonymisés), non menteurs
* certains serveurs fonctionnent avec whitelist
* certains serveurs fonctionnent proposent cryptdns

Liens:  

* [OpenNic Project](https://www.opennicproject.org/)
* [DNS sur android rooté](https://play.google.com/store/apps/details?id=uk.co.mytechie.setDNS)
* [wiki openNic](http://wiki.opennic.org/)
* [liste des serveurs](https://servers.opennicproject.org/) avec leurs caractéristiques (cryptDns, voir CA, DE)  


## DNS 	récursif OpenNic

le mieux est de demander une liste actuelles des serveurs actifs:  

[serveurs DNS proches ipv4](https://api.opennicproject.org/geoip/?ns&ipv=4&res=3)   
[serveurs DNS proches ipv6](https://api.opennicproject.org/geoip/?ns&ipv=6&res=3)   


### vérifier que le trafic Dns n'est pas intercepté par le Fai:

#### réponse normale

```shell
dig SOA . @106.186.17.181 -p 5353 +short
```
```text
ns0.opennic.glue. hostmaster.opennic.glue. 2017031906 1800 900 604800 3600
```
**serveur OpenNic visible**, pas de traces apparentes de Dns hijacking


#### problème


```shell
dig SOA . @106.186.17.181 +short
```
```text
a.root-servers.net. nstld.verisign-grs.com. 2015080300 1800 900 604800 86400
```
**/!\** a.root-servers.net n'est **pas un serveur OpenNic**, mais cela pourrait être un autre serveur DNS introduit comme serveur faisant autorité

pour éviter de détournement, via l'interception du port 53, OpenNic propose un port alternatif 5353

```shell
dig SOA . @106.186.17.181 -p 5353 +short
```
```text
ns0.opennic.glue. hostmaster.opennic.glue. 2017031906 1800 900 604800 3600
```
bonne réponse, utiliser ce **port alternatif**


## FDN

Associatif, non censuré, non indiscret https://www.fdn.fr/actions/dns/  

```shell
nameserver 80.67.169.12		# ns0.fdn.fr
nameserver 80.67.169.40		# ns1.fdn.fr

nameserver 2001:910:800::12	
nameserver 2001:910:800::40	
```

## ~~Google DNS~~

## Cloudflare

* non indiscret
* permet TSL ou HTTPS, voir via dautres canaux exotiques comme sms, twitter, telegram, mail...
* rapide, enfin [_marketingement_ parlant](https://kyodev.frama.io/kyopages/scripts/nstest/#bench)
* [1.1.1.1](https://1.1.1.1/fr-FR/)

```shell
nameserver  1.1.1.1
nameserver  1.0.0.1
nameserver  2606:4700:4700::1111
nameserver  2606:4700:4700::1001
```

## autres serveurs DNS ouverts

[adguard](https://adguard.com/en/adguard-dns/overview.html)
[alternate dns](https://alternate-dns.com/)
[cleanbrowsing](https://cleanbrowsing.org/)
[cloudfare](https://1.1.1.1/)
[comodo](https://www.comodo.com/secure-dns/)
[dns watch](https://dns.watch/index)
[dyn oracle](https://help.dyn.com/internet-guide-setup/)
[freenom](http://www.freenom.world/fr/index.html?lang=fr)
[freedns](https://freedns.zone/en/)
[google](https://developers.google.com/speed/public-dns/)
[level3](http://www.level3.com/en/)
[neustar](https://www.security.neustar/dns-services/recursive-dns#free)
[norton connectsafe](https://dns.norton.com/faq.html)
[opendns](https://www.opendns.com/)
[quad9](https://www.quad9.net/)
[uncensoredDNS](https://blog.uncensoreddns.org/)
[verisign](https://www.verisign.com/en_US/security-services/public-dns/index.xhtml)
[yandex](https://dns.yandex.com/advanced/)


## test performance

voir [script utilitaire *_nstest_*](https://kyodev.frama.io/kyopages/scripts/nstest/), permettant de tester les serveurs de resolv.conf et/ou d'un fichier tiers et/ou la liste des serveurs ouverts ci-dessus


## DNS FAI

notes pour mémoire historique, ces serveurs sont censeurs en France, menteurs et pas toujours d'un bonne fiabilité

```
aliceBox	212.27.40.240	212.27.40.241	(vérifié)
BT			194.158.122.10	194.158.122.15	(vérifié)
free		212.27.40.240	212.27.40.241	(vérifié)
nerim		195.5.209.150	194.79.128.150 	(vérifié)
orange		80.10.246.2	80.10.246.129		(vérifié)
ovh			91.121.161.184	188.165.197.144	(vérifié)
sfr			109.0.66.10	109.0.66.20	(?)
numericable					(?)
```

## a voir

* /etc/resolv.conf  
* man resolv.conf
* /etc/dhcp/dhclient.conf
