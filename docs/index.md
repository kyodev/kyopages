## debian 

* stable: version **9**: [Stretch](https://wiki.debian.org/fr/DebianStretch)
   * sortie 06/2017 
* testing:
   * version **10**: [Buster](https://wiki.debian.org/fr/DebianBuster)
      * debut freeze 12/01/2019, full freeze 12/03/2019. **AUCUNE** date de sortie officielle, mais 
      raisonnablement, 3 à 4 mois après le full freeze, soit Juin/Juillet 2019
   * version **11**: [Bullseye ](https://wiki.debian.org/fr/DebianBullseye)
   * version **12**: [Bookworm](https://wiki.debian.org/DebianBookworm)

_avril 2018_


stable 9.6 ciblée pour mi septembre 2018

_Juillet2018_

## doc

<https://www.debian.org/doc/manuals/debian-reference/>  
<https://www.debian.org/releases/stable/amd64/release-notes/>  
<https://www.debian.org/doc/manuals/debian-handbook/>  

## contact sdeb

[##sdeb @freenode.net](http://webchat.freenode.net?channels=%23%23sdeb)
