# formation live-build 

 * 19/05/2017

```text
<arpinux> attention au flood :P
[----------------------
Bienvenue @tou-te-s sur ce chan pour la première session de formation empirique live-build Debian :)
le but de cette formation est de vous guider dans le processus de configuration, de construction et 
de diffusion d'un live Debian.
la session commence maintenant et se poursuit tout le week-end afin de vous permettre de construire, 
tester, puis revenir ici pour résoudre d'éventuels soucis ou ajouter des options de configuration.
il y aura quelques pauses, en fonction de l'heure mais aussi de l'activité de chacun et/ou de mes 5 enfants :D
pour me présenter rapidement : arpinux aka arnault, dev autodidacte qui ne sait pas coder en C, maintainer/builder 
de https://arpinux.org/livarp , https://handylinux.org , https://lescahiersdudebutant.fr/dflinux.html et 
https://kissballad.arpinux.org/kiss0s.html . rédacteur principal de https://lescahiersdudebutant.fr 
et anarchiste assumé.
je bidouille live-build depuis 2011 et non, la documentation officielle et les mans ne sont pas à jour car ça 
bouge trop vite et ils ne sont pas des tonnes à assurer la documentation. donc je me propose de vous aider 
à y voir plus clair dans les options et les possibilités de live-build.
si vous avez déposé vos sources de build sur git ou cloud quelque part, n'hésitez à filer le lien pour que je 
puisse cloner, jeter un œil aux fichiers et tester avec vous :)
si vous êtes là, c'est que vous avez construit au moins 1 live et que vous avez des questions, alors je vous 
en prie, c'est le but, soyez pas timide :)
-----------------------]
```

## principe live-build
```text
<arpinux> 
l'installeur étant buggé pour l'instant le processus par défaut initialisé par le udeb 
live-build et le processus en ligne appelé sont en conflit. en fait, l'installation fait 
cohabiter deux programmes en très gros.
le premier programme, live-build, construit le live, il fonctionne en local. on peut configurer
les dépôts en local, ça ne le gêne pas

le second programme, l'installeur fait appel aux dépôts en ligne, donc si il y a la moindre 
différence dans les processus initiés par ces deux programmes, on se retrouve avec un bug de 
l'installeur au final
c'est rare car les tâches sont normalement bien séparées mais lors de l'installation d'un 
liveCD de type 'live' c'est le système du squaqhfs qui est copié sur le système donc à ce 
moment, les versions se "croisent" si je puis dire

la configuration intégrée dans le live (modifiée par nos soins) et la configuration appelée 
par l'installeur, en UEFI, j'ai remarqué que le preseed n'était pas pris en compte au même 
moment par l'installeur effectivement, alors qu'en démarrage BIOS, oui.
donc ça confirme que la différence vient du traitement efi
```

## conseil
```text
<arpinux> 
le principe de synchronisation des configuration :
un dossier de build contenant les sources + un dossier en ligne sur cloud ou serveur + le /home 
d'une machine de test 3 dossiers à synchroniser pour être certain que le dossier de build soit à jour
comme l'indiquait coyotus, il arrive parfois que des applications changent l'adresse de leur fichier 
de conf le cas ~/.blah.cfg -> ~/.config/blah.cfg est le pus courant effectivement
pourquoi ?
car la normalisation veut que les programmes placent leur configuration par défaut dans /etc/xdg
qui est le dossier système qui correspond à ~/.config pour l'utilisateur
un peu comme le /usr/share/icons/ correspond à votre ~/.icons
un changement d'adresse provoque immanquablement une mauvaise lecture de la configuration
sauf chez conky, merci les devs, qui sait même traduire tes anciennes conf !!! je voulais le préciser 
quand même :D
et donc, une machine test/dédié permet de ne pas se soucier de ce genre de détails tant que votre 
machine test tourne comme vous voulez, c'est que les conf sont lues un bug qui apparaît dans votre 
machine test vous renseigne sur le bug à venir de votre build de la même façon :)
donc prenez soin de tester le plus possible. il vaut mieux tester sur la machine dédiée, plutôt 
que de refaire des builds à l'aveugle car on est sûr que "ça va passer" :)
je me suis fait avoir assez souvent par arrogance :/
pour les dossiers systèmes et les paquets c'est la même chose mais pas de synchronisation
les tests et vérifications se situent au niveau du terminal,
donc pour les paquets, le plus important est de s'assurer de ne pas installer de truc superflus
c'est pourquoi il faut régulièrement faire les mises à jour, bien sûr, mais surtout, envoyer un 
apt autoremove --purge si des paquets ne sont plus nécessaires dans le système test, 
ils seront potentiellement en conflit dans le futur build si vous ne lez retirez pas
```

## live-wrapper
```text
<arpinux> alors pour rassurer, effectivement, ce sera le nouvel outil pour faire des live debian
<arpinux> mais live-build est maintenu
<arpinux> et fabrique des live similaires
<arpinux> hormis cette compatibilité uefi après personnalisation sur une install sans réseau ...
<arpinux> en fait, live-wrapper n'accepte pas de conf locale pour l'instant
<arpinux> pas sur le modèle du chroot.includes de live-build
<arpinux> il est conçu pour choper du paquet
<arpinux> alors d'où tu veux
<arpinux> mais du paquet
<arpinux> donc c'est un outil de fork plus que personnalisation
<arpinux> il impose l'utilisation de dépôts externe pour modifier le comportement des applications
<arpinux> par exemple
<arpinux> avec live-build
<arpinux> pour modifier le menu fluxbox,
<arpinux> il faut modifier le ficher /config/includes.chroot/etc/skel/.fluxbox/menu et voilà
<arpinux> rien de plus
<arpinux> donc en gros, un copier coller de ton menu et hop
<arpinux> simplissime si tu sais où le coller
<arpinux> avec live-wrapper
<arpinux> la modification doit s'effectuer au niveau du menu par défaut de fluxbox
<arpinux> donc fork du paquet fluxbox dans un dépôt externe + utilisation de ce dépôt pour faire ton live
<arpinux> c'est une autre façon de travailler
<arpinux> plus professionnel je dirais :)
<arpinux> dans le cas de live-wrapper, les compétences de maintainer de dépôts sont obligatoire
<arpinux> avec live-build
<arpinux> faut savoir lire et faire des copier coller
	<starsheep> Donc avec live-wrapper tu ne peux rien personnaliser tant que tu n'as pas une infrastructure réseau
<arpinux> tu peux toujours utiliser un dépôt en local starsheep , mais il faudra de toute façon un dépôt en ligne à un moment pour les users
<arpinux> ou alors tes mods risquent d'être écrasées par une mise à jour debian
<arpinux> oui ... bien lire le chapitre "ce qui ne fonctionne pas" ! :D
<arpinux> sinon, c'est la haine
<arpinux> genre tu te modifies tout plein de .desktop dans /usr/share/applications .....
<arpinux> ah non, mais c'est excellent comme outil
<arpinux> mais comme le reste ça devient une "affaire de spécialistes"
<arpinux> par principe, tout fichier appartenant à un paquet est potentiellement écrasable
<arpinux> donc modifier un .desktop ou un menu dans /etc/xdg ne sert à rien
<arpinux> il sera écrasé
<arpinux> donc il faut passer par les dossier de personnalisation prévus
<arpinux> pour l'utilisateur, c'est le /etc/skel
<arpinux> les paquets ne déposent rien là-dedans
<arpinux> c'est le dossier qui forme le /home/$USER lors de l'installation
<arpinux> et aussi le /home/$USER du live
<arpinux> donc c'est l'endroit idéal.
<arpinux> mais ça ne suffit pas toujours
	<kyodev> d'ac et live-wrapper ne permet pas d'exploiter /etc/skel ?
<arpinux> genre pour modifier le grub, le fstab ou ce genre d chose
<arpinux> alors si, avec un paquet externe qui irait peupler le /etc/skel
<arpinux> voilà, toute la conf user dans un paquet
<arpinux> pareil pour la conf système
<arpinux> la plupart des programmes acceptent les configuration de type /etc/apt/sources.list.d
<arpinux> le ".d"
	<starsheep> Donc plus de copier-coller dans live-wrapper
<arpinux> non starsheep
	<starsheep> mais modifier le $HOME via des paquets c'est dégueulasse
	<starsheep> donc faut quand même une autre méthode
<arpinux> mais en ajoutant de la conf système dans les ".d" des dossiers système,n on peut aussi modififer le comportement sans forker les paquets
<arpinux> et oui starsheep , c'est pourri
<arpinux> donc pour faire propre faut forker les paquets
	<starsheep> donc avec live-wrapper faut créer des paquets avec des fichiers de conf dans /etc
<arpinux> donc faut maintenir un dépôt
<arpinux> par le biais d'un paquet, oui starsheep
	<starsheep> Mais si le logiciel n'accepte pas de conf dans etc mais juste dans le $HOME, ben faut reprogrammer
	<starsheep> ou je me trompe ?
<arpinux> d'où les deux paquets, pour le user et pour le sys
<arpinux> la modification user d'un côté et les mods système de l'autre
<arpinux> sinon, pour finir avec live-wrapper, ils vont peut-être ajouter une fonction de personnalisation en local, mais rien pour l'instant
<arpinux> donc je continue d travailler avec live-build
<arpinux> qui est en plus maintenant maintenu par Raphael Hertzog
<arpinux> cool d'avoir un français qui dev dessus :)
<arpinux> il a d’ailleurs déjà filé un coup de main à Patrick D'Emmabuntüs Debian Edition pour rendre l'EmmaDE compatible UEFI sur Jessie :)
<arpinux> devant ce silence, je me dois d'indiquer que la seconde raison qui m'a poussé à faire cette session
<arpinux> c'est le cassage de mythe du dev
<arpinux> les gars comme Raphael sont accessibles, il faut le savoir
<arpinux> donc même si vous débutez et que vous voulez un coup de main, n'hésitez pas à contacter les dev
<arpinux> un peu comme en politique ... faudrait pas hésiter à harceler nos députés ;)
<arpinux> ce sont des humains :)
```
## synchronisation
```	text
		<coyotus> comment tu fais pour exporter tes modif sur git pour avoir tes sources à jours en temps réel ?
	<starsheep> git push ? ^^
		<coyotus> depuis le dossier de build ou l'env de test ?
<arpinux> même pas git perso ... je fonctionne avec un cloud openmailbox.
<arpinux> mais pour la synchro
<arpinux> je fonctionne au cloud à la main
<arpinux> entre la machine test et la machine de build
<arpinux> oui
<arpinux> c'est automatisable
<arpinux> genre git push depuis ta machine de test et git pull depuis ton dossier de build
<arpinux> mais ça impose l'installation de git sur la machine test
<arpinux> alors que justement
<arpinux> la machine test doit rester la plus fidèle possible à l'installation par défaut du user
<arpinux> pour ça que j'utilise un cloud
<arpinux> par exemple
<arpinux> juste avant le build
<arpinux> j'archive sur la machine test mon ~/.mozilla , ~/.config ...
<arpinux> j'upload sur cloud
<arpinux> je récupère sur la machine de build
<arpinux> je mets à jour les dossiers dans le build et hop
<arpinux> ça laisse le système de teste vierge
<arpinux> car c'est ça le piège
<starsheep> Tu fonctionnes en 3 temps alors ?
<arpinux> quand on geek souvent, on se dit que modifier la bonne ligne suffit, mais comme tu l'as dit tout à l'heure, une modification de syntaxe peut aussi intervenir sans ton "consentement" et donc, tu ne le reportera pas dans le build, sauf à synchroniser les dossiers
	<starsheep> Tu build un premier coup, tu balances sur la machine de test, tu fais ta config et tu balance le $HOME pour créer le skel et tu rebuild ?
<arpinux> en gros starsheep oui :)
	<starsheep> heeeeeeeein
	<starsheep> ah ben je viens de comprendre un truc
	<starsheep> et avec une VM et un partage de fichier ça ne peut pas être plus simple ?
<arpinux> mais im ne faut pas non plus tout prendre dans le home de ton test
<arpinux> genre la résolution de l'écran
<arpinux> les icpnes du bureau
	<starsheep> ah oui oui
<arpinux> le cache ...
	<starsheep> c'était pour résumer
<arpinux> bref .. il y a un gros tri à faire ...
<arpinux> oui :)
<arpinux> mais c'est le principe
```
## aparté 
```text
	<starsheep> alors pour le skel je comprends. Mais /usr/share il n'est pas modifié par les paquets lui ?
<arpinux> si on peut
<arpinux> mais sans ajouter de programmes qui seront appelés plus tard
<arpinux> par exemple
<arpinux> si tu veux coller ta version de conky dans le build
<arpinux> tu peux
<arpinux> - forker le paquet et l'appeler depuis la liste des paquets
<arpinux> - forker ton paquet et l'ajouter en dur dans le build
<arpinux> - forker le binaire et l'ajouter à la main
<arpinux> Mais
<arpinux> dans le cas 1, il faudra configurer apt pour lui indiquer par pinning de préférer ton conky à celui d debian lors de la prochaine mise à jour
<arpinux> dans le cas 2, idem, mais en plus, tu ne pourras pas mettre ton conky à jour
<arpinux> dans le cas 3, ton conky sera écrasé dès le build par l'installation du 'vrai' conky
<arpinux> donc tu dois forker + renommer
<arpinux> ou alors
<arpinux> forker + placer ailleurs
	<starsheep> ok
<arpinux> genre .usr/local/bin
	<starsheep> aaaaah ok
<arpinux> dans le cas de DFLinux ou nos autres projets, le soucis ne se pose pas
	<starsheep> c'est pour ça que dans kissos tu as un /usr/local/share
<arpinux> carnos programmes sont des ajouts et non des forks
	<starsheep> pour dwm-kissed
<arpinux> oui starsheep, et aussi pour respecter l'architecture de base : ajout externe > local
	<starsheep> ajout externe ?
	<starsheep> En dehors des paquets ?
<arpinux> oui
```
## séparation binary/chroot
```text
<arpinux> et donc un autre aspect pratique que je souhaitais souligner à propos de live-build,c'est la séparation binary/chroot visible dans plusieurs dossiers/fichiers.
<arpinux> live-build permet de séparer les applications installées dans le live de celles effectivement intégrées dans la distribution finale
	<starsheep> binary c'est pour le live et chroot pour l'installation ?
<arpinux> donc le live 2 en 1 :)
<arpinux> le principe est d'intégrer des programmes qui seront présent dans le live, mais pas dans la session installée et inversement :)
<arpinux> ainsi, vous pouvez avoir sur un même cd, gparted disponible en live pour vérifier vos disques, mais pas dans la version installée
<arpinux> plusieurs avantages : la disponibilité dans le live de nombreux programmes sans pour autant alourdir le système installé
<arpinux> l'installation de nombreux programmes dans le système installé sans alourdir lechargement ni les menus du live
<arpinux> l'équilibre est à trouver selon l'objectif du live
<arpinux>  mais vous voyez de suite le panel des possibilités :)
	<starsheep> yep
<arpinux> l’extrême étant de faire un live qui n'a rien à voir avec le système installé au final
<arpinux> et c'est possible de façon "transparente"
<arpinux> cad sans que le user du live puisse le voir
<starsheep> Il n'y a pas moyen de faire un truc commun aux deux sans faire doublon dans les sources ?
<arpinux> un système live jessie stable pour tourner tranquille, mais qui renvoi à l'installation d'un sid
<arpinux> car les binary/chroot
<arpinux> sont des options disponibles aussi pour les miroirs consultés lors du build
<arpinux> donc live-build peut appeler des paquets de différents branches de debian pour le live indépendamment des paquets du système final
	<kyodev> genre tu appâtes le chaland et crac! sid :o)
<arpinux> yes ! :D
<arpinux> sans aller jusque là, ça a le mérite de pouvoir éviter la surcharge du live en en supprimant toutes les applications non-essentielles par exemple
<arpinux> un live-ultra-light installant un système complet :)
<arpinux> l'inverse est possible aussi. un live ultra-complet pour montrer de quoi est capable debian, mais plusieurs formats d'installation
<arpinux> car oui
<arpinux> on peut aussi installer plusieurs système différents depuis le même live
<arpinux> en dehors de la distinction binary/chroot
<arpinux> oui
<arpinux> ça en fait des possibilités :)
<arpinux> alors pour ceux que ça branche, voilà le principe
<arpinux> vous trouverez dans les sources de DFLinux https://git.framasoft.org/dflinux/DFiso le dossier /config/includes.binary/isolinux
<arpinux> c'est la config du menu syslinux au boot du live
<arpinux> le fichier install.cfg liste les entrées de menu pour l'installation. il indique lefichier preseed de préconfiguration à utiliser, on en a parlé au début.
<arpinux> il suffit de coller plusieurs entrées, avec un appel à des preseed différents :)
<arpinux> on peut coller une liste de paquets dans le preseed, donc....
<arpinux> potentiellement une autre distro
<arpinux> et autant de sitro que d'entrées de menu syslinux
<arpinux> multi-possibilités ce live-build :)
```
### installer depuis live
```text
	<starsheep> Tu n'as toujours pas trouvé pour appeler les preseed depuis le live ?
<starsheep> Pour ne pas retourner sur syslinux pour installer le système
<arpinux> pour installer sans retourner sur syslinus, il y a le lanceur d'installation de bureau
<arpinux> mais je ne l'intègre jamais
<arpinux> j'explique
<arpinux> le lanceur de bureau existe et permet effectivement de lancer l'installation sur le disque depuis une session live
<arpinux> le soucis
<arpinux> si pendant l'installation, le user ouvre son navigateur ou toute autre application rameuse
<arpinux> il peut freezer son interface graphique
	<starsheep> oui c'est un reproche récurrent qui est fait à DFLinux (et avant HandyLinux)
<arpinux> l'installation continuera en background
<arpinux> mais il n'aura aucun retour ni interaction possible
	<starsheep> ah ouai pas con
	<starsheep> dangereux ce machin
<arpinux> donc le dernier 'installation termniée, cliquez ok" ... bah tant pis ...
<arpinux> et c'est pas trop grave si c'est ça
<arpinux> en revanche, dans le cas d'un dual boot
<arpinux> alors là
<arpinux> bah dommage ....
<arpinux> répare ton MBR
<arpinux> :/
	<starsheep> t'as pété ton grub ^^
	<starsheep> nooon pas le MBR !!!!
	<starsheep> :D
<arpinux> et c'est vraiment le truc tellement con pour un tout petit confort que je ne le fais jamais :)
	<starsheep> Trop de mauvais souvenirs :p
<arpinux> c'était rigolo sur ubuntu de frimer en navigant et installant ... jusqu'au moment ou ça freeze ...
<arpinux> et tu frimes moins
<arpinux> non, vraiment, c'est une feature à éviter
<arpinux> comme le "--interractive"
<arpinux> l'option qui permet d'ouvrir une console pendant le build ....
<arpinux> l'arme absolue ...
<arpinux> alors oui
<arpinux> on peut virer oute les autres options, ne laisser que celle-là
<arpinux> et lors du passage en 'interractive',
<arpinux> bah tu construis ton livesystème directementdans le chrrot
<arpinux> tu fais ton live en temps réel à la main
<arpinux> c'est une possibilité
```
##  les hooks
```text
<arpinux> donc les hooks sont des scripts qui s'exécutent dans le chroot juste avant de le quitter et de le compresser en squashfs
<arpinux> même moment que interractive
<arpinux> mais en automatique
<arpinux> c'est pour le fignolage
<arpinux> :)
	<starsheep> ok !
<arpinux> on peut potentiellement faire tout ce qu'on veut
<arpinux> pas de limite
	<starsheep> Je les oublie toujours ceux là
<arpinux> openbar dans le chroot
	<roodinux> pourquoi ils ont des chiffres les hooks ? Si on ajoute celui que tu proposes dans le tutto on le nomme avec une valeur particulière ?
<arpinux> l'ordre d'exécution
<arpinux> ils s'exécutent dans l'ordre, tout simplement
<arpinux> donc pratique si une action en demande un autre :)
	<roodinux> celui pour prendre en compte les ‘proposed-updates’ tu le nomme comment ?
<arpinux> sans numéro particulier dans les sources car il peut se lancer n'importe quand
<arpinux> proposed-update.chroot
<arpinux> un autre fichier à tout faire
<arpinux> c'est une option du fichier preseed dont on a déjà parlé
<arpinux> -late-command
<arpinux> c'est une option passée à l'installeur debian
<arpinux>  et qui permet de lancer des commandes sur le système fraîchement installé avant le reboot
<arpinux> c'est la dernière chance de modifier le système avant que l'utilisateur prenne la main dessus
<arpinux> dans les sources et le tuto support, il ne fait pas grand chose hormis fixer la date d'installation et virer 3 paquets inutiles au cas ou l'installeur ne l'ai pas fait
<starsheep> après on ne peut même plus balancer de virus
		<starsheep> pas drôle
<arpinux> depuis les dépôts externe, si, mais c'est mal
<arpinux> :)
		<starsheep> :)
<arpinux> faut voir le process de build en 3 temps
<arpinux> installation du système de base dans un chroot
<arpinux> copie du chroot modifié (le includes.chroot) dans le chrrot de build
<arpinux> compression du chroot et build du live
		<starsheep> le système de base c'est donc Debian dans ce cas
<arpinux> le hook se fait juste avant la fin du 2
<arpinux> oui starsheep , plus les paquet listés dans le build
<arpinux> ensuite viennent les modifs du includes
<arpinux> ensuite les hooks système
<arpinux> ensuite les hooks perso
<arpinux> puis la compression en squasjfs
<arpinux> squashfs
		<starsheep> et enfin les -late-command
<arpinux> non starsheep
<arpinux> les late-command, c'est à l'installation
<arpinux> les hooks ,c'est lors du build
<arpinux> oui, il y a vraiment énormément de façon de modifier son live, même après le build
```

## simulation de build 

 * avec suivi git
 * irc 20/05/2017

```text
[20:04:44] <arpinux> attention au flood ....
```
```text
[-----------------------------------------------------------------------
Bonsoir et bienvenue dans la session de simulation de construction d'une ISO personnalisée Debian :D
  - but de la session de travail : décomposer la personnalisation et suivre les modifications apportées
      aux sources du build au fur et à mesure
  - outils utilisés :
    - je travaille sur dépôt git, donc l'outil approprié serait ... git, mais vous pouvez aussi télécharger les sources en 
        archives zip https://git.framasoft.org/arpinux/dfl-apprentissage/repository/archive.zip?ref=master puis suivre les 
        différentes modification en ligne directement depuis la page des commits du projet 
        https://git.framasoft.org/arpinux/dfl-apprentissage/commits/master
    - si vous utilisez git, vous pouvez cloner les sources depuis un 'git clone https://arpinux@framagit.org/arpinux/dfl-apprentissage.git' 
        puis suivre la progression avec 'git pull' à chaque étape.
 
je vais partir d'un live vide puis ajouter par section les différents éléments.
  pour l'exemple, je ne construirai pas de live compatible efi, car l'option est encore en travaux, et le but principal est de bien 
  suivre une construction, pas de tester la compatibilité UEFI :)
vous pourrez personnaliser plus finement votre environnement après la session, je ne rajouterais que peu d'éléments, ceux qui me 
  semblent les plus pertinents pour commencer avec live-build.
le contenu de cette session sera ensuite disponible sous forme de wiki+archives pour consultation hors ligne.
  
 notez que le processus simulé ce soir n'est pas fixé et que ce n'est qu'un exemple ... vous êtes libre ! :D
-----------------------------------------------------------------------]
```
```shell
git clone https://arpinux@framagit.org/arpinux/dfl-apprentissage.git

```
```text
<arpinux> donc cd ./dfl-apprentissage
<arpinux> puis git pull
<arpinux> quand j'ai fait les modifs :)
<arpinux> je vais tout expliquer au fur et à mesure :)
<arpinux> donc la première chose à ajouter dans les sources d'un build, ce sont les premiers scripts de config et de construction
<arpinux> situés dans le dossier /auto des sources
<arpinux> il n'est pas encire là :)
<arpinux> donc le premier ajout
<arpinux> quand le réseau voudra bien me l'uploader ..
<arpinux> vous pouvez git pull ou visitez les commits https://git.framasoft.org/arpinux/dfl-apprentissage/commits/master
<arpinux> le fichier 'build" est celui livré par défaut dans live-build
<arpinux> le fichier 'build" est celui livré par défaut dans live-build
<arpinux> le "clean" est modifié pour nettoyer plus
<arpinux> pour y voir plus clair sur les modifications des sources
	<kyodev> on aurait pu faire lb config?
		<naguam> dans config il y a --architectures "i386" \  normal ? que pas autres
<arpinux> alors le lb config sans arguments, c'est pour faire une debian netinstall
<arpinux> et pour voir toutes les possibilités en bloc
<arpinux> le sujet de ce soir, c'est partir de rien et ajouter le strict nécessaire à la personnalisation
<arpinux> dans ce cas, un lb config à vide ne sert à rien
<arpinux> donc 3 nouveaux fichiers et le plus important étant le auto/config
<arpinux> script qui va définir les arguments lancés à lb build
<arpinux> donc les options de ce script que vous pouvez parcourir
```
```text
<arpinux> pour la liste complète des options acceptés, un man lb config renseignera
<arpinux> mais le but de ce soir est vraiment d'en faire le minimum pour un résultat optimal
<arpinux> donc dans les options indiquées dans auto/config, on remarque la ligne "architecture" qui permet de construire un live avec une arch différente de celle du système hôte
<arpinux> notez qu'il n'es pas possible de build une amd64 depuis un i386 mais l'inverse, oui
<arpinux> on va déjà se concentrer sur le processus global pas à pas et ne pas détailler chaque option de suite sinon ce sera sans fin et surtout illisible pour quelqu'un qui aimerait suivre le tuto
<arpinux> comme je disais, la liste complète des options est dispo dans le man, mais il n'est pas totalement à jour
<arpinux> donc pour vérifier celles que vous voudriez modifier dans les sources de ce soir,
<arpinux> on peut se reporter aux fameux "test à vide" effectué hier ou simplement se contenter de celui que je fournis et qui vous permettra de build sereinement sans trop vous poser de question au début :)
<arpinux> le plus important reste la personnalisation
<arpinux> les paquets à installer
<arpinux> et le final
<arpinux> la custo du bureau de base :)
<arpinux> pour se faire Son ISO
<arpinux> :)
```
```text
<arpinux> donc la suite de la personnalisation réside dans les applications à installer
<arpinux> ce soir, on va faire simple, on va coller les mêmes apps dans le live et dans le système final
<arpinux> mais il est possible de séparer les paquets pour avoir des applications en plus/moins dans le live et le système installé, on y reviendra au final
<arpinux> donc la liste des paquets se trouve dans le dossier config/package-lists/
<arpinux> qui arrive :)
<arpinux> alors effectivement, dans la liste que je vais poster, on pourrait encore simplifier
<arpinux> et vous pourrez d'ailleurs le faire
<arpinux> je push ... j'arrive :)
<arpinux> vous pouvez pull depuis les sources
<arpinux> ou visitez les commits https://git.framasoft.org/arpinux/dfl-apprentissage/commits/master
```
```shell
git pull
```
```
<arpinux> il y a deux listes
<arpinux> la liste live* a été ajoutée automatiquement par live-build
<arpinux> elle contient des paquets présents dans le live et qui ne seront pas installés
<arpinux> vous comprenez que vous pouvez éditer cette liste	
<arpinux> et y ajouter tout les applications que vous voudriez voir dans le live et pas dans le système
<arpinux> attention
<arpinux> donc cette liste live* est ajoutée par live-build
<arpinux> mais vous pouvez en coller une autre
<arpinux> en fait tout fichier placé ici /config/package-lists/live* avec l'extension .live
<arpinux> sera prit en compte par live-build
<arpinux> donc vous pouvez faire des listes à thème si vous voulez
<arpinux> genre multimedia.live ....
<arpinux> et là vous auriez du me stopper car ne n'est pas l'extension live, mais le nom ....
* arpinux le boulet
<arpinux> :D
<arpinux> mais si vous avez les sources devant les yeux, vous avez capté le truc :)
<arpinux> bref
```
```text
<arpinux> l'autre liste, la *.chroot, est destinée aux paquets installés dans le live + dans le système installé
<arpinux> donc la seconde liste avec les applications
<arpinux> elle est détaillée car je n'ai pas mis de gros méta-paquets
<arpinux> le système des dépendances debian permet de réduire la liste à 5 ou 6 paquets grace aus "taskel"
<arpinux> mais ça fait des live assez gros
<arpinux> comme les live officiel en fait
<arpinux> donc pour éviter ça, on ne colle pas des trucs genre gnome-desktop :)
<arpinux> mais plutôt les composants séparés
<arpinux> donc je vous invite à ouvrir cette liste madebian.list.chroot
<arpinux> vous pouvez supprimer ou ajouter des applications en prenant soin de vérifier qu'elles se trouvent bien dans les dépôts stretch :)
<arpinux> la liste d'origine fournie donne un temps de build de 2h en gros, avec un système final fonctionnel
<arpinux> bureau xfce/firefox/transmission/cups ... le nécessaire :)
<arpinux> mais je vous laisse quelques minutes pour bien mater le fichier, et le modifier à votre guise
<arpinux> vous pouvez yu aller, ça ne gênera pas la suite des ajouts ,quoique vous modifiez :)
<arpinux> voilà, j'espère que vous avez eu le temps de modifier votre liste, vous pourrez y revenir tout à l'heure :)
```
```text
<arpinux> passons à une truc débile et rigolo
<arpinux> la bannière de l'installeur graphique :)
<arpinux> et donc, avant de taper dans les trucs longs et embêtant
<arpinux> un petit plaisir de sucto
<arpinux> la bannière
<arpinux> le truc qui va s'afficher en haut pendant l'installation
<arpinux> et qui dire
<arpinux> dira
<arpinux> "tu installe ma distro" :D
<arpinux> un peu d'ego ne fait pas de mal si il ne fait pas d'ombre à l'autre :)
<arpinux> c'est dans le dossier config/includes.installer/usr/share/grahics/
<arpinux> qui n'est pas encore là :)
<arpinux> vous pouvez git pull ou visiter les commits :)
<arpinux> oui, je sais, c'est futile
<arpinux> mais ça rassure aussi l'utilisateur de savoir que le builder s'est un peu fait chier à personnaliser son truc et n'a pas juste balancé 2 walls et 3 applis :)
<arpinux> c'est pas anodin :)
```
```text
<arpinux> passons à plus sérieux
<arpinux> le menu de boot du live
<arpinux> il est contrôlé par syslinux
<arpinux> pour le personnaliser, vous pouvez changer les labels des entrées , le fond et même en rajouter
<arpinux> par exemple
<arpinux> lister plusieurs entrées avec différentes langues activées par défaut
<arpinux> car les options passées à syslinux sont comme les options de grub
<arpinux> on va voir de suite un custo simple
<arpinux> ça se passe dans /config/includes.binary/isolinux/
<arpinux> qui arrive :)
<arpinux> vous pouvez git pull ou visitez les commits https://git.framasoft.org/arpinux/dfl-apprentissage/commits/master
<arpinux> le nouveau dossier intègre un menu pour le live et un fond d'écran
<arpinux> mais vous pouvez aussi personnaliser le label d'installation, le titre principal etc
<arpinux> vous aurez des exemples plus complet sur DFLinux
<arpinux> donc le fichier live.cfg
<arpinux> vous pouvez voir que ce fichier contient le label "tester debian" en premier
<arpinux> avec un ^
<arpinux> c'est pour indiquer la touche d'accès rapide au clavier
<arpinux> le raccourcis quoi ..
<arpinux> la dernière ligne de chaque section indique les options passées au kernel
<arpinux> append boot = ....
<arpinux> c'est là que vous pouvez spécifier la langue du live
<arpinux> le fichier install.cfg n'est pas dans ces sources car on ne le personnalise pas, il sera ajouté par live-build et donc, on aura bien une entrée 'install' dans le menu final :)
	<kyodev> ligne e: option, c: console
	<kyodev> c'est qui qui fait?
<arpinux> un fichier qui se nomme stdmenu.cfg
<arpinux> qui n'est pas présent dans ces sources mais dans celles de DFLinux
<arpinux> on n'y touche pas ce soir, donc il n'est pas dans ces sources
<arpinux> donc je termine sur la custo du menu
<arpinux> pour les couleurs, la disposition etc, franchement, c'est la galère car c'est tout en code de taré
<arpinux> (terme technique assumé)
<arpinux> donc faites simple, ce sera mieux :)
<arpinux> et puis, ce n'est pas non plus l plus important, le tout étant d'avoir la bonne langue au démarrage :)
```
```text
<arpinux> on va attaquer maintenant le plus cool
<arpinux> la custo directe du système et du user
<arpinux> ça se passe dans /config/includes.chroot/
<arpinux> ce dossier représente la racine du système installé :)
<arpinux> donc c'est le moment de se faire plaisir et d'y ajouter tout ce qu'on veut :)
<arpinux> avec quelques restrictions cependant
<arpinux> badadd n1
<arpinux> les trucs écrasés ....
<arpinux> si vous ajoutez dans ce dossier, un élément qui appartient déjà à un paquet
<arpinux> il sera écrasé par le paquet lors du build
<arpinux> par exemple
<arpinux> vous voulez coller votre version de "ffmpeg"
<arpinux> mais si vous ajoutez
<arpinux> config/includes.chroot/usr/bin/ffmpeg
<arpinux> il sera écrasé par le vrai lors de l'install
<arpinux> vous pouvez coller le votre dans /usr/local/bin :)
<arpinux> il ne sera pas écrasé
<arpinux> et vous pourrez vous en servir sans soucis depuis un alias approprié :)
<arpinux> badadd n2
<arpinux> le paquets perso
<arpinux> live-build gère ses paquets avec apt
<arpinux> donc dépendances, sécurité, mise à jour et tout ce qui va bien
<arpinux> live-build vous permet d'ajouter des paquets perso dans le dossier /config/packages
<arpinux> c'est super :)
<arpinux> mais attention
<arpinux> c'est dpkg qui installe les paquet de /packages
<arpinux> donc avec tout ce que ça implique :)
<arpinux> pas de suivi des mises à jour et pas de gestion des dep
<arpinux> donc faire attention
<arpinux> en fait apt télécharge les paquets, puis dpkg installe les paquets, dont ceux que tu as ajouté à la main
<arpinux> badadd n3
<arpinux> les copyrights
<arpinux> gaffe si vous voulez diffuser votre live
<arpinux> ce que vous y ajoutez est de votre responsabilité
<arpinux> donc respectez les licences, créditez les auteurs, palcez des liens dans un README
<arpinux> bref
<arpinux> soyez responsables de ce que vous diffusez
<arpinux> je sais que ça fait un peu vieux con de dire ça
<arpinux> mais les débutants ou les curieux de linus méritent de recevoir un truc propre :)
<arpinux> linux*
<arpinux> ce n'est pas parce que c'est free et gratuit que ça doit être "passable" ;)
<arpinux> donc pour illustrer la custo
<arpinux> un exemple tout bête
<arpinux> les wallpapers perso :)
<arpinux> bah oui ... c'est quand même super cool d'avoir ses walls dans sa distro :)
<arpinux> vous pouvez git pull, et profitez de quelques walls de Péhä :)
<arpinux> un dernier stade avant de build :)
<arpinux> je vous laisserais ajouter ce que vous voulez dans le includes.chroot mais avant
<arpinux> un point sur les paquets selon ceux que vous avez ajoutés/retirés
<arpinux> si comme dans les sources, vous installé libdvdcss2
<arpinux> vous devez ajouter des dépôts externes
<arpinux> c'est geekland à expliquer alors que c'est simple en voyant ... alors je montre :)
<arpinux> les paquets listés doivent être sur un dépôt
<arpinux> logique
<arpinux> si ils ne sont pas chez debian, il faut indiquer les dépôt + la clé d'authentification
<arpinux> soit en un commit pour faire simple
<arpinux> vous pouvez git pull et voir apparaître le dossier config/archives :)
<arpinux> 2 fichiers pour les dépôts videolan : un pour le liven et un pour le système installé
<arpinux> 2 fichiers pour la clé videolan : idem que pour l'autre :)
<arpinux> donc si vous voulez par exemple, le dépôt TOR
<arpinux> bah pareil, vous ajoutez les dépôts sous forme d'adresse + le fichier avec la clé dans le même dossier
<arpinux> avec les mêmes extensions *.binary pour le live et *.chroot pour le système installé
<arpinux> donc c'est fini pour la custo de base ...
```
```text
<arpinux> pour un premier build fonctionnel , pas de custo du user en phase 1
<arpinux> pourquoi
<arpinux> car il faut d'abord choper les valeurs par défaut avant de les modifier
<arpinux> les valeurs distribuées par le système que vous venez de créer
<arpinux> donc le meilleur moyen
<arpinux> c'est de build
<arpinux> lancer le live
<arpinux> personnaliser en live
<arpinux> puis faire une archive des mods
<arpinux> donc généralement
<arpinux> archiver le ~/.config
<arpinux> le ~/.mozilla sivous voulez mais attention à bien vider le cache et les pass !!!
<arpinux> puis récupérer ces archives pour les placer des includes.chroot/etc/skel afin de personnaliser votre utilisateur :)
<arpinux> et vous retrouverez vos préférences en live
<arpinux> autre solution
<arpinux> ...
<arpinux> pour la custo user
<arpinux> si vous utilisez le même environnement que celui qui sera dans le live
<arpinux> vous pouvez récupérer certaines de vos préférences directement
<arpinux> attention
<arpinux> pensez que le live , s'il est public, comportera vos préférences
<arpinux> donc pas de .ssh
<arpinux> PAS DE .SHH !!!
<arpinux> :D
<arpinux> bref
<arpinux> je ne peux pas lister Vos fichiers sensibles mais vous avez compris le principe :)
<arpinux> et c'est aussi pour ça que pour ce soir, je ne vous conseille pas de custo le user
<arpinux> autant vous concentrer sur les applications à installer
```
```text
<arpinux> donc si vous êtes partant, on peut lb build :D
<arpinux> depuis les sources, vous pouvez envoyer la commande
<arpinux> sudo lb build
<arpinux> plus sûr
<arpinux> donc voilà, avec ces sources https://git.framasoft.org/arpinux/dfl-apprentissage , vous avez une armature pour un live simple sur xfce, que vous pouvez agrémenter avec le tuto live-build et faire votre distro
<arpinux> j'espère avoir été clair et simple
<arpinux> et que vous avez vu le processus en global
<arpinux> :)
```
```text
<arpinux> pour ajouter des dépôts debian supplémentaires, comme les dépôts externes, tu places un fichier avec l'adresse dans /config/archives
<arpinux> tu n'as pas besoin de coller la clé :)
<arpinux> pour les préférences apt/pinning
<arpinux> tu ajoute ton preferences dans config/includes.chroot/etc/apt/preferences.d/preferences
<arpinux> et voilà :)
<arpinux> la ligne "experimentale" sera prise en compte lors du build et à l'installation et sera ajouté à /etc/apt/sources.list.d/experimental.list
```
```text
	<roodinux> donc si je je veux mettre un .mozilla
<arpinux> c'est dans config/includes.chroot/etc/skel/.mozilla
<arpinux> et il vient de ton système ?
	<roodinux> ok, j'ai compris ça. de la clé live que j'ai testé tout à l'heure...
<arpinux> :)
	<roodinux> donc quand tu veux ajouter une config dans le skeleton, tu recréer l’arborescence des fichiers si je comprends bien...
<arpinux> oui, comme si includes.chroot/ était  la racine du système
<arpinux> sauf pour /home qui reste vide
<arpinux> c'est pour ça qu'on colle dans /etc/skel pour le user
	<roodinux> ok... surtout que c'est un user live qui ne conservera rien...
	<roodinux> enfin si la config reste pour les deux en live et après install
		<kyodev> on peut activer la persistances des données, non?
<arpinux> oui, on peut aussi pré-configurer un 'vrai' user avec un login/pass etc, mais c'est un peu plus hard en config :)
<arpinux> et j'avoue que je suis pas fan de la persistance
<arpinux> rien de plus facile à perdre qu'une clé usb ...
<arpinux> donc je préfère le live 'pure'
```
```
	<roodinux> Aïe et si l'écran se met en veille utilisateur: humain ? mot de passe?
<arpinux> live
```

## un essai de bidouille live sid
```shell
#!/bin/sh
set -e
lb config noauto \
    --apt-indices "true" \
    --apt-recommends "true" \
    --apt-secure "true" \
    --apt-source-archives "false" \
    --architectures "amd64" \
    --archive-areas "main contrib non-free" \
    --backports "false" \
    --binary-images "iso-hybrid" \
    --bootloader "syslinux" \
    --clean \
    --debian-installer "netinst" \
    --debian-installer-gui "true" \
    --distribution "sid" \
    --system "live" \
    --iso-application "PengouinSid" \
    --iso-volume "PengouinSid" \
    --linux-package "linux-image" \
    --firmware-binary "true" \
    --firmware-chroot "true" \
    --source "false" \
    --security "false" \
    --updates "false" \
    --verbose \
    --win32-loader "false" \
    "${@}"
```
```text
<arpinux> le live fonctionne
<arpinux> l'install aussi :D
<arpinux> erreur au chargement des noyaux
<arpinux> je continue quand même
<arpinux> ah .. il trouve pas de disque dur ...
	<PengouinBSD> ahhh, très bien :p
<arpinux> :/
	<PengouinBSD> enfin, non, mais bon, t'as compris :D
<arpinux> oui :)
<arpinux> alors pour construire un live sid ... peut être passer par un live ç vide stretch, mais avec un hook, un seul, pour exécuter une dist-upgrade du chroot vers sid avant la fin du chroot :)
<arpinux> donc un installeur stretch, mais qui copie un système sid
<arpinux> parce que normalement, pour installer une sid, on prend la netinstall testing, on passe en mode expert install et on demande à choper la version unstable dans les options
<arpinux>  mais il n'y a pas de live sid proprement dit
<lagout> sid seul n est pas utilisable en tant que tel
<lagout> y à pas asser de paquet
<arpinux> mais c'est marrant que live-build accepte de build en sid quand même
<arpinux> enfin "marrant" ....
<arpinux> une autre idée
<arpinux> choper ta config, mais en collant "stretch"
<arpinux> et en plus
<arpinux> dans config/archives
<arpinux> on rajoute la ligne des dépôts sid
<arpinux> :)
<arpinux> donc juste remplace "sid" par "stretch" dans auto/config
<arpinux> et ajouter dans config/archives
<arpinux> sid.list.chroot et sid.list.binary
<arpinux> avec deb http://ftp.fr.debian.org/debian/ unstable main contrib non-free
<lagout> PengouinBSD: c est ça qui est drole
<arpinux> ah si
<arpinux> j'ai changé "netinst" par "live" pour l'installeur
	<PengouinBSD> arpinux, si je ne me trompe, ça va faire quand même une install qui dure 2 * + de temps, non ?
<arpinux> cad ? 2*+ de temps ?
	<PengouinBSD> beh, oui, y'a la phase d'install stretch puis sid
<arpinux> non
<arpinux> pas comme j'ai fait
<arpinux> pas de hook
<arpinux> en ajoutant la ligne sid dans archives
<arpinux> je dis à live-build de le prendre en compte directement dans le build
	<PengouinBSD> pourtant il récupère bien tous les packages en interrogeant les dépôts Sid !
<arpinux> mais tout en précisant d'utiliser le système de base stretch (donc tous les paquets dispo)
<arpinux> l'option --distribution "stretch" indique à live-build de tout mettre en place avec les outils stretch
<arpinux> la ligne dans archives indique qu'il peut choper aussi des paquets de sid
<arpinux> ce n'est pas pareil que de dire directement à l'installeur de bosser sur sid
<arpinux> là il taf que stretch
<arpinux> et si il peut
<arpinux> il chope du sid
<arpinux> théoriquement, ça devrait marcher :SD
<arpinux> :D
<arpinux> il build là
<arpinux> il va bien chercher l'installeur chez stretch
<arpinux> build stretch/sid terminé
<arpinux> live fonctionnel
<arpinux> je tente l'install ??
<arpinux> install lancée ....
<arpinux> pas de module noyau ....
<arpinux> ça commence mal....
<arpinux> aucuen carte ethernet trouvée ....
<arpinux> bon bah non .... abort install
<arpinux> haha
<arpinux> à jouer avec stretch/sid alors que debian est prévu comme 'stable' ... jte jure ...
	<PengouinBSD> hihihihihihi
	* PengouinBSD "love exciting way :p!"
<arpinux> ça t'amuse de me faire build nawak hein ???!!!
<arpinux> :D
	<PengouinBSD> YESSS!
	<PengouinBSD> XD
<arpinux> alors pourquoi ça ne fonctionne pas
<arpinux> si on utilise distribution "sid", il manque des paquets
```
