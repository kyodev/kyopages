# options live-build

## config/bootstrap

premier stage amorçant un système debian basique
```text
 # $LB_DISTRIBUTION: select distribution to use
 # (Default: stretch)
 LB_DISTRIBUTION="stretch"

 # $LB_PARENT_DISTRIBUTION: select parent distribution to use
 # (Default: stretch)
 LB_PARENT_DISTRIBUTION="stretch"

 # $LB_PARENT_DEBIAN_INSTALLER_DISTRIBUTION: select parent distribution for debian-installer to use
 # (Default: stretch)
 LB_PARENT_DEBIAN_INSTALLER_DISTRIBUTION="stretch"

 # $LB_PARENT_MIRROR_BOOTSTRAP: set parent mirror to bootstrap from
 # (Default: http://ftp.debian.org/debian/)
 LB_PARENT_MIRROR_BOOTSTRAP="http://ftp.debian.org/debian/"

 # $LB_PARENT_MIRROR_CHROOT: set parent mirror to fetch packages from
 # (Default: http://ftp.debian.org/debian/)
 LB_PARENT_MIRROR_CHROOT="http://ftp.debian.org/debian/"

 # $LB_PARENT_MIRROR_CHROOT_SECURITY: set security parent mirror to fetch packages from
 # (Default: http://security.debian.org/)
 LB_PARENT_MIRROR_CHROOT_SECURITY="http://security.debian.org/"

 # $LB_PARENT_MIRROR_BINARY: set parent mirror which ends up in the image
 # (Default: http://httpredir.debian.org/debian/)
 LB_PARENT_MIRROR_BINARY="http://httpredir.debian.org/debian/"

 # $LB_PARENT_MIRROR_BINARY_SECURITY: set security parent mirror which ends up in the image
 # (Default: http://security.debian.org/)
 LB_PARENT_MIRROR_BINARY_SECURITY="http://security.debian.org/"

 # $LB_PARENT_MIRROR_DEBIAN_INSTALLER: set debian-installer parent mirror
 # (Default: http://ftp.debian.org/debian/)
 LB_PARENT_MIRROR_DEBIAN_INSTALLER="http://ftp.debian.org/debian/"

 # $LB_MIRROR_BOOTSTRAP: set mirror to bootstrap from
 # (Default: http://ftp.debian.org/debian/)
 LB_MIRROR_BOOTSTRAP="http://ftp.debian.org/debian/"

 # $LB_MIRROR_CHROOT: set mirror to fetch packages from
 # (Default: http://ftp.debian.org/debian/)
 LB_MIRROR_CHROOT="http://ftp.debian.org/debian/"

 # $LB_MIRROR_CHROOT_SECURITY: set security mirror to fetch packages from
 # (Default: http://security.debian.org/)
 LB_MIRROR_CHROOT_SECURITY="http://security.debian.org/"

 # $LB_MIRROR_BINARY: set mirror which ends up in the image
 # (Default: http://httpredir.debian.org/debian/)
 LB_MIRROR_BINARY="http://httpredir.debian.org/debian/"

 # $LB_MIRROR_BINARY_SECURITY: set security mirror which ends up in the image
 # (Default: http://security.debian.org/)
 LB_MIRROR_BINARY_SECURITY="http://security.debian.org/"

 # $LB_MIRROR_DEBIAN_INSTALLER: set debian-installer mirror
 # (Default: http://ftp.debian.org/debian/)
 LB_MIRROR_DEBIAN_INSTALLER="http://ftp.debian.org/debian/"

 # $LB_BOOTSTRAP_QEMU_ARCHITECTURES: architectures to use foreign bootstrap
 # (Default: )
 LB_BOOTSTRAP_QEMU_ARCHITECTURES=""

 # $LB_BOOTSTRAP_QEMU_EXCLUDE: packages to exclude during foreign bootstrap
 # (Default: )
 LB_BOOTSTRAP_QEMU_EXCLUDE=""

 # $LB_BOOTSTRAP_QEMU_STATIC: static qemu binary for foreign bootstrap
 # (Default: )
 LB_BOOTSTRAP_QEMU_STATIC=""
```


## config/chroot

deuxième stage personnalisant le chroot
```text
 # $LB_CHROOT_FILESYSTEM: set chroot filesystem
 # (Default: squashfs)
 LB_CHROOT_FILESYSTEM="squashfs"

 # $LB_UNION_FILESYSTEM: set union filesystem
 # (Default: overlay)
 LB_UNION_FILESYSTEM="overlay"

 # $LB_INTERACTIVE: set interactive build
 # (Default: false)
 LB_INTERACTIVE="false"

 # $LB_KEYRING_PACKAGES: set keyring packages
 # (Default: empty)
 LB_KEYRING_PACKAGES="debian-archive-keyring"

 # $LB_LINUX_FLAVOURS: set kernel flavour to use
 # (Default: autodetected)
 LB_LINUX_FLAVOURS="686-pae"

 # $LB_LINUX_PACKAGES: set kernel packages to use
 # (Default: autodetected)
 LB_LINUX_PACKAGES="linux-image"

 # $LB_SECURITY: enable security updates
 # (Default: true)
 LB_SECURITY="true"

 # $LB_UPDATES: enable updates updates
 # (Default: true)
 LB_UPDATES="true"

 # $LB_BACKPORTS: enable backports updates
 # (Default: false)
 LB_BACKPORTS="false"
```

## config/binary

troisième stage générant une image binaire
```text
 # $LB_BINARY_FILESYSTEM: set image filesystem
 # (Default: fat32)
 LB_BINARY_FILESYSTEM="fat32"

 # $LB_APT_INDICES: set apt/aptitude generic indices
 # (Default: true)
 LB_APT_INDICES="true"

 # $LB_BOOTAPPEND_LIVE: set boot parameters
 # (Default: empty)
 LB_BOOTAPPEND_LIVE="boot=live components quiet splash"

 # $LB_BOOTAPPEND_INSTALL: set boot parameters
 # (Default: empty)
 LB_BOOTAPPEND_INSTALL=""

 # $LB_BOOTAPPEND_LIVE_FAILSAFE: set boot parameters
 # (Default: empty)
 LB_BOOTAPPEND_LIVE_FAILSAFE="boot=live components memtest noapic noapm nodma nomce \
    nolapic nomodeset nosmp nosplash vga=normal"

 # $LB_BOOTLOADERS: set bootloaders
 # (Default: syslinux,grub-efi)
 LB_BOOTLOADERS="syslinux,grub-efi"

 # $LB_CHECKSUMS: set checksums
 # (Default: md5)
 LB_CHECKSUMS="md5"

 # $LB_COMPRESSION: set compression
 # (Default: none)
 LB_COMPRESSION="none"

 # $LB_ZSYNC: set zsync
 # (Default: true)
 LB_ZSYNC="true"

 # ${LB_BUILD_WITH_CHROOT: control if we build binary images chrooted
 # (Default: true)
 # DO NEVER, *NEVER*, *N*E*V*E*R* SET THIS OPTION to false.
 LB_BUILD_WITH_CHROOT="true"

 # $LB_DEBIAN_INSTALLER: set debian-installer
 # (Default: false)
 LB_DEBIAN_INSTALLER="false"

 # $LB_DEBIAN_INSTALLER_DISTRIBUTION: set debian-installer suite
 # (Default: empty)
 LB_DEBIAN_INSTALLER_DISTRIBUTION="stretch"

 # $LB_DEBIAN_INSTALLER_PRESEEDFILE: set debian-installer preseed filename/url
 # (Default: )
 LB_DEBIAN_INSTALLER_PRESEEDFILE=""

 # $LB_DEBIAN_INSTALLER_GUI: toggle use of GUI debian-installer
 # (Default: true)
 LB_DEBIAN_INSTALLER_GUI="true"

 # $LB_GRUB_SPLASH: set custom grub splash
 # (Default: empty)
 LB_GRUB_SPLASH=""

 # $LB_HDD_LABEL: set hdd label
 # (Default: DEBIAN_LIVE)
 LB_HDD_LABEL="DEBIAN_LIVE"

 # $LB_HDD_SIZE: set hdd filesystem size
 # (Default: auto)
 LB_HDD_SIZE="auto"

 # $LB_HDD_PARTITION_START: set start of partition for the hdd target for BIOSes that 
 # expect a specific boot partition start (e.g. "63s"). If empty, use optimal layout.
 # (Default: )
 LB_HDD_PARTITION_START=""

 # $LB_ISO_APPLICATION: set iso author
 # (Default: Debian Live)
 LB_ISO_APPLICATION="Debian Live"

 # $LB_ISO_PREPARER: set iso preparer
 # (Default: live-build $VERSION; http://live-systems.org/devel/live-build)
 LB_ISO_PREPARER="live-build $VERSION; http://live-systems.org/devel/live-build"

 # $LB_ISO_PUBLISHER: set iso publisher
 # (Default: Live Systems project; http://live-systems.org/; debian-live@lists.debian.org)
 LB_ISO_PUBLISHER="Live Systems project; http://live-systems.org/; debian-live@lists.debian.org"

 # $LB_ISO_VOLUME: set iso volume (max 32 chars)
 # (Default: Debian stretch $(date +%Y%m%d-%H:%M))
 LB_ISO_VOLUME="Debian stretch $(date +%Y%m%d-%H:%M)"

 # $LB_JFFS2_ERASEBLOCK: set jffs2 eraseblock size
 # (Default: unset)
 LB_JFFS2_ERASEBLOCK=""

 # $LB_MEMTEST: set memtest
 # (Default: none)
 LB_MEMTEST="none"

 # $LB_LOADLIN: set loadlin
 # (Default: false)
 LB_LOADLIN="false"

 # $LB_WIN32_LOADER: set win32-loader
 # (Default: false)
 LB_WIN32_LOADER="false"

 # $LB_NET_ROOT_FILESYSTEM: set netboot filesystem
 # (Default: nfs)
 LB_NET_ROOT_FILESYSTEM="nfs"

 # $LB_NET_ROOT_MOUNTOPTIONS: set nfsopts
 # (Default: empty)
 LB_NET_ROOT_MOUNTOPTIONS=""

 # $LB_NET_ROOT_PATH: set netboot server directory
 # (Default: /srv/debian-live)
 LB_NET_ROOT_PATH="/srv/debian-live"

 # $LB_NET_ROOT_SERVER: set netboot server address
 # (Default: 192.168.1.1)
 LB_NET_ROOT_SERVER="192.168.1.1"

 # $LB_NET_COW_FILESYSTEM: set net client cow filesystem
 # (Default: nfs)
 LB_NET_COW_FILESYSTEM="nfs"

 # $LB_NET_COW_MOUNTOPTIONS: set cow mount options
 # (Default: empty)
 LB_NET_COW_MOUNTOPTIONS=""

 # $LB_NET_COW_PATH: set cow directory
 # (Default: )
 LB_NET_COW_PATH=""

 # $LB_NET_COW_SERVER: set cow server
 # (Default: )
 LB_NET_COW_SERVER=""

 # $LB_NET_TARBALL: set net tarball
 # (Default: true)
 LB_NET_TARBALL="true"

 # $LB_FIRMWARE_BINARY: include firmware packages in debian-installer
 # (Default: true)
 LB_FIRMWARE_BINARY="true"

 # $LB_FIRMWARE_CHROOT: include firmware packages in debian-installer
 # (Default: true)
 LB_FIRMWARE_CHROOT="true"

 # $LB_SWAP_FILE_PATH: set swap file path
 # (Default: )
 LB_SWAP_FILE_PATH=""

 # $LB_SWAP_FILE_SIZE: set swap file size
 # (Default: 512)
 LB_SWAP_FILE_SIZE="512"
```

## config/source

quatrième stage optionel générant une image source
```text
 # $LB_SOURCE: set source option
 # (Default: false)
 LB_SOURCE="false"

 # $LB_SOURCE_IMAGES: set image type
 # (Default: tar)
 LB_SOURCE_IMAGES="tar"
```


## config/common

common options for live-build
```text
 # $LB_APT: set package manager
 # (Default: apt)
 LB_APT="apt"

 # $LB_APT_FTP_PROXY: set apt/aptitude ftp proxy
 # (Default: autodetected or empty)
 LB_APT_FTP_PROXY=""

 # $LB_APT_HTTP_PROXY: set apt/aptitude http proxy
 # (Default: autodetected or empty)
 LB_APT_HTTP_PROXY=""

 # $LB_APT_PIPELINE: set apt/aptitude pipeline depth
 # (Default: )
 LB_APT_PIPELINE=""

 # $LB_APT_RECOMMENDS: set apt/aptitude recommends
 # (Default: true)
 LB_APT_RECOMMENDS="true"

 # $LB_APT_SECURE: set apt/aptitude security
 # (Default: true)
 LB_APT_SECURE="true"

 # $LB_APT_SOURCE_ARCHIVES: set apt/aptitude source entries in sources.list
 # (Default: true)
 LB_APT_SOURCE_ARCHIVES="true"

 # $LB_CACHE: control cache
 # (Default: true)
 LB_CACHE="true"

 # $LB_CACHE_INDICES: control if downloaded package indices should be cached
 # (Default: false)
 LB_CACHE_INDICES="false"

 # $LB_CACHE_PACKAGES: control if downloaded packages files should be cached
 # (Default: true)
 LB_CACHE_PACKAGES="true"

 # $LB_CACHE_STAGES: control if completed stages should be cached
 # (Default: bootstrap)
 LB_CACHE_STAGES="bootstrap"

 # $LB_DEBCONF_FRONTEND: set debconf(1) frontend to use
 # (Default: noninteractive)
 LB_DEBCONF_FRONTEND="noninteractive"

 # $LB_DEBCONF_PRIORITY: set debconf(1) priority to use
 # (Default: critical)
 LB_DEBCONF_PRIORITY="critical"

 # $LB_INITRAMFS: set initramfs hook
 # (Default: live-boot)
 LB_INITRAMFS="live-boot"

 # $LB_INITRAMFS_COMPRESSION: set initramfs compression
 # (Default: gzip)
 LB_INITRAMFS_COMPRESSION="gzip"

 # $LB_INITSYSTEM: set init system
 # (Default: systemd)
 LB_INITSYSTEM="systemd"

 # $LB_FDISK: set fdisk program
 # (Default: autodetected)
 LB_FDISK="fdisk"

 # $LB_LOSETUP: set losetup program
 # (Default: autodetected)
 LB_LOSETUP="losetup"

 # $LB_MODE: set distribution mode
 # (Default: debian)
 LB_MODE="debian"

 # $LB_SYSTEM: set system type
 # (Default: live)
 LB_SYSTEM="live"

 # $LB_TASKSEL: set tasksel program
 # (Default: apt)
 LB_TASKSEL="apt"

 # live-build options

 # $_BREAKPOINTS: enable breakpoints
 # (Default: false)
 #_BREAKPOINTS="false"

 # $_DEBUG: enable debug
 # (Default: false)
 #_DEBUG="false"

 # $_COLOR: enable color
 # (Default: false)
 #_COLOR="false"

 # $_FORCE: enable force
 # (Default: false)
 #_FORCE="false"

 # $_QUIET: enable quiet
 # (Default: false)
 _QUIET="false"

 # $_VERBOSE: enable verbose
 # (Default: false)
 #_VERBOSE="false"

 # Internal stuff (FIXME)
 APT_OPTIONS="--yes"
 APTITUDE_OPTIONS="--assume-yes"
 DEBOOTSTRAP_OPTIONS=""
 DEBOOTSTRAP_SCRIPT=""
 GZIP_OPTIONS="-6 --rsyncable"
 ISOHYBRID_OPTIONS=""
```


## config/build

```text
Architecture: 
Archive-Areas: 
```
