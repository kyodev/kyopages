# lb config

Projet Live Systems

## NOM

lb config - Crée un répertoire de configuration

## SYNOPSIS
```text
    lb config [live-build options]

    lb config
     [--apt apt|aptitude]
     [--apt-ftp-proxy URL]
     [--apt-http-proxy URL]
     [--apt-indices true|false]
     [--apt-options OPTION|"OPTIONS"]
     [--aptitude-options OPTION|"OPTIONS"]
     [--apt-pipeline PROFONDEUR]
     [--apt-recommends true|false]
     [--apt-secure true|false]
     [--apt-source-archives true|false]
     [-a|--architectures ARCHITECTURE]
     [-b|--binary-images iso|iso-hybrid|netboot|tar|hdd]
     [--binary-filesystem fat16|fat32|ext2|ext3|ext4]
     [--bootappend-install PARAMÈTRES|"PARAMÈTRES"]
     [--bootappend-live PARAMÈTRES|"PARAMÈTRES"]
     [--bootloader grub|grub2|syslinux]
     [--cache true|false]
     [--cache-indices true|false]
     [--cache-packages true|false]
     [--cache-stages STAGE|"STAGES"]
     [--checksums md5|sha1|sha256|none]
     [--compression bzip2|gzip|lzip|none]
     [--config GIT_URL::GIT_ID]
     [--build-with-chroot true|false]
     [--chroot-filesystem ext2|ext3|ext4|squashfs|jffs2|none]
     [--clean]
     [-c|--conffile FICHIER]
     [--debconf-frontend dialog|editor|noninteractive|readline]
     [--debconf-priority low|medium|high|critical]
     [--debian-installer true|cdrom|netinst|netboot|businesscard|live|false]
     [--debian-installer-distribution daily|NOM_DE_CODE]
     [--debian-installer-preseedfile FICHIER|URL]
     [--debian-installer-gui true|false]
     [--debug]
     [-d|--distribution NOM_DE_CODE]
     [--parent-distribution NOM_DE_CODE]
     [--parent-debian-installer-distribution NOM_DE_CODE]
     [--dump]
     [--fdisk fdisk|fdisk.dist]
     [--firmware-binary true|false]
     [--firmware-chroot true|false]
     [--force]
     [--grub-splash FICHIER]
     [--gzip-options OPTION|"OPTIONS"]
     [--hooks FICHIER]
     [--ignore-system-defaults]
     [--initramfs auto|none|live-boot|casper]
     [--initramfs-compression bzip2|gzip|lzma]
     [--initsystem sysvinit|runit|systemd|upstart|none]
     [--interactive shell]
     [--isohybrid-options OPTION|"OPTIONS"]
     [--iso-application NOM]
     [--iso-preparer NOM]
     [--iso-publisher NOM]
     [--iso-volume NOM]
     [--jffs2-eraseblock TAILLE]
     [--keyring-packages PAQUET|"PAQUETS"]
     [-k|--linux-flavours VARIÉTÉ|"VARIÉTÉS"]
     [--linux-packages "PAQUETS"]
     [--losetup losetup|losetup.orig]
     [--memtest memtest86+|memtest86|none]
     [-m|--parent-mirror-bootstrap URL]
     [--parent-mirror-chroot URL]
     [--parent-mirror-chroot-security URL]
     [--parent-mirror-chroot-updates URL]
     [--parent-mirror-chroot-backports URL]
     [--parent-mirror-binary URL]
     [--parent-mirror-binary-security URL]
     [--parent-mirror-binary-updates URL]
     [--parent-mirror-binary-backports URL]
     [--parent-mirror-debian-installer URL]
     [--mirror-bootstrap URL]
     [--mirror-chroot URL]
     [--mirror-chroot-security URL]
     [--mirror-chroot-updates URL]
     [--mirror-chroot-backports URL]
     [--mirror-binary URL]
     [--mirror-binary-security URL]
     [--mirror-binary-updates URL]
     [--mirror-binary-backports URL]
     [--mirror-debian-installer URL]
     [--mode debian|progress-linux|ubuntu]
     [--system live|normal]
     [--net-root-filesystem nfs|cfs]
     [--net-root-mountoptions OPTIONS]
     [--net-root-path CHEMIN]
     [--net-root-server IP|NOM_D_HÔTE]
     [--net-cow-filesystem nfs|cfs]
     [--net-cow-mountoptions OPTIONS]
     [--net-cow-path CHEMIN]
     [--net-cow-server IP|NOM_D_HÔTE]
     [--net-tarball true|false]
     [--quiet]
     [--archive-areas DOMAINE_D'ARCHIVE|"DOMAINES_D'ARCHIVE"]
     [--parent-archive-areas DOMAINE_D'ARCHIVE_PARENT|"DOMAINE_D'ARCHIVE_PARENT"]
     [--security true|false]
     [--source true|false]
     [-s|--source-images iso|netboot|tar|hdd]
     [--tasksel apt|aptitude|tasksel]
     [--templates CHEMIN]
     [--hdd-size MB]
     [--updates true|false]
     [--backports true|false]
     [--verbose]
     [--win32-loader true|false]
```
## DESCRIPTION

lb config est une commande de haut-niveau (porcelaine) de live-build(7), la suite d'outils live systems.

lb config rempli le répertoire de configuration pour live-build. Par défaut, ce répertoire est nommé 
'config' et est crée dans le répertoire courant où lb config a été exécuté.

Note : actuellement, lb config essaie d'être malin et paramètre les défauts pour plusieurs des 
options dépendamment des paramètres d'autres options (ex quel paquet linux doit être utilisé si un 
système wheezy est construit ou non). Ceci signifie que lorsque vous générez une nouvelle configuration, 
vous devriez appeler lb config une seule fois avec toutes les options spécifiées. L'appeler une seule 
fois avec uniquement un sous-ensemble des options à chaque fois peut résulter dans des configurations 
non-fonctionnelles. Ceci est également engendré par le fait que lb config appelé avec une seule option 
va seulement modifier cette option, et laisser tout le reste en l'état, sauf si ça n'est pas défini. 
Toutefois, lb config ne préviens pas à propos de combinaisons connues comme étant ou semblant impossibles 
qui conduiraient à un système live non-fonctionnel. Si vous n'êtes pas sûr, supprimer 
config/{binary,bootstrap,chroot,common,source} et rappeler lb config.

## OPTIONS

En plus de ses options spécifiques lb config fonctionne avec toutes les options génériques de live-build. 
Voir live-build(7) pour une liste complète de toutes les options génériques de live-build options.

### --apt apt|aptitude
définit si apt-get ou aptitude est utilisé pour installer des paquets lors de la construction de l'image. 
Par défaut : apt.

### --apt-ftp-proxy URL
paramètre le proxy ftp à être utilisé par apt. Par défaut, cette option est vide. Notez que cette variable 
est uniquement pour le proxy qui est utilisé par apt à l'intérieur du chroot, il n'est utilisé pour rien d'autre.

### --apt-http-proxy URL
paramètre le proxy http à être utilisé par apt. Par défaut, cette option est vide. Notez que cette variable 
est uniquement pour le proxy qui est utilisé par apt à l'intérieur du chroot, il n'est utilisé pour rien d'autre.

###--apt-indices true|false|none
définit si les images résultantes devraient avoir des indices apt ou non et paramètre true par défaut. 
Si paramétré à none, aucun indice ne sera inclu.

###--apt-options OPTION|"OPTIONS"
définit les options par défaut qui seront ajoutées à chaque appel apt qui est fait à l'intérieur du chroot 
pendant la construction de l'image. Par défaut, ceci est paramétré à --yes pour permettre l'installation 
non-interactive de paquets.

###--aptitude-options OPTION|"OPTIONS"
définit les options par défaut qui seront ajoutées à chaque appel d'aptitude fait à l'intérieur du chroot 
pendant la construction de l'image. Par défaut, ceci est paramétré à --assume-yes pour permettre 
l'installation non-interactive de paquets.

###--apt-pipeline PROFONDEUR
paramètre la profondeur du tube (pipeline) apt/aptitude. Dans les cas où le serveur distant n'est pas 
conforme aux RFC ou est bogué (comme Squid 2.0.2), cette option peut être une valeur de 0 à 5 indiquant 
combien de requêtes non-résolue APT devrait envoyer. Une valeur de zéro doit être spécifiée si 
l'hôte distant s'attarde improprement sur les connexions TCP - autrement, une corruption des données 
apparaîtra. Les hôtes qui nécessitent ceci sont en violation de la RFC 2068. Par défaut, live-build
ne paramètre pas cette option.

###--apt-recommends true|false
définit si apt devrait installer automatiquement les paquets recommandés. Par défaut : true.

###--apt-secure true|false
définit si apt devrait vérifier les signatures de dépôt. Par défaut : true.

###--apt-source-archives true|false
définit si les entrées deb-src doivent être incluses dans l'image live résultante ou non. Par défaut : true.

###-a|--architectures ARCHITECTURE
définit l'architecture de l'image devant être construite. Par défaut, ceci est paramétré sur l'architecture 
hôte. Notez que vous ne pouvez pas crossbuilder pour une autre architecture si votre système hôte n'est pas 
capable d'exécuter les binaires pour la distribution cible nativement. Par exemple, construire des images 
amd64 sur un i386 et vice versa est possible si vous avez un processeur i386 compatible 64bits et le bon 
noyau. Mais construire des images powerpc sur une système i386 n'est pas possible.

###-b|--binary-images iso|iso-hybrid|netboot|tar|hdd
définit le type d'image à construire. Par défaut, pour les images utilisant syslinux, ceci est paramétré 
pour iso-hybrid pour construire des images CD/DVD qui peuvent également être utilisée comme images hdd, 
pour les images non-syslinux, le défaut est iso.

###--binary-filesystem fat16|fat32|ext2|ext3|ext4
définit le système de fichier à utiliser dans le type d'image. Ceci a un effet si le type d'image binaire 
sélectionnée permet de choisir un système de fichier. Par exemple, lorsque iso est sélectionné, le CD/DVD 
résultant a toujours un système de fichier ISO9660. Lors de la construction d'une image hdd pour des 
clefs usb, ceci est actif. Notez que ceci sélectionne fat16 par défaut sur toutes les architectures excepté 
sparc où le défaut est ext4. Notez également que si vous choisissez fat16 et que votre image binaire 
résultante est plus grosse que 2GB, alors le système de fichier binaire sera automatiquement passé à fat32.

###--bootappend-install PARAMÈTRE|"PARAMÈTRES"
paramètre les options de démarrage spécifiques à debian-installer, si inclus.

###--bootappend-live PARAMÈTRE|"PARAMÈTRES"
paramètre les options de démarrage spécifiques à debian-live. Une liste complète des paramètres de démarrage 
peut être trouvée dans les pages de manuel live-boot(7) et live-config(7).

###--bootloader grub|grub2|syslinux
définit quel chargeur de démarrage est utilisé dans l'image générée. Ceci a effet seulement si l'image 
binaire sélectionnée permet de choisir le chargeur de démarrage. Par exemple, si vous construisez une iso, 
syslinux (ou plus précisément, isolinux) est toujours utilisé. Notez également que certaines 
combinaisons de types d'images binaires et de chargeurs de démarrage peuvent être possibles mais que 
live-build ne les prend pas encore en charge. lb config échouera a créer une de ses configurations pas 
encore supportées et donnera une explication à ce propos. Pour des images hdd sur amd64 et i386, le 
défaut est syslinux.

###--cache true|false
définit globalement si un cache devrait être utilisé. Les différents caches peuvent être contrôlés à travers 
leurs propres options.

###--cache-indices true|false
définit si les indices et les listes de paquets téléchargés devrait être cachés ce qui est faux (false) par 
défaut. L'activer autoriserait la reconstruction d'une image complètement hors-ligne, vous n'auriez donc 
plus les mises-à-jour.

###--cache-packages true|false
définit si les fichiers de paquets téléchargés pourrait être cachés ce qui est vrai (true) par défaut. 
Le désactiver économise la consommation d'espace dans votre répertoire de construction mais rappelez-vous 
que vous créerez beaucoup de trafic non-nécessaire si vous effectuez une paire de reconstructions. En général, 
vous devriez toujours le laisser à vrai (true), toutefois, dans certains cas particuliers d'environnement de 
constructions, il peut être plus rapide de re-télécharger les paquets depuis le miroir réseau local plutôt 
que d'utiliser le disque local.

###--cache-stages true|false|STAGE|"STAGES"
paramètre quels stages seront mis en cache. Par défaut, le paramètre est sur démarrage (bootstrap). En tant 
qu'exception au noms de stages normaux, rootfs peut également être utilisé ici ce qui met en cache uniquement 
le système de fichier racine généré dans filesystem.{dir,ext*,squashfs}. Ceci est utile le développement si 
vous désirez reconstruire le stage binaire mais pas régénéré le système de fichier à chaque reprise.

###--checksums md5|sha1|sha256|none
définit si l'image binaire devrait contenir un fichier appelé md5sums.txt, sha1sums.txt et/ou sha256sums.txt. 
Ceux-ci listent tous les fichiers présents dans l'image avec leurs sommes de vérification. Ils pourront alors 
être utilisés par la vérification d'intégrité inclue dans live-boot pour vérifier le dispositif si spécifié à 
l'invite de démarrage. En général, ceci ne devrait pas être faux (false) et est une fonctionnalité important 
des versions de live system pour le public. Toutefois, pendant le développement de grosses images, ceci peut 
économiser du temps en ne calculant pas les sommes de vérification.

###--compression bzip2|gzip|lzip|none
définit le programme de compression à utiliser pour compresser les tarballs. Par défaut : gzip.

###--config GIT_URL::GIT_ID
permet d'amorcer un arbre de configuration depuis un dépôt GIT, optionnellement avec un numéro d'identification 
GIT (branche, commit, tag, etc.).

###--build-with-chroot true|false
définit si live-build devrait utiliser les outils de l'intérieur du chroot pour construire l'image binaire ou 
non en utilisant et incluant les outils du système hôte. Ceci est une option très dangereuse, l'utilisation 
des outils du système hôte peut amener à des images teintées et même à des images non-démarrables si les 
versions des outils nécessaires du système hôte (principalement il s'agit des bootloaders comme syslinux et 
grub, et des outils auxiliaires tels que dosfstools, xorriso, squashfs-tools et autres) ne correspondent pas 
exactement à ce qui est présent au moment de la construction dans la distribution cible. Ne jamais désactivée 
cette option sauf si vous savez exactement ce que vous faites et avez compris complètement les conséquences.

### --chroot-filesystem ext2|ext3|ext4|squashfs|jffs2|none
définit quel type de système de fichier devrait être utilisé pour l'image du système de fichier racine. 
Si vous utilisez none, alors aucune image de système de fichiers n'est créée et le contenu du système de 
fichier racine est copiée sur le système de fichiers de l'image binaire en tant que fichiers plats. En 
fonction de quel système de fichiers binaire vous avez choisi, il pourrait ne pas être possible de construire 
avec un tel système de fichiers racine plein, exemple : fat16/fat32 ne fonctionneront pas puisque linux ne 
supporte pas directement de fonctionner dessus.

### --clean
minimise le répertoire de configuration en supprimant automatiquement les sous-répertoires non-utilisés et 
donc vides.

### -c|--conffile FICHIER
l'utilisation d'un fichier de configuration alternatif spécifique pour un utilisateur en addition à celui 
utilisé normalement dans le répertoire de configuration.

###--debconf-frontend dialog|editor|noninteractive|readline
définit à quelle valeur le frontend debconf devrait être paramétré à l'intérieur du chroot. Notez que le 
mettre à n'importe lequel sauf noninteractive, qui est le défaut, fait que le processus vous posera des 
questions pendant la construction.

###--debconf-priority low|medium|high|critical
définit à quelle valeur la priorité debconf devra être paramétrée dans le chroot. Par défaut, elle est 
paramétrée à critical, ce qui signifie que presque aucune question n'est affichée. Notez que ceci a 
seulement un effet si vous utilisez un des frontend debconf n'étant pas noninteractive.

###--debian-installer true|cdrom|netinst|netboot|businesscard|live|false
définit quel type, si vous en demandez un, de debian-installer devrait être inclu dans l'image binaire résultante. 
Par défaut, aucun installateur n'est inclus. Toutes les flavours, sauf live, sont les configurations identiques 
utilisées sur le média installateur produit par un cd-debian régulier. Lorsque live est choisi, l'udeb live-installer 
est inclus pour que l'installateur debian ait un comportement différent de l'habituel. Au lieu de l'installation 
du système debian depuis les paquets du média ou du réseau, il installe le système live sur le disque.

###--debian-installer-distribution daily|NOM_DE_CODE
définit la distribution d'où les fichiers de l'installateur debian devrait être pris. Normalement, ceci 
devrait être paramétré pour la même distribution que le système live. Ceci dit, parfois, quelqu'un veut 
utiliser un installateur plus récent ou même une construction du jour.

###--debian-installer-preseedfile FICHIER|URL
paramètre le nom de fichier ou l'URL pour un fichier de pré-configuration inclu ou utilisé optionnellement 
pour l'installateur debian. Si le config/binary_debian-installer/preseed.cfg existe, il sera utilisé par défaut.

###--debian-installer-gui true|false
définit si l'interface graphique GTK de l'installateur-debian devrait être vraie ou pas. En mode Debian et 
pour la plupart des versions d'Ubuntu, cette option est vraie, tandis que sinon fausse, par défaut.

###--debug
active les messages d'information de déboguage.

###-d|--distribution NOM_DE_CODE
définit la distribution du système live résultant.

###-d|--parent-distribution NOM_DE_CODE
définit la distribution parente pour les dérivations du système live résultant.

###-d|--parent-debian-installer-distribution CODENAME
définit la distribution de l'installateur-debian parent pour les dérivations du système live résultant.

###--dump
prepares a report of the currently present live system configuration and the version of live-build used. 
This is useful to provide if you submit bug reports, we do get all informations required for us to locate 
and replicate an error.

###--fdisk fdisk|fdisk.dist
sets the filename of the fdisk binary from the host system that should be used. This is autodetected and 
does generally not need any customization.

###--force
forces re-execution of already run stages. Use only if you know what you are doing. It is generally safer 
to use lb clean to clean up before re-executing lb build.

###--grub-splash FILE
defines the name of an optional to be included splash screen graphic for the grub bootloader.

###--gzip-options OPTION|"OPTIONS"
defines the default options that will be appended to (almost) every gzip call during the building of the image. 
By default, this is set to --best to use highest (but slowest) compression. Dynamically, if the host system 
supports it, also --rsyncable is added.

###--hooks FILE
defines which hooks available in /usr/share/live/build/examples/hooks should be activated. Normally, there 
are no hooks executed. Make sure you know and understood the hook before you enable it.

###--ignore-system-defaults
lb config by default reads system defaults from /etc/live/build.conf and /etc/live/build/* when generating 
a new live system config directory. This is useful if you want to set global settings, such as mirror locations, 
and don't want to specify them all of the time.

###--initramfs auto|none|live-boot|casper
sets the name of package that contains the live system specific initramfs modification. By default, auto is used, 
which means that at build time of the image rather than on configuration time, the value will be expanded to 
casper when building ubuntu systems, to live-boot for all other systems. Using 'none' is useful if the resulting 
system image should not be a live image (experimental).

###--initramfs-compression bzip2|gzip|lzma]
defines the compression program to be used to compress the initramfs. Defaults to gzip.

###--interactive shell
defines if after the chroot stage and before the beginning of the binary stage, a interactive shell login 
should be spawned in the chroot in order to allow you to do manual customizations. Once you close the shell 
with logout or exit, the build will continue as usual. Note that it's strongly discouraged to use this for 
anything else than testing. Modifications that should be present in all builds of a live system should be 
properly made through hooks. Everything else destroys the beauty of being able to completely automatise the 
build process and making it non interactive. By default, this is of course false.

###--isohybrid-options OPTION|"OPTIONS"
defines options to pass to isohybrid.

###--iso-application NAME
sets the APPLICATION field in the header of a resulting CD/DVD image and defaults to "Debian Live" in 
debian mode, and "Ubuntu Live" in ubuntu mode.

###--iso-preparer NAME
sets the PREPARER field in the header of a resulting CD/DVD image. By default this is set to "live-build 
VERSION; http://packages.qa.debian.org/live-build", where VERSION is expanded to the version of live-build 
that was used to build the image.

###--iso-publisher NAME
sets the PUBLISHED field in the header of a resulting CD/DVD image. By default, this is set to 'Live Systems 
project; http:/live-systems.org/; debian-live@lists.debian.org'. Remember to change this to the appropriate 
values at latest when you distributing custom and unofficial images.

###--iso-volume NAME
sets the VOLUME field in the header of a resulting CD/DVD and defaults to '(MODE) (DISTRIBUTION) (DATE)' 
whereas MODE is expanded to the name of the mode in use, DISTRIBUTION the distribution name, and DATE with the 
current date and time of the generation.

###--jffs2-eraseblock SIZE
sets the eraseblock size for a JFFS2 (Second Journaling Flash File System) filesystem. The default is 64 KiB. 
If you use an erase block size different than the erase block size of the target MTD device, JFFS2 may not 
perform optimally. If the SIZE specified is below 4096, the units are assumed to be KiB.

###--keyring-packages PACKAGE|"PACKAGES"
sets the keyring package or additional keyring packages. By default this is set to debian-archive-keyring.

###-k|--linux-flavours FLAVOUR|"FLAVOURS"
sets the kernel flavours to be installed. Note that in case you specify more than that the first will be 
configured the default kernel that gets booted.

###--linux-packages "PACKAGES"
sets the internal name of the kernel packages naming scheme. If you use debian kernel packages, you will 
not have to adjust it. If you decide to use custom kernel packages that do not follow the debian naming 
scheme, remember to set this option to the stub of the packages only (for debian this is linux-image-2.6), 
so that STUB-FLAVOUR results in a valid package name (for debian e.g. linux-image-686-pae). Preferably you 
use the meta package name, if any, for the stub, so that your configuration is ABI independent. Also don't 
forget that you have to include stubs of the binary modules packages for unionfs or aufs, and squashfs if 
you built them out-of-tree.

###--losetup losetup|losetup.orig
sets the filename of the losetup binary from the host system that should be used. This is autodetected and 
does generally not need any customization.

###--memtest memtest86+|memtest86|none
defines if memtest, memtest86+ or no memory tester at all should be included as secondary bootloader 
configuration. This is only available on amd64 and i386 and defaults to memtest86+.

###-m|--parent-mirror-bootstrap URL
sets the location of the debian package mirror that should  be  used  to  bootstrap  from.  This  defaults 
to http://ftp.de.debian.org/debian/ which may not be a good default if you live outside of Europe.

###--parent-mirror-chroot URL
sets the location of the debian package mirror that will be used to fetch the packages in order to build 
the live system. By default, this is set to the value of --parent-mirror-bootstrap.

###--parent-mirror-chroot-security URL
sets the location of the debian security package mirror that will be used to fetch the packages in order 
to build the live system. By default, this points to http://security.debian.org/debian/.

###--parent-mirror-chroot-updates URL
sets the location of the debian updates package mirror that will be used to fetch packages in order to build 
the live system. By default, this is set to the value of --parent-mirror-chroot.

###--parent-mirror-chroot-backports URL
sets the location of the debian backports package mirror that will be used to fetch packages in order to build 
the live system. By default, this points to http://backports.debian.org/debian-backports/.

###--parent-mirror-binary URL
sets the location of the debian package mirror that should end up configured in the final image and which is 
the one a user would see and use. This has not necessarily to be the same that is used to build the image, 
e.g. if you use a local mirror but want to have an official mirror in the image. By default, 
'http://httpredir.debian.org/debian/' is used.

###--parent-mirror-binary-security URL
sets the location of the debian security package mirror that should end up configured in the final image. 
By default, 'http://security.debian.org/' is used.

###--parent-mirror-binary-updates URL
sets the location of the debian updates package mirror that should end up configured in the final image. 
By default, the value of --parent-mirror-binary is used.

###--parent-mirror-binary-backports URL
sets the location of the debian backports package mirror that should end up configured in the final image. 
By default, 'http://backports.debian.org/debian-backports/' is used.

###--parent-mirror-debian-installer URL
sets the location of the mirror that will be used to fetch the debian installer images. By default, this 
points to the same mirror used to build the live system, i.e. the value of --parent-mirror-bootstrap.

###--mirror-bootstrap URL
sets the location of the debian package mirror that should be used to bootstrap the derivative from. 
This defaults to http://ftp.de.debian.org/debian/ which may not be a good default if you live outside of Europe.

###--mirror-chroot URL
sets the location of the debian package mirror that will be used to fetch the packages of the derivative 
in order to build the live system. By default, this is set to the value of --mirror-bootstrap.

###--mirror-chroot-security URL
sets the location of the debian security package mirror that will be used to fetch the packages of the 
derivative in order to build the live system. By default, this points to http://security.debian.org/debian/.

###--mirror-chroot-updates URL
sets the location of the debian updates package mirror that will be used to fetch packages of the derivative 
in order to build the live system. By default, this is set to the value of --mirror-chroot.

###--mirror-chroot-backports URL
sets the location of the debian backports package mirror that will be used to fetch packages of the derivative 
in order to build the live system. By default, this points to http://backports.debian.org/debian-backports/.

###--mirror-binary URL
sets the location of the derivative package mirror that should end up configured in the final image and which 
is the one a user would see and use. This has not necessarily to be the same that is used to build the image, 
e.g. if you use a local mirror but want to have an official mirror in the image.

###--mirror-binary-security URL
sets the location of the derivatives security package mirror that should end up configured in the final image.

###--mirror-binary-updates URL
sets the location of the derivatives updates package mirror that should end up configured in the final image.

###--mirror-binary-backports URL
sets the location of the derivatives backports package mirror that should end up configured in the final image.

###--mirror-debian-installer URL
sets the location of the mirror that will be used to fetch the debian installer images of the derivative. 
By default, this points to the same mirror used to build the live system, i.e. the value of --mirror-bootstrap.

###--mode debian|progress|ubuntu
defines a global mode to load project specific defaults. By default this is set to debian.

###--system live|normal
defines if the resulting system image should a live system or a normal, non-live system.

###--net-root-filesystem nfs|cfs
defines the filesystem that will be configured in the bootloader configuration for your netboot image. 
This defaults to nfs.

###--net-root-mountoptions OPTIONS
sets additional options for mounting the root filesystem in netboot images and is by default empty.

###--net-root-path PATH
sets the file path that will be configured in the bootloader configuration for your netboot image. This 
defaults to /srv/debian-live in debian mode, and /srv/ubuntu-live when in ubuntu mode.

###--net-root-server IP|HOSTNAME
sets the IP or hostname that will be configured in the bootloader configuration for the root filesystem 
of your netboot image. This defaults to 192.168.1.1.

###--net-cow-filesystem nfs|cfs
defines the filesystem type for the copy-on-write layer and defaults to nfs.

###--net-cow-mountoptions OPTIONS
sets additional options for mounting the copy-on-write layer in netboot images and is by default empty.

###--net-cow-path PATH
defines the path to client writable filesystem. Anywhere that client_mac_address is specified in the path 
live-boot will substitute the MAC address of the client delimited with hyphens.

Example:
  /export/hosts/client_mac_address
  /export/hosts/00-16-D3-33-92-E8

###--net-cow-server IP|HOSTNAME
sets the IP or hostname that will be configured in the bootloader configuration for the copy-on-write 
filesystem of your netboot image and is by default empty.

###--net-tarball true|false
defines if a compressed tarball should be created. Disabling this options leads to no tarball at all, 
the plain binary directory is considered the output in this case. Default is true.

###--quiet
reduces the verbosity of messages output by lb build.

###--archive-areas ARCHIVE_AREA|"ARCHIVE_AREAS"
defines which package archive areas of a debian packages archive should be used for configured debian 
package mirrors. By default, this is set to main. Remember to check the licenses of each packages with 
respect to their redistributability in your juristiction when enabling contrib or non-free with this mechanism.

###--parent-archive-areas PARENT_ARCHIVE_AREA|"PARENT_ARCHIVE_AREAS"
defines the archive areas for derivatives of the resulting live system.

###--security true|false
defines if the security repositories specified in the security mirror options should be used or not.

###--source true|false
defines if a corresponding source image to the binary image should be build. By default this is false 
because most people do not require this and would require to download quite a few source packages. 
However, once you start distributing your live image, you should make sure you build it with a source 
image alongside.

###-s|--source-images iso|netboot|tar|hdd
defines the image type for the source image. Default is tar.

###--firmware-binary true|false
defines if firmware packages should be automatically included into the binary pool for debian-installer. 
Note that only firmware packages available within the configured archive areas are included, e.g. an 
image with packages from main only will not automatically include firmware from non-free. This option 
does not interfere with explicitly listed packages in binary package lists.

###--firmware-chroot true|false
defines if firmware packages should be automatically included into the live image. Note that only firmware 
packages available within the configured archive areas are included, e.g. an image with packages from main 
only will not automatically include firmware from non-free. This option does not interfere with explicitly 
listed packages in chroot package lists.

###--swap-file-path PATH
defines the path to a swap file to create in the binary image. Default is not to create a swap file.

###--swap-file-size MB
defines what size in megabytes the swap file should be, if one is to be created. Default is 512MB.

###--tasksel apt|aptitude|tasksel
selects which program is used to install tasks. By default, this is set to tasksel.

###--templates PATH
sets the path to the templates that live-build is going to use, e.g. for bootloaders. By default, this is 
set to /usr/share/live/build/templates/.

###--hdd-size MB
defines what size the hdd image should be. Note that although the default is set to 10000 (= 10GB), it 
will not need 10GB space on your harddisk as the files are created as sparse files.

###--updates true|false
defines if debian updates package archives should be included in the image or not.

###--backports true|false
defines if debian backports package archives should be included in the image or not.

###--verbose
increases the verbosity of messages output by lb build.

###--win32-loader true|false
defines if win32-loader should be included in the binary image or not.

###ENVIRONMENT
Currently, command line switches can also be specified through the corresponding environment variable. 
However, this generally should not be relied upon, as it is an implementation detail that is subject to 
change in future releases. For options applying directly to live-build, environment variables are named 
LB_FOO, meaning, e.g. --apt-ftp-proxy becomes LB_APT_FTP_PROXY (the exception being internal options such 
as --debug). For options passed to another program, as in APT_OPTIONS or GZIP_OPTIONS, no LB_ prefix is used.

FICHIERS

* `auto/config`
* `/etc/live/build.conf`, `/etc/live/build/*`
An optional, global configuration file for lb config variables. It is useful to specify a few system wide 
defaults, like LB_PARENT_MIRROR_BOOTSTRAP. This feature can be false by specifying the 
--ignore-system-defaults option.

VOIR AUSSI

* live-build(7)
* live-boot(7)
* live-config(7)

Ce programme est une partie de live-build.


`5.0~a11-1		21.10.2015		LIVE-BUILD(1)`
