
## live-build
* [live-build](https://debian-live.alioth.debian.org/live-manual/unstable/manual/html/live-manual.en.html)
* [live-manual](http://debian-live.alioth.debian.org/live-manual/)
* [Debian Live git](https://alioth.debian.org/scm/?group_id=30929)
* [Debian Live wiki](http://debian-live.alioth.debian.org/)
* [PTS live-manual](https://tracker.debian.org/pkg/live-build)

## Debian Installer (d-i)
* [www.debian.org/devel/debian-installer](https://www.debian.org/devel/debian-installer/)
* [manuels installation debian](https://d-i.debian.org/manual/)
* [d-i errata](https://www.debian.org/devel/debian-installer/errata)
* [d-i Faq](https://wiki.debian.org/DebianInstaller/FAQ)
* [installation preseed](https://d-i.debian.org/manual/example-preseed.txt)
