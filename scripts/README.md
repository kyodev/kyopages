la nouvelle plateforme officielle est celle du groupe sdeb, [sur framagit](https://framagit.org/sdeb)


## getFirefox

script bash: chargeur / installeur des versions officielles Mozilla Firefox latest, ESR, beta, nightly. installation et fonctionnement en parallèle possible. Mise à jour automatique par Firefox autorisée (selon la périodicité du canal Firefox).

##  getFlashPlayer

script bash: télécharge et installe la version officielle du plugin FlashPlayer pour Firefox (NPAPI)

##  getInfo

script bash collectant des informations sur la configuration d'un PC. Un rapport au format markdown est formé. Ce rapport peut être exporté sur un pastebin avec rendu markdown pour partager les informations dans le but de se faire dépanner

## getIp

script bash: affiche les Ips publiques ou locales d'un PC

##  getThunderbird

script bash: chargeur / installeur des versions officielles Thunderbird latest, beta. installation et fonctionnement en parallèle possible. Mise à jour automatique par Thunderbird autorisée (selon la prériodicité du canal).

##  getXfox

script bash: chargeur / installeur des versions officielles de Waterfox et Tor Browser, Mise à jour automatique par les applications autorisée.

## ipupdate

mise à jour IP sur service de DNS dynamique ( duckdns / dynu / freemyip / nh / noip )

## nsOpenNic

manage OpenNic DNS in resolv.conf

## nstest

speed test of open dns

## pastit

export to a pastebin with command line
