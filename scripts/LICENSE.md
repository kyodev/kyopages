# LPRAB / WTFPL


## LICENSE PUBLIQUE RIEN À BRANLER

```txt
                      Version 1, Mars 2009

 Copyright (C) 2009 Sam Hocevar
  14 rue de Plaisance, 75014 Paris, France
 
 La copie et la distribution de copies exactes de cette license sont
 autorisées, et toute modification est permise à condition de changer
 le nom de la license. 

         CONDITIONS DE COPIE, DISTRIBUTON ET MODIFICATION
               DE LA LICENCE PUBLIQUE RIEN À BRANLER

  0. Faites ce que vous voulez, j’en ai RIEN À BRANLER.
```

## LICENSE DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 

```txt
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
```

## getInfo - licenses spécifiques couvrant des parties de code incluses

### kernel.org

* parties concernées: **tags cpu**
* license [GPL-2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt): 
  <https://github.com/torvalds/linux/blob/master/COPYING>

### neofetch

* parties concernées: **DE, shells, VM**
* license MIT: <https://github.com/dylanaraps/neofetch/blob/master/LICENSE.md>

