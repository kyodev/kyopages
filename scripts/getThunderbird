#!/bin/bash

version=2.11.O
date="03/07/2018"
script="getThunderbird"
# contact="IRC ##sdeb@freenode.net ou https://framagit.org/sdeb/getThunderbird/issues"

##### license LPRAB/WTFPL
#  auteur: sdeb
#    contributeurs: kyodev
#####

	# détection architecture système, assigne $fu_archi: linux|linux64
	# return 1 on unknown architecture
	# remarque, debian: dpkg --print-architecture affiche i386
f__architecture(){		# 09/06/2018  SPÉCIFIQUE

	case "$(uname -m)" in
		amd64 | x86_64 )	fu_archi="linux64";;
		i?86 | x86 )		fu_archi="linux";;
		* )
			case "$(getconf LONG_BIT)" in
				64 )
					fu_archi="linux64";;
				32 )
					fu_archi="linux";;
				*)
					return 1
			esac ;;
	esac
}

	# test validité archive, $1 archive, assigne $archive_detect (gzip|xz|deb|zip), return 1 si inconnue
f__archive_test(){		# 13/06/2018
	local filetest test

	filetest=$( file -b "$1" )
	test=${filetest/*gzip compressed data*/1234567890}
	[ "$test" == "1234567890" ] && archive_detect="gzip"
	test=${filetest/*bzip2 compressed data*/1234567890}
	[ "$test" == "1234567890" ] && archive_detect="bzip2"
	test=${filetest/*binary compressed data*/1234567890}
	[ "$test" == "1234567890" ] && archive_detect="deb"
	test=${filetest/*Zip compressed data*/1234567890}
	[ "$test" == "1234567890" ] && archive_detect="Zip"
	test=${filetest/*XZ compressed data*/1234567890}
	[ "$test" == "1234567890" ] && archive_detect="XZ"
	if [[ -z "$archive_detect" || "$archive_detect" == "empty" ]]; then return 1; fi
}

# shellcheck disable=SC2034
f__color(){		# 29/05/2018

	if type -p tput &>/dev/null && tput setaf 1 &>/dev/null; then
		MAGENTA=$( tput setaf 5 )
		  BLACK=$( tput setaf 0 )
		   CYAN=$( tput setaf 6 )
		 YELLOW=$( tput setaf 3 )		# question
		  GREEN=$( tput setaf 2 )		# ok
		   BLUE=$( tput setaf 4 )		# info
			RED=$( tput setaf 1 )		# alerte
			STD=$( tput sgr0 )			# retour "normal"
		BOLD=$( tput bold )
		ITAL=$( tput sitm )
		SOUL=$( tput smul )
	else 
		YELLOW=$'\033[0;33m'		# ?
		 GREEN=$'\033[0;32m'		# ok
		  BLUE=$'\033[0;34m'		# info
		   RED=$'\033[0;31m'		# alerte
		   STD=$'\033[0m'			# standard
	fi
}

	# affichage $1 en rouge, $1++ optionnels en bleu, sortie script sur erreur, log $1 si $operation=upgrade
f__error(){		# 07/O6/2018
	local depart=1 i

	echo -e "\\n$RED  $script $version, erreur critique: $1 $STD" >&2
	for (( i=2 ; i<=$# ; i++ )); do
		echo -e "  $BLUE${!i}$STD" >&2
	done
	echo
	[ "$operation" == "upgrade" ] && f__log "$script $version: $1"
	exit 1
}

	# affichage en bleu, si $1=raw pas de ligne vide à la fin, si $1=log alors uniquement $2 logué, combiné: $1="log:raw"
f__info(){		# 07/06/2018
	local depart=1 log i

	if [[ "$1" =~ "raw"  ||  "$1" =~ "log" ]]; then
		depart=2
	fi
	if [[ "$1" =~ "log" ]]; then
		log="$2"
		log=${log//$'\t'}
		log=${log//$'\n'}
		f__trim log
		f__log "$log"
	fi
	for (( i=depart ; i<=$# ; i++ )); do
		echo -e "  $BLUE${!i}$STD"
	done
	[[ "$1" =~ raw ]] || echo 
}

	# log spécifique, fichier log limité à 10000 octets, $1 message à loguer
f__log(){		# 05/03/2018

	if [ -w "$script_logs" ];  then 
		if [ "$( stat -c %s  "$script_logs" )" -ge "10000" ]; then
			echo "$(date +%Y%m%d\ %H%M%S) $1"  &>/dev/null > "$script_logs"
		else 
			echo "$(date +%Y%m%d\ %H%M%S) $1"  &>/dev/null >> "$script_logs"
		fi
	fi
}

	# $1 file, [$2=n|l] (si vide, last line)
f__read_file(){		# 13/06/2018
	local i=1 no

	case $2 in 
		l | last ) no=0 ;;
		*        ) no=$(( $2 )) ;;
	esac
	while read -r; do
		(( i >= no )) && break
		(( i++ ))
	done < "$1"
	echo "$REPLY"
}

	# recherche commandes/paquets, $1 liste: cmd1|cmd2[>paquet] (séparées par espaces) ex: "gawk|mawk>gawk wget"
	# si manque, return 1 & affiche commandes manquantes (si debian, ajout proposition paquet à installer)
	# requiert f__sort_uniq
f__requis(){		# 30/06/2018
	local ENV_DEBIAN c_echo ireq cmds package commands command 
	local commandsMissing packagesMissing t_commandsMissing t_packagesMissing

	if type -p dpkg &>/dev/null ; then
		ENV_DEBIAN="oui"							# debian
	fi
	if type -t f__info &>/dev/null; then
		c_echo="f__info"
	else
		c_echo="echo -e"					# f__info existe? sinon echo
	fi
	for ireq in $1; do
		IFS='>' read -r cmds package <<< "$ireq"			# ex: "awk|gawk>gawk wget file tar killall>psmisc"
		IFS='|' read -ra commands <<< "${cmds}"
		[ -z "$package" ] && package=${commands[0]}	# pas de packages dans les options, donc = commands[0]
		unset t_commandsMissing t_packagesMissing
		for command in "${commands[@]%$'\n'}"; do
			if type -p "$command" &>/dev/null ; then
				unset t_commandsMissing t_packagesMissing
				break
			else	# inexistant
				t_commandsMissing+="$command "
				t_packagesMissing="$package "
			fi
		done
		[ "$t_commandsMissing" ] && commandsMissing+="$t_commandsMissing "
		[ "$t_packagesMissing" ] && packagesMissing+="$t_packagesMissing "
	done

		# dédoublonnage & tri
	commandsMissing=$( f__sort_uniq "$commandsMissing" )
	packagesMissing=$( f__sort_uniq "$packagesMissing" )

		# affichage final
	if [ "$commandsMissing" ] && [ "$ENV_DEBIAN" ]; then
		$c_echo "${RED}erreur critique, manquant: $STD$BOLD$commandsMissing$STD" \
						"vous devriez exécuter:$GREEN apt install $packagesMissing$STD"
	elif [ "$commandsMissing" ]; then
		$c_echo "${RED}erreur critique, manquant: $STD$BOLD$commandsMissing$STD"
	fi

	[ "$commandsMissing" ] && return 1 || return 0
}

	# options: liste à trier [-lf], affiche flat liste (\n enlevés en sortie) ou si -ln  liste avec \n à chaque ligne
	# des caractères non permis =$/`... sont transformés en ø
f__sort_uniq(){		# 02/07/2018
	local lf list array result item

	while (( $# )) ; do
		case "$1" in
			-lf ) lf='yes' ;;
			 * ) list=${1} ;;
		esac
		shift
	done

	while read -r ; do		# lecture ligne
		[ -z "$REPLY" ] && continue
		REPLY=${REPLY# } 		# trim gauche simple
		REPLY=${REPLY% }		# trim droite simple
		REPLY=${REPLY// /\~}	# espace remplacé avec ~
			# interdits/remplacés: ()/`;      ø utilisé comme indicateur, ~ remplacé pas espace
		REPLY=${REPLY//[^a-zA-Z0-9~.,\{\}!\[\]#*%µ£°⇉:_+-]/ø}	# caractère <> de autorisé remplacés par ø 
		if [ "$lf" ]; then
			array+=( "${REPLY}" )
		else
			IFS='~' read -ra array <<< "$REPLY"
		fi
	done <<< "${list}"

	result=$(
		for item in "${array[@]}"; do
			eval "alias ${item}=ls"
		done
		alias
	)
	result=${result//alias }
	result=${result//=\'ls\'}
	result=${result//\~/ }
	[ "$lf" ] && echo "${result}" || echo "${result//$'\n'/ }"
}

	# $@=cmd à lancer en root avec su ou sudo. si sudo possible: prioritairesu su
f__sudo(){		# 11/06/2018

	if sudo -v &>/dev/null && [ $EUID -ne 0 ] ; then
		sudo su --preserve-environment -c "$@"
	else
		echo -n "[su]   "
		su --preserve-environment -c "$@"
	fi
}

	# $1=NOM de la variable à trimer (variable et non $variable), [$2=left|right|all], all si vide
f__trim(){		# 07/03/2018
	local trim=${!1}

	[[ "$2" == right || "$2" == all || -z "$2" ]] && trim="${trim%${trim##*[^[:space:]]}}"		# fin right
	[[ "$2" == left  || "$2" == all  || -z "$2" ]] && trim="${trim#${trim%%[^[:space:]]*}}"		# début left
	eval "$1=\"$trim\""
}

	# user ayant initié la session graphique, assigne $fu_user
	# return 1 sur échec identification user, return 2 sur absence home/
	# gestion variable environnement user avec: USER_INSTALL=<user> script
f__user(){		# 08/03/2018
	local user userid root_login

	root_login="$(grep ':0:' /etc/passwd | cut -d':' -f1)" || root_login="root"
	if [ "$USER_INSTALL" ]; then 	# user via variable environnement, moyen d'injecter root si pb 
		fu_user="$USER_INSTALL"; 
		return 0
	elif [[ "$TERM" =~ linux ]]; then	#debian 9 recovery ou nomodeset TERM=linux
		if [ "$USER" ]; then
			user="$USER"
		elif [ "$EUID" -eq 0 ]; then
			fu_user="$root_login"
			return 0
		fi
	fi
	if [ "$SUDO_UID" ]; then 
		userid="$SUDO_UID"; 
	elif grep -qEo '[0-9]+' <<< "$XDG_RUNTIME_DIR" ; then 
		userid=$( grep -Eo '[0-9]+' <<< "$XDG_RUNTIME_DIR" | cut -d'/' -f4 )
	else
		userid=$( grep -Eo '[0-9]+' <<< "$XAUTHORITY" | cut -d'/' -f4 )
	fi
	[ "$userid" ] && user=$( grep "$userid" /etc/passwd | cut -d ":" -f 1 )
	if [ "$user" ] && [ "$user" != "$root_login" ]; then
		fu_user="$user"
		return 0
	else
		if [ "$SUDO_USER" ] && [ "$SUDO_USER" != "$root_login" ]; then 
			user="$SUDO_USER"; 
		elif who | grep -qv 'root'; then 
			user=$( who | grep -v 'root' | head -n1 | cut -d ' ' -f1 );	# who | grep -v 'root' | awk 'FNR==1{print $1}'
		else
			user=$( grep -m1 'hourly.*get[A-Z].*\.anacrontab.*\.config/anacron/spool' /etc/crontab | cut -d' ' -f2 );
		fi
	fi
	fu_user="$user"
	[ "$fu_user" ] || return 1
	[ -d "/home/$fu_user" ] || return 2
	return 0
}

	# test wget, $1=url à tester, $2=''|print|loc|test
	# par défaut, sortie du script (même si url testée ok) avec affichage erreur ou ok
	# si $2=print affiche url testée & entêtes http & location, return 0
	# si $2=loc affiche seulement location, return 0
	# si $2=test return 0 si ok, return 1 si KO
f__wget_test(){		# 07/06/2018
	local file_test_wget="/tmp/testWget-$script" retourWget retourHttp location

	wget -Sq --timeout=5 --tries=2 --user-agent="$user_agent" --spider --save-headers "$1" &>"$file_test_wget"
	retourWget="$?"
	[ "$retourWget" == 1 ] && retourWget="1: code erreur générique"
	[ "$retourWget" == 2 ] && retourWget="2: parse erreur (ligne de commande?)"
	[ "$retourWget" == 3 ] && retourWget="3: erreur Entrée/sortie fichier"
	[ "$retourWget" == 4 ] && retourWget="4: défaut réseau"
	[ "$retourWget" == 5 ] && retourWget="5: défaut vérification SSL"
	[ "$retourWget" == 6 ] && retourWget="6: défaut authentification"
	[ "$retourWget" == 7 ] && retourWget="7: erreur de protocole"
	[ "$retourWget" == 8 ] && retourWget="8: réponse serveur en erreur"
	retourHttp=$( grep -i 'HTTP/' "$file_test_wget" | tr -d '\n' | xargs )
	location=$( grep -i 'location' $file_test_wget | xargs )
	if [ "$2" == "test" ]; then
		rm -f "$file_test_wget"
			# spécial maintenance frama.link, pas de redirection sur page status framalink
		grep -q '303' <<< "$retourHttp" && return 1			# 303 See Other
		[ "$retourWget" == "0" ] && return 0 || return 1
	fi
	if [ "$2" == "print" ]; then
		if [ "$retourWget" != "0" ]; then
			echo "  erreur wget: erreur $RED$retourWget" 
			echo -e "$BLUE  $1$STD\\t$RED  $retourHttp$STD"
		else
			echo -e "$BLUE  $1$STD\\t$GREEN  $retourHttp$STD" 
		fi
	fi
	if [ "$2" == "print" ] || [ "$2" == "loc" ]; then
		[ "$location" ] && echo -n "$YELLOW  $location" || echo -n "$YELLOW  no location"
		echo "$STD"
		rm -f "$file_test_wget"
		return 0
	fi
	if [ "$retourWget" != "0" ]; then
		rm -f "$file_test_wget"
		f__error "wget, erreur $retourWget" "$1" "$YELLOW$retourHttp" "$location"
		echo -e "$RED   erreur wget, $retourWget \\n  $1 \\n  $YELLOW$retourHttp \\n  $location$STD" # pour les diags
		return 1
	fi
	if grep -q '200' <<< "$retourHttp"; then 
		echo -e "$GREEN\\ntout est ok, réessayer$STD\\n"
	fi
	rm -f "$file_test_wget"
	exit 0
}

# shellcheck disable=SC1117
f_affichage(){		# 09/06/2018

	clear 2>/dev/null || tput clear 2>/dev/null
	echo -n "$BLUE"
	cat <<- end
	              _  _____ _                     _           _     _         _  
	    __ _  ___| ||_   _| |__  _   _ _ __   __| | ___ _ __| |__ (_)_ __ __| | 
	   / _' |/ _ \ __|| | | '_ \| | | | '_ \ / _' |/ _ \ '__| '_ \| | '__/ _' | 
	  | (_| |  __/ |_ | | | | | | |_| | | | | (_| |  __/ |  | |_) | | | | (_| | 
	   \__, |\___|\__||_| |_| |_|\__,_|_| |_|\__,_|\___|_|  |_.__/|_|_|  \__,_| 
	   |___/   $YELLOW  version $version - $date$STD

	end
}

	# affichage help
f_help(){		# 11/06/2018

	cat <<- end
		      canaux possibles:$GREEN latest, beta$STD ( <all> = tous les canaux )

		  exemple, installation version Release (latest): $BLUE$script i-latest$STD
		  ----------------------------------------------------------------------
		  $BLUE$script i-$STD${GREEN}canal       :$STD ${RED}i${STD}nstallation de $appli <canal> $RED(root)$STD
		  $BLUE$script d-$STD${GREEN}canal       :$STD copier un profil ${RED}.d${STD}efault existant sur <canal>
		  $BLUE$script m-$STD${GREEN}canal archi :$STD installation sur le <canal> d'une <archi>ve téléchargée ${RED}m${STD}anuellement $RED(root)$STD
		  $BLUE$script r-$STD${GREEN}canal       :$STD désinstallation (${RED}r${STD}emove) du <canal> $RED(root)$STD
		  $BLUE$script ri$STD${GREEN}            :$STD ${RED}r${STD}éparation$RED i${STD}cône(s) dans le menu
		  $BLUE$script t-$STD${GREEN}canal       :$STD téléchargement du <canal> dans le répertoire courant (sans installation)
		  $BLUE$script u-$STD${GREEN}canal       :$STD profil pour l'${RED}u${STD}tilisateur en cours et comme défaut système $RED(root)$STD

		  $BLUE$script version       :$STD versions installées et en ligne
		
		  $GREEN  --dev   :$STD une version de dev du script (si existante) est recherchée
		  $GREEN  --sauve :$STD le téléchargement est sauvegardé dans le répertoire courant ${BOLD}en$STD plus de l'installation
		  ----------------------------------------------------------------------
		  $BLUE./$script$STD (ou $BLUE./$script -i$STD) : installation du script dans le système $RED(root)$STD
		  $BLUE$script -h$STD, --help    : affichage aide
		  $BLUE$script -r$STD, --remove  : désinstallation du script $RED(root)$STD
		  $BLUE$script -u$STD, --upgrade : mise à jour du script
		  $BLUE$script -v$STD, --version : version du script
		  ----------------------------------------------------------------------
		  ${STD}plus d'infos: $GREEN$url_notice$STD

	end
}

f_tb_alertIcedove(){		# 25/12/2017

	if [ -d "/home/$fu_user/.icedove" ]; then
		f__info "${RED}Profil icedove présent.$BLUE le script n'a pas été conçu pour" \
			"    prendre en charge automatiquement cette version de Thunderbird en place."
	fi
}

	# traitement utilisateur, $1=canal, [$2='menu' si appel indépendant pour reconfiguration]
f_tb_config_profil(){		# 09/06/2018
	local nbProfiles canal="$1"

	if [ ! -d "$dirInstall$canal" ] && [ "$2" == "menu" ]; then	# pas de répertoire programme
		f__info "$appli $canal n'est pas installé"
		exit 1
	fi

	if [ ! -d "$dirProfil/$canal" ]; then			# pas de répertoire profil
		mkdir -p "$dirProfil/$canal"						# répertoire du répertoire profil
		chown -R "$fu_user:" "$dirProfil/$canal/"			# propriété du répertoire profil à l'user
		chmod g-rx,o-rx "$dirProfil/$canal/"				# droits répertoire
	fi
	if [ ! -e "$dirProfil/profiles.ini" ]; then		# pas de profile.ini
		echo -e "[General]\\nStartWithLastProfile=1" > "$dirProfil/profiles.ini"
		chown "$fu_user:" "$dirProfil/profiles.ini"			# propriété du fichier profile.ini à l'user
		chmod u+rw,go+r "$dirProfil/profiles.ini"			# permissions du fichier profile.ini à l'user
	fi
		# inscription canal dans profil.ini, à la fin, si inexistant
	if ! grep -q "Name=$canal" "$dirProfil/profiles.ini" ; then
			# comptage profils existants
		nbProfiles=$( grep -cEs '\[Profile[0-9]+\]' "$dirProfil/profiles.ini" )
			# création profil dans profile.ini
		{
			echo 
			echo "[Profile$nbProfiles]"
			echo "Name=$canal"
			echo "IsRelative=1"
			echo "Path=$canal"
		} >> "$dirProfil/profiles.ini"
	fi

	f__info "log:raw" "profil $appli $canal configuré"
}

	# traitement système: lien /usr/bin, default profiles.ini, $1 canal
f_tb_config_system(){		# 09/06/2018
	local canal="$1"
	local ligne numero marqueur stockage profileIni="$dirProfil/profiles.ini"

	if [ ! -d "$dirInstall$canal" ]; then
		f__info "$appli $canal n'est pas installé"
		return 1
	fi

		#lanceurs
	ln -sf "/usr/bin/thunderbird-$canal" "/usr/bin/thunderbird"
			# lanceur paquet debian éventuel
	if [ -x "/usr/bin/thunderbird" ] && [ "$(stat -c %s /usr/bin/thunderbird)" -gt "2000" ]; then
		cp /usr/bin/thunderbird /usr/bin/thunderbird.old
		f__info "lanceur du paquet debian détecté et renommé en /usr/bin/thunderbird.old"
	fi

		# pas d'alternatives pour les mails

		# set default=1 dans profiles.ini
	while read -r ligne ; do
		# shellcheck disable=SC2034
		[[ ${ligne} =~ ^\[Profile.*\]$ ]] && numero=${ligne//[\[Profile\]]}		# n° profil, pas encore utilisé
		[ "$ligne" == "Name=$canal" ] && marqueur="ok"	# détection canal recherché? marqueur actif
		[[ $ligne == "Default=1" ]] && continue			# si Default existant, on saute, la ligne ne fera donc pas partie de $stockage (=effacement)

		if [ "$marqueur" == "ok" ] && [ -z "$ligne" ]; then		# marqueur actif, première ligne vide rencontrée 
			ligne+="Default=1"$'\n'						# ajout Default=1
			unset marqueur								# effacement marqueur
		fi
		stockage+="$ligne"$'\n'							# stockage lignes brutes ou traitées
	done < "$profileIni"
	if [ "$marqueur" == "ok" ]; then	# marqueur encore actif (dernier profil), read ne permet de choper dernière ligne vide
		stockage+="Default=1"							# ajout Default=1
		unset marqueur									# effacement marqueur
	fi
	stockage=${stockage%[[:cntrl:]]}					# suppression dernier saut de ligne superflu

	if type -p uniq &>/dev/null ; then					# si commande uniq dispo
		uniq <<< "$stockage"  > "$profileIni"			# on élimine doublons adjacents (lignes vides éventuelles)
	else
		echo "$stockage" > "$profileIni"
	fi

	f__info "log:raw" "$appli $canal est le défaut système"
}

	# copie du répertoire .default dans canal, $1=canal
f_tb_copie_default(){		# 09/03/2018
	local canal="$1" profileDefault

	if [ ! -d "$dirProfil/$canal" ]; then
		f__info "$appli $canal n'a pas de profil et ne peut donc pas recevoir .default"
		return 1
	fi
	f_tb_alertIcedove

	profileDefault=$( sed -En 's/Path=(.*\.default).*$/\1/p' "$dirProfil/profiles.ini" )	# recherche profil .default
	if [ -z "$profileDefault" ] || [ ! -d "$dirProfil/$profileDefault" ]; then
		f__info "pas de profil .default existant, opération abandonnée"
		return 1
	fi

	f__info "copie du profil default <$profileDefault> sur $canal"
#~	cp -R --preserve "$dirProfil/$profileDefault"/*  "$dirProfil/$canal/"
	cp -R "$dirProfil/$profileDefault"/*  "$dirProfil/$canal/"
	chown -R "$fu_user:" "$dirProfil/$canal/"		# propriété du répertoire profil à l'user
	chmod g-rx,o-rx "$dirProfil/$canal/"			# droits répertoire
}

	# détermination canal bas installé, assigne $tb_canal_bas
f_tb_get_canalBas(){		# 25/12/2017
	local ii

	for ii in $produit_all_inverse; do
		[ -d "$dirInstall$ii" ] && tb_canal_bas="$ii"
	done
	[ "$tb_canal_bas" ] && return 0 || return 1
}

	#  version en ligne et installée, [$1=quiet], affichage & assigne $version_tb_latest $version_tb_beta
f_tb_get_versions(){		# 13/06/2018
	local canal recup_url prefixe verOnline verTbOnline verInstall verTbInstall

	x_tb_get_version=1
	for canal in latest beta; do
			# version online
		[ "$canal" == "beta" ] && prefixe="$canal-"
		recup_url="https://download.mozilla.org/?product=thunderbird-$prefixe""latest&os=$fu_archi&lang=fr"
		verOnline=$( f__wget_test "$recup_url" loc )
		if [[ "$verOnline" =~ thunderbird/releases/(.*)/linux- ]]; then
			verOnline=${BASH_REMATCH[1]}
		fi
		verTbOnline+=$( printf "%-7s: %-12s" "$canal" "$verOnline" )
		case "$canal" in
			latest ) version_tb_latest="$verOnline" ;;
			beta   ) version_tb_beta="$verOnline" ;;
		esac
			# version installée
		if [ -x "$dirInstall$canal/thunderbird" ]; then 
			verInstall=$( "$dirInstall$canal"/thunderbird -v | grep -Eo '[0-9].*' )
			verTbInstall+=$( printf "%-7s: %-12s" "$canal" "$verInstall" )
		else
			verTbInstall+=$( printf " %.s" $( seq 21 ) )
		fi
	done
	[ "$1" == "quiet" ] && return 0

	f__trim verTbInstall
	f__trim verTbOnline
	verTbInstall=${verTbInstall:="Non installé"}
	verTbOnline=${verTbOnline:="${RED}n/a$STD"}
	[[ "$verTbOnline" =~ location ]] && verTbOnline="${RED}n/a$STD"
	f__info "raw" "$appli en place: $GREEN$verTbInstall"
	f__info "$appli en ligne: $YELLOW$verTbOnline"
}

	# installation, $1 canal [$2='manuel', $3 archive]
f_tb_install(){		# 12/06/2018
	local versArchive tbVersion fileTemp prefixe canal="$1" appli="thunderbird"  dirTemp="/tmp/$script-install_tb"

	if [ "$2" == "manuel" ]; then
		versArchive=${3%\.tar*}				# version mozilla thunderbird-52.8.0.tar.bz2
		versArchive=${versArchive%\.linux*}	# ancienne version thunderbird-52.0.0.linux64.tar.bz2
		versArchive=${versArchive%_*}		# version encours
		versArchive=${versArchive##*-}
		f__info raw "installation manuelle dans $canal de thunderbird version $versArchive ($3)"
		fileTemp="$3"
	else 
		if ! mkdir -p "$dirTemp" 2>/dev/null; then		# contournement bug? bash, utile si téléchargement et mauvais effacement précédent
			dirTemp+="-$RANDOM"
			! mkdir -p "$dirTemp" 2>/dev/null && f__error "droits insuffisants, recommencer avec les droits root"
		fi
		[ -z "$dl_only" ] && f__info "installation $appli-$canal"
		(( x_tb_get_version == 1 )) || f_tb_get_versions quiet
		case "$1" in
			latest ) tbVersion="$version_tb_latest" ;;
			beta   ) tbVersion="$version_tb_beta"   ;;
		esac
		fileTemp="$dirTemp/${appli,,}-${tbVersion}_$canal.$fu_archi.tar.bz2"

		f__info "  - téléchargement..."
			# calcul url
		[ "$canal" == "beta" ] && prefixe="$canal-"
		recup_url="https://download.mozilla.org/?product=thunderbird-$prefixe""latest&os=$fu_archi&lang=fr"
			# téléchargement
		if ! wget -q -c --show-progress --tries=2 --timeout=15 --user-agent="$user_agent" \
			-o /dev/null -O "$fileTemp" "$recup_url" ; then
					f__wget_test "$recup_url"
		fi
	fi
	f__archive_test "$fileTemp" || f__error "Le fichier $fileTemp n'est pas une archive tar.bz2 valide"
	echo

	if [[ "$dl_to_svg" || "$dl_only" ]]; then
		chown "$fu_user:" "$fileTemp"
		cp --preserve "$fileTemp" ./
	fi
	if [ "$dl_only" ]; then
		rm -f "$fileTemp"
		echo
		return 0
	fi

		# décompression archive téléchargée dans dossier de travail
	f__info " - décompression..."
	tar -xaf "$fileTemp" -C "$dirTemp/"

		# mise en place finale
	f__info " - installation..."
	rm -fr "$dirInstall$canal"		# effacement éventuel répertoire existant
	mkdir -p "$dirInstall$canal"
	mv -f "$dirTemp/thunderbird/"* "$dirInstall$canal/"
	chown -R "$fu_user:" "$dirInstall$canal/"
	chmod -R g+wrs,a+r "$dirInstall$canal/"
		# svg éventuelle archive, effacement répertoire temporaire
	rm -fr "$dirTemp"

		# traitement finaux système
	ln -sf "$dirInstall$canal/chrome/icons/default/default48.png" "/usr/share/pixmaps/thunderbird-$canal.png"
	f_tb_lanceur_desktop "$canal"
		# suppression liens lanceurs éventuels
	unlink "/usr/bin/thunderbird-$canal" &>/dev/null
	unlink "/usr/bin/thunderbird" &>/dev/null

		# lanceur dans /usr/bin
	echo '#!/bin/sh' 			> "/usr/bin/thunderbird-$canal"
	{
		echo 
		# shellcheck disable=SC1117
		echo "echo \"\$@\" | grep -qE '\-P|\-\-ProfileManager' && exec $dirInstall$canal/thunderbird -P || \\"
		echo "    exec $dirInstall$canal/thunderbird -P $canal \"\$@\""
	} >> "/usr/bin/thunderbird-$canal"
	chmod 755 "/usr/bin/thunderbird-$canal"

	f_tb_config_profil "$canal"		# profiles.ini, répertoire profil
	f_tb_config_system "$canal"		# lien /usr/bin, default dans le profile

	[ "$1" != "manuel" ] && versArchive=$tbVersion
	echo 
	f__info "log" "$appli-$canal $versArchive installé"
}

	# installation d'une archive manuelle, $1=canal, $2=archive
f_tb_install_manuel(){		# 25/12/2017

	[ -e "$2" ] || f__error "fichier $2 introuvable"
	f_tb_install "$1" "manuel" "$2"
	[ -e "$fileDev" ] || rm "$2" &>/dev/null	## on n'efface pas si fileDev (dev)
}

	# création lanceur.desktop, $1=canal à traiter
f_tb_lanceur_desktop(){		# 09/03/2018
	local canal="$1" fileDesktop="/usr/share/applications/thunderbird-$1.desktop"

	[ -d "$dirInstall$canal" ] || return 0		# pas d'installation? sortie

	echo "[Desktop Entry]" 													> "$fileDesktop"
	{                                                                      
	echo "Name=$appli $canal"
	echo "Name[fr]=$appli $canal"
	echo "X-GNOME-FullName=$appli-$canal Web Browser"
	echo "X-GNOME-FullName[fr]=$appli-$canal Navigateur Web"
	echo "StartupWMClass=$appli-$canal"
	echo "Exec=/usr/bin/thunderbird %u"
	if [ -e "$dirInstall$canal/chrome/icons/default/default256.png" ]; then
		echo "Icon=$dirInstall$canal/chrome/icons/default/default256.png" 
	else
		echo "Icon=$dirInstall$canal/chrome/icons/default/default48.png" 
	fi
	echo "Comment=Read/Write Mail/News with $appli"
	echo "Comment[fr]=Lire/écrire des mails/news avec $appli"
	echo "GenericName=Mail Client"
	echo "GenericName[fr]=Client de messagerie"
	echo "Terminal=false"
	echo "X-MultipleArgs=false"
	echo "Type=Application"
	echo "Categories=Network;Email;News;"
	echo "MimeType=message/rfc822;x-scheme-handler/mailto;text/calendar;text/x-vcard;"
	echo "StartupNotify=true"
	echo "Keywords=EMail;E-mail;Contact;Addressbook;News;"
	} >> "$fileDesktop"
}

	# désinstallation, $1=canal
f_tb_remove(){		# 09/06/2018
	local canal="$1" fileDesktop="/usr/share/applications/thunderbird-$1.desktop"

	if [ ! -d "$dirInstall$canal" ]; then 
		f__info "$appli $canal n'est pas installé"
		return 1
	fi

	killall "$dirInstall$canal/thunderbird" &>/dev/null
		# suppression du répertoire
	rm -fr "$dirInstall$canal"
		# suppression lanceurs
	rm -f "/usr/share/applications/thunderbird-$canal.desktop"
	rm -f "/usr/bin/thunderbird-$canal"
		# suppression des liens
	unlink "/usr/share/pixmaps/thunderbird-$canal.png" &>/dev/null
	unlink "/usr/bin/thunderbird" &>/dev/null

		# canal bas comme nouveau défaut
	f_tb_get_canalBas && f_tb_config_system "$tb_canal_bas" 		# nouveau canal par défaut 

	f__info "log" "thunderbird-$canal désinstallé, mais le profil est toujours en place" \
		"\\tle supprimer manuellement si nécessaire, pour cela:" \
		"fermer toutes les instances ouvertes de thunderbird" \
		"et lancer en console, en user:$GREEN thunderbird -P"
}

	# anacron hebdomadaire, via cron horaire, $1=upgrade|install|remove
fscript_cronAnacron(){		# 11/06/2018
	local dirAnacron dirSpool fileAnacron

	type -t fscript_cronAnacron_special &>/dev/null && fscript_cronAnacron_special		# test, si fonction spécifique, appel
	dirAnacron="/home/$fu_user/.config/anacron"
	dirSpool="$dirAnacron/spool"
	fileAnacron="$dirAnacron/$script.anacrontab"
	[ "$EUID" -eq 0 ] && sed -i "/$script.anacrontab/d" /etc/crontab
	case "$1" in 
		install | upgrade )
			mkdir -p  "$dirAnacron"
				# table anacron
			echo "7 10 $script nice $script_install --upgrade 1>/dev/null" > "$fileAnacron"	# juste erreurs en syslog
				## anacron journalier pour dev logname 
			if [ -e "$fileDev" ]; then
				echo "1 00 ${script}Dev nice $script_install --upgrade 1>/dev/null" >> "$fileAnacron"
			fi
				# création spool anacron utilisateur
			mkdir -p "$dirSpool"
			chown -R "$fu_user:" "$dirAnacron" "$dirSpool"
			if [ "$EUID" -eq 0 ]; then
					# crontab pour activation horaire anacron
				echo "@hourly $fu_user /usr/sbin/anacron -t $fileAnacron -S $dirSpool" >> /etc/crontab
			fi
			grep -q "$script" "/etc/crontab" || echo f__error "inscription crontab"
		;;
		remove )
			rm -f "${dirSpool:?}/$script"*
			rm -f "$fileAnacron"
			rmdir "$${dirSpool:?}" "${dirAnacron:?}" 2>/dev/null
		;;
	esac
}

	# assigne $ver_script_install, $ver_script_online, $script_a_jour=ok|KO
fscript_get_version(){		# 09/06/2018

	x_script_get_version=1
		# version online
	if ver_script_online=$( wget -q --timeout=15 -o /dev/null -O - "$url_script" ); then
		ver_script_online=${ver_script_online#*version=}
		read -r ver_script_online <<< "$ver_script_online"
	else
		f__wget_test "$url_script"
	fi
		# version installée
	if [ -e "$script_install" ]; then
		while read -r ; do
			if [[ "$REPLY" =~ ^version= ]]; then
				ver_script_install=${REPLY#*=}
			fi
		done < "$script_install"
	fi
		# maj ?
	if [[ "$ver_script_online" && "$script_install" ]]; then 
		if [ "$ver_script_install" != "$ver_script_online" ]; then
			script_a_jour="KO"
		else
			script_a_jour="ok"
		fi
	fi
		# affichage
	ver_script_online=${ver_script_online:="${RED}n/a$STD"}
	ver_script_install=${ver_script_install:="Non installé"}
	f__info "raw" "script en place: $GREEN$ver_script_install"
	f__info "script en ligne: $YELLOW$ver_script_online"
}

fscript_install(){		# 09/06/2018

	if grep -Eq "$script_install|/usr/bin/$script" <<< "$0"; then
		f__info "${RED}l'installation dans le système doit se faire depuis le script non installé $GREEN(./$script -i )"
		return 1
	fi
	type -t fscript_install_special &>/dev/null && fscript_install_special		# test, si fonction spécifique, appel
	f__requis "wget anacron cron" || exit 1 
		# install /opt
	mkdir -p /opt/bin/
	cp "$0" "$script_install"
	ln -s "$script_install" "/usr/bin/$script" 2>/dev/null
	chmod 755 "$script_install"
		# cron/anacron install
	fscript_cronAnacron "install"
		# création fichier log
	touch "$script_logs"
	chmod 644 "$script_logs"
	chown "$fu_user:" "$script_logs" "$script_install"
	[ -e "$fileDev" ] || rm -f "$0"			## on efface pas si fileDev (dev)

	f__info "log" "$script $version installé dans le système." "maintenant, appel du script par: $GREEN$script$BLUE (sans ./)"
}

fscript_remove(){		# 09/06/2018

	if ! grep -Eq "$script_install|/usr/bin/$script" <<< "$0"; then
		f__info "${RED}cette fonction doit être appelée depuis le script installé dans le système $GREEN($script -r)"
		return 1
	fi
	if [ ! -x "$script_install" ];then
		f__info "$RED$script n'est pas installé"
		return 1
	fi

	type -t fscript_remove_special &>/dev/null && fscript_remove_special		# test, si fonction spécifique, appel
		# suppression /opt, lien /usr/bin
	rm -f "$script_install"
	unlink "/usr/bin/$script" 2>/dev/null
		# cron/anacron remove
	fscript_cronAnacron "remove"

	f__info "log" "$script $version supprimé du système."
}

	# $1: update standard $1=std, si update en place $1=message d'info
fscript_update(){		# 11/06/2018
	local dirTemp="/tmp/$script-maj" upgradeEnPlace

	[ "$1" != 'std' ] && upgradeEnPlace="$1"
	type -t fscript_update_special &>/dev/null && fscript_update_special		# test, si fonction spécifique, appel
	if [ -z "$upgradeEnPlace" ] && ! grep -Eq "$script_install|/usr/bin/$script" <<< "$0"; then
		f__info "${RED}cette fonction doit être appelée depuis le script installé dans le système $GREEN($script -u)"
		return 1
	fi
	(( x_script_get_version == 1 )) || fscript_get_version
	if [ "$script_a_jour" == "ok" ]; then 
		f__info "log" "pas de mise à jour disponible pour $script $version"
		return 0
	else
		f__info "mise à jour en cours"
	fi
	mkdir -p "$dirTemp"
	if ! wget -q --tries=2 --timeout=15 -o /dev/null -O "$dirTemp/$script" "$url_script"; then
		rm -fr "$dirTemp"
		f__wget_test "$url_script"
	fi
	if grep -q '#!/bin/bash' "$dirTemp/$script" && grep -q '^### END CONTROL' "$dirTemp/$script"; then 
		cp "$dirTemp/$script" "$script_install"
		chmod 755 "$script_install"
		chown "$fu_user:" "$script_install"
		[ -z "$upgradeEnPlace" ] && fscript_cronAnacron "upgrade"
		f__info "log" "$script mis à jour en version $ver_script_online $upgradeEnPlace"
	else
		f_info "log" "$script: échec update" "mauvais téléchargement, réessayer plus tard"
	fi
	rm -fr "$dirTemp"
}

prg_init(){		# 09/06/2018

	PATH='/usr/sbin:/usr/bin:/sbin:/bin' 
	TERM=xterm  
	IFS=$' \t\n'
	export PATH TERM IFS

		# options bash figées
	shopt -s checkwinsize complete_fullquote extglob extquote interactive_comments sourcepath
	shopt -u force_fignore execfail failglob

		# test bash v4
	[ "${BASH_VERSINFO[0]}" == 4 ] || f__error "bash v4 requis" "version installée: $BASH_VERSION"

		# architectures possibles
	f__architecture || f__error "Seules les architecture 32 et 64 bits sont supportée (i686 & amd64) par Thunderbird"

		# détermination user derrière root
	f__user
	retourFUser="$?"
	[ "$retourFUser" -eq 1 ] && f__error "user indéterminé" \
		"pour contourner, lancer le script avec:\\n$GREEN  USER_INSTALL=<user> $0 \\n"
	if [ "$retourFUser" -eq 2 ]; then
		if [ "$EUID" -eq 0 ]; then
			fu_user="root"
		else
			f__error "user détecté, mais pas de home: /home/$fu_user"
		fi
		f__info "user root"
	fi

		# définition couleurs
	f__color

		# requis pour fonctionnement programme
	f__requis "awk>gawk wget file tar killall>psmisc" || exit 1
}

######## début script / initialisation

	# tests au démarrage
prg_init

	# logo
f_affichage

	# paramètres script
user_agent="Mozilla/5.0 Firefox"
fileDev="/opt/bin/fileDev"
script_install="/opt/bin/$script"
script_logs="/var/log/sdeb_$script.log"
url_script="https://framagit.org/sdeb/getThunderbird/raw/master/getThunderbird"
url_notice="https://framaclic.org/h/doc-getthunderbird"

	# paramètres Thunderbird
appli="Thunderbird"
dirInstall="/opt/usr/share/thunderbird-"
dirProfil="/home/$fu_user/.thunderbird"		# emplacement profiles.ini
produit_all="latest beta"
produit_all_inverse="beta latest"

script_options="$*"
	# options
while (( $# )) ; do
	case "$1" in 
		--sauve ) dl_to_svg="ok" ;;
		--dev   ) url_script=${url_script//\/master\///dev/} ;;
		* ) options+=( "$1" ) ;;
	esac
	shift
done

	# auto-installation script éventuel, si pas de marqueur $fileDev
if [[ $( dirname "$0" ) != $( dirname "$script_install" ) && $( dirname "$0" ) != "/usr/bin" && ! -e "$fileDev" ]]; then
	options=( -i )									# option installation script
fi
(( ${#options} == 0 )) && options=( -h )			# vide help (si fileDev), sinon install

	#actions
for i in "${!options[@]}"; do							# deuxième passe options, actions
	produit=${options[$i]#*-}
	# shellcheck disable=SC2221,SC2222
	case ${options[$i]} in
		-i | --install | -r | --remove | ri | i-* | m-* | r-* | u-* )
			if [ "$EUID" -ne 0 ]; then
				f__info raw "vous devez être$RED ROOT$BLUE pour cette opération"
				f__sudo "exec $0 $script_options"
				exit
			fi 	;;&
		d-latest | d-beta | d-all )
			[ "$produit" == "all" ] && produit="$produit_all"
			for k in $produit; do
				f_tb_copie_default "$k"			# copier un profil default sur un canal
			done ;;
		i-latest | i-beta | i-all )
			[ "$produit" == "all" ] && produit="$produit_all"
			for k in $produit; do
				f_tb_install "$k"				# installation canal
			done ;;
		m-latest | m-beta )
			f_tb_install_manuel "$produit" "${options[((i+1))]}"	# installation manuelle d'une archive, $2=archive
			exit ;;
		r-latest | r-beta | r-all )
			[ "$produit" == "all" ] && produit="$produit_all"
			for k in $produit; do
				f_tb_remove "$k"				# remove
			done ;;
		ri )
			produit="$produit_all"
			for k in $produit; do 
				f_tb_lanceur_desktop "$k"		# réparation icône de tous les canaux installés
			done ;;
		t-latest | t-beta | t-all )
			[ "$produit" == "all" ] && produit="$produit_all"
			for k in $produit; do
				dl_only="ok"
				f_tb_install "$k"				# téléchargement
			done ;;
		u-latest | u-beta | u-all )
			[ "$produit" == "all" ] && produit="$produit_all"
			for k in $produit; do
				f_tb_config_profil "$k" "menu"	# configurer un canal pour user en cours
				f_tb_config_system "$k"			# défaut système
				echo
			done ;;

		version | versions )					# affiche versions thunderbird en ligne & installées, script
			fscript_get_version
			f_tb_get_versions ;;

		-i | --install )						# installation du script dans le système
			fscript_install
			exit ;;
		-r | --remove )							# désinstallation du script
			fscript_remove
			exit ;;
		-u | --upgrade )						# upgrade script
			operation="upgrade"
			# shellcheck disable=SC2119
			fscript_update std					# std argument obligatoire pour upgrade normal
			exit ;;
		-v | --version )						# version du script, en ligne et exécuté
			fscript_get_version ;;
		-h | --help | * )						# affichage help
			f_help 
			exit ;;
	esac
done

exit 0

### END CONTROL (contrôle chargement)

wget -nv -O getThunderbird  https://framaclic.org/h/getthunderbird
curl -L -o getThunderbird  https://framaclic.org/h/getthunderbird
chmod +x getThunderbird && ./getThunderbird

wget -nv -O getThunderbird  https://framagit.org/sdeb/getThunderbird/raw/master/getThunderbird
curl -LO  https://framagit.org/sdeb/getThunderbird/raw/master/getThunderbird
